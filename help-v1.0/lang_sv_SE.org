<?php
session_start();
// Edit what is on the right side of =>
// Edit: => "XXX XXX"
// the " character is written \"
// Example:
// "Click Search" => "Click \"Search\"",
// Line breaks are written: <br>
$lang = array(
"language" => "sv_SE", // Do not edit
"streamCapture2 Manual" => "streamCapture2 1.0 Manual",
"Many thanks to bovirus for the Italian translation." => "Stort tack till bovirus för den italienska översättningen.",
"Contact me" => "Kontakta mig",
"Pleas visit" => "Vänligen besök",
"Website" => "Hemsida",
// Download one Episode
"Download a single episode" => "Ladda ner ett avsnitt",
"Find a web page that streams video" => "Leta upp en webbsida som strömmar video",
"Copy the page address" => "Kopiera sidans adress",
"Paste into streamCapture2" => "Klistra in i streamCapture2",
"Click Search" => "Klicka \"Sök\"",
"Choose quality" => "Välj kvalitet",
"Click Download" => "Klicka \"Ladda ner\"",
// Download from Download List
"Download multiple episodes" => "Ladda ner flera avsnitt",
"Click add to Download list" => "Klicka på \"Lägg till på nedladdningslistan\"",
"Click Download all" => "Klicka på \"Ladda ner alla\"",
// Download all episodes
"Download all episodes" => "Ladda ner alla avsnitt",
"Click all Episodes, List all Episodes" => "Klicka  \"Alla avsnitt\", \"Lista alla avsnitt\"",
"Click All Episodes, Direct Download of all Episodes" => "Klicka \"Alla avsnitt\", \"Direkt nedladdning av alla avsnitt\"",
// image 1
"If you hold the mouse over..." => "Om du håller musen över en menyrubrik syns en förklaring i statuslisten",
// image 2
"Select the quality..." => "Välj kvalitet på filen du laddar ner",
"Select bite rate..." => "Välj bithastighet (4354) och metod (HLS).",
"If the download requires login..." => "Om nedladdningen kräver inloggning, ange namnet på leverantören (Viafree). Du kan skapa en användare i \"Logga in\" menyn.",
"If you have not let the program save..." => "Om du inte låtit programmet spara lösenordet, klickar du här och anger lösenordet",
"Here you specify how much..." =>  "Här anger du hur mycket bithastigheten får avvika från bithastigheten du valt (4353 &plusmn; 300)).",
// image 3
"If you let svtplay-dl choose method..." => "Om du låter svtplay-dl välja metod, bithastighet och avvikelse så står det \"unknown\"",
// Download List
"Download List" => "Nedladdningslistan",
"The list where you can save what..."=> "Listan där du sparar sådant som du vill ladda ner senare.<br>Varje rad representerar en nedladdning.<br>Raden delas upp av kommatecken (',').<br>Vi tittar på första raden.",
"The address..." => "Adressen https://www.svtplay.se/video/30937682/so-long-my-son",
"The method to be used" => "Metoden som används",
"Quality (bit rate)" => "Kvalitet (bithastighet)",
"npassword if..." => "Namnet på strömningstjänsten eller \"npassword\" om inget lösenord ska användas.",
"ysub means that subtitles..." => "\"ysub\" betyder att undertexten ska laddas ner, \"nsub\" betyder att undertexten inte ska laddas ner.",
"nst if no cookie is to be used..." => "\"nst\" om ingen <a href=\"https://youtu.be/XQ5LzMMgIa8\" target=\"_blank\">'st'</a> cookie ska användas, annars visas 'st'' cookien här.",
"Permitted deviation..." => "Tillåten avvikelse (518 +/- 400) från den valda bithastigheten.",
// Problem solving
"Problem solving" => "Problemlösning",
"If you are using the Windows..." => "Om du använder operativsystemet Windows kan svtplay-dl eller streamCapture2, eller båda stoppas av en brandvägg. Du kan behöva ändra dina inställninar.",
// Form
"streamCapture2 contact form" => "streamCapture2 kontaktformulär",
"Fill out this form..." => "Fyll i formuläret och klicka på \"Skicka\".<br>Jag svarar så fort jag hinner.",
"Required" => "Obligatorisk",
"Name" => "Namn",
"E-mail" => "E-post",
"Heading" => "Rubrik",
"Bug report" => "Buggrapport",
"Help" => "Hjälp",
"My point" => "Min synpunkt",
"Other" => "Övrigt",
"Message" => "Meddelande",
"Send" => "Skicka",
"Clear" => "Rensa",
"Up" => "Upp"
);


function vadUt()
{
  if ($_SESSION['failure_email']=="Incorrect email address") {
      $ut = "Felaktig e-postadress"; // Edit
      $ut = "<p class=\"rod\">".$ut."</p>";
      unset($_SESSION['failure_email']);
      return $ut;
  }

  if ($_SESSION['failure_email']=="Incorrect name") {
      $ut = "Felaktigt namn"; // Edit
      $ut = "<p class=\"rod\">".$ut."</p>";
      unset($_SESSION['failure_email']);
      return $ut;
  }

  if ($_SESSION['failure_email']=="You must enter something in the message field") {
      $ut = "Du måste skriva något i meddelandefältet"; // Edit
      $ut = "<p class=\"rod\">".$ut."</p>";
      unset($_SESSION['failure_email']);
      return $ut;
  }

  if ($_SESSION['mission_done'] == "failure") {
    unset($_SESSION['mission_done']);
    $ut = "Du har inte fyllt i de obligatoriska fälten"; // Edit
    $ut = "<p class=\"rod\">".$ut."</p>";

  }
  else if ($_SESSION['mission_done'] == "success") {
    unset($_SESSION['mission_done']);
    $ut = "Tack! Meddelandet har skickats."; // Edit
    $ut = "<p class=\"fet\">".$ut."</p>";
  }
  else {
    unset($_SESSION['mission_done']);
    $ut = "Välkommen att skicka ditt meddelande. På engelska eller svenska."; // Edit
    $ut = "<p class=\"fet\">".$ut."</p>";
  }
  return $ut;
}


?>
