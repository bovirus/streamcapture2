<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>CheckUpdate</name>
    <message>
        <location filename="../checkupdate.cpp" line="51"/>
        <location filename="../checkupdate.cpp" line="134"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Ingen internetanslutning hittades.
Kontrollera dina Internetinställningar och brandvägg.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="70"/>
        <location filename="../checkupdate.cpp" line="81"/>
        <source>You have the latest version of </source>
        <translation>Du har senaste versonen av </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="92"/>
        <source>There was an error when the version was checked.</source>
        <translation>Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="153"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="200"/>
        <source>Updates:</source>
        <translation>Uppdateringar:</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="223"/>
        <source>There is a new version of </source>
        <translation>Det finns en ny version av </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="225"/>
        <source>Latest version: </source>
        <translation>Senaste versionen: </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="54"/>
        <location filename="../checkupdate.cpp" line="73"/>
        <location filename="../checkupdate.cpp" line="84"/>
        <location filename="../checkupdate.cpp" line="95"/>
        <location filename="../checkupdate.cpp" line="137"/>
        <location filename="../checkupdate.cpp" line="156"/>
        <location filename="../checkupdate.cpp" line="229"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
</TS>
