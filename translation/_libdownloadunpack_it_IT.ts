<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>DownloadUnpack</name>
    <message>
        <location filename="../downloadunpack.ui" line="37"/>
        <source>MainWindow</source>
        <translation>Finestra principale</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="198"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="232"/>
        <source>&amp;Tools</source>
        <translation>&amp;Strumenti</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="289"/>
        <source>Try to decompress</source>
        <translation>Prova a decomprimere</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="338"/>
        <source>Source code...</source>
        <translation>Codice sorgente...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="346"/>
        <source>Binary files...</source>
        <translation>File binari...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="298"/>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="236"/>
        <source>Choose location for</source>
        <translation>Scegli percorso per</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="215"/>
        <source>V&amp;isit</source>
        <translation>V&amp;isita</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="222"/>
        <source>&amp;View</source>
        <translation>&amp;Visualizza</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="257"/>
        <source>Search beta</source>
        <translation>Cerca beta</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="260"/>
        <location filename="../downloadunpack.ui" line="420"/>
        <location filename="../downloadunpack.ui" line="448"/>
        <location filename="../downloadunpack.ui" line="502"/>
        <source>&quot;&quot;</source>
        <translation>&quot;&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="272"/>
        <location filename="../downloadunpack.ui" line="275"/>
        <source>Download...</source>
        <translation>Download...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="278"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="301"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="310"/>
        <source>English</source>
        <translation>Inglese</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="319"/>
        <source>Swedish</source>
        <translation>Svedese</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="324"/>
        <source>Load external language file...</source>
        <translation>Carica file lingua esterno...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="333"/>
        <source>Italian</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="481"/>
        <location filename="../downloadunpack.ui" line="484"/>
        <location filename="../downloadunpack.ui" line="487"/>
        <source>beta...</source>
        <translation>beta...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="496"/>
        <location filename="../downloadunpack.ui" line="499"/>
        <source>Search stable</source>
        <translation>Cerca stabile</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="511"/>
        <source>Download stable...</source>
        <translation>Download stabile...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="514"/>
        <source>Download
stable...</source>
        <translation>Download stabile...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="518"/>
        <source>Download stable</source>
        <translation>Download stabile</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="527"/>
        <source>Download stable to directory &quot;stable&quot;</source>
        <translation>Download stabile nella cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="530"/>
        <source>Download stable to
directory &quot;stable&quot;</source>
        <translation>Download stabile nella
cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="534"/>
        <source>Download stable
to directory &quot;stable&quot;</source>
        <translation>Download stabile nella
cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="544"/>
        <source>Download beta to directory &quot;beta&quot;</source>
        <translation>Download beta nella 
cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="547"/>
        <location filename="../downloadunpack.ui" line="551"/>
        <source>Download beta to
directory &quot;beta&quot;</source>
        <translation>Download beta nella
cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="364"/>
        <source>Download latest</source>
        <translation>Scarica più recente</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="466"/>
        <location filename="../downloadunpack.ui" line="469"/>
        <location filename="../downloadunpack.ui" line="472"/>
        <source>stable...</source>
        <translation>stabile...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="441"/>
        <source>Download to directory &quot;stable&quot;</source>
        <translation>Scarica nella cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="358"/>
        <location filename="../downloadunpack.ui" line="361"/>
        <source>Download beta...</source>
        <translation>Download beta...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="413"/>
        <source>Download to directory &quot;beta&quot;</source>
        <translation>Download nella cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="416"/>
        <source>Download to
directory &quot;beta&quot;</source>
        <translation>Download nella
cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="444"/>
        <source>Download to
directory &quot;stable&quot;</source>
        <translation>Scarica nella
cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="373"/>
        <source>Zoom In</source>
        <translation>Zoom +</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="376"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="381"/>
        <source>Zoom Default</source>
        <translation>Zoom predefinito</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="384"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="393"/>
        <source>Zoom Out</source>
        <translation>Zoom -</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="396"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="401"/>
        <source>Show Toolbar</source>
        <translation>Visualizza barra strumenti</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="61"/>
        <source>Is not writable. Cancels</source>
        <translation>Non è scrivibile. Annulla</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="65"/>
        <source>Starting to download...
</source>
        <translation>Avvio download...
</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="88"/>
        <location filename="../download.cpp" line="215"/>
        <location filename="../download.cpp" line="217"/>
        <source>to</source>
        <translation>in</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="88"/>
        <location filename="../download.cpp" line="90"/>
        <source>Check your file permissions and antivirus software.</source>
        <translation>Verifica permessi file e software antivirus.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="88"/>
        <source>Can not save</source>
        <translation>Impossibile salvare</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="90"/>
        <source>Can not save to</source>
        <translation>Impossibile salvare in</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="92"/>
        <source>is downloaded to</source>
        <translation>è stato scaricato in</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="105"/>
        <source>Starting to decompress</source>
        <translation>Avvio decompressione</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="122"/>
        <source>is decompressed.</source>
        <translation>è stato decompresso.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="128"/>
        <source>Removed</source>
        <translation>Rimosso</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="140"/>
        <source>Could not delete</source>
        <translation>Impossibile eliminare</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="166"/>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Linux operating systems. It will not work with Windows operating system.</source>
        <translation>Attenzione! La versione di svtplay-dl che hai scaricato è destinata ai sistemi operativi Linux. 
Non funzionerà con il sistema operativo Windows.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="174"/>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Windows operating systems. It will not work with Linux operating system.</source>
        <translation>Attenzione! La versione di svtplay-dl che hai scaricato è destinata ai sistemi operativi Windows. 
Non funzionerà con il sistema operativo Linux.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="207"/>
        <source>Download failed, please try again.</source>
        <translation>Download non riuscito, riprova.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="215"/>
        <location filename="../download.cpp" line="217"/>
        <source>Changed name from</source>
        <translation>Modifica nome da</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="233"/>
        <source>Select &quot;Tools&quot;, &quot;Use svtplay-dl beta&quot; to use this version.</source>
        <translation>Per usare questa versione seleziona &quot;Strumenti&quot; -&gt; &quot;Usa svtplay-dl beta&quot;.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="235"/>
        <source>Select &quot;Tools&quot;, &quot;Use svtplay-dl stable&quot; to use this version.</source>
        <translation>Per usare questa versione seleziona &quot;Strumenti&quot; -&gt; &quot;Usa svtplay-dl stabile&quot;.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="265"/>
        <source>Failed to decompress.</source>
        <translation>Decompressione fallita.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="146"/>
        <location filename="../downloadunpack.cpp" line="149"/>
        <source>For</source>
        <translation>Per</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="181"/>
        <location filename="../downloadunpack.cpp" line="213"/>
        <location filename="../downloadunpack.cpp" line="273"/>
        <location filename="../downloadunpack.cpp" line="296"/>
        <source>will be downloaded to</source>
        <translation>verrà scaricato in</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="213"/>
        <source>The beta version of svtplay-dl</source>
        <translation>La versione beta di svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="213"/>
        <location filename="../downloadunpack.cpp" line="296"/>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl beta&quot;.</source>
        <translation>Per usare questa versione seleziona
&quot;Strumenti&quot; -&gt; &quot;Usa svtplay-dl beta&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="410"/>
        <source>files</source>
        <translation>file</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="412"/>
        <source>file</source>
        <translation>file</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="326"/>
        <location filename="../downloadunpack.cpp" line="349"/>
        <location filename="../filedialog.cpp" line="36"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="32"/>
        <source>Download svtplay-dl from bin.ceicer.com</source>
        <translation>Download svtplay-dl da bin.ceicer.com</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="68"/>
        <source>Tool bar 1</source>
        <translation>Barra strumenti 1</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="89"/>
        <source>Tool bar 2</source>
        <translation>Barra strumenti 2</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="125"/>
        <source>Current location for stable:</source>
        <translation>Percorso attuale versioen stabile:</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="134"/>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation>Cattura una schermata (ritardo 5 secondi)</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="181"/>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use latest stable svtplay-dl&quot;.</source>
        <translation>Per usare questa versione di svtplay-dl, seleziona
&quot;Strumenti&quot;, &quot;Usa versione stabile più recente di svtplay-dl&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="224"/>
        <location filename="../downloadunpack.cpp" line="273"/>
        <location filename="../downloadunpack.cpp" line="296"/>
        <source>Selected svtplay-dl</source>
        <translation>svtplay-dl selezionato</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="125"/>
        <source>Current location for beta:</source>
        <translation>Percorso attuale per beta:</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="181"/>
        <source>The stable version of svtplay-dl</source>
        <translation>La versione stabile di svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="224"/>
        <location filename="../downloadunpack.cpp" line="241"/>
        <location filename="../downloadunpack.cpp" line="258"/>
        <source>will be downloaded.</source>
        <translation>verrà scaricato.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="241"/>
        <source>stable svtplay-dl</source>
        <translation>svtplay-dl stabile</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="258"/>
        <source>svtplay-dl beta</source>
        <translation>svtplay-dl beta</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="273"/>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl stable&quot;.</source>
        <translation>Per usare questa versione di svtplay-dl, fai clic su
&quot;Strumenti&quot;, &quot;Usa svtplay-dl stabile&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="325"/>
        <location filename="../downloadunpack.cpp" line="348"/>
        <location filename="../filedialog.cpp" line="35"/>
        <source>Choose</source>
        <translation>Scegli</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="327"/>
        <location filename="../downloadunpack.cpp" line="350"/>
        <location filename="../filedialog.cpp" line="37"/>
        <source>Save svtplay-dl in directory</source>
        <translation>Salva svtplay-dl nella cartella</translation>
    </message>
    <message>
        <location filename="../zoom.cpp" line="37"/>
        <source>The font size changes to the selected font size</source>
        <translation>Dimensione tipo di carattere modificata come da dimensione selezionata</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="40"/>
        <source>Save a screenshot</source>
        <translation>Salva schermata</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="42"/>
        <source>Images (*.png)</source>
        <translation>Immagini (*.png)</translation>
    </message>
</context>
</TS>
