<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE" sourcelanguage="en_US">
<context>
    <name>DownloadUnpack</name>
    <message>
        <location filename="../download.cpp" line="61"/>
        <source>Is not writable. Cancels</source>
        <translation>Är inte skrivbar. Avbryter</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="65"/>
        <source>Starting to download...
</source>
        <translation>Börjar att ladda ner...
</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="88"/>
        <location filename="../download.cpp" line="215"/>
        <location filename="../download.cpp" line="217"/>
        <source>to</source>
        <translation>till</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="88"/>
        <location filename="../download.cpp" line="90"/>
        <source>Check your file permissions and antivirus software.</source>
        <translation>Kontrollera dina filbehörigheter och antivirusprogram.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="88"/>
        <source>Can not save</source>
        <translation>Kan inte spara</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="90"/>
        <source>Can not save to</source>
        <translation>Kan inte spara till</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="92"/>
        <source>is downloaded to</source>
        <translation>är nedladdad till</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="105"/>
        <source>Starting to decompress</source>
        <translation>Börjar dekomprimera</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="122"/>
        <source>is decompressed.</source>
        <translation>är dekomprimerad.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="128"/>
        <source>Removed</source>
        <translation>Tog bort</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="140"/>
        <source>Could not delete</source>
        <translation>Kunde inte ta bort</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="166"/>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Linux operating systems. It will not work with Windows operating system.</source>
        <translation>Varning! Den version av svtplay-dl som du laddade ner är avsedd för Linux operativsystem. Det kommer inte att fungera med Windows operativsystem.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="174"/>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Windows operating systems. It will not work with Linux operating system.</source>
        <translation>Varning! Den version av svtplay-dl som du laddade ner är avsedd för Windows operativsystem. Det kommer inte att fungera med Linux operativsystem.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="207"/>
        <source>Download failed, please try again.</source>
        <translation>Nedladdningen misslyckades, försök igen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="215"/>
        <location filename="../download.cpp" line="217"/>
        <source>Changed name from</source>
        <translation>Ändrade namn från</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="233"/>
        <source>Select &quot;Tools&quot;, &quot;Use svtplay-dl beta&quot; to use this version.</source>
        <translation>Välj &quot;Verktyg&quot;, &quot;Använd svtpay-dl beta&quot; för att använda den här versionen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="235"/>
        <source>Select &quot;Tools&quot;, &quot;Use svtplay-dl stable&quot; to use this version.</source>
        <translation>Välj &quot;Verktyg&quot;, &quot;Använd svtpay-dl stabila för att använda den här versionen.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="265"/>
        <source>Failed to decompress.</source>
        <translation>Misslyckades med att dekomprimera.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="127"/>
        <source>Current location for stable:</source>
        <translation>Nuvarande plats för stabila:</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="136"/>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation>Ta en skärmdump (5 sekunders fördröjning)</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="148"/>
        <location filename="../downloadunpack.cpp" line="151"/>
        <source>For</source>
        <translation>För</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="183"/>
        <location filename="../downloadunpack.cpp" line="215"/>
        <location filename="../downloadunpack.cpp" line="275"/>
        <location filename="../downloadunpack.cpp" line="298"/>
        <source>will be downloaded to</source>
        <translation>Kommer att laddas ner till</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="183"/>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use latest stable svtplay-dl&quot;.</source>
        <translation>För att använda denna svtplay-dl-version, klicka
&quot;Verktyg&quot;, &quot;Använd senaste stabila svtplay-dl&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="226"/>
        <location filename="../downloadunpack.cpp" line="275"/>
        <location filename="../downloadunpack.cpp" line="298"/>
        <source>Selected svtplay-dl</source>
        <translation>Vald svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="32"/>
        <source>Download svtplay-dl from bin.ceicer.com</source>
        <translation>Ladda ner svtplay-dl från bin.ceicer.com</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="226"/>
        <location filename="../downloadunpack.cpp" line="243"/>
        <location filename="../downloadunpack.cpp" line="260"/>
        <source>will be downloaded.</source>
        <translation>kommer att laddas ner.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="68"/>
        <source>Tool bar 1</source>
        <translation>Verktygsfält 1</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="89"/>
        <source>Tool bar 2</source>
        <translation>Verktygsfält 2</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="127"/>
        <source>Current location for beta:</source>
        <translation>Nuvarande plats för beta:</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="183"/>
        <source>The stable version of svtplay-dl</source>
        <translation>Den stabila versionen av svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="215"/>
        <source>The beta version of svtplay-dl</source>
        <translation>Betaversionen av svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="215"/>
        <location filename="../downloadunpack.cpp" line="298"/>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl beta&quot;.</source>
        <translation>För att använda denn version, Klicka
&quot;Verktyg&quot;, &quot;Använd svtplay-dl beta.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="412"/>
        <source>files</source>
        <translation>filer</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="414"/>
        <source>file</source>
        <translation>fil</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="37"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="198"/>
        <source>&amp;File</source>
        <translation>&amp;Arkiv</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="232"/>
        <source>&amp;Tools</source>
        <translation>&amp;Verktyg</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="289"/>
        <source>Try to decompress</source>
        <translation>Försöker att dekomprimera</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="338"/>
        <source>Source code...</source>
        <translation>Källkod...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="346"/>
        <source>Binary files...</source>
        <translation>Binära filer...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="298"/>
        <source>Exit</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="236"/>
        <source>Choose location for</source>
        <translation>Välj platsen för</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="215"/>
        <source>V&amp;isit</source>
        <translation>&amp;Besök</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="222"/>
        <source>&amp;View</source>
        <translation>&amp;Visa</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="257"/>
        <source>Search beta</source>
        <translation>Sök beta</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="260"/>
        <location filename="../downloadunpack.ui" line="420"/>
        <location filename="../downloadunpack.ui" line="448"/>
        <location filename="../downloadunpack.ui" line="502"/>
        <source>&quot;&quot;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="272"/>
        <location filename="../downloadunpack.ui" line="275"/>
        <source>Download...</source>
        <translation>Ladda ner...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="278"/>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="301"/>
        <source>F4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="310"/>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="319"/>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="324"/>
        <source>Load external language file...</source>
        <translation>Ladda extern språkfil...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="333"/>
        <source>Italian</source>
        <translation>Italienska</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="481"/>
        <location filename="../downloadunpack.ui" line="484"/>
        <location filename="../downloadunpack.ui" line="487"/>
        <source>beta...</source>
        <translation>beta...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="496"/>
        <location filename="../downloadunpack.ui" line="499"/>
        <source>Search stable</source>
        <translation>Sök stabila</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="511"/>
        <source>Download stable...</source>
        <translation>Ladda ner stabila...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="514"/>
        <source>Download
stable...</source>
        <translation>Ladda ner
stabila...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="518"/>
        <source>Download stable</source>
        <translation>Ladda ner stabila</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="527"/>
        <source>Download stable to directory &quot;stable&quot;</source>
        <translation>Ladda ner stabila till mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="530"/>
        <source>Download stable to
directory &quot;stable&quot;</source>
        <translation>Ladda ner stabila till
mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="534"/>
        <source>Download stable
to directory &quot;stable&quot;</source>
        <translation>Ladda ner stabila
till mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="544"/>
        <source>Download beta to directory &quot;beta&quot;</source>
        <translation>Ladda ner beta till mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="547"/>
        <location filename="../downloadunpack.ui" line="551"/>
        <source>Download beta to
directory &quot;beta&quot;</source>
        <translation>ladda ner beta till
mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="364"/>
        <source>Download latest</source>
        <translation>Ladda ner senaste</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="466"/>
        <location filename="../downloadunpack.ui" line="469"/>
        <location filename="../downloadunpack.ui" line="472"/>
        <source>stable...</source>
        <translation>stabila...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="441"/>
        <source>Download to directory &quot;stable&quot;</source>
        <translation>Ladda ner till mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="358"/>
        <location filename="../downloadunpack.ui" line="361"/>
        <source>Download beta...</source>
        <translation>Ladda ner beta...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="413"/>
        <source>Download to directory &quot;beta&quot;</source>
        <translation>Ladda ner till mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="416"/>
        <source>Download to
directory &quot;beta&quot;</source>
        <translation>Ladda ner till
mappen &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="444"/>
        <source>Download to
directory &quot;stable&quot;</source>
        <translation>Ladda ner till
mappen &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="373"/>
        <source>Zoom In</source>
        <translation>Zooma in</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="376"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="381"/>
        <source>Zoom Default</source>
        <translation>Zooma som standard</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="384"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="393"/>
        <source>Zoom Out</source>
        <translation>Zooma ut</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="396"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="401"/>
        <source>Show Toolbar</source>
        <translation>Visa verktygsfält</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="328"/>
        <location filename="../downloadunpack.cpp" line="351"/>
        <location filename="../filedialog.cpp" line="36"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="327"/>
        <location filename="../downloadunpack.cpp" line="350"/>
        <location filename="../filedialog.cpp" line="35"/>
        <source>Choose</source>
        <translation>Välj</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="243"/>
        <source>stable svtplay-dl</source>
        <translation>stabila svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="260"/>
        <source>svtplay-dl beta</source>
        <translation>svtplay-dl beta</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="275"/>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl stable&quot;.</source>
        <translation>För att använda denna version, klicka
&quot;Verktyg&quot;, Använd stabila svtplay-dl&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="329"/>
        <location filename="../downloadunpack.cpp" line="352"/>
        <location filename="../filedialog.cpp" line="37"/>
        <source>Save svtplay-dl in directory</source>
        <translation>Spara svtplay-dl i mappen</translation>
    </message>
    <message>
        <location filename="../zoom.cpp" line="37"/>
        <source>The font size changes to the selected font size</source>
        <translation>Teckenstorleken ändras till vald teckenstorlek</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="40"/>
        <source>Save a screenshot</source>
        <translation>Spara en skärmdump</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="42"/>
        <source>Images (*.png)</source>
        <translation>bildfiler (*.png)</translation>
    </message>
</context>
</TS>
