<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>DownloadListDialog</name>
    <message>
        <location filename="../downloadlistdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="75"/>
        <source>Exit</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="92"/>
        <location filename="../downloadlistdialog.cpp" line="92"/>
        <source>Save</source>
        <translation>Spara</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="109"/>
        <source>Save as</source>
        <translation>Spara som</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="93"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="94"/>
        <source>Save as text file</source>
        <translation>Spara som textfil</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="111"/>
        <source>Could not save the file.
Check your file permissions.</source>
        <translation>Det gick inte att spara filen.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="193"/>
        <source>Unable to find file</source>
        <translation>Kan inte hitta filen</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="194"/>
        <source>Unable to find</source>
        <translation>Kan inte hitta</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="112"/>
        <location filename="../downloadlistdialog.cpp" line="197"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <location filename="../info.cpp" line="39"/>
        <source> streamCapture2 handles downloads of video streams.&lt;br&gt;</source>
        <translation> streamCapture2 hanterar nedladdningar av videoströmmar.&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="40"/>
        <source>Many thanks to </source>
        <translation>Många tack till </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="34"/>
        <source>A graphical shell for </source>
        <translation>Ett grafiskt skal för </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="36"/>
        <source> and </source>
        <translation> och </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="31"/>
        <source> is free software, license </source>
        <translation> är fri mjukvara, licens </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="62"/>
        <source>Created:</source>
        <translation>Skapad:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="64"/>
        <source>Compiled on:</source>
        <translation>Kompilerad på:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="67"/>
        <source>Qt version:</source>
        <translation>Qt version:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="113"/>
        <source>Full version number:</source>
        <translation>Fullständigt versionsnummer:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="207"/>
        <source>Unknown version</source>
        <translation>Okänd version</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="223"/>
        <source>Unknown compiler.</source>
        <translation>Okänd kompilator.</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="238"/>
        <source>Revision:</source>
        <translation>Revision:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="256"/>
        <source>Phone: </source>
        <translation>Telefon: </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="268"/>
        <source>is located in</source>
        <translation>finns i</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="269"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="249"/>
        <source>Home page</source>
        <translation>Hemsida</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="41"/>
        <source> for the Italian translation. And for many good ideas that have made the program better.</source>
        <translation> för den italienska översättningen. Och för många bra idéer som har gjort programmet bättre.</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="77"/>
        <source>for</source>
        <translation>för</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="79"/>
        <location filename="../info.cpp" line="95"/>
        <location filename="../info.cpp" line="212"/>
        <source>Compiler:</source>
        <translation>Kompilator:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="103"/>
        <source>compatible with</source>
        <translation>kompatibel med</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="251"/>
        <source>Source code</source>
        <translation>Källkod</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="253"/>
        <source>Wiki</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../newprg.cpp" line="718"/>
        <location filename="../newprg.cpp" line="742"/>
        <source>Enter your password</source>
        <translation>Skriv in ditt lösenord</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="719"/>
        <location filename="../newprg.cpp" line="743"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation>Det är inte tillåtet med mellanslag. Använd endast skrivtecken som din streaminleverantör godkänner.
Lösenordet sparas inte.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="808"/>
        <source>svtplay-dl crashed.</source>
        <translation>svtplay-dl kraschade.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="814"/>
        <source>Could not stop svtplay-dl.</source>
        <translation>Kunde inte stoppa svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="825"/>
        <source>Delete any files that may have already been downloaded.</source>
        <translation>Ta bort alla filer som kan ha blivit nedladdade.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="822"/>
        <source>svtplay-dl stopped. Exit code </source>
        <translation>svtplay-dl stoppade. Exit kod </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="847"/>
        <source>Copy to: </source>
        <translation>Kopiera till: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="980"/>
        <source>Download to: </source>
        <translation>Ladda ner till: </translation>
    </message>
</context>
<context>
    <name>Newprg</name>
    <message>
        <location filename="../about.cpp" line="43"/>
        <source>Version history</source>
        <translation>Versionshistorik</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="53"/>
        <source>License streamCapture2</source>
        <translation>Licens streamCapture2</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="63"/>
        <location filename="../about.cpp" line="73"/>
        <source>License svtplay-dl</source>
        <translation>Licens svtplay-dl</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="84"/>
        <source>License 7zip</source>
        <translation>Licens 7zip</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="100"/>
        <source> could not be found. Please download a portable streamCapture2 where FFmpeg is included.</source>
        <translation> kunde inte hittas. Ladda ner en portable streamCapture2 där FFmpeg ingår.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="105"/>
        <location filename="../about.cpp" line="109"/>
        <source>Or install </source>
        <translation>Eller installera </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="105"/>
        <location filename="../about.cpp" line="109"/>
        <source> in your system.</source>
        <translation> i ditt system.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="153"/>
        <location filename="../about.cpp" line="157"/>
        <source> You can download svtplay-dl from bin.ceicer.com. Select &quot;Tools&quot;, &quot;Download svtplay-dl...&quot;</source>
        <translation> Du kan ladda ner svtplay-dl från bin.ceicer.com. Välj &quot;Verktyg&quot;, &quot;Ladda ner svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="103"/>
        <source> could not be found. Please go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install FFmpeg.</source>
        <translation> kan inte hittas. Gå till &quot;Verktyg&quot;, Underhållsverktyg&quot; för att installera FFmpeg.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="108"/>
        <source> could not be found. Please download an AppImage where FFmpeg is included.</source>
        <translation> kunde inte hittas. Ladda ner en AppImage där FFmpeg ingår.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="137"/>
        <source>About FFmpeg</source>
        <translation>Om FFmpeg</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="151"/>
        <source>svtplay-dl is not found in the system path.</source>
        <translation>svtplay-dl finns inte i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="156"/>
        <source>svtplay-dl.exe is not found in the system path.</source>
        <translation>svtplay-dl.exe finns inte i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="199"/>
        <location filename="../about.cpp" line="204"/>
        <source> cannot be found. Go to &quot;Tools&quot;, &quot;Download svtplay-dl...&quot; to download.</source>
        <translation> kan inte hittas. Gå till &quot;Verktyg&quot;, &quot;Ladda ner svtplay-dl...&quot; för att ladda ner.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="207"/>
        <source>Please click &quot;Tools&quot; and select svtplay-dl.</source>
        <translation>Klicka på &quot;Verktyg&quot; och välj svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="221"/>
        <source>version </source>
        <translation>version </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="160"/>
        <location filename="../about.cpp" line="211"/>
        <location filename="../about.cpp" line="246"/>
        <source>About svtplay-dl</source>
        <translation>Om svtplay-dl</translation>
    </message>
    <message>
        <location filename="../language.cpp" line="34"/>
        <location filename="../language.cpp" line="73"/>
        <location filename="../language.cpp" line="112"/>
        <source>Restart Now</source>
        <translation>Starta om nu</translation>
    </message>
    <message>
        <location filename="../language.cpp" line="37"/>
        <location filename="../language.cpp" line="76"/>
        <location filename="../language.cpp" line="115"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation>Programmet måste startas om för att de nya språkinställningarna ska börja gälla.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="300"/>
        <location filename="../download_runtime.cpp" line="29"/>
        <location filename="../downloadall.cpp" line="145"/>
        <location filename="../downloadall.cpp" line="241"/>
        <location filename="../downloadallepisodes.cpp" line="45"/>
        <location filename="../downloadallepisodes.cpp" line="62"/>
        <location filename="../downloadallepisodes.cpp" line="80"/>
        <location filename="../language.cpp" line="33"/>
        <location filename="../language.cpp" line="72"/>
        <location filename="../language.cpp" line="111"/>
        <location filename="../newprg.cpp" line="867"/>
        <location filename="../newprg.cpp" line="917"/>
        <location filename="../newprg.cpp" line="1174"/>
        <location filename="../newprg.cpp" line="1611"/>
        <location filename="../offline_installer.cpp" line="35"/>
        <location filename="../paytv_create.cpp" line="59"/>
        <location filename="../paytv_create.cpp" line="88"/>
        <location filename="../paytv_create.cpp" line="120"/>
        <location filename="../paytv_edit.cpp" line="31"/>
        <location filename="../paytv_edit.cpp" line="116"/>
        <location filename="../paytv_edit.cpp" line="147"/>
        <location filename="../paytv_edit.cpp" line="207"/>
        <location filename="../save.cpp" line="36"/>
        <location filename="../st_create.cpp" line="28"/>
        <location filename="../st_create.cpp" line="36"/>
        <location filename="../st_edit.cpp" line="32"/>
        <location filename="../st_edit.cpp" line="110"/>
        <location filename="../st_edit.cpp" line="125"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="317"/>
        <source>Up and running</source>
        <oldsource>Upp and running</oldsource>
        <translation>Uppe och kör</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="111"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="52"/>
        <source>Open the folder where streamCapture2 is located</source>
        <translation>Öppna mappen där streamCapture2 finns</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="56"/>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation>Ta en skärmdump (5 sekunders fördröjning)</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="61"/>
        <source>Force update</source>
        <translation>Tvinga en uppdatering</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="74"/>
        <source>streamCapture2 is not allowed to open </source>
        <translation>streamCapture2 får inte öppna </translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="74"/>
        <source> Use the file manager instead.</source>
        <translation> Använd filhanteraren i stället.</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="40"/>
        <source>Save a screenshot</source>
        <translation>Spara en skärmdump</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="42"/>
        <source>Images (*.png)</source>
        <translation>bildfiler (*.png)</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="112"/>
        <source>Update this AppImage to the latest version</source>
        <translation>Uppdatera den här AppImagen till senaste version</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="276"/>
        <source>Please click on &quot;Tools&quot; and &quot;Maintenance Tool...&quot;</source>
        <translation>Klicka på &quot;Verktyg&quot; och &quot;Underhållsverktyg...&quot;</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="381"/>
        <source>Resolution</source>
        <translation>Upplösning</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="441"/>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Download an AppImage that contains FFmpeg.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation>&lt;b&gt;FFmpeg kan inte hittas eller är inte ett körbart program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Ladda ner en AppImage som innehåller FFmpeg.&lt;br&gt;&lt;br&gt;Eller installera FFmpeg i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="452"/>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Download a portable streamCapture2 that contains FFmpeg.&lt;br&gt;&lt;br&gt;Or download and place ffmpeg.exe in the same folder as streamapture2.exe.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation>&lt;b&gt;FFmpeg kan inte hittas eller är inget körbart program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Ladda ner en portable streamCapture2 som innehåller FFmpeg.&lt;br&gt;&lt;br&gt;Eller ladda ner och placera ffmpeg.exe i samma mapp som streamcapture2.exe.&lt;br&gt;&lt;br&gt;Eller installera FFmpeg i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="455"/>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation>&lt;b&gt;FFmpeg kan inte hittas eller är inte ett körbart program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Gå till &quot;Verktyg&quot;, &quot;Underhållsverktyg&quot; för att installera.&lt;br&gt;&lt;br&gt;Eller installera FFmpeg i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="599"/>
        <source>Could not save a file to store Recent Search list.
Check your file permissions.</source>
        <translation>Det gick inte att spara en fil för att lagra listan med senaste sökningar.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="624"/>
        <source>Could not save a file to store the list of downloads.
Check your file permissions.</source>
        <translation>Det gick inte att spara en fil för att lagra nedladdningslistan.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="140"/>
        <source>The request is processed...
Preparing to download...</source>
        <translation>Begäran behandlas...
Förbereder sig för att ladda ner...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="251"/>
        <location filename="../downloadall.cpp" line="184"/>
        <source>Selected folder to copy to is </source>
        <translation>Vald mapp att kopiera till är </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="495"/>
        <location filename="../listallepisodes.cpp" line="53"/>
        <source>The request is processed...
Starting search...</source>
        <translation>Begäran behandlas ...
Startar att söka...</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="67"/>
        <location filename="../nfo.cpp" line="45"/>
        <location filename="../sok.cpp" line="65"/>
        <source>The search field is empty!</source>
        <translation>Sökrutan är tom!</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="71"/>
        <location filename="../sok.cpp" line="69"/>
        <source>Incorrect URL</source>
        <translation>Felaktig URL</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1527"/>
        <source>Can not find any video streams, please check the address.

</source>
        <translation>Det går inte att hitta några videoströmmar , kontrollera adressen.

</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1481"/>
        <source>Downloading...</source>
        <translation>Laddar ner...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="326"/>
        <source>Starts downloading: </source>
        <translation>Startar nedladdningen: </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="363"/>
        <location filename="../downloadall.cpp" line="452"/>
        <location filename="../downloadallepisodes.cpp" line="303"/>
        <source>Removing old files, if there are any...</source>
        <translation>Tar bort gamla filer, om det finns några...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="379"/>
        <location filename="../download.cpp" line="382"/>
        <source>The download failed.</source>
        <translation>Nedladdningen misslyckades.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="473"/>
        <location filename="../downloadallepisodes.cpp" line="360"/>
        <source>No folder is selected</source>
        <translation>Ingen mapp är vald</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="253"/>
        <location filename="../listallepisodes.cpp" line="109"/>
        <location filename="../sok.cpp" line="111"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation>Det är inte tillåtet med mellanslag. Använd endast skrivtecken som din streaminleverantör godkänner.
Lösenordet sparas inte.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="28"/>
        <location filename="../downloadall.cpp" line="32"/>
        <source>cannot be found or is not an executable program.</source>
        <translation>kan inte hittas eller är inte ett körbart program.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="29"/>
        <source>Please click on &quot;Tools&quot; and select svtplay-dl.exe.</source>
        <translation>Klicka på &quot;Verktyg&quot; och välj svtplay-dl.exe.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="137"/>
        <source>You have chosen to download more than one file with the same name. In order not to overwrite files, folders will be created for each file.</source>
        <translation>Du har valt att ladda ner mer än en fil med samma namn. För att inte skriva över filer skapas mappar för varje fil.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="200"/>
        <source>The video streams are saved in</source>
        <translation>Videoströmmarna sparas i</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="389"/>
        <location filename="../downloadall.cpp" line="394"/>
        <source>The download failed. If a username and password or &apos;st&apos; cookie is required, you must enter these.</source>
        <translation>Nedladdningen misslyckades. Om ett användarnamn och lösenord eller &apos;st&apos; cookie krävs måste du ange dessa.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="30"/>
        <location filename="../downloadallepisodes.cpp" line="27"/>
        <location filename="../listallepisodes.cpp" line="28"/>
        <location filename="../newprg.cpp" line="194"/>
        <location filename="../setgetconfig.cpp" line="509"/>
        <location filename="../sok.cpp" line="30"/>
        <source> cannot be found or is not an executable program.</source>
        <translation> kan inte hittas eller är inget körbart program.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="33"/>
        <location filename="../downloadallepisodes.cpp" line="28"/>
        <location filename="../listallepisodes.cpp" line="29"/>
        <location filename="../setgetconfig.cpp" line="510"/>
        <location filename="../sok.cpp" line="31"/>
        <source>Please click on &quot;Tools&quot; and select svtplay-dl.</source>
        <translation>Vänligen klicka på &quot;Verktyg&quot; och välj svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="31"/>
        <source>Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>Klicka på &quot;Verktyg och &quot;Ladda ner svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="64"/>
        <source>The request is processed...</source>
        <translation>Begäran behandlas...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="64"/>
        <source>Preparing to download...</source>
        <translation>Förbereder för nedladdning...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="78"/>
        <source>Download streaming media to folder</source>
        <translation>Laddar ner strömmande media till mapp</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="177"/>
        <source>The video stream is saved in </source>
        <oldsource>The video file is saved in </oldsource>
        <translation>Videoströmmen sparas i </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="186"/>
        <location filename="../downloadall.cpp" line="59"/>
        <location filename="../downloadallepisodes.cpp" line="153"/>
        <source>The default folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation>Standardmappen för att ladda ner videoströmmar kan inte hittas.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="204"/>
        <source>You do not have the right to save to the default folder.
Download is interrupted.</source>
        <translation>Du har inte rätt att spara i standardmappen.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="226"/>
        <source>The folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation>Mappen för nedladdning av videoströmmar kan inte hittas.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="161"/>
        <location filename="../download.cpp" line="240"/>
        <location filename="../downloadall.cpp" line="73"/>
        <location filename="../downloadall.cpp" line="104"/>
        <location filename="../downloadall.cpp" line="178"/>
        <location filename="../downloadallepisodes.cpp" line="167"/>
        <location filename="../downloadallepisodes.cpp" line="196"/>
        <source>You do not have the right to save to the folder.
Download is interrupted.</source>
        <translation>Du har inte rätten att spara i mappen du valt.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="359"/>
        <location filename="../downloadall.cpp" line="448"/>
        <location filename="../downloadallepisodes.cpp" line="297"/>
        <source>Merge audio and video...</source>
        <translation>Slår samman ljud och video...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="391"/>
        <location filename="../download.cpp" line="400"/>
        <location filename="../downloadall.cpp" line="359"/>
        <location filename="../downloadall.cpp" line="366"/>
        <source>Download succeeded</source>
        <translation>Nedladdningen lyckades</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="451"/>
        <location filename="../download.cpp" line="456"/>
        <source>The download failed </source>
        <translation>Nedladdningen misslyckades </translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="166"/>
        <source>The default folder for copying video streams cannot be found.
Download is interrupted.</source>
        <translation>Standardmappen för att kopiera videoströmmar kan inte hittas.
Nedladdningen avbryts.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="433"/>
        <location filename="../download.cpp" line="438"/>
        <location filename="../downloadall.cpp" line="404"/>
        <location filename="../downloadall.cpp" line="412"/>
        <location filename="../downloadallepisodes.cpp" line="341"/>
        <location filename="../downloadallepisodes.cpp" line="344"/>
        <location filename="../downloadallepisodes.cpp" line="349"/>
        <source>Download completed</source>
        <translation>Nedladdningen klar</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="44"/>
        <location filename="../downloadallepisodes.cpp" line="61"/>
        <location filename="../downloadallepisodes.cpp" line="79"/>
        <source>Download anyway</source>
        <translation>Ladda ner i all fall</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="37"/>
        <source>You have chosen to create folders for each file and have the files copied.
Unfortunately, this is not possible as streamCapture2 does not know the file names.</source>
        <translation>Du har valt att skapa mappar för varje fil och kopiera filerna.
Tyvärr är detta inte möjligt eftersom streamCapture2 inte känner till filnamnen.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="55"/>
        <source>You have chosen to have the files copied.
Unfortunately, this is not possible as streamCapture2 does not know the file names.</source>
        <translation>Du har valt att kopiera filerna.
Tyvärr är detta inte möjligt eftersom streamCapture2 inte känner till filnamnen.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="72"/>
        <source>You have chosen to create folders for each file.
Unfortunately, this is not possible as streamCapture2 does not know the file names.</source>
        <translation>Du har valt att skapa mappar för varje fil.
Tyvärr är detta inte möjligt eftersom streamCapture2 inte känner till filnamnen.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="120"/>
        <source>Once svtplay-dl has started downloading all episodes, streamCapture2 no longer has control.
If you want to cancel, you may need to log out or restart your computer.
You can try to cancel using the command
&quot;sudo killall python3&quot;

Do you want to start the download?</source>
        <translation>När svtplay-dl har börjat ladda ner alla avsnitt har streamCapture2 inte längre kontroll.
Om du vill avbryta kan du behöva logga ut eller starta om datorn.
Du kan försöka avbryta med kommandot
&quot;sudo killall python3&quot;

Vill du starta nedladdningen?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="178"/>
        <source>Download all episodes to folder</source>
        <translation>Ladda ner alla avsnitt till mappen</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="272"/>
        <source>Starts downloading all episodes: </source>
        <translation>Börjar ladda ner alla avsnitt: </translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="308"/>
        <source>The download failed. Did you forget to enter username and password?</source>
        <translation>Går inte att logga in. Har du glömt att ange användarnamn och lösenord?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="322"/>
        <source>Episode</source>
        <translation>Avsnitt</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="322"/>
        <source>of</source>
        <translation>av</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="334"/>
        <source>The media files (and if you have selected the subtitles) have been downloaded.</source>
        <translation>Mediefilerna (och om du har valt undertexterna) har laddats ner.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="342"/>
        <source>Preparing to download</source>
        <translation>Förbereder att ladda ner</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="493"/>
        <location filename="../download.cpp" line="494"/>
        <location filename="../listallepisodes.cpp" line="51"/>
        <location filename="../listallepisodes.cpp" line="52"/>
        <source>Searching...</source>
        <translation>Söker...</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="147"/>
        <location filename="../sok.cpp" line="153"/>
        <source> crashed.</source>
        <translation> kraschade.</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="149"/>
        <location filename="../sok.cpp" line="155"/>
        <source>Can not find any video streams, please check the address.</source>
        <translation>Kan inte hitta någon videoström, kontrollera adressen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="209"/>
        <source>normal</source>
        <translation>normal</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="212"/>
        <source>bold and italic</source>
        <translation>fet och kursiv</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="215"/>
        <source>bold</source>
        <translation>fet</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="217"/>
        <source>italic</source>
        <translation>kursivt</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="221"/>
        <source>Current font:</source>
        <translation>Nuvarande teckensnitt:</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="222"/>
        <source>size:</source>
        <translation>storlek:</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="254"/>
        <source>View download list</source>
        <translation>Titta på nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="273"/>
        <source>Edit download list</source>
        <translation>Redigera nedladningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="282"/>
        <source>The list of video streams to download will be deleted and can not be restored.
Do you want to continue?</source>
        <translation>Listan över videoströmmar som ska laddas ned raderas och kan inte återställas.
Vill du fortsätta?</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="393"/>
        <source>Select &quot;Tools&quot;, &quot;Maintenance Tool&quot; and &quot;Update components&quot;.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="866"/>
        <location filename="../newprg.cpp" line="916"/>
        <location filename="../newprg.cpp" line="1173"/>
        <location filename="../save.cpp" line="35"/>
        <source>Choose</source>
        <translation>Välj</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="86"/>
        <location filename="../newprg.cpp" line="918"/>
        <source>Download streaming media to directory</source>
        <translation>Ladda ner strömmande media till mappen</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1114"/>
        <location filename="../newprg.cpp" line="1124"/>
        <source>Cannot find </source>
        <translation>Kan inte hitta </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1175"/>
        <location filename="../newprg.cpp" line="1194"/>
        <source>Select svtplay-dl</source>
        <translation>Välj svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1194"/>
        <source>*.exe (svtplay-dl.exe)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1213"/>
        <source>svtplay-dl is not an executable program.</source>
        <translation>svtplay-dl är inget körbart program.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1475"/>
        <location filename="../newprg.cpp" line="1476"/>
        <location filename="../newprg.cpp" line="1558"/>
        <location filename="../sok.cpp" line="204"/>
        <source>Auto select</source>
        <translation>Välj automatiskt</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1549"/>
        <source>The search is complete</source>
        <translation>Sökningen är klar</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1551"/>
        <source>The search failed</source>
        <translation>Sökningen misslyckades</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1627"/>
        <source>Edit saved searches...</source>
        <translation>Redigera sparade sökningar...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="463"/>
        <source>Path to svtplay-dl: </source>
        <translation>Sökväg till svtplay-dl: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="467"/>
        <source>Path to svtplay-dl.exe: </source>
        <translation>Sökväg till svtplay-dl.exe: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="438"/>
        <source>svtplay-dl is in the system path.</source>
        <translation>svtplay-dl finns i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="130"/>
        <source>svtplay-dl is not found in the system path.
You can download any svtplay-dl of your choice.</source>
        <translation>svtplay-dl hittas inte i systemsökvägen.
Du kan ladda ner vilken svtplay-dl du vill.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="388"/>
        <source>Please click on &quot;Tools&quot; and &quot;Update&quot;</source>
        <translation>Klicka på &quot;Verktyg&quot; och &quot;Uppdatera&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="443"/>
        <source>svtplay-dl.exe is in the system path.</source>
        <translation>svtplay-dl.exe finns i systemsökvägenvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="449"/>
        <source>ERROR! svtplay-dl is not in the system path.</source>
        <translation>FEL! svtplay-dl finns inte i systemsökvägenvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="453"/>
        <source>ERROR! svtplay-dl.exe is not in the system path.</source>
        <translation>FEL! svtplay-dl.exe finns inte i systemsökvägenvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="474"/>
        <source>ERROR! svtplay-dl could not be found.</source>
        <translation>FEL! svtplay-dl kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="478"/>
        <source>ERROR! svtplay-dl.exe could not be found.</source>
        <translation>FEL! svtplay-dl.exe kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="495"/>
        <source>ERROR! FFmpeg cannot be found.</source>
        <translation>FEL! FFmpeg kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="868"/>
        <source>Copy streaming media to directory</source>
        <translation>Kopiera mediafilen till folder</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1289"/>
        <source>This is an experimental version. This AppImage cannot be updated.</source>
        <translation>Detta är en experimentell version. Den här AppImagen går inte att uppdatera.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1289"/>
        <source>You can find more experimental versions</source>
        <translation>Du kan hitta fler experimentella versioner</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1368"/>
        <location filename="../newprg.cpp" line="1392"/>
        <source>Failed to create desktop shortcut.
Check your file permissions.</source>
        <translation>Misslyckades med att skapa en genväg på skrivbordet.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../st_create.cpp" line="37"/>
        <location filename="../st_edit.cpp" line="126"/>
        <source>Enter &apos;st&apos; cookie</source>
        <translation>Ange &apos;st&apos; cookie</translation>
    </message>
    <message>
        <location filename="../st_create.cpp" line="39"/>
        <location filename="../st_edit.cpp" line="129"/>
        <source>Enter the &apos;st&apos; cookie that your video stream provider has saved in your browser.</source>
        <translation>Ange &apos;st&apos;-cookien som vidioströmleverantören har sparat i din webbläsare.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="271"/>
        <source>Select &quot;Tools&quot;, &quot;Update&quot; to update.</source>
        <translation>Välj &quot;Verktyg&quot;, &quot;Uppdatera&quot; för att uppdatera.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="396"/>
        <location filename="../setgetconfig.cpp" line="279"/>
        <source>Download a new</source>
        <translation>Ladda ner en ny</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="499"/>
        <source>Path to ffmpeg.exe: </source>
        <translation>Sökvägen till ffmpeg.exe: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="502"/>
        <source>Path to ffmpeg: </source>
        <translation>Sökvägen till ffmpeg: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="885"/>
        <location filename="../newprg.cpp" line="935"/>
        <source>You do not have the right to save to</source>
        <translation>Du har inte rättighet att spara i</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="885"/>
        <source>and it cannot be used as the default folder to copy to.</source>
        <translation>och den kan inte användas som standardfolder att kopiera till.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="935"/>
        <source>and it cannot be used as the default folder for downloads.</source>
        <translation>och den kan inte användas som standardfolder för nedladdningar.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1114"/>
        <location filename="../newprg.cpp" line="1124"/>
        <source> in system path.</source>
        <translation> i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1216"/>
        <source>svtplay-dl.exe is not an executable program.</source>
        <translation>svtplay-dl.exe är inte ett körbart program.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1289"/>
        <source>here</source>
        <translation>här</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1325"/>
        <source>Maintenance Tool cannot be found.
Only if you install</source>
        <translation>Underhållsverktyget kan inte hittas.
Endast om du installerar</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1327"/>
        <source>is it possible to update and uninstall the program.</source>
        <translation>är det möjligt att uppdatera och avinstallera programmet.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1578"/>
        <source>Click to copy to the search box.</source>
        <translation>Klicka för att kopiera till sökrutan.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1601"/>
        <source>The number of previous searches to be saved...</source>
        <oldsource>The number of recently opened files to be displayed...</oldsource>
        <translation>Antalet tidigare sökningar som ska sparas...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1603"/>
        <source>Specify how many previous searches you want to save. If the number of searches exceeds the specified number, the oldest search is deleted.</source>
        <translation>Bestäm antalet tidigare sökningar som ska sparas. Om antalet sökningar överstiger det angivna antalet kommer den äldsta sökningen att tas bort.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1636"/>
        <source>Edit saved searches</source>
        <translation>Redigera sparade sökningar</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1680"/>
        <source>All your saved settings will be deleted.
All lists of files to download will disappear.
Do you want to continue?</source>
        <translation>Alla dina sparade inställningar raderas.
Alla listor över filer som ska laddas ned försvinner.
Vill du fortsätta?</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1726"/>
        <source>Failed to delete your configuration files.
Check your file permissions.</source>
        <translation>Det gick inte att ta bort konfigurationsfilerna.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1739"/>
        <source>was normally terminated. Exit code = </source>
        <translation>avslutades normalt. Exit kod = </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1743"/>
        <source>crashed. Exit code = </source>
        <translation>kraschade. Exit kod = </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1630"/>
        <source>Click to edit all saved searches.</source>
        <translation>Klicka för att redigera alla sparade sökningar.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="81"/>
        <source>Edit streamCapture2 settings</source>
        <translation>Redigera inställningarna för streamCapture2</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="105"/>
        <source>Edit download svtplay-dl settings</source>
        <translation>Redigera inställningarna för ladda ner svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="195"/>
        <source>Please click on &quot;Tools&quot; and select svtplay-dl...</source>
        <translation>Klicka på &quot;Verktyg&quot; och välj svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1032"/>
        <source>svtplay-dl cannot be found or is not an executable program.
Go to &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>svtplay-d.exel kan inte hittas eller är inte ett körbart program.
Klicka på &quot;Verktyg&quot; och &quot;Ladda ner svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1043"/>
        <source>svtplay-dl.exe cannot be found or is not an executable program.
Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>svtplay-dl.exe kan inte hittas eller är inget körbart program.
Klicka på &quot;Verktyg&quot; och &quot;Ladda ner svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1082"/>
        <source>svtplay-dl cannot be found or is not an executable program.
 Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>svtplay-dl kan inte hittas eller är inget körbart program.
Klicka på &quot;Verktyg&quot; och &quot;Ladda ner svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1091"/>
        <source>svtplay-dl cannot be found or is not an executable program.
Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>svtplay-dl kan inte hittas eller är inget körbart program.
Klicka på &quot;Verktyg&quot; och &quot;Ladda ner svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1250"/>
        <source>svtplay-dl cannot be found or is not an executable program.
Click &quot;Tools&quot;, &quot;Select svtplay-dl...&quot;
to select svtplay-dl.</source>
        <translation>svtplay-dl kan inte hittas eller är inte ett körbart program.
Klicka &quot;Verktyg&quot;, &quot;Välj svtplay-dl...&quot;
för att välja svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1642"/>
        <source>Remove all saved searches</source>
        <translation>Ta bort alla sparade sökningar</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1645"/>
        <source>Click to delete all saved searches.</source>
        <translation>Klicka för att ta bort alla sparade sökningar.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1618"/>
        <source>The number of searches to be saved: </source>
        <oldsource>Set number of Recent Files: </oldsource>
        <translation>Ange maxantalet sparade sökningar: </translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="89"/>
        <location filename="../paytv_edit.cpp" line="118"/>
        <source>Enter your username</source>
        <translation>Skriv in ditt användarnamn</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="301"/>
        <location filename="../downloadall.cpp" line="242"/>
        <location filename="../downloadallepisodes.cpp" line="252"/>
        <location filename="../listallepisodes.cpp" line="108"/>
        <location filename="../paytv_create.cpp" line="121"/>
        <location filename="../paytv_edit.cpp" line="150"/>
        <location filename="../sok.cpp" line="110"/>
        <source>Enter your password</source>
        <translation>Skriv in ditt lösenord</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="55"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="75"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="91"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="162"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="280"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="300"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="316"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="416"/>
        <location filename="../coppytodefaultlocation.cpp" line="40"/>
        <location filename="../download.cpp" line="165"/>
        <location filename="../download.cpp" line="190"/>
        <location filename="../download.cpp" line="208"/>
        <location filename="../download.cpp" line="230"/>
        <location filename="../download.cpp" line="244"/>
        <location filename="../download.cpp" line="299"/>
        <location filename="../downloadall.cpp" line="63"/>
        <location filename="../downloadall.cpp" line="77"/>
        <location filename="../downloadall.cpp" line="108"/>
        <location filename="../downloadall.cpp" line="144"/>
        <location filename="../downloadall.cpp" line="170"/>
        <location filename="../downloadall.cpp" line="179"/>
        <location filename="../downloadall.cpp" line="240"/>
        <location filename="../downloadallepisodes.cpp" line="157"/>
        <location filename="../downloadallepisodes.cpp" line="170"/>
        <location filename="../downloadallepisodes.cpp" line="200"/>
        <location filename="../newprg.cpp" line="131"/>
        <location filename="../newprg.cpp" line="886"/>
        <location filename="../newprg.cpp" line="938"/>
        <location filename="../newprg.cpp" line="1036"/>
        <location filename="../newprg.cpp" line="1046"/>
        <location filename="../newprg.cpp" line="1084"/>
        <location filename="../newprg.cpp" line="1092"/>
        <location filename="../newprg.cpp" line="1117"/>
        <location filename="../newprg.cpp" line="1127"/>
        <location filename="../newprg.cpp" line="1220"/>
        <location filename="../newprg.cpp" line="1255"/>
        <location filename="../newprg.cpp" line="1290"/>
        <location filename="../newprg.cpp" line="1330"/>
        <location filename="../newprg.cpp" line="1371"/>
        <location filename="../newprg.cpp" line="1395"/>
        <location filename="../newprg.cpp" line="1610"/>
        <location filename="../newprg.cpp" line="1728"/>
        <location filename="../nfo.cpp" line="55"/>
        <location filename="../nfo.cpp" line="65"/>
        <location filename="../nfo.cpp" line="75"/>
        <location filename="../nfo.cpp" line="87"/>
        <location filename="../nfo.cpp" line="157"/>
        <location filename="../offline_installer.cpp" line="58"/>
        <location filename="../offline_installer.cpp" line="91"/>
        <location filename="../paytv_create.cpp" line="58"/>
        <location filename="../paytv_create.cpp" line="87"/>
        <location filename="../paytv_create.cpp" line="119"/>
        <location filename="../paytv_edit.cpp" line="115"/>
        <location filename="../paytv_edit.cpp" line="146"/>
        <location filename="../paytv_edit.cpp" line="206"/>
        <location filename="../setgetconfig.cpp" line="75"/>
        <location filename="../setgetconfig.cpp" line="444"/>
        <location filename="../setgetconfig.cpp" line="457"/>
        <location filename="../setgetconfig.cpp" line="603"/>
        <location filename="../setgetconfig.cpp" line="627"/>
        <location filename="../shortcuts.cpp" line="69"/>
        <location filename="../shortcuts.cpp" line="140"/>
        <location filename="../st_create.cpp" line="27"/>
        <location filename="../st_create.cpp" line="35"/>
        <location filename="../st_edit.cpp" line="109"/>
        <location filename="../st_edit.cpp" line="124"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="304"/>
        <location filename="../downloadall.cpp" line="245"/>
        <location filename="../paytv_create.cpp" line="91"/>
        <location filename="../paytv_create.cpp" line="124"/>
        <location filename="../paytv_edit.cpp" line="120"/>
        <location filename="../paytv_edit.cpp" line="152"/>
        <source>Spaces are not allowed. Use only the characters
your streaming provider approves.</source>
        <translation>Mellanslag är inte tillåtna. Använd endast tecken
som din leverantör godkänner.</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="60"/>
        <location filename="../paytv_edit.cpp" line="208"/>
        <location filename="../st_create.cpp" line="29"/>
        <location filename="../st_edit.cpp" line="111"/>
        <source>Streaming service</source>
        <translation>Strömningstjänst</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="62"/>
        <location filename="../paytv_edit.cpp" line="210"/>
        <location filename="../st_create.cpp" line="31"/>
        <location filename="../st_edit.cpp" line="114"/>
        <source>Enter the name of your streaming service.</source>
        <translation>Ange namnet på din strömningstjänst.</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="134"/>
        <location filename="../paytv_edit.cpp" line="162"/>
        <source>Save password?</source>
        <translation>Spara lösenordet?</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="136"/>
        <location filename="../paytv_edit.cpp" line="164"/>
        <source>Do you want to save the password? (unsafe)?</source>
        <translation>Vill du spara lösenordet (osäkert)?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="124"/>
        <location filename="../newprg.cpp" line="290"/>
        <location filename="../newprg.cpp" line="1682"/>
        <location filename="../paytv_create.cpp" line="142"/>
        <location filename="../paytv_edit.cpp" line="170"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="125"/>
        <location filename="../newprg.cpp" line="291"/>
        <location filename="../newprg.cpp" line="1683"/>
        <location filename="../paytv_create.cpp" line="143"/>
        <location filename="../paytv_edit.cpp" line="171"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="26"/>
        <source>Manage Login details for </source>
        <translation>Hantera inloggningsuppgifter för </translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="27"/>
        <source>Edit, rename or delete
</source>
        <translation>Redigera, byt namn eller ta bort
</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="28"/>
        <location filename="../st_edit.cpp" line="30"/>
        <source>Edit</source>
        <translation>Redigera</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="29"/>
        <source>Rename</source>
        <translation>Byt namn</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="30"/>
        <location filename="../st_edit.cpp" line="31"/>
        <source>Delete</source>
        <oldsource>Delet</oldsource>
        <translation>Ta bort</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="58"/>
        <source>Create New</source>
        <translation>Skapa ny</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="67"/>
        <location filename="../setgetconfig.cpp" line="386"/>
        <source>No Password</source>
        <translation>Inget lösenord</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="37"/>
        <source>You have not selected any place to copy the media files.
Please select a location before proceeding.</source>
        <translation>Du har inte valt någon plats att kopiera mediafilerna till.
Var vänlig och bestäm en plats innan du fortsätter.</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="63"/>
        <source>A file with the same name already exists. The file will not be copied.</source>
        <translation>Det finns redan en fil med samma namn. Filen kopieras inte.</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="70"/>
        <location filename="../coppytodefaultlocation.cpp" line="94"/>
        <source>Copy succeeded</source>
        <translation>Kopieringen lyckades</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="74"/>
        <location filename="../coppytodefaultlocation.cpp" line="98"/>
        <source>Copy failed</source>
        <translation>Kopieringen misslyckades</translation>
    </message>
    <message>
        <location filename="../qsystemtrayicon.cpp" line="27"/>
        <source>The mission failed!</source>
        <translation>Uppdraget misslyckades!</translation>
    </message>
    <message>
        <location filename="../qsystemtrayicon.cpp" line="30"/>
        <source>Mission accomplished!</source>
        <translation>Uppdrag slutfört!</translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="68"/>
        <source>Failure!
The shortcut could not be created in
&quot;~/.local/share/applications&quot;
Check your file permissions.</source>
        <translation>Fel!
Genvägen kunde inte skapas i
&quot;~/.local/share/applikationer&quot;
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="115"/>
        <location filename="../shortcuts.cpp" line="117"/>
        <source>Download video streams.</source>
        <translation>Ladda ner videoströmmar.</translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="139"/>
        <source>Failure!
The shortcut could not be created.
Check your file permissions.</source>
        <translation>Fel!
Genvägen kunde inte skapas.
Kontrollera dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="33"/>
        <source>Compiled language file (*.qm)</source>
        <translation>Kompilerade språkfil (*.qm)</translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="33"/>
        <source>Open your language file</source>
        <translation>Öppna din språkfil</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="25"/>
        <source>Manage &apos;st&apos; cookie for </source>
        <translation>Hantera &apos;st&apos; cookie för </translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="27"/>
        <source>Use, Do not use, Edit or Delete
&apos;st&apos; cookie for
</source>
        <translation>Använd, Använd inte, Redigera eller Ta bort
&apos;st&apos; cookie för
</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="28"/>
        <source>Use</source>
        <translation>Använd</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="29"/>
        <source>Do not use</source>
        <translation>Använd inte</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="176"/>
        <source>Set new &apos;st&apos; cookie</source>
        <translation>Ställ in en ny &apos;st&apos;-cookie</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="177"/>
        <source>Paste and save the &apos;st&apos; cookie that your streaming provider has downloaded to your browser.</source>
        <translation>Klistra in och spara &apos;st&apos; cookien som din streamingleverantör har laddat ner till din webbläsare.</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="38"/>
        <source>Quality, Method, Codec, Resolution, Language and Role</source>
        <translation>Kvalitet, Metod, Codec, Upplösning, Språk och Roll</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="162"/>
        <source>Unable to log in. Forgot your username and password?</source>
        <translation>Går inte att logga in. Har du glömt att ange användarnamn och lösenord?</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="214"/>
        <source>ERROR: No videos found. Cant find video id for the video.</source>
        <translation>FEL: Inga videor hittades. Kan inte hitta video-id för videon.</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="37"/>
        <source>The information from svtplay-dl may or may not contain:</source>
        <translation>Informationen kan innehålla, eller inte innehålla:</translation>
    </message>
    <message>
        <location filename="../zoom.cpp" line="31"/>
        <source>The font size changes to the selected font size</source>
        <translation>Teckenstorleken ändras till vald teckenstorlek</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="126"/>
        <source>You are using version</source>
        <translation>Du använder version</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="137"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="141"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="146"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="148"/>
        <source>Stable:</source>
        <translation>Stabila:</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="137"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="141"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="146"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="148"/>
        <source>You have selected the option</source>
        <translation>Du har valt alternativet</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="127"/>
        <source>You are NOT using the latest version.</source>
        <translation>Du använder INTE senaste versionen.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="28"/>
        <source>You have downloaded</source>
        <translation>Du har laddat ner</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="31"/>
        <source>Nothing</source>
        <translation>Ingenting</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="32"/>
        <source>To folder &quot;stable&quot;: Nothing.</source>
        <translation>Till mappen &quot;stable&quot;: Ingenting.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="34"/>
        <source>To folder &quot;stable&quot;: </source>
        <translation>Till mappen &quot;stable&quot;: </translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="39"/>
        <source>To folder &quot;beta&quot;: Nothing.</source>
        <translation>Till mappen &quot;beta&quot;: Ingenting.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="41"/>
        <source>To folder &quot;beta&quot;: </source>
        <translation>Till mappen &quot;beta&quot;: </translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="57"/>
        <source>An unexpected error occurred.&lt;br&gt;Information about svtplay-dl beta can not be found.&lt;br&gt;Check your internet connection.</source>
        <translation>Ett oväntat fel inträffade.&lt;br&gt;Information om svtplay-dl beta kan inte hittas.&lt;br&gt;Kontrollera din internetanslutning.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="77"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="93"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="282"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="302"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="318"/>
        <source>An unexpected error occurred.&lt;br&gt;Information about svtplay-dl stable can not be found.&lt;br&gt;Check your internet connection.</source>
        <translation>Ett oväntat fel inträffade.&lt;br&gt;Information om svtplay-dl beta kan inte hittas.&lt;br&gt;Kontrollera din internetanslutning.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="126"/>
        <source>of svtplay-dl.</source>
        <translation>av svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="130"/>
        <source>You are not using svtplay-dl.</source>
        <translation>Du använder inte svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="137"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="141"/>
        <source>Beta:</source>
        <translation>Beta:</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="137"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="141"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="146"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="148"/>
        <source>svtplay-dl available for download</source>
        <translation>svtplay-dl tillgänglig för nedladdning</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="137"/>
        <source>You are using the beta version.</source>
        <translation>Du använder betaversionen.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="146"/>
        <source>You are using the stable version.</source>
        <translation>Du använder den stabila versionen.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="224"/>
        <source>&quot;Use stable svtplay-dl.&quot;</source>
        <translation>&quot;Använd stabila svtplay-dl.&quot;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="226"/>
        <source>&quot;Use stable svtplay-dl&quot;, but it can not be found.</source>
        <translation>&quot;Använd stabila svtplay-dl&quot;, men den kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="245"/>
        <source>&quot;Use svtplay-dl beta.&quot;</source>
        <translation>&quot;Använd svtplay-dl beta&quot;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="247"/>
        <source>&quot;Use svtplay-dl beta&quot;, but it can not be found.</source>
        <translation>&quot;Använd svtplay-dl beta&quot;, men den kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="373"/>
        <source>&lt;b&gt;Welcome to streamCapture2!&lt;/b&gt;</source>
        <translation>&lt;b&gt;Välkommen till streamCapture2!&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="378"/>
        <source>&lt;b&gt;svtplay-dl stable&lt;/b&gt; can not be found. You have entered an incorrect path or no path has been specified at all.&lt;br&gt;Or the files have been moved or deleted.</source>
        <translation>&lt;b&gt;svtplay-dl stabila&lt;/b&gt; kan inte hittas. Du har angett en felaktig sökväg eller så har ingen sökväg angetts alls.&lt;br&gt;Eller så har filerna flyttats eller tagits bort.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="379"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="397"/>
        <source>&lt;i&gt;Click &quot;Download&quot; and &quot;Download to directory stable&quot;&lt;/i&gt;</source>
        <translation>&lt;i&gt;Klicka &quot;ladda ner&quot; och &quot;Ladda ner till mappen stable&quot;&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="385"/>
        <source>&lt;b&gt;svtplay-dl beta&lt;/b&gt; can not be found. You have entered an incorrect path or no path has been specified at all.&lt;br&gt;Or the files have been moved or deleted.</source>
        <translation>&lt;b&gt;svtplay-dl beta&lt;/b&gt;kan inte hittas. Du har angett en felaktig sökväg eller så har ingen sökväg angets.&lt;br&gt;Eller så har filerna blivit flyttade eller borttagna.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="386"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="408"/>
        <source>&lt;i&gt;Click &quot;Download&quot; and &quot;Download to directory beta&quot;&lt;/i&gt;</source>
        <translation>&lt;i&gt;Klicka &quot;Ladda ner&quot; och Ladda ner till mappen beta&quot;&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="394"/>
        <source>&lt;b&gt;svtplay-dl stable is available for download.&lt;/b&gt;&lt;br&gt;Version: </source>
        <translation>&lt;b&gt;svtplay-dl stabila finns för nedladdning. &lt;/b&gt;&lt;br&gt;Version: </translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="405"/>
        <source>&lt;b&gt;svtplay-dl beta is available for download.&lt;/b&gt;&lt;br&gt;Version: </source>
        <translation>&lt;b&gt;svtplay-dl beta finns för nedladdning. &lt;/b&gt;&lt;br&gt;Version: </translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="260"/>
        <source>&quot;Use the selected svtplay-dl.&quot;</source>
        <translation>&quot;Använd vald svtplay-dl.&quot;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="266"/>
        <source>svtplay-dl is not found in the specified path.</source>
        <translation>svtplay-dl hittas inte i den angivna sökvägen.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="262"/>
        <source>&quot;Use the selected svtplay-dl&quot;, but it can not be found.</source>
        <translation>&quot;Använd vald svtplay-dl&quot;, men den kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../download_runtime.cpp" line="27"/>
        <source>Only if svtplay-dl does not work do you need to install
&quot;Microsoft Visual C++ Redistributable&quot;.
Download and double-click to install.</source>
        <translation>Bara om svtplay-dl inte fungerar behöver du installera
&quot;Microsoft Visual C++ Redistributable&quot;.
Ladda ner och dubbelklicka för att installera.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="161"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="415"/>
        <location filename="../download_runtime.cpp" line="28"/>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="47"/>
        <source>NFO files contain release information about the media. No NFO file was found.</source>
        <translation>NFO-filer innehåller releaseinformation om media. Ingen NFO-fil hittades.</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="121"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="125"/>
        <source>Episode title:</source>
        <translation>Avsnitstitel:</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="129"/>
        <source>Season:</source>
        <translation>Säsong:</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="133"/>
        <source>Episode:</source>
        <translation>Avsnitt:</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="137"/>
        <source>Plot:</source>
        <translation>Handling:</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="141"/>
        <source>Published:</source>
        <translation>Publicerad:</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="156"/>
        <source>An unexpected error occurred while downloading the file.</source>
        <translatorcomment>Ett oväntat fel inträffade när filen laddades ned.</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="175"/>
        <source>NFO Info</source>
        <translation>NFO Information</translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="31"/>
        <source>To the website</source>
        <translation>Till webbsidan</translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="34"/>
        <source>You have installed with an offline installer.&lt;br&gt;&lt;b&gt;You must uninstall before you can install a new version.&lt;/b&gt;</source>
        <translation>Du har installerat med ett offlineinstallationsprogram.&lt;br&gt;&lt;b&gt;Du måste avinstallera innan du kan installera en ny version.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="37"/>
        <source>Uninstall</source>
        <translation>Avinstallera</translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="39"/>
        <source>Uninstall and install new version</source>
        <translation>Avinstallera och installera en ny version</translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="60"/>
        <location filename="../offline_installer.cpp" line="93"/>
        <source>An unexpected error occurred.&lt;br&gt;</source>
        <translation>Ett oväntat fel inträffade.&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="60"/>
        <location filename="../offline_installer.cpp" line="93"/>
        <source>&lt;br&gt;can not be found or is not an executable program.</source>
        <translation>&lt;br&gt;kan inte hittas eller är inget körbart program.</translation>
    </message>
</context>
<context>
    <name>newprg</name>
    <message>
        <location filename="../newprg.ui" line="120"/>
        <location filename="../newprg.ui" line="927"/>
        <source>Search</source>
        <translation>Sök</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="324"/>
        <source>Method</source>
        <translation>Metod</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="421"/>
        <location filename="../newprg.ui" line="954"/>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="672"/>
        <source>&amp;File</source>
        <translation>&amp;Arkiv</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="35"/>
        <source>TEST</source>
        <translation>TEST</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="68"/>
        <source>Paste the link to the page where the video is displayed</source>
        <translation>Klistra in länken till sidan där videon visas</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="255"/>
        <source>Quality (Bitrate)</source>
        <oldsource>Bitrate</oldsource>
        <translation>Kvalitet (Bithastighet)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="309"/>
        <source>Media streaming communications protocol.</source>
        <translation>Protokoll för att strömma media.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="456"/>
        <location filename="../newprg.ui" line="1125"/>
        <source>Download all</source>
        <translation>Ladda ner alla</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="472"/>
        <source>Select quality on the video you download</source>
        <translation>Välj kvalitet på videon du laddar ner</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="71"/>
        <location filename="../newprg.ui" line="939"/>
        <source>Paste</source>
        <translation>Klistra in</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="481"/>
        <source>Quality (bitrate) and method. Higher bitrate gives better quality and larger file.</source>
        <translation>Kvalitet (bithastighet) och metod. Högre bithastighet ger högre kvalitet och större fil.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="380"/>
        <location filename="../newprg.ui" line="1087"/>
        <source>Include Subtitle</source>
        <translation>Inkludera undertexten</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="418"/>
        <source>Download the file you just searched for.</source>
        <translation>Ladda ner filen som du just sökt efter.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="453"/>
        <source>Download all files you added to the list.</source>
        <translation>Ladda ner alla filer som du lagt till på listan.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="517"/>
        <source>Select quality on the video you download.</source>
        <translation>Välj kvalitet på videoströmmen du laddar ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="561"/>
        <source>Select provider. If yoy need a password.</source>
        <translation>Välj en tjänsteleverantör. Om du behöver lösenord.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="577"/>
        <location filename="../newprg.ui" line="1072"/>
        <location filename="../newprg.ui" line="1180"/>
        <source>Password</source>
        <translation>Lösenord</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="685"/>
        <source>&amp;Language</source>
        <translation>&amp;Språk</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="729"/>
        <source>&amp;Tools</source>
        <translation>&amp;Verktyg</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="778"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjälp</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="796"/>
        <source>&amp;Recent</source>
        <oldsource>Recent</oldsource>
        <translation>S&amp;enaste</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="829"/>
        <source>&apos;&amp;st&apos; cookies</source>
        <translation>&apos;&amp;st&apos; cookies</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="858"/>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="867"/>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="733"/>
        <source>Check for updates at program start</source>
        <translation>Sök efter uppdateringar när programmet startar</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="533"/>
        <source>720p=1280x720, 1080p=1920x1080</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="603"/>
        <source>Allows given quality to differ by an amount. 300 usually works well. (Bit rate +/- 300).</source>
        <translation>Tillåter given kvalitet att skilja sig åt med en mängd. 300 brukar fungera bra. (Bithastighet +/- 300).</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="894"/>
        <source>Exit</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="897"/>
        <source>Exits the program.</source>
        <translation>Avsluta programmet.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="900"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="966"/>
        <source>License streamCapture2...</source>
        <translation>Licens streamCapture2...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1042"/>
        <source>Italian</source>
        <translation>Italienska</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1060"/>
        <source>Create new user</source>
        <translation>Skapa ny användare</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1136"/>
        <source>Create folder &quot;method_quality_amount_resolution&quot;</source>
        <translation>Skapa folder &quot;metod_kvalitet_belopp_upplösning&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1165"/>
        <source>View more information from svtplay-dl. Appears in purple text color.</source>
        <translation>Visa mer information från svtplay-dl. Visas med lila text.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1243"/>
        <source>Copy to Selected Location</source>
        <translation>Kopiera till vald plats</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1246"/>
        <source>Direct copy to the default copy location.</source>
        <translation>Direkt kopiering till förvald folder.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1261"/>
        <source>Save the location where the finished video file is copied. If you use &quot;Direct download of all...&quot; no files are ever copied.</source>
        <translation>Spara platsen ditr den färdiga videofilen kopieras. Om du använder &quot;Direktnedladdning av alla...&quot; kopieras inga filer.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1292"/>
        <source>Add all Episodes to Download List</source>
        <translation>Lägg till alla avsnitt till nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1295"/>
        <source>Adds all episodes to the download list.</source>
        <translation>Lägger till alla avsnitt till nedladdningslistan.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1347"/>
        <source>Use svtplay-dl beta</source>
        <translation>Använd svtplay-dl beta</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1361"/>
        <source>Use svtplay-dl stable</source>
        <translation>Använd stabila svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1372"/>
        <source>Use svtplay-dl from the system path</source>
        <translation>Använd svtplay-dl från systemsökvägen</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1387"/>
        <source>Select svtplay-dl that you have in your computer.</source>
        <translation>Välj svtplay-dl som du har i din dator.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1395"/>
        <source>Use the selected svtplay-dl</source>
        <translation>Använd vald svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1398"/>
        <source>Use svtplay-dl that you selected in your computer.</source>
        <translation>Använd den svtplay-dl som du valt i din dator.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1406"/>
        <source>Do not show notifications</source>
        <translation>Visa inte aviseringar</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1409"/>
        <source>Do not show notifications when the download is complete.</source>
        <translation>Visa inte aviseringar när nedladdningen är klar.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1421"/>
        <source>Create a shortcut</source>
        <translation>Skapa en genväg</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1432"/>
        <source>Desktop Shortcut</source>
        <translation>Skrivbordsgenväg</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1435"/>
        <source>Create a shortcut to streamCapture2 on the desktop.</source>
        <translation>Skapa genväg på skrivbordet till streamCapture2.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1446"/>
        <source>Create a shortcut to streamCapture2 in the operating system menu.</source>
        <translation>Skapa genväg till streamCapture2 i operativsystemets meny.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1454"/>
        <source>Useful when testing your own translation.</source>
        <translation>Användbart när du testar din egen översättning.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1462"/>
        <source>Set new &apos;st&apos; cookie</source>
        <translation>Ange ny &apos;st&apos; cookie</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1578"/>
        <source>streamCapture2 settings</source>
        <translation>Inställningar för streamCapture2</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1583"/>
        <source>download svtplay-dl settings</source>
        <translation>Inställningar för ladda ner svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1592"/>
        <source>NFO info</source>
        <translation>NFO information</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1595"/>
        <source>NFO files contain media release information. Available at svtplay.se, among other places.</source>
        <translation>NFO-filer innehåller releaseinformation om media. Tillgänglig bland annat på svtplay.se.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="740"/>
        <source>Edit settings (Advanced)</source>
        <translation>Redigera inställningarna (avancerat)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1471"/>
        <source>Download svtplay-dl...</source>
        <translation>Ladda ner svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1474"/>
        <source>Download and decompress svtplay-dl from bin.ceicer.com</source>
        <translation>Ladda ner och dekomprimera svtplay-dl från bin.ceicer.com</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1486"/>
        <source>Zoom In</source>
        <translation>Zooma in</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1489"/>
        <source>Increase the font size.</source>
        <translation>Öka teckenstorleken.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1492"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1497"/>
        <source>Zoom Default</source>
        <translation>Zooma som standard</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1500"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1509"/>
        <source>Zoom Out</source>
        <translation>Zooma ut</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1512"/>
        <source>Decrease the font size.</source>
        <translation>Minska teckenstorleken.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1515"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1524"/>
        <source>Check for the latest svtplay-dl from bin.ceicer.com...</source>
        <translation>Sök efter senaste svtplay-dl från bin.ceicer.com...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1533"/>
        <location filename="../newprg.ui" line="1536"/>
        <location filename="../newprg.ui" line="1539"/>
        <source>Download Microsoft runtime (required for svtplay-dl)...</source>
        <translation>Ladda ner Microsoft runtime (krävs för svtplay-dl)...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1542"/>
        <source>Download the runtime file from bin.ceicer.com.</source>
        <translation>Ladda ner runtimefilen från bin.ceicer.com.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1554"/>
        <source>License 7zip...</source>
        <translation>Licens 7zip...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1565"/>
        <source>Check streamCapture2 for updates at start</source>
        <translation>Kolla om det finns uppdateringar till streamCapture2 vid start</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1573"/>
        <source>Check for new versions of svtplay-dl at start</source>
        <translation>Kolla efter nya versioner av svtplay-dl vid start</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1443"/>
        <source>Applications menu Shortcut</source>
        <translation>Genväg i programmenyn</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1451"/>
        <source>Load external language file...</source>
        <translation>Ladda extern språkfil...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1270"/>
        <source>Select Default Download Location...</source>
        <translation>Välj standard nedladdningsplats...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1199"/>
        <source>Direct Download of all Episodes</source>
        <translation>Direkt nedladdning av alla avsnitt</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1202"/>
        <source>Trying to immediately download all episodes. Unable to create folders or select quality.</source>
        <translation>Försöker omedelbart att ladda ner alla avsnitt. Det går inte att skapa mappar eller välja kvalitet.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1224"/>
        <source>List all Episodes</source>
        <translation>Lista alla avsnitt</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1227"/>
        <source>Looking for video streams for all episodes.</source>
        <translation>Letar efter videoströmmar för alla avsnitt.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1273"/>
        <source>Save the location for direct download.</source>
        <translation>Spara platsen för direkt nedladdning.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1284"/>
        <source>Direct download to the default location.</source>
        <translation>Direktnedladdning till standardplatsen.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1304"/>
        <source>Select font...</source>
        <translation>Välj teckensnitt...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1313"/>
        <source>Visit svtplay-dl forum for issues...</source>
        <translation>Besök svtplay-dl forum...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1325"/>
        <source>Maintenance Tool...</source>
        <translation>Underhållsverktyg...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1375"/>
        <source>Uses (if available) svtplay-dl in the system path.</source>
        <translation>Använder (om den finns) svtplay-dl i systemsökvägen.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1384"/>
        <source>Select svtplay-dl...</source>
        <translation>Välj svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1328"/>
        <source>Starts the Maintenance Tool. To update or uninstall.</source>
        <translation>Startar Underhållsverktyget. För att uppdatera eller avinstallera.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1258"/>
        <source>Select Copy Location...</source>
        <translation>Välj kopieringsplats...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1339"/>
        <source>All saved searches, download list and settings are deleted.</source>
        <translation>Alla sparade sökningar, nedladdningslistan och alla inställningar tas bort.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1232"/>
        <location filename="../newprg.ui" line="1336"/>
        <source>Delete all settings and Exit</source>
        <translation>Ta bort alla inställningsfiler och avsluta</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1235"/>
        <source>All saved searches and the list of streams to be downloaded will be deleted.</source>
        <translation>Alla sparade sökningar och listan med filer som ska laddas ner tas bort.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1281"/>
        <source>Download to Default Location</source>
        <translation>Ladda ner till standardplatsen</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="117"/>
        <location filename="../newprg.ui" line="930"/>
        <source>Search for video streams.</source>
        <oldsource>Search for video files.</oldsource>
        <translation>Sök efter videoströmmar.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="149"/>
        <source>Add current video to the list of files that will be downloaded.</source>
        <translation>Lägg till denna videoström till listan med videoströmmar som ska laddas ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="246"/>
        <source>The number of bits that are conveyed or processed per unit of time. Higher numbers give better quality and larger file..</source>
        <translation>Antalet bitar som transporteras eller behandlas per tidsenhet. Högre siffror ger bättre kvalitet och större fil ..</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="377"/>
        <source>Searches for the subtitle and downloads it at the same time as the video stream.</source>
        <translation>Söker efter undertexten och laddar ner den samtidigt som videoströmmen.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="801"/>
        <source>&amp;Download List</source>
        <translation>&amp;Nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="813"/>
        <source>L&amp;ogin</source>
        <translation>L&amp;ogga in</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="821"/>
        <source>&amp;All Episodes</source>
        <translation>&amp;Alla avsnitt</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="834"/>
        <source>&amp;View</source>
        <translation>&amp;Visa</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="942"/>
        <source>Paste the link to the page where the video is displayed.</source>
        <translation>Klistra in länken till webbsidan där videon visas.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="957"/>
        <source>Download the stream you just searched for.</source>
        <oldsource>Download the file you just searched for.</oldsource>
        <translation>Ladda ner videoströmmen du just sökt efter.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1016"/>
        <source>Look at the list of all the streams to download.</source>
        <oldsource>Look at the list of all the files to download.</oldsource>
        <translation>Titta på listan över alla videoströmmar som ska laddas ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1063"/>
        <source>Save the name of a video stream provider, your username and, if you want, your password.</source>
        <translation>Spara namnet på en leverantör av videoströmmar, ditt användarnamn, och om du vill, ditt lösenord.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1188"/>
        <source>Uninstall streamCapture</source>
        <translation>Avinstallera streamCapture</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1191"/>
        <source>Uninstall and remove all components</source>
        <translation>Avinstallera och ta bort alla komponenter</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1207"/>
        <source>Download after Date...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1216"/>
        <source>Stop all downloads</source>
        <translation>Stoppa alla nedladdningar</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1219"/>
        <source>Trying to stop svtplay-dl.</source>
        <translation>Försöker stoppa svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="574"/>
        <location filename="../newprg.ui" line="1075"/>
        <location filename="../newprg.ui" line="1183"/>
        <source>If no saved password is found, click here.</source>
        <translation>Om inget lösenord är sparat, klicka här.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1090"/>
        <source>Searching for and downloading subtitles.</source>
        <translation>Söker efter och laddar ner undertexter.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1113"/>
        <source>Add current video to the list of streams that will be downloaded.</source>
        <oldsource>Add current video to the list of files that will be downloaded.</oldsource>
        <translation>Lägg till aktuell video till listan på videoströmmar som ska laddas ner.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1128"/>
        <source>Download all the streams in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</source>
        <oldsource>Download all the files in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</oldsource>
        <translation>Ladda ner alla videoströmmar i listan. Om det är samma videoström i olika kvaliteter kommer foldrar för varje videoström att skapas automatiskt.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1151"/>
        <source>Edit Download List (Advanced)</source>
        <translation>Redigera nedladdningslistan (avancerat)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="876"/>
        <source>About...</source>
        <translation>Om...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="885"/>
        <source>Check for updates...</source>
        <translation>Sök efter uppdateringar...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="909"/>
        <source>About svtplay-dl...</source>
        <translation>Om svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="918"/>
        <source>About FFmpeg...</source>
        <translation>Om FFmpeg...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="975"/>
        <source>License svtplay-dl...</source>
        <translation>Licens svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="984"/>
        <source>License FFmpeg...</source>
        <translation>Licens FFmpeg...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1001"/>
        <source>Help...</source>
        <translation>Hjälp...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1013"/>
        <source>View Download List</source>
        <translation>Titta på nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1025"/>
        <source>Delete Download List</source>
        <translation>Radera nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1051"/>
        <source>Version history...</source>
        <translation>Versionshistorik...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1098"/>
        <source>Explain what is going on</source>
        <translation>Förklar vad som händer</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="152"/>
        <location filename="../newprg.ui" line="1110"/>
        <source>Add to Download List</source>
        <translation>Lägg till på nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1139"/>
        <source>Create folder &quot;method_quality_amount&quot;</source>
        <translation>Skapa folder &quot;metod_kvalitet_belopp&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1142"/>
        <source>Automatically creates a folder for each downloaded video stream. If you use &quot;Direct download of all...&quot; no folders are ever created.</source>
        <translation>Skapar automatiskt en mapp för varje nedladdad videoström. Om du använder &quot;Direktnedladdning av alla...&quot; skapas inga mappar.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1154"/>
        <source>Change method or quality. Remove a download from the list. NOTE! If you change incorrectly, it will not work.</source>
        <translation>Ändra metod eller kvalitet. Ta bort en fil från nedladdning. OBS! Om du gör felaktiga ändringar kommer det inte att fungera.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1162"/>
        <source>Show more</source>
        <translation>Visa mer</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="989"/>
        <source>Recent files</source>
        <translation>Senaste filerna</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1033"/>
        <source>Delete download list</source>
        <translation>Ta bort nedladdningslistan</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1028"/>
        <source>All saved streams in the download list are deleted.</source>
        <oldsource>All saved items in the download list are deleted.</oldsource>
        <translation>Alla sparade videoströmmar i nedladdningslistan tas bort.</translation>
    </message>
</context>
</TS>
