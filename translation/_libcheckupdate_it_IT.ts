<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT" sourcelanguage="it_IT">
<context>
    <name>CheckUpdate</name>
    <message>
        <location filename="../checkupdate.cpp" line="51"/>
        <location filename="../checkupdate.cpp" line="134"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Nessuna connessione internet disponibile.
Verifica le impostazioni internet ed il firewall.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="54"/>
        <location filename="../checkupdate.cpp" line="73"/>
        <location filename="../checkupdate.cpp" line="84"/>
        <location filename="../checkupdate.cpp" line="95"/>
        <location filename="../checkupdate.cpp" line="137"/>
        <location filename="../checkupdate.cpp" line="156"/>
        <location filename="../checkupdate.cpp" line="229"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="70"/>
        <location filename="../checkupdate.cpp" line="81"/>
        <source>You have the latest version of </source>
        <translation>Hai la versione aggiornata di </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="92"/>
        <source>There was an error when the version was checked.</source>
        <translation>Si è verificato un errore durante il controllo versione.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="153"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Si è verificato un errore durante il controllo versione.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="200"/>
        <source>Updates:</source>
        <translation>Aggiornamenti:</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="223"/>
        <source>There is a new version of </source>
        <translation>È dispoibile una nuova versione di </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="225"/>
        <source>Latest version: </source>
        <translation>Versione aggiornata: </translation>
    </message>
</context>
</TS>
