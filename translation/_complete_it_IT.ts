<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>DownloadListDialog</name>
    <message>
        <location filename="../downloadlistdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Finestra</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="75"/>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="92"/>
        <location filename="../downloadlistdialog.cpp" line="92"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="109"/>
        <source>Save as</source>
        <translation>Salva come</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="93"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="94"/>
        <source>Save as text file</source>
        <translation>Salva come file testo</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="111"/>
        <source>Could not save the file.
Check your file permissions.</source>
        <translation>Impossibile salvare il file.
Verifica i permessi del file.</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="193"/>
        <source>Unable to find file</source>
        <translation>Impossibile trovare il file</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="194"/>
        <source>Unable to find</source>
        <translation>Impossibile trovare</translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="112"/>
        <location filename="../downloadlistdialog.cpp" line="197"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <location filename="../info.cpp" line="31"/>
        <source> is free software, license </source>
        <translation> è un software gratuito, licenza </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="34"/>
        <source>A graphical shell for </source>
        <translation>streamCapture2 è una shell grafica per </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="36"/>
        <source> and </source>
        <translation> e </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="39"/>
        <source> streamCapture2 handles downloads of video streams.&lt;br&gt;</source>
        <translation> stremcapture2 gestisce i download degli stream video.&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="40"/>
        <source>Many thanks to </source>
        <translation>Molte grazie a </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="41"/>
        <source> for the Italian translation. And for many good ideas that have made the program better.</source>
        <translation> per la traduzioen italiana.
E per molte buone idee per rendere il programma migliore.</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="62"/>
        <source>Created:</source>
        <translation>Creato:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="64"/>
        <source>Compiled on:</source>
        <translation>Compilato:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="67"/>
        <source>Qt version:</source>
        <translation>Versione Qt:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="113"/>
        <source>Full version number:</source>
        <translation>Versione completa:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="238"/>
        <source>Revision:</source>
        <translation>Revisione:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="103"/>
        <source>compatible with</source>
        <translation>compatibile con</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="77"/>
        <source>for</source>
        <translation>per</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="79"/>
        <location filename="../info.cpp" line="95"/>
        <location filename="../info.cpp" line="212"/>
        <source>Compiler:</source>
        <translation>Compilatore:</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="207"/>
        <source>Unknown version</source>
        <translation>Versione sconosciuta</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="223"/>
        <source>Unknown compiler.</source>
        <translation>Compilatore sconosciuto.</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="249"/>
        <source>Home page</source>
        <translation>Sito web</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="251"/>
        <source>Source code</source>
        <translation>Codice sorgente</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="253"/>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="256"/>
        <source>Phone: </source>
        <translation>Telefono: </translation>
    </message>
    <message>
        <location filename="../info.cpp" line="268"/>
        <source>is located in</source>
        <translation>è posizonato in</translation>
    </message>
    <message>
        <location filename="../info.cpp" line="269"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../newprg.cpp" line="718"/>
        <location filename="../newprg.cpp" line="742"/>
        <source>Enter your password</source>
        <translation>Inserisci password</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="719"/>
        <location filename="../newprg.cpp" line="743"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation>Gli spazi non sono consentiti. Usa solo i caratteri consentiti dal provider. La password non verrà salvata.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="808"/>
        <source>svtplay-dl crashed.</source>
        <translation>svtplay-dl è crashato.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="814"/>
        <source>Could not stop svtplay-dl.</source>
        <translation>Impossibile fermare svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="822"/>
        <source>svtplay-dl stopped. Exit code </source>
        <translation>svtplay-dl fermato. Codice uscita </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="825"/>
        <source>Delete any files that may have already been downloaded.</source>
        <translation>Elimina tutti i file che potresti aver già scaricato.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="847"/>
        <source>Copy to: </source>
        <translation>Copia in: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="980"/>
        <source>Download to: </source>
        <translation>Download in: </translation>
    </message>
</context>
<context>
    <name>Newprg</name>
    <message>
        <location filename="../about.cpp" line="43"/>
        <source>Version history</source>
        <translation>Cronologia versioni</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="55"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="75"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="91"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="162"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="280"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="300"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="316"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="416"/>
        <location filename="../coppytodefaultlocation.cpp" line="40"/>
        <location filename="../download.cpp" line="165"/>
        <location filename="../download.cpp" line="190"/>
        <location filename="../download.cpp" line="208"/>
        <location filename="../download.cpp" line="230"/>
        <location filename="../download.cpp" line="244"/>
        <location filename="../download.cpp" line="299"/>
        <location filename="../downloadall.cpp" line="63"/>
        <location filename="../downloadall.cpp" line="77"/>
        <location filename="../downloadall.cpp" line="108"/>
        <location filename="../downloadall.cpp" line="144"/>
        <location filename="../downloadall.cpp" line="170"/>
        <location filename="../downloadall.cpp" line="179"/>
        <location filename="../downloadall.cpp" line="240"/>
        <location filename="../downloadallepisodes.cpp" line="157"/>
        <location filename="../downloadallepisodes.cpp" line="170"/>
        <location filename="../downloadallepisodes.cpp" line="200"/>
        <location filename="../newprg.cpp" line="131"/>
        <location filename="../newprg.cpp" line="886"/>
        <location filename="../newprg.cpp" line="938"/>
        <location filename="../newprg.cpp" line="1036"/>
        <location filename="../newprg.cpp" line="1046"/>
        <location filename="../newprg.cpp" line="1084"/>
        <location filename="../newprg.cpp" line="1092"/>
        <location filename="../newprg.cpp" line="1117"/>
        <location filename="../newprg.cpp" line="1127"/>
        <location filename="../newprg.cpp" line="1220"/>
        <location filename="../newprg.cpp" line="1255"/>
        <location filename="../newprg.cpp" line="1290"/>
        <location filename="../newprg.cpp" line="1330"/>
        <location filename="../newprg.cpp" line="1371"/>
        <location filename="../newprg.cpp" line="1395"/>
        <location filename="../newprg.cpp" line="1610"/>
        <location filename="../newprg.cpp" line="1728"/>
        <location filename="../nfo.cpp" line="55"/>
        <location filename="../nfo.cpp" line="65"/>
        <location filename="../nfo.cpp" line="75"/>
        <location filename="../nfo.cpp" line="87"/>
        <location filename="../nfo.cpp" line="157"/>
        <location filename="../offline_installer.cpp" line="58"/>
        <location filename="../offline_installer.cpp" line="91"/>
        <location filename="../paytv_create.cpp" line="58"/>
        <location filename="../paytv_create.cpp" line="87"/>
        <location filename="../paytv_create.cpp" line="119"/>
        <location filename="../paytv_edit.cpp" line="115"/>
        <location filename="../paytv_edit.cpp" line="146"/>
        <location filename="../paytv_edit.cpp" line="206"/>
        <location filename="../setgetconfig.cpp" line="75"/>
        <location filename="../setgetconfig.cpp" line="444"/>
        <location filename="../setgetconfig.cpp" line="457"/>
        <location filename="../setgetconfig.cpp" line="603"/>
        <location filename="../setgetconfig.cpp" line="627"/>
        <location filename="../shortcuts.cpp" line="69"/>
        <location filename="../shortcuts.cpp" line="140"/>
        <location filename="../st_create.cpp" line="27"/>
        <location filename="../st_create.cpp" line="35"/>
        <location filename="../st_edit.cpp" line="109"/>
        <location filename="../st_edit.cpp" line="124"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="53"/>
        <source>License streamCapture2</source>
        <translation>Licenza streamCapture2</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="63"/>
        <location filename="../about.cpp" line="73"/>
        <source>License svtplay-dl</source>
        <translation>Licenza svtplay-dl</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="84"/>
        <source>License 7zip</source>
        <translation>Licenza 7zip</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="100"/>
        <source> could not be found. Please download a portable streamCapture2 where FFmpeg is included.</source>
        <translation> non è stato trovato. Scarica streamCapture2 portatile in cui è incluso FFmpeg.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="105"/>
        <location filename="../about.cpp" line="109"/>
        <source>Or install </source>
        <translation>O installa </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="105"/>
        <location filename="../about.cpp" line="109"/>
        <source> in your system.</source>
        <translation> nel sistema.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="153"/>
        <location filename="../about.cpp" line="157"/>
        <source> You can download svtplay-dl from bin.ceicer.com. Select &quot;Tools&quot;, &quot;Download svtplay-dl...&quot;</source>
        <translation> Puoi scaricare svtplay-dl da bin.ceicer.com. Seleziona &quot;Strumenti&quot; e &quot;Scarica svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="103"/>
        <source> could not be found. Please go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install FFmpeg.</source>
        <translation> non è stato trovato. Per installare FFmpeg seleziona &quot;Strumenti&quot;, &quot;Strumento manutenzione&quot;.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="108"/>
        <source> could not be found. Please download an AppImage where FFmpeg is included.</source>
        <translation> non è stato trovato. Scarica un&apos;AppImage in cui è incluso FFmpeg.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="137"/>
        <source>About FFmpeg</source>
        <translation>Info su FFmpeg</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="151"/>
        <source>svtplay-dl is not found in the system path.</source>
        <translation>svtplay-dl non trovato nel path di sistema.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="156"/>
        <source>svtplay-dl.exe is not found in the system path.</source>
        <translation>svtplay-dl.exe non trovato nel percorso di sistema.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="199"/>
        <location filename="../about.cpp" line="204"/>
        <source> cannot be found. Go to &quot;Tools&quot;, &quot;Download svtplay-dl...&quot; to download.</source>
        <translation> non è stato trovato. Per scaricarlo seleziona &quot;Strumenti&quot;, &quot;Scarica svtplay-dl...&quot;.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="207"/>
        <source>Please click &quot;Tools&quot; and select svtplay-dl.</source>
        <translation>Fai clic su &quot;Strumenti&quot; e seleziona svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="221"/>
        <source>version </source>
        <translation>versione </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="160"/>
        <location filename="../about.cpp" line="211"/>
        <location filename="../about.cpp" line="246"/>
        <source>About svtplay-dl</source>
        <translation>Info su svtplay-dl</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="37"/>
        <source>You have not selected any place to copy the media files.
Please select a location before proceeding.</source>
        <translation>Non hai selezionato alcun percorso dove copiare i file multimediali.
Prima di procedere seleziona un percorso.</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="63"/>
        <source>A file with the same name already exists. The file will not be copied.</source>
        <translation>Esiste già un file con lo stesso nome. 
Il file non verrà copiato.</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="70"/>
        <location filename="../coppytodefaultlocation.cpp" line="94"/>
        <source>Copy succeeded</source>
        <translation>Copia completata</translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="74"/>
        <location filename="../coppytodefaultlocation.cpp" line="98"/>
        <source>Copy failed</source>
        <translation>Copia fallita</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="30"/>
        <location filename="../downloadallepisodes.cpp" line="27"/>
        <location filename="../listallepisodes.cpp" line="28"/>
        <location filename="../newprg.cpp" line="194"/>
        <location filename="../setgetconfig.cpp" line="509"/>
        <location filename="../sok.cpp" line="30"/>
        <source> cannot be found or is not an executable program.</source>
        <translation> non trovato o non è un programam eseguibile.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="33"/>
        <location filename="../downloadallepisodes.cpp" line="28"/>
        <location filename="../listallepisodes.cpp" line="29"/>
        <location filename="../setgetconfig.cpp" line="510"/>
        <location filename="../sok.cpp" line="31"/>
        <source>Please click on &quot;Tools&quot; and select svtplay-dl.</source>
        <translation>Fai clic su &quot;Strumenti&quot; e seleziona svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="64"/>
        <source>The request is processed...</source>
        <translation>Elaborazione richiesta...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="64"/>
        <source>Preparing to download...</source>
        <translation>Preparazione download...</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="140"/>
        <source>The request is processed...
Preparing to download...</source>
        <translation>La richiesta è stata elaborata - preparazione download...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="78"/>
        <source>Download streaming media to folder</source>
        <translation>Download media streaming nella cartella</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="177"/>
        <source>The video stream is saved in </source>
        <translation>Lo stream video è stato salvato in </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="186"/>
        <location filename="../downloadall.cpp" line="59"/>
        <location filename="../downloadallepisodes.cpp" line="153"/>
        <source>The default folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation>Impossibile trovare la cartella predefinita per il download degli stream video.
Il download è stato annullato.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="204"/>
        <source>You do not have the right to save to the default folder.
Download is interrupted.</source>
        <translation>Non hai i diritti per salvare nella cartella predefinita.
Il download è stato interrotto.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="226"/>
        <source>The folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation>Impossibile trovare la cartella per il download dei flussi video.
Il download è interrotto.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="161"/>
        <location filename="../download.cpp" line="240"/>
        <location filename="../downloadall.cpp" line="73"/>
        <location filename="../downloadall.cpp" line="104"/>
        <location filename="../downloadall.cpp" line="178"/>
        <location filename="../downloadallepisodes.cpp" line="167"/>
        <location filename="../downloadallepisodes.cpp" line="196"/>
        <source>You do not have the right to save to the folder.
Download is interrupted.</source>
        <translation>Non hai diritti sufficienti per salvare in questa cartella.
Download annullato.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="251"/>
        <location filename="../downloadall.cpp" line="184"/>
        <source>Selected folder to copy to is </source>
        <translation>La cartella selezionata in cui copiare è </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="300"/>
        <location filename="../download_runtime.cpp" line="29"/>
        <location filename="../downloadall.cpp" line="145"/>
        <location filename="../downloadall.cpp" line="241"/>
        <location filename="../downloadallepisodes.cpp" line="45"/>
        <location filename="../downloadallepisodes.cpp" line="62"/>
        <location filename="../downloadallepisodes.cpp" line="80"/>
        <location filename="../language.cpp" line="33"/>
        <location filename="../language.cpp" line="72"/>
        <location filename="../language.cpp" line="111"/>
        <location filename="../newprg.cpp" line="867"/>
        <location filename="../newprg.cpp" line="917"/>
        <location filename="../newprg.cpp" line="1174"/>
        <location filename="../newprg.cpp" line="1611"/>
        <location filename="../offline_installer.cpp" line="35"/>
        <location filename="../paytv_create.cpp" line="59"/>
        <location filename="../paytv_create.cpp" line="88"/>
        <location filename="../paytv_create.cpp" line="120"/>
        <location filename="../paytv_edit.cpp" line="31"/>
        <location filename="../paytv_edit.cpp" line="116"/>
        <location filename="../paytv_edit.cpp" line="147"/>
        <location filename="../paytv_edit.cpp" line="207"/>
        <location filename="../save.cpp" line="36"/>
        <location filename="../st_create.cpp" line="28"/>
        <location filename="../st_create.cpp" line="36"/>
        <location filename="../st_edit.cpp" line="32"/>
        <location filename="../st_edit.cpp" line="110"/>
        <location filename="../st_edit.cpp" line="125"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="301"/>
        <location filename="../downloadall.cpp" line="242"/>
        <location filename="../downloadallepisodes.cpp" line="252"/>
        <location filename="../listallepisodes.cpp" line="108"/>
        <location filename="../paytv_create.cpp" line="121"/>
        <location filename="../paytv_edit.cpp" line="150"/>
        <location filename="../sok.cpp" line="110"/>
        <source>Enter your password</source>
        <translation>Inserisci la password</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="304"/>
        <location filename="../downloadall.cpp" line="245"/>
        <location filename="../paytv_create.cpp" line="91"/>
        <location filename="../paytv_create.cpp" line="124"/>
        <location filename="../paytv_edit.cpp" line="120"/>
        <location filename="../paytv_edit.cpp" line="152"/>
        <source>Spaces are not allowed. Use only the characters
your streaming provider approves.</source>
        <translation>Gli spazi non sono consentiti.
Usa solo caratteri approvati dal provider.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="326"/>
        <source>Starts downloading: </source>
        <translation>Avvio download: </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="359"/>
        <location filename="../downloadall.cpp" line="448"/>
        <location filename="../downloadallepisodes.cpp" line="297"/>
        <source>Merge audio and video...</source>
        <translation>Unisci audio e video...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="363"/>
        <location filename="../downloadall.cpp" line="452"/>
        <location filename="../downloadallepisodes.cpp" line="303"/>
        <source>Removing old files, if there are any...</source>
        <translation>Rimozione vecchi file se presenti...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="391"/>
        <location filename="../download.cpp" line="400"/>
        <location filename="../downloadall.cpp" line="359"/>
        <location filename="../downloadall.cpp" line="366"/>
        <source>Download succeeded</source>
        <translation>Download completato</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="451"/>
        <location filename="../download.cpp" line="456"/>
        <source>The download failed </source>
        <translation>Download fallito </translation>
    </message>
    <message>
        <location filename="../download.cpp" line="433"/>
        <location filename="../download.cpp" line="438"/>
        <location filename="../downloadall.cpp" line="404"/>
        <location filename="../downloadall.cpp" line="412"/>
        <location filename="../downloadallepisodes.cpp" line="341"/>
        <location filename="../downloadallepisodes.cpp" line="344"/>
        <location filename="../downloadallepisodes.cpp" line="349"/>
        <source>Download completed</source>
        <translation>Download completato</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="31"/>
        <source>Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>Seleziona &quot;Strumenti&quot; e &quot;Scarica svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="379"/>
        <location filename="../download.cpp" line="382"/>
        <source>The download failed.</source>
        <translation>Download fallito.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="473"/>
        <location filename="../downloadallepisodes.cpp" line="360"/>
        <source>No folder is selected</source>
        <translation>Nessuna cartella selezionata</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="493"/>
        <location filename="../download.cpp" line="494"/>
        <location filename="../listallepisodes.cpp" line="51"/>
        <location filename="../listallepisodes.cpp" line="52"/>
        <source>Searching...</source>
        <translation>Ricerca...</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="495"/>
        <location filename="../listallepisodes.cpp" line="53"/>
        <source>The request is processed...
Starting search...</source>
        <translation>Richiesta elaborata. 
Avvio ricerca...</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="166"/>
        <source>The default folder for copying video streams cannot be found.
Download is interrupted.</source>
        <translation>Cartella predefinita per copia stream video non trovata. 
Download annullato.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="137"/>
        <source>You have chosen to download more than one file with the same name. In order not to overwrite files, folders will be created for each file.</source>
        <translation>Hai scelto di scaricare più di un file con lo stesso nome. 
Per non sovrascrivere i file, verranno create delle cartelle per ogni file.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="28"/>
        <location filename="../downloadall.cpp" line="32"/>
        <source>cannot be found or is not an executable program.</source>
        <translation>non è  stato trovato o non è un programma eseguibile.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="29"/>
        <source>Please click on &quot;Tools&quot; and select svtplay-dl.exe.</source>
        <translation>Fai clic su &quot;Strumenti&quot; e seleziona svtplay-dl.exe.</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="200"/>
        <source>The video streams are saved in</source>
        <translation>I flussi video saranno salvati in</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="342"/>
        <source>Preparing to download</source>
        <translation>Pereparazione al download</translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="389"/>
        <location filename="../downloadall.cpp" line="394"/>
        <source>The download failed. If a username and password or &apos;st&apos; cookie is required, you must enter these.</source>
        <translation>Download fallito.
Se è richiesto un nome utente e una password o un cookie &apos;st&apos;, è necessario inserirli.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="124"/>
        <location filename="../newprg.cpp" line="290"/>
        <location filename="../newprg.cpp" line="1682"/>
        <location filename="../paytv_create.cpp" line="142"/>
        <location filename="../paytv_edit.cpp" line="170"/>
        <source>Yes</source>
        <translation>Sì</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="125"/>
        <location filename="../newprg.cpp" line="291"/>
        <location filename="../newprg.cpp" line="1683"/>
        <location filename="../paytv_create.cpp" line="143"/>
        <location filename="../paytv_edit.cpp" line="171"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="44"/>
        <location filename="../downloadallepisodes.cpp" line="61"/>
        <location filename="../downloadallepisodes.cpp" line="79"/>
        <source>Download anyway</source>
        <translation>Conferma download</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="37"/>
        <source>You have chosen to create folders for each file and have the files copied.
Unfortunately, this is not possible as streamCapture2 does not know the file names.</source>
        <translation>Hai scelto di creare cartelle per ogni file e di copiare i file.
Questo non è possibile poiché streamCapture2 non conosce i nomi dei file.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="55"/>
        <source>You have chosen to have the files copied.
Unfortunately, this is not possible as streamCapture2 does not know the file names.</source>
        <translation>Hai scelto di copiare i file.
Questo non è possibile poiché streamCapture2 non conosce i nomi dei file.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="72"/>
        <source>You have chosen to create folders for each file.
Unfortunately, this is not possible as streamCapture2 does not know the file names.</source>
        <translation>Hai scelto di creare cartelle per ogni file.
Questo non è possibile poiché streamCapture2 non conosce i nomi dei file.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="120"/>
        <source>Once svtplay-dl has started downloading all episodes, streamCapture2 no longer has control.
If you want to cancel, you may need to log out or restart your computer.
You can try to cancel using the command
&quot;sudo killall python3&quot;

Do you want to start the download?</source>
        <translation>Una volta che svtplay-dl ha iniziato a scaricare tutti gli episodi, streamCapture2 non ha più il controllo.
Se vuoi annullare il download, potrebbe essere necessario disconnettersi o riavviare il computer.
Puoi provare ad annullare il download usando il comando
&quot;sudo killall python3&quot;

Vuoi avviare il download?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="178"/>
        <source>Download all episodes to folder</source>
        <translation>Download di tutti gli episodi nella cartella</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="253"/>
        <location filename="../listallepisodes.cpp" line="109"/>
        <location filename="../sok.cpp" line="111"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation>Non sono ammessi spazi. Usa solo i caratteri approvati dal provider. La password non verrà salvata.</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="272"/>
        <source>Starts downloading all episodes: </source>
        <translation>Avvio download di tutti gli episdi: </translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="308"/>
        <source>The download failed. Did you forget to enter username and password?</source>
        <translation>Download fallito. 
Hai dimenticato di inserire nome utente e password?</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="322"/>
        <source>Episode</source>
        <translation>Episodio</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="322"/>
        <source>of</source>
        <translation>di</translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="334"/>
        <source>The media files (and if you have selected the subtitles) have been downloaded.</source>
        <translation>Download file multimediali (e se li hai selezionati i dei sottotitoli) completato.</translation>
    </message>
    <message>
        <location filename="../language.cpp" line="34"/>
        <location filename="../language.cpp" line="73"/>
        <location filename="../language.cpp" line="112"/>
        <source>Restart Now</source>
        <translation>Riavvia ora</translation>
    </message>
    <message>
        <location filename="../language.cpp" line="37"/>
        <location filename="../language.cpp" line="76"/>
        <location filename="../language.cpp" line="115"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation>Affinché le nuove impostazioni della lingua abbiano effetto il programma deve essere riavviato.</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="67"/>
        <location filename="../nfo.cpp" line="45"/>
        <location filename="../sok.cpp" line="65"/>
        <source>The search field is empty!</source>
        <translation>Il campo ricerca è vuoto!</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="71"/>
        <location filename="../sok.cpp" line="69"/>
        <source>Incorrect URL</source>
        <translation>URL non valida</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="147"/>
        <location filename="../sok.cpp" line="153"/>
        <source> crashed.</source>
        <translation> crashato.</translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="149"/>
        <location filename="../sok.cpp" line="155"/>
        <source>Can not find any video streams, please check the address.</source>
        <translation>Impossibile trovare uno stream video. 
Verifica l&apos;indirizzo.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="209"/>
        <source>normal</source>
        <translation>normale</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="212"/>
        <source>bold and italic</source>
        <translation>grassetto e corsivo</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="215"/>
        <source>bold</source>
        <translation>grasetto</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="217"/>
        <source>italic</source>
        <translation>corsivo</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="221"/>
        <source>Current font:</source>
        <translation>Font attuale:</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="222"/>
        <source>size:</source>
        <translation>dimensione:</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="254"/>
        <source>View download list</source>
        <translation>Visualizza elenco download</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="273"/>
        <source>Edit download list</source>
        <translation>Modifica elenco download</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="271"/>
        <source>Select &quot;Tools&quot;, &quot;Update&quot; to update.</source>
        <translation>Per aggiornare seleziona &quot;Strumenti&quot; -&gt; &quot;Aggiorna&quot;.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="393"/>
        <source>Select &quot;Tools&quot;, &quot;Maintenance Tool&quot; and &quot;Update components&quot;.</source>
        <translation>Selezionare &quot;Strumenti&quot;, &quot;Strumento manutenzione&quot; e &quot;Aggiorna componenti&quot;.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="396"/>
        <location filename="../setgetconfig.cpp" line="279"/>
        <source>Download a new</source>
        <translation>Scarica un nuovo</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="438"/>
        <source>svtplay-dl is in the system path.</source>
        <translation>svtplay-dl è nel percorso di sistema.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="443"/>
        <source>svtplay-dl.exe is in the system path.</source>
        <translation>svtplay-dl.exe è nel percorso di sistema.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="449"/>
        <source>ERROR! svtplay-dl is not in the system path.</source>
        <translation>ERRORE! svtplay-dl non è nel percorso di sistema.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="453"/>
        <source>ERROR! svtplay-dl.exe is not in the system path.</source>
        <translation>ERRORE! svtplay-dl.exe non è nel percorso di sistema.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="463"/>
        <source>Path to svtplay-dl: </source>
        <translation>percorso svtplay.dl: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="467"/>
        <source>Path to svtplay-dl.exe: </source>
        <translation>Percorso svtplay-dl.exe: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="474"/>
        <source>ERROR! svtplay-dl could not be found.</source>
        <translation>ERRORE! svtplay-dl non trovato.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="478"/>
        <source>ERROR! svtplay-dl.exe could not be found.</source>
        <translation>ERRORE! svtplay-dl.exe non trovato.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="495"/>
        <source>ERROR! FFmpeg cannot be found.</source>
        <translation>ERRORE! ffmpeg.exe non trovato.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="866"/>
        <location filename="../newprg.cpp" line="916"/>
        <location filename="../newprg.cpp" line="1173"/>
        <location filename="../save.cpp" line="35"/>
        <source>Choose</source>
        <translation>Scegli</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="868"/>
        <source>Copy streaming media to directory</source>
        <translation>Copia media streaming nella cartella</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1739"/>
        <source>was normally terminated. Exit code = </source>
        <translation>era terminato normalmente. Codice di uscita = </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1743"/>
        <source>crashed. Exit code = </source>
        <translation>crashato. Codice di uscita = </translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="86"/>
        <location filename="../newprg.cpp" line="918"/>
        <source>Download streaming media to directory</source>
        <translation>Download file media nella cartella</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="130"/>
        <source>svtplay-dl is not found in the system path.
You can download any svtplay-dl of your choice.</source>
        <translation>svtplay-dl non è stato trovato nel percorso di sistema.
Puoi scaricare qualsiasi svtplay-dl a tua scelta.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1043"/>
        <source>svtplay-dl.exe cannot be found or is not an executable program.
Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>svtplay-dl.exe non è stato trovato o non è un programma eseguibile.
Seleziona &quot;Strumenti&quot; e &quot;Scarica svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1082"/>
        <source>svtplay-dl cannot be found or is not an executable program.
 Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>svtplay-dl non è stato trovato o non è un programma eseguibile.
Seleziona &quot;Strumenti&quot; e &quot;Scarica svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1091"/>
        <source>svtplay-dl cannot be found or is not an executable program.
Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>svtplay-dl non è stato trovato o non è un programma eseguibile.
Seleziona &quot;Strumenti&quot; e &quot;Scarica svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1114"/>
        <location filename="../newprg.cpp" line="1124"/>
        <source>Cannot find </source>
        <translation>Impossibile trovare </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1175"/>
        <location filename="../newprg.cpp" line="1194"/>
        <source>Select svtplay-dl</source>
        <translation>Seelziona svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1194"/>
        <source>*.exe (svtplay-dl.exe)</source>
        <translation>*.exe (svtplay-dl.exe)</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1213"/>
        <source>svtplay-dl is not an executable program.</source>
        <translation>svtplay-dl non è un programma eseguibile.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1250"/>
        <source>svtplay-dl cannot be found or is not an executable program.
Click &quot;Tools&quot;, &quot;Select svtplay-dl...&quot;
to select svtplay-dl.</source>
        <translation>svtplay-dl non è stato trovato o non è un programma eseguibile.
Fai clic su &quot;Strumenti&quot; e seleziona svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1289"/>
        <source>This is an experimental version. This AppImage cannot be updated.</source>
        <translation>Questa è una versione sperimentale. Questa AppImage non può essere aggiornata.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1289"/>
        <source>You can find more experimental versions</source>
        <translation>Puoi trovare altre versioni sperimentali</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1368"/>
        <location filename="../newprg.cpp" line="1392"/>
        <source>Failed to create desktop shortcut.
Check your file permissions.</source>
        <translation>Impossibile creare il collegamento sul desktop.
Verifica i tuoi permessi sui file.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1475"/>
        <location filename="../newprg.cpp" line="1476"/>
        <location filename="../newprg.cpp" line="1558"/>
        <location filename="../sok.cpp" line="204"/>
        <source>Auto select</source>
        <translation>Selezione automatica</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1481"/>
        <source>Downloading...</source>
        <translation>Download...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1527"/>
        <source>Can not find any video streams, please check the address.

</source>
        <translation>Impossibile trovare uno stream video. 
Verifica l&apos;indirizzo.

</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1549"/>
        <source>The search is complete</source>
        <translation>Ricerca completata</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1551"/>
        <source>The search failed</source>
        <translation>Ricerca fallita</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1578"/>
        <source>Click to copy to the search box.</source>
        <translation>Fai clic per copiare nel riquadro ricerca.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1601"/>
        <source>The number of previous searches to be saved...</source>
        <translation>Imposta numero precedenti ricerche salvate...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1603"/>
        <source>Specify how many previous searches you want to save. If the number of searches exceeds the specified number, the oldest search is deleted.</source>
        <translation>Specifica quante ricerche precedenti vuoi salvare. 
Se il numero di ricerche supera il numero specificato, la ricerca più vecchia verrà eliminata.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1618"/>
        <source>The number of searches to be saved: </source>
        <translation>Numero precedenti ricerche da salvare: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1627"/>
        <source>Edit saved searches...</source>
        <translation>Modifica ricerche salvate...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1636"/>
        <source>Edit saved searches</source>
        <translation>Modifica ricerche salvate</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1630"/>
        <source>Click to edit all saved searches.</source>
        <translation>Fai clic per modificare le ricerche salvate.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="81"/>
        <source>Edit streamCapture2 settings</source>
        <translation>Modifica impostazioni streamCapture2</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="105"/>
        <source>Edit download svtplay-dl settings</source>
        <translation>Modifica impostazioni download svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="195"/>
        <source>Please click on &quot;Tools&quot; and select svtplay-dl...</source>
        <translation>Fai clic su &quot;Strumenti&quot; e seleziona svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="282"/>
        <source>The list of video streams to download will be deleted and can not be restored.
Do you want to continue?</source>
        <translation>L&apos;elenco dei flussi video da scaricare verrà eliminato e non potrà essere ripristinato.
Vuoi continuare?</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="388"/>
        <source>Please click on &quot;Tools&quot; and &quot;Update&quot;</source>
        <translation>Seleziona &quot;Strumenti e &quot;Aggiorna&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="499"/>
        <source>Path to ffmpeg.exe: </source>
        <translation>Percorso FFmpeg: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="502"/>
        <source>Path to ffmpeg: </source>
        <translation>Percorso FFmpeg: </translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="885"/>
        <location filename="../newprg.cpp" line="935"/>
        <source>You do not have the right to save to</source>
        <translation>Non hai diritti sufficienti per salvare in</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="885"/>
        <source>and it cannot be used as the default folder to copy to.</source>
        <translation>e non può essere usata come cartella predefinita in cui copiare.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="935"/>
        <source>and it cannot be used as the default folder for downloads.</source>
        <translation>e non può essere usata come cartella predefinita per i download.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1032"/>
        <source>svtplay-dl cannot be found or is not an executable program.
Go to &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation>svtplay-dl non è stato trovato o non è un programma eseguibile.
Seleziona &quot;Strumenti&quot;, e &quot;Scarica svtplay-dl...&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1114"/>
        <location filename="../newprg.cpp" line="1124"/>
        <source> in system path.</source>
        <translation> nel percorso di sistema.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1216"/>
        <source>svtplay-dl.exe is not an executable program.</source>
        <translation>svtplay-dl.exe non è un programma eseguibile.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1289"/>
        <source>here</source>
        <translation>qui</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1325"/>
        <source>Maintenance Tool cannot be found.
Only if you install</source>
        <translation>Impossibile trovare lo strumento manutenzione.
Solo se installi</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1327"/>
        <source>is it possible to update and uninstall the program.</source>
        <translation>è possibile aggiornare e disinstallare il programma.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1642"/>
        <source>Remove all saved searches</source>
        <translation>Rimuovi tutte le ricerche salvate</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1645"/>
        <source>Click to delete all saved searches.</source>
        <translation>Fai clc per eliminare tuitte le ricerche salvate.</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1680"/>
        <source>All your saved settings will be deleted.
All lists of files to download will disappear.
Do you want to continue?</source>
        <translation>Tutte le impostazioni salvate verranno eliminate.
Tutti gli elenchi di file da scaricare verranno eliminati.
Vuoi continuare?</translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1726"/>
        <source>Failed to delete your configuration files.
Check your file permissions.</source>
        <translation>Impossibile eliminare i file di configurazione.
Controlla i permessi dei file.</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="60"/>
        <location filename="../paytv_edit.cpp" line="208"/>
        <location filename="../st_create.cpp" line="29"/>
        <location filename="../st_edit.cpp" line="111"/>
        <source>Streaming service</source>
        <translation>Servizio streaming</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="62"/>
        <location filename="../paytv_edit.cpp" line="210"/>
        <location filename="../st_create.cpp" line="31"/>
        <location filename="../st_edit.cpp" line="114"/>
        <source>Enter the name of your streaming service.</source>
        <translation>Inserisci il nome del servizio streaming.</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="89"/>
        <location filename="../paytv_edit.cpp" line="118"/>
        <source>Enter your username</source>
        <translation>Inserisci nome utente</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="134"/>
        <location filename="../paytv_edit.cpp" line="162"/>
        <source>Save password?</source>
        <translation>Vuoi salvare la password?</translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="136"/>
        <location filename="../paytv_edit.cpp" line="164"/>
        <source>Do you want to save the password? (unsafe)?</source>
        <translation>Vuoi salvare la password (non sicuro)?</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="26"/>
        <source>Manage Login details for </source>
        <translation>Gestione accessi per </translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="27"/>
        <source>Edit, rename or delete
</source>
        <translation>Modifica, rinomina o elimina
</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="28"/>
        <location filename="../st_edit.cpp" line="30"/>
        <source>Edit</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="29"/>
        <source>Rename</source>
        <translation>Rinomina</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="30"/>
        <location filename="../st_edit.cpp" line="31"/>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="58"/>
        <source>Create New</source>
        <translation>Crea nuovo</translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="67"/>
        <location filename="../setgetconfig.cpp" line="386"/>
        <source>No Password</source>
        <translation>Nessuna password</translation>
    </message>
    <message>
        <location filename="../qsystemtrayicon.cpp" line="27"/>
        <source>The mission failed!</source>
        <translation>Missione fallita!</translation>
    </message>
    <message>
        <location filename="../qsystemtrayicon.cpp" line="30"/>
        <source>Mission accomplished!</source>
        <translation>Missione compiuta!</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="52"/>
        <source>Open the folder where streamCapture2 is located</source>
        <translation>Apri la cartella in cui si trova streamCapture2</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="56"/>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation>Cattura una schermata (ritardo 5 secondi)</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="61"/>
        <source>Force update</source>
        <translation>Forza aggiornamento</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="74"/>
        <source>streamCapture2 is not allowed to open </source>
        <translation>streamCapture2 non è autoizzato ad aprire </translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="74"/>
        <source> Use the file manager instead.</source>
        <translation> Usa invece il file manager.</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="40"/>
        <source>Save a screenshot</source>
        <translation>Salva schermata</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="42"/>
        <source>Images (*.png)</source>
        <translation>Immagini (*.png)</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="111"/>
        <source>Update</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="112"/>
        <source>Update this AppImage to the latest version</source>
        <translation>Aggiorna questa applicazione alla versione più recente</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="276"/>
        <source>Please click on &quot;Tools&quot; and &quot;Maintenance Tool...&quot;</source>
        <translation>Selezionare &quot;Strumenti&quot; e &quot;Aggiorna componenti...&quot;</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="317"/>
        <source>Up and running</source>
        <translation>Attiva ed in esecuzione</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="381"/>
        <source>Resolution</source>
        <translation>Risoluzione</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="441"/>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Download an AppImage that contains FFmpeg.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation>&lt;b&gt;Impossibile trovare FFmpeg o non è un programma eseguibile.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Scarica un&apos;AppImage che contiene FFmpeg.&lt;br&gt;&lt;br&gt;Oppure installa FFmpeg nel percorso di sistema.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="452"/>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Download a portable streamCapture2 that contains FFmpeg.&lt;br&gt;&lt;br&gt;Or download and place ffmpeg.exe in the same folder as streamapture2.exe.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation>&lt;b&gt;Impossibile trovare FFmpeg o non è un programma eseguibile.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Scarica streamCapture2 portatile che contiene FFmpeg.&lt;br&gt;&lt;br&gt;Oppure scarica e salva ffmpeg.exe nella stessa cartella di streamapture2.exe.&lt;br&gt;&lt;br&gt;Oppure installa FFmpeg nel percorso di sistema.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="455"/>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation>&lt;b&gt;Impossibile trovare FFmpeg o non è un programma eseguibile.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Per installare ffmpeg seleziona &quot;Strumenti&quot;, &quot;Strumento manutenzione&quot;.&lt;br&gt;&lt;br&gt;Oppure installa FFmpeg nel percorso di sistema .</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="599"/>
        <source>Could not save a file to store Recent Search list.
Check your file permissions.</source>
        <translation>Impossibile salvare un file per memorizzare l&apos;elenco delle ricerche recenti.
Controlla i permessi dei file.</translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="624"/>
        <source>Could not save a file to store the list of downloads.
Check your file permissions.</source>
        <translation>Impossibile salvare un file per memorizzare l&apos;elenco download.
Controlla i permessi dei file.</translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="68"/>
        <source>Failure!
The shortcut could not be created in
&quot;~/.local/share/applications&quot;
Check your file permissions.</source>
        <translation>Errore! Impossibile creare il collegamento in
&quot;~ / .local / share / applications&quot;
Controlla i tuoi permessi sui file.</translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="115"/>
        <location filename="../shortcuts.cpp" line="117"/>
        <source>Download video streams.</source>
        <translation>Download stream video.</translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="139"/>
        <source>Failure!
The shortcut could not be created.
Check your file permissions.</source>
        <translation>Errore! Impossibile creare il collegamento.
Verifica i tuoi permessi sui file.</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="37"/>
        <source>The information from svtplay-dl may or may not contain:</source>
        <translation>L&apos;informazione da svtplay-dl può o non può contenere:</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="38"/>
        <source>Quality, Method, Codec, Resolution, Language and Role</source>
        <translation>Metodo, qualità, codec, risoluzione, lingua e ruolo</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="162"/>
        <source>Unable to log in. Forgot your username and password?</source>
        <translation>Impossibile accedere. 
Hai dimenticato il nome utente e la password?</translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="214"/>
        <source>ERROR: No videos found. Cant find video id for the video.</source>
        <translation>ERRORE: nessun video trovato - impossibile trovare l&apos;ID video  per il video.</translation>
    </message>
    <message>
        <location filename="../st_create.cpp" line="37"/>
        <location filename="../st_edit.cpp" line="126"/>
        <source>Enter &apos;st&apos; cookie</source>
        <translation>Inserisci cookie &apos;st&apos;</translation>
    </message>
    <message>
        <location filename="../st_create.cpp" line="39"/>
        <location filename="../st_edit.cpp" line="129"/>
        <source>Enter the &apos;st&apos; cookie that your video stream provider has saved in your browser.</source>
        <translation>Inserisci il cookie &quot;st&quot; che il provider di streaming video ha salvato nel browser.</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="25"/>
        <source>Manage &apos;st&apos; cookie for </source>
        <translation>Gestione cookie &apos;st&apos; per </translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="27"/>
        <source>Use, Do not use, Edit or Delete
&apos;st&apos; cookie for
</source>
        <translation>Usa, non usare, modifica o elimina
cookie &apos;st&apos; per
</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="28"/>
        <source>Use</source>
        <translation>Usa</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="29"/>
        <source>Do not use</source>
        <translation>Non usare</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="176"/>
        <source>Set new &apos;st&apos; cookie</source>
        <translation>Imposta nuovo cookie &apos;st&apos;</translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="177"/>
        <source>Paste and save the &apos;st&apos; cookie that your streaming provider has downloaded to your browser.</source>
        <translation>Incolla e salva il cookie &apos;st&apos; che il provider dello streaming ha scaricato nel browser.</translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="33"/>
        <source>Open your language file</source>
        <translation>Apri il file lingua</translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="33"/>
        <source>Compiled language file (*.qm)</source>
        <translation>Compila fiile lingua (*.qm)</translation>
    </message>
    <message>
        <location filename="../zoom.cpp" line="31"/>
        <source>The font size changes to the selected font size</source>
        <translation>Dimensione tipo di carattere modificata come da dimensione selezionata</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="126"/>
        <source>You are using version</source>
        <translation>Stai usando la versione</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="137"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="141"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="146"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="148"/>
        <source>Stable:</source>
        <translation>Stabile:</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="137"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="141"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="146"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="148"/>
        <source>You have selected the option</source>
        <translation>Hai selezionato l&apos;opzione</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="127"/>
        <source>You are NOT using the latest version.</source>
        <translation>NON stai utilizzando la versione più recente.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="28"/>
        <source>You have downloaded</source>
        <translation>Hai scaricato</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="31"/>
        <source>Nothing</source>
        <translation>Niente</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="32"/>
        <source>To folder &quot;stable&quot;: Nothing.</source>
        <translation>Nella cartella &quot;stable&quot;: niente.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="34"/>
        <source>To folder &quot;stable&quot;: </source>
        <translation>Nella cartella &quot;stable&quot;: </translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="39"/>
        <source>To folder &quot;beta&quot;: Nothing.</source>
        <translation>Nella cartella &quot;beta&quot;: niente.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="41"/>
        <source>To folder &quot;beta&quot;: </source>
        <translation>Nella cartella &quot;beta&quot;: </translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="57"/>
        <source>An unexpected error occurred.&lt;br&gt;Information about svtplay-dl beta can not be found.&lt;br&gt;Check your internet connection.</source>
        <translation>Si è verificato un errore imprevisto.&lt;br&gt;Informazioni : impossibile trovare svtplay-dl beta .&lt;br&gt;Verifica la tua connessione Internet.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="77"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="93"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="282"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="302"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="318"/>
        <source>An unexpected error occurred.&lt;br&gt;Information about svtplay-dl stable can not be found.&lt;br&gt;Check your internet connection.</source>
        <translation>Si è verificato un errore imprevisto.&lt;br&gt;Impossibile trovare  svtplay-dl stable.&lt;br&gt;Controlla la tua connessione Internet.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="126"/>
        <source>of svtplay-dl.</source>
        <translation>di svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="130"/>
        <source>You are not using svtplay-dl.</source>
        <translation>Non stai usando svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="137"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="141"/>
        <source>Beta:</source>
        <translation>Beta:</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="137"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="141"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="146"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="148"/>
        <source>svtplay-dl available for download</source>
        <translation>Disponibile per il download svtplay-dl</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="137"/>
        <source>You are using the beta version.</source>
        <translation>Stai usando la versione beta.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="146"/>
        <source>You are using the stable version.</source>
        <translation>Stai usando la versione stabile.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="224"/>
        <source>&quot;Use stable svtplay-dl.&quot;</source>
        <translation>&quot;Usa svtplay-dl stabile.&quot;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="226"/>
        <source>&quot;Use stable svtplay-dl&quot;, but it can not be found.</source>
        <translation>&quot;Usa svtplay-dl stabile&quot;, ma non è stata trovata.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="245"/>
        <source>&quot;Use svtplay-dl beta.&quot;</source>
        <translation>&quot;Usa svtplay-dl beta.&quot;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="247"/>
        <source>&quot;Use svtplay-dl beta&quot;, but it can not be found.</source>
        <translation>&quot;Usa svtplay-dl beta&quot;, ma non è stata trovata.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="373"/>
        <source>&lt;b&gt;Welcome to streamCapture2!&lt;/b&gt;</source>
        <translation>&lt;b&gt;Benvenuto in streamCapture2!&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="378"/>
        <source>&lt;b&gt;svtplay-dl stable&lt;/b&gt; can not be found. You have entered an incorrect path or no path has been specified at all.&lt;br&gt;Or the files have been moved or deleted.</source>
        <translation>Impossibile trovare &lt;b&gt;svtplay-dl stabile&lt;/b&gt;.&lt;br&gt;Hai inserito un percorso errato o non è stato specificato alcun percorso.&lt;br&gt;Oppure i file sono stati spostati o eliminati.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="379"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="397"/>
        <source>&lt;i&gt;Click &quot;Download&quot; and &quot;Download to directory stable&quot;&lt;/i&gt;</source>
        <translation>&lt;i&gt;Fai clic su &quot;Download&quot; ​​e &quot;Download nella cartella stable&quot;&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="385"/>
        <source>&lt;b&gt;svtplay-dl beta&lt;/b&gt; can not be found. You have entered an incorrect path or no path has been specified at all.&lt;br&gt;Or the files have been moved or deleted.</source>
        <translation>Impossibile trovare &lt;b&gt;svtplay-dl beta&lt;/b&gt;.&lt;br&gt;Hai inserito un percorso errato o non è stato specificato alcun percorso.&lt;br&gt;Oppure i file sono stati spostati o eliminati.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="386"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="408"/>
        <source>&lt;i&gt;Click &quot;Download&quot; and &quot;Download to directory beta&quot;&lt;/i&gt;</source>
        <translation>&lt;i&gt;Fai clic su &quot;Download&quot; ​​e &quot;Download nella cartella beta&quot;&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="394"/>
        <source>&lt;b&gt;svtplay-dl stable is available for download.&lt;/b&gt;&lt;br&gt;Version: </source>
        <translation>&lt;b&gt;È disponibile per il download svtplay-dl stabile.&lt;/b&gt;&lt;br&gt;Versione: </translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="405"/>
        <source>&lt;b&gt;svtplay-dl beta is available for download.&lt;/b&gt;&lt;br&gt;Version: </source>
        <translation>&lt;b&gt;È disponibile per il download svtplay-dl beta.&lt;/b&gt;&lt;br&gt;Versione: </translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="260"/>
        <source>&quot;Use the selected svtplay-dl.&quot;</source>
        <translation>&quot;Usa svtplay-dl selezionato.&quot;</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="266"/>
        <source>svtplay-dl is not found in the specified path.</source>
        <translation>svtplay-dl non è stato trovato nel percorso specificato.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="262"/>
        <source>&quot;Use the selected svtplay-dl&quot;, but it can not be found.</source>
        <translation>&quot;Usa svtplay-dl selezionato&quot;, ma il file non è stato trovato.</translation>
    </message>
    <message>
        <location filename="../download_runtime.cpp" line="27"/>
        <source>Only if svtplay-dl does not work do you need to install
&quot;Microsoft Visual C++ Redistributable&quot;.
Download and double-click to install.</source>
        <translation>Solo se svtplay-dl non funziona è necessario installare
&quot;Microsoft Visual C++ ridistribuibile&quot;.
Scaricalo e fai doppio clic per installare.</translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="161"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="415"/>
        <location filename="../download_runtime.cpp" line="28"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="47"/>
        <source>NFO files contain release information about the media. No NFO file was found.</source>
        <translation>I file NFO contengono informazioni sulla versione del media. Nessun file NFO trovato.</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="121"/>
        <source>Title:</source>
        <translation>Titolo:</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="125"/>
        <source>Episode title:</source>
        <translation>Titolo episodio:</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="129"/>
        <source>Season:</source>
        <translation>Stagione:</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="133"/>
        <source>Episode:</source>
        <translation>Episodio:</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="137"/>
        <source>Plot:</source>
        <translation>Contenuto:</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="141"/>
        <source>Published:</source>
        <translation>Pubblicato:</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="156"/>
        <source>An unexpected error occurred while downloading the file.</source>
        <translation>Durante il download del file si è verificato un errore imprevisto.</translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="175"/>
        <source>NFO Info</source>
        <translation>Info NFO</translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="31"/>
        <source>To the website</source>
        <translation>Al sito web</translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="34"/>
        <source>You have installed with an offline installer.&lt;br&gt;&lt;b&gt;You must uninstall before you can install a new version.&lt;/b&gt;</source>
        <translation>Hai eseguito l&apos;installazione con un programma di installazione offline.&lt;br&gt;&lt;b&gt;Prima di poter installare una nuova versionedevi disinstallare la versione attuale .&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="37"/>
        <source>Uninstall</source>
        <translation>Disinstalla</translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="39"/>
        <source>Uninstall and install new version</source>
        <translation>Disinstalla e installa la nuova versione</translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="60"/>
        <location filename="../offline_installer.cpp" line="93"/>
        <source>An unexpected error occurred.&lt;br&gt;</source>
        <translation>Si è verificato un errore imprevisto.&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="60"/>
        <location filename="../offline_installer.cpp" line="93"/>
        <source>&lt;br&gt;can not be found or is not an executable program.</source>
        <translation>&lt;br&gt;non è stato trovato o non è un programma eseguibile.</translation>
    </message>
</context>
<context>
    <name>newprg</name>
    <message>
        <location filename="../newprg.ui" line="35"/>
        <source>TEST</source>
        <translation>TEST</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="68"/>
        <source>Paste the link to the page where the video is displayed</source>
        <translation>Incolla il collegamento alla pagina dove viene visualizzato il video</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="71"/>
        <location filename="../newprg.ui" line="939"/>
        <source>Paste</source>
        <translation>Incolla</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="117"/>
        <location filename="../newprg.ui" line="930"/>
        <source>Search for video streams.</source>
        <translation>Cerca stream video.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="120"/>
        <location filename="../newprg.ui" line="927"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="149"/>
        <source>Add current video to the list of files that will be downloaded.</source>
        <translation>Aggiungi video attuale all&apos;elenco di file che verranno scaricati.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="246"/>
        <source>The number of bits that are conveyed or processed per unit of time. Higher numbers give better quality and larger file..</source>
        <translation>Il numero di bit trasmessi o elaborati per unità di tempo. 
Numeri più alti indicano una qualità migliore ma creano file più grandi. .</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="255"/>
        <source>Quality (Bitrate)</source>
        <translation>Qualità (bitrate)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="309"/>
        <source>Media streaming communications protocol.</source>
        <translation>Protocollo comunicazione streaming media.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="324"/>
        <source>Method</source>
        <translation>Metodo</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="377"/>
        <source>Searches for the subtitle and downloads it at the same time as the video stream.</source>
        <translation>Cerca il sottotitolo e lo scarica contemporaneamente allo stream video.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="380"/>
        <location filename="../newprg.ui" line="1087"/>
        <source>Include Subtitle</source>
        <translation>Includi sottotitoli</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="418"/>
        <source>Download the file you just searched for.</source>
        <translation>Scarica il file che hai appena cercato.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="453"/>
        <source>Download all files you added to the list.</source>
        <translation>Scarica tutti i file aggiunti all&apos;elenco.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="456"/>
        <location filename="../newprg.ui" line="1125"/>
        <source>Download all</source>
        <translation>Scarica tutto</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="421"/>
        <location filename="../newprg.ui" line="954"/>
        <source>Download</source>
        <translation>Scarica</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="472"/>
        <source>Select quality on the video you download</source>
        <translation>Seleziona la qualità del video da scaricare</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="481"/>
        <source>Quality (bitrate) and method. Higher bitrate gives better quality and larger file.</source>
        <translation>Qualità (bitrate) e metodo. 
Un bitrate più alto offre una qualità migliore ma crea un file più grande.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="517"/>
        <source>Select quality on the video you download.</source>
        <translation>Seleziona la qualità del video da scaricare.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="561"/>
        <source>Select provider. If yoy need a password.</source>
        <translation>Seleziona provider. 
Se hai bisogno di una password..</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="574"/>
        <location filename="../newprg.ui" line="1075"/>
        <location filename="../newprg.ui" line="1183"/>
        <source>If no saved password is found, click here.</source>
        <translation>Se non viene trovata alcuna password salvata, fai clic qui.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="577"/>
        <location filename="../newprg.ui" line="1072"/>
        <location filename="../newprg.ui" line="1180"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="672"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="685"/>
        <source>&amp;Language</source>
        <translation>&amp;Lingua</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="729"/>
        <source>&amp;Tools</source>
        <translation>S&amp;trumenti</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="778"/>
        <source>&amp;Help</source>
        <translation>&amp;Guida</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="796"/>
        <source>&amp;Recent</source>
        <translation>&amp;Recenti</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="829"/>
        <source>&apos;&amp;st&apos; cookies</source>
        <translation>Cookie &apos;&amp;st&apos;</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="858"/>
        <source>English</source>
        <translation>Inglese</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="867"/>
        <source>Swedish</source>
        <translation>Svedese</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="733"/>
        <source>Check for updates at program start</source>
        <translation>Controlla aggiornamenti ad avvio programma</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="876"/>
        <source>About...</source>
        <translation>Info su streamCapture2...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="885"/>
        <source>Check for updates...</source>
        <translation>Controllo aggiornamenti...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="894"/>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="897"/>
        <source>Exits the program.</source>
        <translation>Esci dal programma.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="900"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="909"/>
        <source>About svtplay-dl...</source>
        <translation>Info su svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="918"/>
        <source>About FFmpeg...</source>
        <translation>Info su FFmpeg...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="942"/>
        <source>Paste the link to the page where the video is displayed.</source>
        <translation>Incolla il collegamento alla pagina dove viene visualizzato il video.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="957"/>
        <source>Download the stream you just searched for.</source>
        <translation>Scarica lo stream che hai appena cercato.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="966"/>
        <source>License streamCapture2...</source>
        <translation>Licenza streamCapture2...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="975"/>
        <source>License svtplay-dl...</source>
        <translation>Licenza svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="984"/>
        <source>License FFmpeg...</source>
        <translation>Licenza FFmpeg...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="989"/>
        <source>Recent files</source>
        <translation>File recenti</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1001"/>
        <source>Help...</source>
        <translation>Guida in linea...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1016"/>
        <source>Look at the list of all the streams to download.</source>
        <translation>Visualizza l&apos;elenco di tutti gli stream da scaricare.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1033"/>
        <source>Delete download list</source>
        <translation>Elimina elenco download</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1028"/>
        <source>All saved streams in the download list are deleted.</source>
        <translation>Tutti gli stream salvati nell&apos;elenco download sono stati eliminati.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="603"/>
        <source>Allows given quality to differ by an amount. 300 usually works well. (Bit rate +/- 300).</source>
        <translation>Consenti ad una determinata qualità di differire di un valore. 
300 di solito funziona bene. (Bitrate +/- 300).</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="801"/>
        <source>&amp;Download List</source>
        <translation>Elenco &amp;download</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="813"/>
        <source>L&amp;ogin</source>
        <translation>&amp;Accedi</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="821"/>
        <source>&amp;All Episodes</source>
        <translation>Tutti &amp;gli episodi</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="834"/>
        <source>&amp;View</source>
        <translation>&amp;Visualizza</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1013"/>
        <source>View Download List</source>
        <translation>Visualizza elenco download</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1025"/>
        <source>Delete Download List</source>
        <translation>Elimina elenco download</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1042"/>
        <source>Italian</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1051"/>
        <source>Version history...</source>
        <translation>Cronologia versioni...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1060"/>
        <source>Create new user</source>
        <translation>Crea nuovo utente</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1063"/>
        <source>Save the name of a video stream provider, your username and, if you want, your password.</source>
        <translation>Salva il nome di un provider di streaming video, il nome utente e, se lo desideri, la password.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1090"/>
        <source>Searching for and downloading subtitles.</source>
        <translation>Ricerca e download di sottotitoli.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1098"/>
        <source>Explain what is going on</source>
        <translation>Descrivi cosa sta succedendo</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="152"/>
        <location filename="../newprg.ui" line="1110"/>
        <source>Add to Download List</source>
        <translation>Aggiungi ad elenco download</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1113"/>
        <source>Add current video to the list of streams that will be downloaded.</source>
        <translation>Aggiungi il video attuale all&apos;elenco degli stream che verranno scaricati.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1128"/>
        <source>Download all the streams in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</source>
        <translation>Scarica tutti gli stream nell&apos;elenco. 
Se si tratta dello stesso stream video in qualità diverse, verranno engono create automaticamente per ogni flusso video.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1139"/>
        <source>Create folder &quot;method_quality_amount&quot;</source>
        <translation>Crea cartella &quot;metodo_qualità_quantità&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1227"/>
        <source>Looking for video streams for all episodes.</source>
        <translation>Ricerca stream video per tutti gli episodi.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1246"/>
        <source>Direct copy to the default copy location.</source>
        <translation>Copia diretta nel percorso copia predefinito.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1261"/>
        <source>Save the location where the finished video file is copied. If you use &quot;Direct download of all...&quot; no files are ever copied.</source>
        <translation>Salva il percorso in cui viene copiato il file video completato. 
Se usi &quot;Download diretto di tutti...&quot; non verrà mai copiato nessun file.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1295"/>
        <source>Adds all episodes to the download list.</source>
        <translation>Aggiungi tutti gli episodi all&apos;elenco download.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1347"/>
        <source>Use svtplay-dl beta</source>
        <translation>Usa svtplay-dl beta</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1361"/>
        <source>Use svtplay-dl stable</source>
        <translation>Usa svtplay-dl stabile</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1387"/>
        <source>Select svtplay-dl that you have in your computer.</source>
        <translation>Seleziona svtplay-dl che hai nel computer.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1398"/>
        <source>Use svtplay-dl that you selected in your computer.</source>
        <translation>Usa svtplay-dl che hai selezionato nel computer.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1454"/>
        <source>Useful when testing your own translation.</source>
        <translation>Utile quando si testa una traduzione.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="740"/>
        <source>Edit settings (Advanced)</source>
        <translation>Modifica impostazioni (avanzate)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1142"/>
        <source>Automatically creates a folder for each downloaded video stream. If you use &quot;Direct download of all...&quot; no folders are ever created.</source>
        <translation>Crea automaticamente una cartella per ogni flusso video scaricato. 
Se usi &quot;Download diretto di tutti...&quot; non verranno mai create le cartelle.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1151"/>
        <source>Edit Download List (Advanced)</source>
        <translation>Modifica elenco download (avanzato)</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1154"/>
        <source>Change method or quality. Remove a download from the list. NOTE! If you change incorrectly, it will not work.</source>
        <translation>Cambia metodo o qualità. 
Rimuovi un download dall&apos;elenco. 
NOTA! Se cambi in modo errato, non funzionerà.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1162"/>
        <source>Show more</source>
        <translation>Visualizza altro</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1188"/>
        <source>Uninstall streamCapture</source>
        <translation>Disinstalla streamCapture2</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1191"/>
        <source>Uninstall and remove all components</source>
        <translation>Disinstalla e rimuovi tutti i componenti</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1207"/>
        <source>Download after Date...</source>
        <translation>Download dopo una data...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1216"/>
        <source>Stop all downloads</source>
        <translation>Stop di tutti i download</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1219"/>
        <source>Trying to stop svtplay-dl.</source>
        <translation>Prova a fermare svtplay-dl.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1232"/>
        <location filename="../newprg.ui" line="1336"/>
        <source>Delete all settings and Exit</source>
        <translation>Elimina tutte le impostazioni ed esci</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1235"/>
        <source>All saved searches and the list of streams to be downloaded will be deleted.</source>
        <translation>Tutte le ricerche salvate e l&apos;elenco degli stream da scaricare verranno eliminati.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1243"/>
        <source>Copy to Selected Location</source>
        <translation>Copia nel percorso selezionato</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1258"/>
        <source>Select Copy Location...</source>
        <translation>Seleziona percorso in cui copiare...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1270"/>
        <source>Select Default Download Location...</source>
        <translation>Seleziona percorso predefinito download...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1273"/>
        <source>Save the location for direct download.</source>
        <translation>Salva impostazione percorso download diretto.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1281"/>
        <source>Download to Default Location</source>
        <translation>Download nel percorso predefinito</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1284"/>
        <source>Direct download to the default location.</source>
        <translation>Download diretto nel percorso predefinito.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1199"/>
        <source>Direct Download of all Episodes</source>
        <translation>Download diretto di tutti gli episodi</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="533"/>
        <source>720p=1280x720, 1080p=1920x1080</source>
        <translation>720p=1280x720, 1080p=1920x1080</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1136"/>
        <source>Create folder &quot;method_quality_amount_resolution&quot;</source>
        <translation>Crea cartella &quot;metodo_qualità_quantità_risoluzione&quot;</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1165"/>
        <source>View more information from svtplay-dl. Appears in purple text color.</source>
        <translation>Visualizza maggiori informazioni con svtplay-dl.
Appare con testo in color viola.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1202"/>
        <source>Trying to immediately download all episodes. Unable to create folders or select quality.</source>
        <translation>Cerca di scaricare immediatamente tutti gli episodi. 
Impossibile creare cartelle o selezionare la qualità.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1224"/>
        <source>List all Episodes</source>
        <translation>Elenco di tutti gli episodi</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1292"/>
        <source>Add all Episodes to Download List</source>
        <translation>Aggiungi tutti gli episodi all&apos;elenco download</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1304"/>
        <source>Select font...</source>
        <translation>Seleziona font...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1313"/>
        <source>Visit svtplay-dl forum for issues...</source>
        <translation>Visita forum svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1325"/>
        <source>Maintenance Tool...</source>
        <translation>Strumento manutenzione...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1328"/>
        <source>Starts the Maintenance Tool. To update or uninstall.</source>
        <translation>Avvia lo strumento di manutenzione per aggiornamenti o disinstallazione.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1339"/>
        <source>All saved searches, download list and settings are deleted.</source>
        <translation>Tutte le ricerche salvate, l&apos;elenco download e le impostazioni verranno eliminate.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1372"/>
        <source>Use svtplay-dl from the system path</source>
        <translation>Usa svtplay-dl dal percorso di sistema</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1375"/>
        <source>Uses (if available) svtplay-dl in the system path.</source>
        <translation>Usa (se disponibile) svtplay-dl nel percorso di sistema.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1384"/>
        <source>Select svtplay-dl...</source>
        <translation>Seleziona svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1395"/>
        <source>Use the selected svtplay-dl</source>
        <translation>Usa svtplay-dl selezionato</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1406"/>
        <source>Do not show notifications</source>
        <translation>Non visualizzare le notifiche</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1409"/>
        <source>Do not show notifications when the download is complete.</source>
        <translation>Non visualizzare le notifiche a download completato.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1421"/>
        <source>Create a shortcut</source>
        <translation>Crea un collegamento</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1432"/>
        <source>Desktop Shortcut</source>
        <translation>Collegamento sul desktop</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1435"/>
        <source>Create a shortcut to streamCapture2 on the desktop.</source>
        <translation>Crea collegamento streamCapture2 sul desktop.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1443"/>
        <source>Applications menu Shortcut</source>
        <translation>Collegamentto menu applicazioni</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1446"/>
        <source>Create a shortcut to streamCapture2 in the operating system menu.</source>
        <translation>Crea collegamento streamCapture2 nel menu del sistema operativo.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1451"/>
        <source>Load external language file...</source>
        <translation>Carica file esterno lingua...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1462"/>
        <source>Set new &apos;st&apos; cookie</source>
        <translation>Imposta nuovo cookie &apos;st&apos;</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1471"/>
        <source>Download svtplay-dl...</source>
        <translation>Download svtplay-dl...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1474"/>
        <source>Download and decompress svtplay-dl from bin.ceicer.com</source>
        <translation>Scarica e decomprime svtplay-dl da bin.ceicer.com</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1486"/>
        <source>Zoom In</source>
        <translation>Zoom +</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1489"/>
        <source>Increase the font size.</source>
        <translation>Aumenta dimensione tipo di carattere.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1492"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1497"/>
        <source>Zoom Default</source>
        <translation>Zoom predefinito</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1500"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1509"/>
        <source>Zoom Out</source>
        <translation>Zoom -</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1512"/>
        <source>Decrease the font size.</source>
        <translation>Diminuisci dimensione tipo di carattere.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1515"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1524"/>
        <source>Check for the latest svtplay-dl from bin.ceicer.com...</source>
        <translation>Controllo versione aggiornata svtplay-dl in ceicer.com...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1533"/>
        <location filename="../newprg.ui" line="1536"/>
        <location filename="../newprg.ui" line="1539"/>
        <source>Download Microsoft runtime (required for svtplay-dl)...</source>
        <translation>Scarica Microsoft Runtime (richiesto per svtplay-dl)...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1542"/>
        <source>Download the runtime file from bin.ceicer.com.</source>
        <translation>Scarica il file runtime da bin.ceicer.com.</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1554"/>
        <source>License 7zip...</source>
        <translation>Licenza 7zip...</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1565"/>
        <source>Check streamCapture2 for updates at start</source>
        <translation>Controlla aggiornamenti streamCapture2 ad avvio programma</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1573"/>
        <source>Check for new versions of svtplay-dl at start</source>
        <translation>Controlla aggiornamenti svtplay-dl ad avvio programma</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1578"/>
        <source>streamCapture2 settings</source>
        <translation>impostazioni streamCapture2</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1583"/>
        <source>download svtplay-dl settings</source>
        <translation>scarica impostazioni svtplay-dl</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1592"/>
        <source>NFO info</source>
        <translation>Info NFO</translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1595"/>
        <source>NFO files contain media release information. Available at svtplay.se, among other places.</source>
        <translation>I file NFO contengono informazioni sulla versione dei media. Disponibile, tra gli altri, su svtplay.</translation>
    </message>
</context>
<context>
    <name>CheckUpdate</name>
    <message>
        <location filename="../checkupdate.cpp" line="51"/>
        <location filename="../checkupdate.cpp" line="134"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Nessuna connessione internet disponibile.
Verifica le impostazioni internet ed il firewall.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="54"/>
        <location filename="../checkupdate.cpp" line="73"/>
        <location filename="../checkupdate.cpp" line="84"/>
        <location filename="../checkupdate.cpp" line="95"/>
        <location filename="../checkupdate.cpp" line="137"/>
        <location filename="../checkupdate.cpp" line="156"/>
        <location filename="../checkupdate.cpp" line="229"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="70"/>
        <location filename="../checkupdate.cpp" line="81"/>
        <source>You have the latest version of </source>
        <translation>Hai la versione aggiornata di </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="92"/>
        <source>There was an error when the version was checked.</source>
        <translation>Si è verificato un errore durante il controllo versione.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="153"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Si è verificato un errore durante il controllo versione.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="200"/>
        <source>Updates:</source>
        <translation>Aggiornamenti:</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="223"/>
        <source>There is a new version of </source>
        <translation>È dispoibile una nuova versione di </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="225"/>
        <source>Latest version: </source>
        <translation>Versione aggiornata: </translation>
    </message>
</context>
<context>
    <name>Update</name>
    <message>
        <location filename="../update.cpp" line="34"/>
        <location filename="../update.cpp" line="67"/>
        <source>Update</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="35"/>
        <source>zsync cannot be found in path:
</source>
        <translation>zsync non trovato nel percorso:
</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="36"/>
        <source>Unable to update.</source>
        <translation>Impossibile effettuare aggiornamento.</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="68"/>
        <source>An unexpected error occurred. Error message:</source>
        <translation>Si è verificato un errore inaspettato. Messaggio errore:</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="78"/>
        <source>is updated.</source>
        <translation>è aggiornato.</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="79"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <location filename="../updatedialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Finestra</translation>
    </message>
    <message>
        <location filename="../updatedialog.cpp" line="46"/>
        <source>Updating, please wait...</source>
        <translation>Aggiornamento...</translation>
    </message>
</context>
<context>
    <name>DownloadUnpack</name>
    <message>
        <location filename="../downloadunpack.ui" line="37"/>
        <source>MainWindow</source>
        <translation>Finestra principale</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="198"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="215"/>
        <source>V&amp;isit</source>
        <translation>V&amp;isita</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="222"/>
        <source>&amp;View</source>
        <translation>&amp;Visualizza</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="232"/>
        <source>&amp;Tools</source>
        <translation>&amp;Strumenti</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="236"/>
        <source>Choose location for</source>
        <translation>Scegli percorso per</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="257"/>
        <source>Search beta</source>
        <translation>Cerca beta</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="260"/>
        <location filename="../downloadunpack.ui" line="420"/>
        <location filename="../downloadunpack.ui" line="448"/>
        <location filename="../downloadunpack.ui" line="502"/>
        <source>&quot;&quot;</source>
        <translation>&quot;&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="272"/>
        <location filename="../downloadunpack.ui" line="275"/>
        <source>Download...</source>
        <translation>Download...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="278"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="289"/>
        <source>Try to decompress</source>
        <translation>Prova a decomprimere</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="298"/>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="301"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="310"/>
        <source>English</source>
        <translation>Inglese</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="319"/>
        <source>Swedish</source>
        <translation>Svedese</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="324"/>
        <source>Load external language file...</source>
        <translation>Carica file lingua esterno...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="333"/>
        <source>Italian</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="338"/>
        <source>Source code...</source>
        <translation>Codice sorgente...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="346"/>
        <source>Binary files...</source>
        <translation>File binari...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="358"/>
        <location filename="../downloadunpack.ui" line="361"/>
        <source>Download beta...</source>
        <translation>Download beta...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="364"/>
        <source>Download latest</source>
        <translation>Scarica più recente</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="373"/>
        <source>Zoom In</source>
        <translation>Zoom +</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="376"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="381"/>
        <source>Zoom Default</source>
        <translation>Zoom predefinito</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="384"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="393"/>
        <source>Zoom Out</source>
        <translation>Zoom -</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="396"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="401"/>
        <source>Show Toolbar</source>
        <translation>Visualizza barra strumenti</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="413"/>
        <source>Download to directory &quot;beta&quot;</source>
        <translation>Download nella cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="416"/>
        <source>Download to
directory &quot;beta&quot;</source>
        <translation>Download nella
cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="441"/>
        <source>Download to directory &quot;stable&quot;</source>
        <translation>Scarica nella cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="444"/>
        <source>Download to
directory &quot;stable&quot;</source>
        <translation>Scarica nella
cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="466"/>
        <location filename="../downloadunpack.ui" line="469"/>
        <location filename="../downloadunpack.ui" line="472"/>
        <source>stable...</source>
        <translation>stabile...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="481"/>
        <location filename="../downloadunpack.ui" line="484"/>
        <location filename="../downloadunpack.ui" line="487"/>
        <source>beta...</source>
        <translation>beta...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="496"/>
        <location filename="../downloadunpack.ui" line="499"/>
        <source>Search stable</source>
        <translation>Cerca stabile</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="511"/>
        <source>Download stable...</source>
        <translation>Download stabile...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="514"/>
        <source>Download
stable...</source>
        <translation>Download stabile...</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="518"/>
        <source>Download stable</source>
        <translation>Download stabile</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="527"/>
        <source>Download stable to directory &quot;stable&quot;</source>
        <translation>Download stabile nella cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="530"/>
        <source>Download stable to
directory &quot;stable&quot;</source>
        <translation>Download stabile nella
cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="534"/>
        <source>Download stable
to directory &quot;stable&quot;</source>
        <translation>Download stabile nella
cartella &quot;stable&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="544"/>
        <source>Download beta to directory &quot;beta&quot;</source>
        <translation>Download beta nella 
cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../downloadunpack.ui" line="547"/>
        <location filename="../downloadunpack.ui" line="551"/>
        <source>Download beta to
directory &quot;beta&quot;</source>
        <translation>Download beta nella
cartella &quot;beta&quot;</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="61"/>
        <source>Is not writable. Cancels</source>
        <translation>Non è scrivibile. Annulla</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="65"/>
        <source>Starting to download...
</source>
        <translation>Avvio download...
</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="88"/>
        <location filename="../download.cpp" line="215"/>
        <location filename="../download.cpp" line="217"/>
        <source>to</source>
        <translation>in</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="88"/>
        <location filename="../download.cpp" line="90"/>
        <source>Check your file permissions and antivirus software.</source>
        <translation>Verifica permessi file e software antivirus.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="88"/>
        <source>Can not save</source>
        <translation>Impossibile salvare</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="90"/>
        <source>Can not save to</source>
        <translation>Impossibile salvare in</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="92"/>
        <source>is downloaded to</source>
        <translation>è stato scaricato in</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="105"/>
        <source>Starting to decompress</source>
        <translation>Avvio decompressione</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="122"/>
        <source>is decompressed.</source>
        <translation>è stato decompresso.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="128"/>
        <source>Removed</source>
        <translation>Rimosso</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="140"/>
        <source>Could not delete</source>
        <translation>Impossibile eliminare</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="166"/>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Linux operating systems. It will not work with Windows operating system.</source>
        <translation>Attenzione! La versione di svtplay-dl che hai scaricato è destinata ai sistemi operativi Linux. 
Non funzionerà con il sistema operativo Windows.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="174"/>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Windows operating systems. It will not work with Linux operating system.</source>
        <translation>Attenzione! La versione di svtplay-dl che hai scaricato è destinata ai sistemi operativi Windows. 
Non funzionerà con il sistema operativo Linux.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="207"/>
        <source>Download failed, please try again.</source>
        <translation>Download non riuscito, riprova.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="215"/>
        <location filename="../download.cpp" line="217"/>
        <source>Changed name from</source>
        <translation>Modifica nome da</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="233"/>
        <source>Select &quot;Tools&quot;, &quot;Use svtplay-dl beta&quot; to use this version.</source>
        <translation>Per usare questa versione seleziona &quot;Strumenti&quot; -&gt; &quot;Usa svtplay-dl beta&quot;.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="235"/>
        <source>Select &quot;Tools&quot;, &quot;Use svtplay-dl stable&quot; to use this version.</source>
        <translation>Per usare questa versione seleziona &quot;Strumenti&quot; -&gt; &quot;Usa svtplay-dl stabile&quot;.</translation>
    </message>
    <message>
        <location filename="../download.cpp" line="265"/>
        <source>Failed to decompress.</source>
        <translation>Decompressione fallita.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="32"/>
        <source>Download svtplay-dl from bin.ceicer.com</source>
        <translation>Download svtplay-dl da bin.ceicer.com</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="68"/>
        <source>Tool bar 1</source>
        <translation>Barra strumenti 1</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="89"/>
        <source>Tool bar 2</source>
        <translation>Barra strumenti 2</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="125"/>
        <source>Current location for stable:</source>
        <translation>Percorso attuale versioen stabile:</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="125"/>
        <source>Current location for beta:</source>
        <translation>Percorso attuale per beta:</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="134"/>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation>Cattura una schermata (ritardo 5 secondi)</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="146"/>
        <location filename="../downloadunpack.cpp" line="149"/>
        <source>For</source>
        <translation>Per</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="181"/>
        <location filename="../downloadunpack.cpp" line="213"/>
        <location filename="../downloadunpack.cpp" line="273"/>
        <location filename="../downloadunpack.cpp" line="296"/>
        <source>will be downloaded to</source>
        <translation>verrà scaricato in</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="181"/>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use latest stable svtplay-dl&quot;.</source>
        <translation>Per usare questa versione di svtplay-dl, seleziona
&quot;Strumenti&quot;, &quot;Usa versione stabile più recente di svtplay-dl&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="181"/>
        <source>The stable version of svtplay-dl</source>
        <translation>La versione stabile di svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="213"/>
        <source>The beta version of svtplay-dl</source>
        <translation>La versione beta di svtplay-dl</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="213"/>
        <location filename="../downloadunpack.cpp" line="296"/>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl beta&quot;.</source>
        <translation>Per usare questa versione seleziona
&quot;Strumenti&quot; -&gt; &quot;Usa svtplay-dl beta&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="224"/>
        <location filename="../downloadunpack.cpp" line="273"/>
        <location filename="../downloadunpack.cpp" line="296"/>
        <source>Selected svtplay-dl</source>
        <translation>svtplay-dl selezionato</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="224"/>
        <location filename="../downloadunpack.cpp" line="241"/>
        <location filename="../downloadunpack.cpp" line="258"/>
        <source>will be downloaded.</source>
        <translation>verrà scaricato.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="241"/>
        <source>stable svtplay-dl</source>
        <translation>svtplay-dl stabile</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="258"/>
        <source>svtplay-dl beta</source>
        <translation>svtplay-dl beta</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="273"/>
        <source>To use this svtplay-dl version, click
&quot;Tools&quot;, &quot;Use svtplay-dl stable&quot;.</source>
        <translation>Per usare questa versione di svtplay-dl, fai clic su
&quot;Strumenti&quot;, &quot;Usa svtplay-dl stabile&quot;.</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="325"/>
        <location filename="../downloadunpack.cpp" line="348"/>
        <location filename="../filedialog.cpp" line="35"/>
        <source>Choose</source>
        <translation>Scegli</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="326"/>
        <location filename="../downloadunpack.cpp" line="349"/>
        <location filename="../filedialog.cpp" line="36"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="327"/>
        <location filename="../downloadunpack.cpp" line="350"/>
        <location filename="../filedialog.cpp" line="37"/>
        <source>Save svtplay-dl in directory</source>
        <translation>Salva svtplay-dl nella cartella</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="410"/>
        <source>files</source>
        <translation>file</translation>
    </message>
    <message>
        <location filename="../downloadunpack.cpp" line="412"/>
        <source>file</source>
        <translation>file</translation>
    </message>
    <message>
        <location filename="../zoom.cpp" line="37"/>
        <source>The font size changes to the selected font size</source>
        <translation>Dimensione tipo di carattere modificata come da dimensione selezionata</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="40"/>
        <source>Save a screenshot</source>
        <translation>Salva schermata</translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="42"/>
        <source>Images (*.png)</source>
        <translation>Immagini (*.png)</translation>
    </message>
</context>
<context>
    <name>SelectFont</name>
    <message>
        <location filename="../selectfont.ui" line="26"/>
        <location filename="../selectfont.cpp" line="26"/>
        <source>Select font</source>
        <translation>Seleziona font</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="45"/>
        <source>Choose the type of font:&lt;br&gt;&quot;Monospace fonts&quot;, fonts where all characters are the same width. &lt;br&gt;&quot;Proportional fonts&quot;, fonts where the width varies depending on the character.</source>
        <translation>Scegli il tipo di carattere:&lt;br&gt;&quot;Caratteri monospazio&quot;, caratteri in cui tutti i caratteri hanno la stessa larghezza. &lt;br&gt;&quot;Caratteri proporzionali&quot;, caratteri la cui larghezza varia a seconda del carattere.</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="139"/>
        <source>Normal</source>
        <translation>Normale</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="160"/>
        <source>Bold</source>
        <translation>Grassettto</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="178"/>
        <source>Italic</source>
        <translation>Corsivo</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="197"/>
        <source>Bold and italic</source>
        <translation>Grassetto/corsivo</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="282"/>
        <source>Default</source>
        <translation>Predefiniti</translation>
    </message>
    <message>
        <location filename="../selectfont.ui" line="314"/>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="28"/>
        <source>Select font size.</source>
        <translation>Seleziona la dimensione del font.</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="83"/>
        <source>All fonts</source>
        <translation>Tutte le font</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="83"/>
        <source>Monospaced fonts</source>
        <translation>Font monospazio</translation>
    </message>
    <message>
        <location filename="../selectfont.cpp" line="84"/>
        <source>Proportional fonts</source>
        <translation>Font proporzionali</translation>
    </message>
</context>
</TS>
