#!/bin/bash
date_now=$(date "+%FT%H-%M-%S")
for i in *; do # Whitespace-safe but not recursive.
	echo "--------------------------------------------------" > "$i-MD5.txt"
	echo "MD5 HASH SUM" >> "$i-MD5.txt"
	md5sum "$i" >> "$i-MD5.txt"
	echo "Created $date_now" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "HOW TO CHECK THE HASH SUM" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "LINUX" >> "$i-MD5.txt"
	echo "\"md5sum\" is included in most Linus distributions." >> "$i-MD5.txt"
	echo "md5sum $i" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "WINDOWS" >> "$i-MD5.txt"
	echo "\"certutil\" is included in Windows." >> "$i-MD5.txt"
	echo "certutil -hashfile $i md5" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "READ MORE" >> "$i-MD5.txt"
	echo "https://gitlab.com/posktomten" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
done
