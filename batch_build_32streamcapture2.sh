#!/bin/bash

		echo -----------------------------------------------------------
		PS3='Please enter your choice: '
		options=("Qt 5.15.2 32-bit" "Qt 5.15.4 32-bit" "Quit")
		select opt in "${options[@]}"
do
    case $opt in

        "Qt 5.15.2 32-bit")
            echo "You chose choice $REPLY which is $opt"
			qmakePath="/opt/Qt5.15.2_shared/bin/qmake"
            build_executable="build-executable5"
            break;
             ;;
        "Qt 5.15.4 32-bit")
            echo "You chose choice $REPLY which is $opt"
			qmakePath="/opt/Qt/Qt5.15.4/bin/qmake"
            build_executable="build-executable5"
            break;
            ;;
        "Quit")
            break;
            ;;
        *) echo "invalid option $REPLY";;
    esac
done

        echo -----------------------------------------------------------
		PS3='Please enter your choice: '
		options=("Debug" "Release" "Quit")
		select opt in "${options[@]}"
do
    case $opt in
        "Debug")
            echo "You chose choice $REPLY which is $opt"
			mkdir build
			cd build
			$qmakePath -project ../code/newprg.pro
			$qmakePath ../code/newprg.pro -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug && /usr/bin/make qmake_all
			make -j4
			make clean -j4
			cd ..
			rm -r build
            break;
            ;;
        "Release")
            echo "You chose choice $REPLY which is $opt"
			mkdir build
			cd build
			$qmakePath -project ../code/newprg.pro
	        $qmakePath ../code/newprg.pro -spec linux-g++ CONFIG+=qtquickcompiler && /usr/bin/make qmake_all
				
			                          
			make -j4
			make clean -j4
			cd ..
			#rm -r build
			#
						    echo -----------------------------------------------------------
							PS3='Please enter your choice: '
							options=("Copy to AppDir" "Copy to AppDir_ffmpeg" "Quit")
							select opt in "${options[@]}"
					do
						case $opt in
							"Copy to AppDir")
								#sokvag=`pwd`
								#echo "$sokvag"
								cp -f $build_executable/streamcapture2 AppDir/usr/bin/
								break;
								;;
							"Copy to AppDir_ffmpeg")
							    #sokvag=`pwd`
							    #echo "$sokvag"
								cp -f $build_executable/streamcapture2 AppDir_ffmpeg/usr/bin/
								break;
								;;
							"Quit")
								break
								;;
							*) echo "invalid option $REPLY";;
						esac
					done
		#
            break;
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done



# AppImage
        echo -----------------------------------------------------------
		PS3='Please enter your choice: '
		options=("Run this script again" "Build AppImage" "Do not build AppImage" "Quit")
		select opt in "${options[@]}"
do
    case $opt in
            "Run this script again")
            echo "You chose choice $REPLY which is $opt"
            ./`basename "$0"`

            break;
            ;;
        "Build AppImage")
            echo "You chose choice $REPLY which is $opt"
            ./batch_build_appimage.sh
            break;
            ;;
        "Do not build AppImage")
            echo "You chose choice $REPLY which is $opt"
            echo "Goodbye"
	        exit 0
            break;
            ;;
        "Quit")
            break
            exit 0;
            ;;
        *) echo "invalid option $REPLY";;
    esac
done


