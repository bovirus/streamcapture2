 # streamCapture2


<h1>Read more and download: [**Wiki**](https://gitlab.com/posktomten/streamcapture2/wikis/home)</h1>
&copy; Coppyright Ingemar Ceicer GPL v3<br><br>

@posktomten

A program to save streaming video to your computer. A graphical shell for the command line program svtplay-dl
The program works with Linux, Windows and MacOS X. The program is written in C++ and uses Qt5 or Qt6 graphic library. There is a setup program for Windows. AppImage is available for Linux and Portable version for Windows. No installation required.

[Tested](https://gitlab.com/posktomten/streamcapture2/-/wikis/Home) with several operating systems.

The program is translated into English, Italian and Swedish.

A [translation](https://gitlab.com/posktomten/streamcapture2/-/wikis/Translate) into additional languages would be appreciated!

# streamCapture2 is a graphical shell for the command line program svtplay-dl.
https://svtplay-dl.se/


FFmpeg is used in turn by svtplay-dl

https://www.ffmpeg.org/

streamCapture2 uses the binary libraries 
[downloadunpack](https://gitlab.com/posktomten/downloadunpack),
[libcheckforupdates](https://gitlab.com/posktomten/libcheckforupdates),
[libselectfont](https://gitlab.com/posktomten/libselectfont)
and 
[libzsyncupdateappimage](https://gitlab.com/posktomten/libzsyncupdateappimage).

binary library files can be found <a href="https://bin.ceicer.com/streamcapture2/lib/">here.</a>
