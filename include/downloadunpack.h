// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          downloadunpack
//          Copyright (C) 2020 - 2022 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef DOWNLOADUNPACK_H
#define DOWNLOADUNPACK_H

#include "downloadunpack_global.h"
#include "rename.h"
//#include "ui_downloadunpack.h"

#include <QMainWindow>
#include <QNetworkReply>
#include <QFile>
#include <QProcess>
#include <QFileInfo>
#include <QTimer>
#include <QFileDialog>
#include <QSettings>
#include <QFontDatabase>
#include <QMessageBox>
#include <QScreen>
#include <QStandardPaths>
#include <QDesktopServices>
#include <QCloseEvent>
#include <QTranslator>
#include <QDebug>
#include <QListView>
#include <QFlags>
#include <QApplication>
#include <QToolButton>
#include <QToolTip>
#include <QToolBar>




//#ifdef Q_OS_WIN



//#ifdef Q_PROCESSOR_X86_64 // Windows 64 bit
// TEST
//#define CHECK_LATEST_STABLE "http://bin.ceicer.com/svtplay-dl-test/Windows/64bit/version_stable.txt"
//#define CHECK_LATEST "http://bin.ceicer.com/svtplay-dl-test/Windows/64bit/version.txt"
//#define CHECK_LATEST_STABLE "http://bin.ceicer.com/svtplay-dl/Windows/64bit/version_stable.txt"
//#define CHECK_LATEST "http://bin.ceicer.com/svtplay-dl/Windows/64bit/version.txt"
//#endif

//#ifdef Q_PROCESSOR_X86_32 // Windows 32 bit
//#define CHECK_LATEST_STABLE "http://bin.ceicer.com/svtplay-dl/Windows/32bit/version_stable.txt"
//#define CHECK_LATEST "http://bin.ceicer.com/svtplay-dl/Windows/32bit/version.txt"
//#endif
//#endif // Windows


//#ifdef Q_OS_LINUX
//#define FONT 12
//#define VERSION_PATH "http://bin.ceicer.com/streamcapture2/version_linux.txt"
//#define CHECK_LATEST_STABLE "http://bin.ceicer.com/svtplay-dl/Linux/version_stable.txt"
//#define CHECK_LATEST "http://bin.ceicer.com/svtplay-dl/Linux/version.txt"
//#endif // Linux

#ifdef Q_OS_WIN
#define CURRENT_YEAR __DATE__
#endif


#define EXECUTABLE_DISPLAY_NAME "streamCapture2"
#define EXECUTABLE_NAME "streamcapture2"
#define LIBRARY_NAME "downloadunpack"

//#define EXECUTABLE_DISPLAY_NAME "Test Program"
//#define EXECUTABLE_NAME "testproggram"
//#define LIBRARY_NAME "downloadunpack"


#define RED 218
#define GREEN 255
#define BLUE 221

#ifdef Q_OS_LINUX
#define DOWNLOADUNPACK_FONT_SIZE 12
#endif
#ifdef Q_OS_WIN
#define DOWNLOADUNPACK_FONT_SIZE 11
#endif

QT_BEGIN_NAMESPACE
namespace Ui
{
class DownloadUnpack;
}
QT_END_NAMESPACE
#ifdef Q_OS_WINDOWS
#ifndef QT_STATIC
class DOWNLOADUNPACK_EXPORT DownloadUnpack : public QMainWindow
#endif

#ifdef QT_STATIC
    class DownloadUnpack : public QMainWindow
#endif
#endif
#ifdef Q_OS_LINUX
        class DownloadUnpack : public QMainWindow
#endif
        {
            Q_OBJECT


        private:
            int stable_or_snapshot;
            Ui::DownloadUnpack *ui;
            QString selectedsvtplaydl;
            QTimer *timer;
            QString networkErrorMessages(QNetworkReply::NetworkError error);
            void startConfig();
            QString currentOs();

            void getLatest(const QString version);

            void download(bool downloadlatest, const QString &latest, const QString &path);
            void openFolder();
//            zoom
            void zoom();
            void pteDisplayTextChanged();
            void zoomDefault();
            void zoomMinus();
            void zoomPlus();
            QString getStablebetapath(QString stable_bleeding);
            void directoryListing(QString version);
            void downloadLatest(QString *version);
            void downloadLatest(QString *version, QString &path);





        public:
            DownloadUnpack(QWidget *parent = nullptr);
            ~DownloadUnpack();
            void endConfig();
            void closeEvent(QCloseEvent *e);



        private slots:



            void klocka();
            QString fileDialog();
            void takeAScreenshot();


        signals:


        };
#endif // DOWNLOADUNPACK_H
