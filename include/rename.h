// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          rename
//          Copyright (C) 2022 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef RENAME_H
#define RENAME_H

#include <QObject>
#include <QDir>
#include <QTimer>
#include <QDebug>
#include "rename_global.h"



#ifdef Q_OS_WINDOWS
#ifndef QT_STATIC
class RENAME_EXPORT Rename : public QObject
#endif
#ifdef QT_STATIC
    class Rename : public QObject
#endif
#endif
#ifdef Q_OS_LINUX
        class Rename : public QObject
#endif


        {
            Q_OBJECT
        public:
            explicit Rename(QObject *parent = nullptr);
            bool reName(const QString &oldname, const QString &newname);


        signals:

        };

#endif // RENAME_H
