//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          zsyncUpdateAppImage
//          Copyright (C) 2020 - 2022  Ingemar Ceicer
//          https://gitlab.com/posktomten/zsyncupdateappimage
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef UPDATEDIALOG_H
#define UPDATEDIALOG_H

#include <QDialog>
#include <QFontDatabase>
#include <QDebug>
#include <QResource>

namespace Ui
{
class UpdateDialog;
}

class UpdateDialog : public QDialog
{
    Q_OBJECT


public:
    explicit UpdateDialog(QWidget *parent = nullptr);
    ~UpdateDialog();



private:
    Ui::UpdateDialog *ui;

};

#endif // UPDATEDIALOG_H
