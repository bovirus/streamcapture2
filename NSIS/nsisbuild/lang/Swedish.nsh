﻿
; Language ID
; Swedish (Sweden) 1053
; Swedish (Finland) 2077

LangString UNINST_EXCLUDE_ERROR 1053 "Det gick inte att skapa en undantagslista."
LangString UNINST_DATA_ERROR 1053 "Fel vid skapande av avinstallationsdata: $\r$\Kan inte hitta en undantagslista."
LangString UNINST_DAT_NOT_FOUND 1053 "$UNINST_DAT hittades inte, det går inte att utföra avinstallation. Radera filerna manuellt."
LangString UNINST_DAT_MISSING 1053 "$UNINST_DAT saknas, vissa element kunde inte tas bort. Dessa kan tas bort manuellt."
LangString UNINST_DEL_FILE 1053 "Radera fil"
LangString UNINSTALL 1053 "Avinstallera"
LangString NEXT_CD 1053 "Vänligen sätt i skivan märkt"
LangString CD_NOT_FOUND 1053 "Kan inte hitta filen$\r$\nSätt i rätt disk."
LangString EXTRACT 1053 "Packar upp:"
LangString REMOVEALL 1053 "Ta bort alla filer och mappar från '$INSTDIR'?"
LangString UNCOMPLATE 1053 "Avinstallationen av $(^Name) är utförd."


LangString DESC_Sec1 1053 "Installera streamCapture2 och alla nödvändiga biblioteksfiler samt 7za.exe. streamCapture2 är ett program för att spara strömmande video till din dator."

LangString DESC_Sec2 1053 "FFmpeg är ett gratis och öppen källkodsprogram som består av en svit med bibliotek och program för att hantera video, ljud och andra multimediafiler och strömmar."

LangString DESC_Sec3 1053 "Qt är en plattformsoberoende programvara för att skapa grafiska användargränssnitt såväl som plattformsoberoende applikationer."

LangString INST_TYPE_FULL 1053 "Komplett"
LangString INST_TYPE_MINIMAL 1053 "Minimal"
;LangString INST_TYPE_CUSTOM 1053 "Valfri"