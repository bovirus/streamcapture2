﻿
;General
Unicode true
!include MUI2.nsh

!include "lang\English.nsh"
!include "lang\Italian.nsh"
!include "lang\Swedish.nsh"
 
  ;Name and file
 
  !define VERSION "2.5.0"
  !define PUBLICER "Ingemar Ceicer"
   Name "streamCapture2-${VERSION}"
  !define OUTFILE "streamCapture2-${VERSION}_installer.exe"
  OutFile ${OUTFILE}
 
  ;Use solid LZMA compression
  ;SetCompressor /SOLID lzma
 

 
  ;Default installation folder
  InstallDir "$PROGRAMFILES64\$(^Name)"
 
  ;Get installation folder from registry if available
  InstallDirRegKey HKCU "Software\$(^Name)" ""
 
  ;Define uninstaller name
  !define UninstName "uninstall"

  
 
;--------------------------------
;Header Files
 

  ;!include "UnInst.nsh"
  ;!nclude "zipdll.nsh"
  

  
;--------------------------------
;Variables
 
  Var StartMenuFolder

 
;--------------------------------
; DetailsButtonText 
VIProductVersion "${VERSION}.0"

VIAddVersionKey "ProductVersion" "${VERSION}.0"
VIFileVersion "${VERSION}.0"
VIAddVersionKey "FileVersion" "${VERSION}.0"

VIAddVersionKey "ProductName" "streamCapture2"

VIAddVersionKey "CompanyName" "${PUBLICER}"
VIAddVersionKey "LegalCopyright" "© ${PUBLICER}"
VIAddVersionKey "FileDescription" "Offline installer for streamCapure2"

VIAddVersionKey "InternalName" "streamCapture2"
VIAddVersionKey "OriginalFilename" ${OUTFILE}


;--------------------------------
;Interface Settings

  !define MUI_ICON "icon.ico"
  !define MUI_UNICON "unicon.ico"
  

 
  !define MUI_ABORTWARNING
  ;!define MUI_FINISHPAGE_NOAUTOCLOSE
 
  ;!define MUI_WELCOMEFINISHPAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Wizard\win.bmp"
  ;!define MUI_UNWELCOMEFINISHPAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Wizard\win.bmp"
  

  
  !define MUI_WELCOMEFINISHPAGE_BITMAP "streamcapture2.bmp"
  !define MUI_UNWELCOMEFINISHPAGE_BITMAP "streamcapture2.bmp"
  
  

 
  !define MUI_HEADERIMAGE
  !define MUI_HEADERIMAGE_RIGHT
  ;!define MUI_HEADERIMAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Header\nsis.bmp"
  
  !define MUI_HEADERIMAGE_BITMAP "streamcapture2.bmp"
 
  !define MUI_FINISHPAGE_RUN "$INSTDIR\streamcapture2.exe"
  ;!define MUI_FINISHPAGE_SHOWREADME
  ;!define MUI_FINISHPAGE_SHOWREADME_TEXT "Create Desktop Shortcut"
  ;!define MUI_FINISHPAGE_SHOWREADME_FUNCTION CreateDesktopShortCut
  ;!define MUI_FINISHPAGE_SHOWREADME_NOTCHECKED
 
  !define MUI_FINISHPAGE_LINK "gitlab.com/posktomten/streamcapture2"
  !define MUI_FINISHPAGE_LINK_LOCATION "https://gitlab.com/posktomten/streamcapture2"
 

   ;InstType "Full"
   ;InstType "Minimal"
   ;InstType "Custom"
   InstType $(INST_TYPE_FULL)
   InstType $(INST_TYPE_MINIMAL)
   ;InstType $(INST_TYPE_CUSTOM)
   
  ShowInstDetails show
  ShowUninstDetails show

;--------------------------------
;Language Selection Dialog Settings


;@INSERT_TRANSLATIONS@

 
  ;Remember the installer language and select it by default
  ;(should be set before installation page)
  !define MUI_LANGDLL_REGISTRY_ROOT "HKCU" 
  !define MUI_LANGDLL_REGISTRY_KEY "Software\$(^Name)" 
  !define MUI_LANGDLL_REGISTRY_VALUENAME "Installer Language"
 
  ;Always show the language selection dialog (override the stored value)
  !define MUI_LANGDLL_ALWAYSSHOW
 
  ;Don't filter languages according to their codepage
  ;!define MUI_LANGDLL_ALLLANGUAGES
 
;--------------------------------
;Macros
 
!macro MUI_FINISHPAGE_SHORTCUT
 
  !ifndef MUI_FINISHPAGE_NOREBOOTSUPPORT
    !define MUI_FINISHPAGE_NOREBOOTSUPPORT
    !ifdef MUI_FINISHPAGE_RUN
      !undef MUI_FINISHPAGE_RUN
    !endif
  !endif
  !define MUI_PAGE_CUSTOMFUNCTION_SHOW DisableCancelButton
  !insertmacro MUI_PAGE_FINISH
  !define MUI_PAGE_CUSTOMFUNCTION_SHOW DisableBackButton
 
  Function DisableCancelButton
 
    EnableWindow $mui.Button.Cancel 0
 
  FunctionEnd
 
  Function DisableBackButton
 
    EnableWindow $mui.Button.Back 0
 
  FunctionEnd
 
!macroend
 
!macro NextCD Label IDFile
 
  IfFileExists "${IDFile}" +7
  MessageBox MB_OK|MB_ICONINFORMATION "$(NextCD) ${Label}..."
 
  IfFileExists "${IDFile}" +5
  Sleep 1000
 
  StrCpy $0 "${IDFile}"
  MessageBox MB_RETRYCANCEL|MB_ICONEXCLAMATION "$(CDNotFound)" IDRETRY -3
  Quit
 
!macroend
 
!include "FileFunc.nsh"
 
!macro Extract7z Label Archive Part
 
  !insertmacro NextCD "${Label}" "${Archive}"
  ${GetFileName} "${Archive}" $0
  DetailPrint "$(Extract) $0... ${Part}"
  Nsis7z::ExtractWithDetails "${Archive}" "$(Extract) $0... %s"
 
!macroend
 
!macro SHORTCUTS Name File Icon
 
  !if "${Name}" == ""
    !undef Name
    !define Name "$(^Name)"
  !endif
  !ifdef UninstName
    StrCpy $1 "${UninstName}.exe"
  !else
    StrCpy $1 "uninstall.exe"
  !endif
 
  ;Create uninstaller
  WriteUninstaller "$OUTDIR\$1"
 
  ;Use "All Users" shell folder
  SetShellVarContext all
 
  ;Get Start Menu Folder from registry if available
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
 
  ;Create shortcuts
  
  CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
  CreateShortCut "$SMPROGRAMS\$StartMenuFolder\${Name}.lnk" "$OUTDIR\${File}" "" "$INSTDIR\icon.ico"
 
  ;CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk" "$OUTDIR\$1" "" "$INSTDIR\unicon.ico"
  

 
  ;Store Start Menu Folder in registry
  !insertmacro MUI_STARTMENU_WRITE_END
  
 
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "DisplayName" "${Name} (remove only)"
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "DisplayIcon" "$INSTDIR\icon.ico"
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "UninstallString" "$OUTDIR\$1"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "Publisher" "${PUBLICER}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "DisplayName" "streamCapture2 (64-bit)"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "DisplayVersion" "${VERSION}"
 
!macroend
 
;--------------------------------
;Pages
 
  !insertmacro MUI_PAGE_WELCOME
 
  !insertmacro MUI_PAGE_LICENSE "Licenses\license_streamCapture2.txt"
  !insertmacro MUI_PAGE_LICENSE "Licenses\license_svtplay-dl.txt"
  !insertmacro MUI_PAGE_LICENSE "Licenses\license_ffmpeg.txt"
  !insertmacro MUI_PAGE_LICENSE "Licenses\license_qt.txt"
  !insertmacro MUI_PAGE_LICENSE "Licenses\license_7zip.txt"
  
  
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
 
  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\$(^Name)"
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
 
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
 
  !insertmacro MUI_PAGE_INSTFILES
 
  ;Create desktop shortcut before reboot
  ;!insertmacro MUI_FINISHPAGE_SHORTCUT
 
  !insertmacro MUI_PAGE_FINISH
 
  ;Uninstaller pages
  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH
 
;--------------------------------
;Language Include
 
  !insertmacro MUI_LANGUAGE "English"
  !insertmacro MUI_LANGUAGE "Italian"
  !insertmacro MUI_LANGUAGE "Swedish"

 
;--------------------------------
;Reserve Files
 
  ;If you are using solid compression this will make installer start faster
  ;!insertmacro MUI_RESERVEFILE_LANGDLL
  

 
;--------------------------------
;Language Strings
 
  !define UNINST_LOCALIZE
  ; In language files

  
 
;--------------------------------
;Installer Sections
 
Section "streamCapture2" Sec1
	SectionIn 1 2 RO	
 
  ;Set selected in "Full" and "Minimal" install types (see InstType) and 
  ;Make unavailable to change section state
  ;SectionIn 1 2 RO
 
  ;Set output path ($OUTDIR) and create it recursively if necessary
  SetOutPath "$INSTDIR"
  file "icon.ico"
  ;Create an exclusion list (UnInst.nsh)
  ;!insertmacro UNINSTALLER_DATA_BEGIN
 
  ;Add/Install Files
	SetOutPath "$INSTDIR"
	file "icon.ico"
	file "7za.exe"
	file "streamcapture2.exe"
	file "selectfont.dll"
	file "downloadunpack.dll"
	file "checkupdate.dll"


	SetOutPath "$INSTDIR\Licenses"
	file "Licenses\license_7zip.txt"
	file "Licenses\license_ffmpeg.txt"
	file "Licenses\license_qt.txt"
	file "Licenses\license_streamCapture2.txt"
	file "Licenses\license_svtplay-dl.txt"


	SetOutPath "$INSTDIR"
  ;File /r App\*.*
 
  ;Use nsis7z plug-in (nsis7z.dll) to extract 7-zip archive
  ;(unicode version is supported in MakeNSIS v2.50 or later)
  ;for multi-volume distribution sort your files and archive them separately
  ;!insertmacro Extract7z "CD 1" "$EXEDIR\archive1.a2b" "(1/2)"
  ;!insertmacro Extract7z "CD 2" "$EXEDIR\archive2.a2b" "(2/2)"
 
  ;Use ZipDLL plug-in (ZipDLL.dll) to extract zip-archive
  ;!insertmacro NextCD "CD 3" "$EXEDIR\archive3.a2b"
  ;ZipDLL::extractall "$EXEDIR\archive3.a2b" "$OUTDIR"
 
  ;Localize ZipDLL and extract zip-archive (zipdll.nsh)
  ;!insertmacro ZIPDLL_EXTRACT "$EXEDIR\archive3.zip" "$OUTDIR" "<ALL>"
 
  ;Require additional disk space to extract the archives (size_kb)
  ;AddSize 1000000
 
  ;Store uninstaller data (UnInst.nsh)
  ;!insertmacro UNINSTALLER_DATA_END
 
  ;Get section name
  SectionGetText ${Sec1} $0
 
  ;Write uninstaller and create shortcuts
  !insertmacro SHORTCUTS "$0" "streamCapture2.exe" "unicon.ico"
 
  ;Extract Temporary files
  ;SetOutPath "$PLUGINSDIR\Resource"
  ;File Tmp\*.*
 
  ;Execute commands
  ;ExecWait 'msiexec /i PhysX-9.13.0725-SystemSoftware.msi /qb'
  ;ExecWait 'vcredist_x86_5069.exe /q:a /c:"msiexec /i vcredist.msi /qb"'
 
  ;SetRebootFlag true
 ;!insertmacro SHORTCUTS "" "streamcapture2.exe" "icon.ico"
 SectionEnd

 Section "FFmpeg" Sec2
  SectionIn  1
  SetOutPath "$INSTDIR"
file "ffmpeg.exe"
 
SectionEnd

Section "Qt" Sec3

	SectionIn  1
	SetOutPath "$INSTDIR"

	file "D3Dcompiler_47.dll"
	file "libgcc_s_seh-1.dll"
	file "libstdc++-6.dll"
	file "libwinpthread-1.dll"
	file "opengl32sw.dll"
	file "Qt6Core.dll"
	file "Qt6Gui.dll"
	file "Qt6Network.dll"
	file "Qt6Svg.dll"
	file "Qt6Widgets.dll"

	SetOutPath "$INSTDIR\iconengines"
	file "iconengines\qsvgicon.dll"

	SetOutPath "$INSTDIR\imageformats"
	file "imageformats\qgif.dll"
	file "imageformats\qicns.dll"
	file "imageformats\qico.dll"
	file "imageformats\qjpeg.dll"
	file "imageformats\qsvg.dll"
	file "imageformats\qtga.dll"
	file "imageformats\qtiff.dll"
	file "imageformats\qwbmp.dll"
	file "imageformats\qwebp.dll"

	SetOutPath "$INSTDIR\platforms"
	file "platforms\qwindows.dll"

	SetOutPath "$INSTDIR\styles"
	file "styles\qwindowsvistastyle.dll"

	SetOutPath "$INSTDIR\tls"
	file "tls\qcertonlybackend.dll"
	file "tls\qopensslbackend.dll"
	file "tls\qschannelbackend.dll"

SectionEnd

 
;Another section in the group
;SectionGroup /e "Components" SecGrp1
;Section "Basic" Sec1
;!insertmacro SHORTCUTS "" "streamcapture2.exe" "icon.ico"
;  SetOutPath "$INSTDIR\2"
 ; File Basic.nsi
;SectionEnd
;SectionGroupEnd
 
;--------------------------------
;Descriptions
 
  ;Language strings
  ; In language files

  
  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${Sec1} $(DESC_Sec1)
    !insertmacro MUI_DESCRIPTION_TEXT ${Sec2} $(DESC_Sec2)
	!insertmacro MUI_DESCRIPTION_TEXT ${Sec3} $(DESC_Sec3)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END
 
;--------------------------------
;Uninstaller Section
 
Section "Uninstall"
 
  ;Require confirmation to delete every other file except installed (UnInst.nsh)
  ;!define UNINST_INTERACTIVE
 
  ;Terminate uninstaller if the .dat file does not exist (UnInst.nsh)
  ;!define UNINST_TERMINATE
 
  ;Use generated header to delete files
  ;!include "UnFiles.nsh"
 
  ;Get installation folder from registry if available
  ;ReadRegStr $INSTDIR HKCU "Software\$(^Name)" ""
  ;StrCmp $INSTDIR "" 0 +2
  ;StrCpy $INSTDIR $OUTDIR
 
  ;Remove files with uninstaller data (UnInst.nsh)
  ;Note that, the $INSTDIR will be equal to uninstaller directory in this section
  ;StrCpy $INSTDIR "$PROGRAMFILES\$(^Name)"
  ;!insertmacro UNINST_DELETE "$INSTDIR" "${UninstName}"
  ;!insertmacro UNINST_DELETE "$INSTDIR\..\2" "${UninstName}"
 
  ;Delete files
  Delete "$INSTDIR\streamCapture2.nsi"
  Delete "$INSTDIR\*.*"
  Delete "$INSTDIR\iconengines\*.*"
  Delete "$INSTDIR\imageformats\*.*"
  Delete "$INSTDIR\platforms\*.*"
  Delete "$INSTDIR\styles\*.*"
  Delete "$INSTDIR\tls\*.*"
  Delete "$INSTDIR\Licenses\*.*"
 
  ;Remove installation folder
  RMDir "$INSTDIR\iconengines"
  RMDir "$INSTDIR\imageformats"
  RMDir "$INSTDIR\platforms"
  RMDir "$INSTDIR\styles"
  RMDir "$INSTDIR\tls"
  RMDir "$INSTDIR\Licenses"
  Delete "$INSTDIR\${UninstName}.exe"
  RMDir "$INSTDIR"
  ;IfErrors 0 +3
  ;MessageBox MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON1 "$(REMOVEALL)" /SD IDNO IDNO +2
  ;RMDir /r /REBOOTOK "$INSTDIR"
 
  ;Use "All Users" shell folder
  SetShellVarContext all
 
  ;Get Start Menu Folder
  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
 
  ;Delete shortcuts
  Delete "$SMPROGRAMS\$StartMenuFolder\$(^Name).lnk"
  Delete "$SMPROGRAMS\$StartMenuFolder\$(uninstall).lnk"
  RMDir "$SMPROGRAMS\$StartMenuFolder"
  RMDir /r "$SMPROGRAMS\$StartMenuFolder"
 
  Delete "$DESKTOP\$(^Name).lnk"
 
  ;Remove from control panel programs list
  DeleteRegKey HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\uninstall\streamCapture2"
 
  ;Remove Start Menu Folder, language and installation folder from registry
  DeleteRegKey /ifempty HKCU "Software\$(^Name)"
 
SectionEnd
 
;--------------------------------
;Installer Functions
 
Function .onInit
 
  ;Display language selection dialog
  !insertmacro MUI_LANGDLL_DISPLAY
 
FunctionEnd
 
Function .onGUIEnd
 
  ;Store installation folder in registry
  WriteRegStr HKCU "Software\$(^Name)" "" $INSTDIR
 
  ;Escape from $PLUGINSDIR to completely remove it
  SetOutPath "$INSTDIR"

FunctionEnd
 
;Function CreateDesktopShortCut
 
;  CreateShortCut "$DESKTOP\$(^Name).lnk" "$INSTDIR\streamcapture2.exe" "" "$INSTDIR\icon.ico"
 
;FunctionEnd
 
;--------------------------------
;Uninstaller Functions
 
Function un.onInit
 
  ;Get stored language preference
  !insertmacro MUI_UNGETLANGUAGE
 
FunctionEnd
 
Function un.onUninstSuccess
 
  MessageBox MB_OK|MB_ICONINFORMATION "$(UNCOMPLATE)" /SD IDOK
 
FunctionEnd
