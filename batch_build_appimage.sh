#!/bin/bash

echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("Qt 5.15.2 64-bit" "Qt 5.15.4 64-bit" "Qt 6.3.0 64-bit" "Qt 6.3.0 64-bit zsync2" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Qt 5.15.2 64-bit")
            APPDIR="AppDir"
            APPDIR_FFMPEG="AppDir_ffmpeg"
            echo "You chose choice $REPLY which is $opt"
            export PATH=/opt/Qt/5.15.2/gcc_64/bin:$PATH
            qmakeexecutable="/opt/Qt/5.15.2/gcc_64/bin/qmake"
            HIGHGLIBC=0
            break;
            ;;
        "Qt 5.15.4 64-bit")
            APPDIR="AppDir"
            APPDIR_FFMPEG="AppDir_ffmpeg"
            echo "You chose choice $REPLY which is $opt"
            export PATH=/opt/Qt/Qt5.15.4/bin:$PATH
            qmakeexecutable="/opt/Qt/Qt5.15.4/bin/qmake"
            HIGHGLIBC=0
            break;
            ;;
        "Qt 6.3.0 64-bit")
            APPDIR="AppDir"
            APPDIR_FFMPEG="AppDir_ffmpeg"
            echo "You chose choice $REPLY which is $opt"
            qmakeexecutable="/opt/Qt/6.3.0/gcc_64/bin/qmake"
            unsuported="-extra-plugins=${APPDIR}/usr/plugins/platforms/libqxcb.so -unsupported-allow-new-glibc"
            unsuported_ffmpeg="-extra-plugins=${APPDIR_FFMPEG}/usr/plugins/platforms/libqxcb.so -unsupported-allow-new-glibc"
            export LD_LIBRARY_PATH=/opt/Qt/6.3.0/gcc_64/lib:$LD_LIBRARY_PATH
            export PATH=/opt/Qt/6.3.0/gcc_64/bin:$PATH
            HIGHGLIBC=1
            break;
            ;;
            
        "Qt 6.3.0 64-bit zsync2")
            APPDIR="AppDir2"
            APPDIR_FFMPEG="AppDir_ffmpeg2"
            echo "You chose choice $REPLY which is $opt"
            qmakeexecutable="/opt/Qt/6.3.0/gcc_64/bin/qmake"
            unsuported="-extra-plugins=${APPDIR}/usr/plugins/platforms/libqxcb.so -unsupported-allow-new-glibc"
            unsuported_ffmpeg="-extra-plugins=${APPDIR_FFMPEG}/usr/plugins/platforms/libqxcb.so -unsupported-allow-new-glibc"
            export LD_LIBRARY_PATH=/opt/Qt/6.3.0/gcc_64/lib:$LD_LIBRARY_PATH
            export PATH=/opt/Qt/6.3.0/gcc_64/bin:$PATH
            HIGHGLIBC=10
            break;
            ;;
           
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("Sign AppImage" "Do not sign AppImage" "Quit")
select opt in "${options[@]}"
do


	case $opt in
		"Sign AppImage")
			echo "You chose choice $REPLY which is $opt"
			signera=1
			break;
			;;
		"Do not sign AppImage")
			echo "You chose choice $REPLY which is $opt"
			signera=0  
			break;
			;;
		"Quit")
			break
			;;
		*) echo "invalid option $REPLY";;
	esac
done
echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("AppImage" "AppImage ffmpeg" "AppImage and AppImage ffmpeg" "Quit")
select opt in "${options[@]}"
do

    case $opt in
        "AppImage")
            echo "You chose choice $REPLY which is $opt"
            linuxdeployqt ${APPDIR}/usr/bin/streamcapture2 -qmake="$qmakeexecutable" -bundle-non-qt-libs -verbose=3 -no-translations -no-copy-copyright-files -always-overwrite -executable=${APPDIR}/usr/bin/zsync $unsuported
            
            if [ "$signera" -gt 0 ] ;
            then
				appimagetool --sign ${APPDIR}
			else
				appimagetool ${APPDIR}
			fi
           
            break;
            ;;
        "AppImage ffmpeg")
            echo "You chose choice $REPLY which is $opt"
            linuxdeployqt ${APPDIR_FFMPEG}/usr/bin/streamcapture2 -qmake="$qmakeexecutable" -bundle-non-qt-libs -verbose=3 -no-translations -no-copy-copyright-files -always-overwrite -executable=${APPDIR_FFMPEG}/usr/bin/zsync $unsuported_ffmpeg
            
            if [ "$signera" -gt 0 ] ;
            then
				appimagetool --sign ${APPDIR_FFMPEG}
			else
				appimagetool ${APPDIR_FFMPEG}
			fi
            
            break;
            ;;
        "AppImage and AppImage ffmpeg")
            echo "You chose choice $REPLY which is $opt"
            linuxdeployqt ${APPDIR}/usr/bin/streamcapture2 -qmake="$qmakeexecutable" -bundle-non-qt-libs -verbose=3 -no-translations -no-copy-copyright-files -always-overwrite -executable=${APPDIR}/usr/bin/zsync $unsuported
            linuxdeployqt ${APPDIR_FFMPEG}/usr/bin/streamcapture2 -qmake="$qmakeexecutable" -bundle-non-qt-libs -verbose=3 -no-translations -no-copy-copyright-files -always-overwrite -executable=${APPDIR_FFMPEG}/usr/bin/zsync $unsuported_ffmpeg
            #/opt/Qt/6.3.0/gcc_64/plugins/platforms
            if [ "$signera" -gt 0 ] ;
            then
                appimagetool --sign ${APPDIR}
				appimagetool --sign ${APPDIR_FFMPEG}
			else
			    appimagetool ${APPDIR}
				appimagetool ${APPDIR_FFMPEG}
			fi
            break;
            ;;
            
        "Quit")
        echo "Goodbye"
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done

echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("Upload AppImage" "Upload AppImage later" "Quit")
select opt in "${options[@]}"
do


case $opt in
    "Upload AppImage")
        echo "You chose choice $REPLY which is $opt"

if [ "$HIGHGLIBC" -eq 10 ] ;
 then
   cp -f *.AppImage streamcapture2-zsyncmake2/
else 
   cp -f *.AppImage streamcapture2-zsyncmake/
fi

if [ "$HIGHGLIBC" -eq 1 ] ;
 then
   
	ZSYNC_ADDRESS="http://bin.ceicer.com/zsync/HIGH_GLIBC"
	ZSYNC_DIR="/bin.ceicer.com/public_html/zsync/HIGH_GLIBC"
	BIN_DIR="/bin.ceicer.com/public_html/streamcapture2/bin/HIGH_GLIBC"
	
   
 elif [ "$HIGHGLIBC" -eq 0 ] ;
  then
   
   	ZSYNC_ADDRESS="http://bin.ceicer.com/zsync"
   	ZSYNC_DIR="/bin.ceicer.com/public_html/zsync"
	BIN_DIR="/bin.ceicer.com/public_html/streamcapture2/bin"

 
 else 

   	ZSYNC_ADDRESS="https://bin.ceicer.com/zsync2/HIGH_GLIBC"
	ZSYNC_DIR="/bin.ceicer.com/public_html/zsync2/HIGH_GLIBC"
	BIN_DIR="/bin.ceicer.com/public_html/streamcapture2/bin/beta/HIGH_GLIBC/zsync2"
  
fi


if [ "$HIGHGLIBC" -eq 10 ] ;
 then
    cd streamcapture2-zsyncmake2
else 
    cd streamcapture2-zsyncmake
fi


date_now=$(date "+%FT%H-%M-%S")

for i in *.AppImage; do # Whitespace-safe but not recursive.

   
    ./zsyncmake "$i" -u "${ZSYNC_ADDRESS}/$i" 
    echo "--------------------------------------------------" > "$i-MD5.txt"
	echo "MD5 HASH SUM" >> "$i-MD5.txt"
	md5sum "$i" >> "$i-MD5.txt"
	echo "Created $date_now" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "HOW TO CHECK THE HASH SUM" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "LINUX" >> "$i-MD5.txt"
	echo "\"md5sum\" is included in most Linus distributions." >> "$i-MD5.txt"
	echo "md5sum $i" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "WINDOWS" >> "$i-MD5.txt"
	echo "\"certutil\" is included in Windows." >> "$i-MD5.txt"
	echo "certutil -hashfile $i md5" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "READ MORE" >> "$i-MD5.txt"
	echo "https://gitlab.com/posktomten" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
    
    
done



HOST="`sed -n 1p ../../secret`"
USER="`sed -n 2p ../../secret`"
PASSWORD="`sed -n 3p ../../secret`"

echo "$BIN_DIR"

echo "$ZSYNC_DIR"



for fil in *.AppImage; do # Whitespace-safe but not recursive.

ftp -v -inv $HOST <<EOF
user $USER $PASSWORD
passive
cd "$BIN_DIR"
put $fil
cd "$ZSYNC_DIR"
put $fil
bye
EOF

done



for fil in *.zsync; do # Whitespace-safe but not recursive.

ftp -v -inv $HOST <<EOF
user $USER $PASSWORD
passive
cd "$ZSYNC_DIR"
put $fil
bye
EOF

done

for fil in *-MD5.txt; do # Whitespace-safe but not recursive.

ftp -v -inv $HOST <<EOF
user $USER $PASSWORD
passive
cd "$BIN_DIR"
put $fil
bye
EOF

done
break
;;
			
  "Upload AppImage later")
		##
echo "You chose choice $REPLY which is $opt"

if [ "$HIGHGLIBC" -eq 10 ] ;
 then
   cp -f *.AppImage streamcapture2-zsyncmake2/
else 
   cp -f *.AppImage streamcapture2-zsyncmake/
fi

#HIGHGLIBC=0

if [ "$HIGHGLIBC" -eq 1 ] ;
 then
   
	ZSYNC_ADDRESS="http://bin.ceicer.com/zsync/HIGH_GLIBC"
	ZSYNC_DIR="/bin.ceicer.com/public_html/zsync/HIGH_GLIBC"
	BIN_DIR="/bin.ceicer.com/public_html/streamcapture2/bin/HIGH_GLIBC"
	
   
 elif [ "$HIGHGLIBC" -eq 0 ] ;
  then
   
   	ZSYNC_ADDRESS="http://bin.ceicer.com/zsync"
   	ZSYNC_DIR="/bin.ceicer.com/public_html/zsync"
	BIN_DIR="/bin.ceicer.com/public_html/streamcapture2/bin"

 
 else 

   	ZSYNC_ADDRESS="https://bin.ceicer.com/zsync2/HIGH_GLIBC"
	ZSYNC_DIR="/bin.ceicer.com/public_html/zsync2/HIGH_GLIBC"
	BIN_DIR="/bin.ceicer.com/public_html/streamcapture2/bin/beta/HIGH_GLIBC/zsync2"
  
fi

if [ "$HIGHGLIBC" -eq 10 ] ;
 then
    cd streamcapture2-zsyncmake2
else 
    cd streamcapture2-zsyncmake
fi


date_now=$(date "+%FT%H-%M-%S")

for i in *.AppImage; do # Whitespace-safe but not recursive.

    ./zsyncmake "$i" -u "${ZSYNC_ADDRESS}/$i" 
    
    
    echo "--------------------------------------------------" > "$i-MD5.txt"
	echo "MD5 HASH SUM" >> "$i-MD5.txt"
	md5sum "$i" >> "$i-MD5.txt"
	echo "Created $date_now" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "HOW TO CHECK THE HASH SUM" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "LINUX" >> "$i-MD5.txt"
	echo "\"md5sum\" is included in most Linus distributions." >> "$i-MD5.txt"
	echo "md5sum $i" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "WINDOWS" >> "$i-MD5.txt"
	echo "\"certutil\" is included in Windows." >> "$i-MD5.txt"
	echo "certutil -hashfile $i md5" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "READ MORE" >> "$i-MD5.txt"
	echo "https://gitlab.com/posktomten" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
    
    
done
		##
			break
			;;

		"Quit")
		echo "Goodbye"
			break
			;;
		*) echo "invalid option $REPLY";;
	esac
done



