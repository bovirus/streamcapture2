// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>


#include "newprg.h"
void Newprg::english()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString sp = settings.value("language", "").toString();
    settings.endGroup();

    if(sp != "en_US") {
        QMessageBox *msgBox = new QMessageBox(this);
        QAbstractButton *cancelButton = msgBox->addButton(tr("Cancel"), QMessageBox::RejectRole);
        QAbstractButton *restartButton = msgBox->addButton(tr("Restart Now"), QMessageBox::YesRole);
        msgBox->setDefaultButton(msgBox->findChildren<QPushButton *>().first());
        msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox->setText(tr("The program must be restarted for the new language settings to take effect."));
        msgBox->setStyleSheet("QLabel{min-width:500 px;} QPushButton{ width:250px;}");
        msgBox->exec();

        if(msgBox->clickedButton() == restartButton) {
            settings.beginGroup("Language");
            settings.setValue("language", "en_US");
            settings.endGroup();
            settings.sync();
            const QString EXECUTE = QDir::toNativeSeparators(
                                        QCoreApplication::applicationDirPath() + "/" +
                                        QFileInfo(QCoreApplication::applicationFilePath()).fileName());
            QProcess p;
            p.setProgram(EXECUTE);
            p.startDetached();
            close();
        } else if(msgBox->clickedButton() == cancelButton) {
            delete msgBox;
            return;
        }
    }
}

void Newprg::swedish()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString sp = settings.value("language", "").toString();
    settings.endGroup();

    if(sp != "sv_SE") {
        sp = "sv_SE";
        QMessageBox *msgBox = new QMessageBox(this);
        QAbstractButton *cancelButton = msgBox->addButton(tr("Cancel"), QMessageBox::RejectRole);
        QAbstractButton *restartButton = msgBox->addButton(tr("Restart Now"), QMessageBox::YesRole);
        msgBox->setDefaultButton(msgBox->findChildren<QPushButton *>().first());
        msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox->setText(tr("The program must be restarted for the new language settings to take effect."));
        msgBox->setStyleSheet("QLabel{min-width:500 px;} QPushButton{ width:250px;}");
        msgBox->exec();

        if(msgBox->clickedButton() == restartButton) {
            settings.beginGroup("Language");
            settings.setValue("language", sp);
            settings.endGroup();
            settings.sync();
            const QString EXECUTE = QDir::toNativeSeparators(
                                        QCoreApplication::applicationDirPath() + "/" +
                                        QFileInfo(QCoreApplication::applicationFilePath()).fileName());
            QProcess p;
            p.setProgram(EXECUTE);
            p.startDetached();
            close();
        } else if(msgBox->clickedButton() == cancelButton) {
            delete msgBox;
            return;
        }
    }
}

void Newprg::italian()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString sp = settings.value("language", "none").toString();
    settings.endGroup();

    if(sp != "it_IT") {
        sp = "it_IT";
        QMessageBox *msgBox = new QMessageBox(this);
        QAbstractButton *cancelButton = msgBox->addButton(tr("Cancel"), QMessageBox::RejectRole);
        QAbstractButton *restartButton = msgBox->addButton(tr("Restart Now"), QMessageBox::YesRole);
        msgBox->setDefaultButton(msgBox->findChildren<QPushButton *>().first());
        msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox->setText(tr("The program must be restarted for the new language settings to take effect."));
        msgBox->setStyleSheet("QLabel{min-width:500 px;} QPushButton{ width:250px;}");
        msgBox->exec();

        if(msgBox->clickedButton() == restartButton) {
            settings.beginGroup("Language");
            settings.setValue("language", sp);
            settings.endGroup();
            settings.sync();
            const QString EXECUTE = QDir::toNativeSeparators(
                                        QCoreApplication::applicationDirPath() + "/" +
                                        QFileInfo(QCoreApplication::applicationFilePath()).fileName());
            QProcess p;
            p.setProgram(EXECUTE);
            p.startDetached();
            close();
        } else if(msgBox->clickedButton() == cancelButton) {
            delete msgBox;
            return;
        }
    }
}
