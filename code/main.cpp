// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"

int main(int argc, char *argv[])
{
    QString currentLocale(QLocale::system().name());
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    // cppcheck sp -> sprak
    QString sprak = settings.value("language", currentLocale).toString();
    settings.endGroup();
    QApplication app(argc, argv);
#ifdef QT_STATIC
    Q_INIT_RESOURCE(resurs_downloadunpack);
    Q_INIT_RESOURCE(resurs_libselectfont);
#endif
    /*   */
    QLocale locale(sprak);
    const QString qttranslationsPath(":/i18n/qttranslations");
    QTranslator qtTranslator;

    if(qtTranslator.load(locale, "qt", "_", qttranslationsPath)) {
        QApplication::installTranslator(&qtTranslator);
    }

    QTranslator qtBaseTranslator;

    if(qtBaseTranslator.load(locale, "qtbase", "_", qttranslationsPath)) {
        QApplication::installTranslator(&qtBaseTranslator);
    }

    /*  */
    QString fontPath = ":/fonts/Ubuntu-R.ttf";
    int fontId = QFontDatabase::addApplicationFont(fontPath);

    if(fontId != -1) {
        QFont f("Ubuntu", FONT);
        QApplication::setFont(f);
    }

//        qDebug() << "BRA";
//    }
//    } else {
//        qDebug() << "KASS";
//    }
    QTranslator translator;
    settings.beginGroup("Language");
    QString testlanguage = settings.value("testlanguage").toString();
    QString language = settings.value("language", "").toString();
    settings.endGroup();
    QFileInfo fi(testlanguage);

    if((language.isEmpty()) && (fi.exists())) {
        if(translator.load(testlanguage)) {
            QApplication::installTranslator(&translator);
        }
    }
// END Test translation
    else {
        // ELSE
        settings.beginGroup("Language");
        QString sp = settings.value("language", "").toString();
        settings.endGroup();
        const QString translationPath = ":/i18n/_complete";
//        const QString built_inTranslation = ":/i18n";

        if(sp == "") {
            sp = QLocale::system().name();

            if(translator.load(translationPath + "_" + sp + ".qm")) {
                settings.beginGroup("Language");
                settings.setValue("language", sp);
                settings.endGroup();
                /*  */
                /*  */
                QApplication::installTranslator(&translator);
            } else if(sp != "en_US") {
                QMessageBox::information(
                    nullptr, DISPLAY_NAME " " VERSION,
                    DISPLAY_NAME
                    " is unfortunately not yet translated into your language, the "
                    "program will start with English menus.");
                settings.beginGroup("Language");
                settings.setValue("language", "en_US");
                settings.endGroup();
            }
        } else {
            if(translator.load(translationPath + "_" + sp + ".qm")) {
                QApplication::installTranslator(&translator);
            }
        }

        // END ELSE Test translation
    }

    Newprg *mNewPrg = new Newprg;
// End config
    QObject::connect(&app, &QCoreApplication::aboutToQuit,
                     [mNewPrg]() -> void { mNewPrg->setEndConfig(); });
    mNewPrg->show();
    int r = QApplication::exec();
    return r;
}
