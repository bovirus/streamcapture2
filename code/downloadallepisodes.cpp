// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"

void Newprg::downloadAllEpisodes()
{
    if(!fileExists(svtplaydl)) {
        ui->teOut->setTextColor(QColor("red"));
        ui->teOut->setText(svtplaydl +
                           tr(" cannot be found or is not an executable program."));
        ui->teOut->append(tr("Please click on \"Tools\" and select svtplay-dl."));
        ui->teOut->setTextColor(QColor("black"));
        return;
    }

    if(ui->actionCopyToDefaultLocation->isChecked() && ui->actionCreateFolder->isChecked()) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Information);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("You have chosen to create folders for each file and have the files copied.\nUnfortunately,"
                          " this is not possible as streamCapture2 does not know the file names."));
//        msgBox.addButton(QMessageBox::Ok);
//        msgBox.addButton(QMessageBox::Cancel);
//        msgBox.setButtonText(QMessageBox::Ok, tr("Download anyway"));
//        msgBox.setButtonText(QMessageBox::Cancel, tr("Cancel"));
//        msgBox.setDefaultButton(QMessageBox::Cancel);
        msgBox.addButton(tr("Download anyway"), QMessageBox::AcceptRole);
        QPushButton *but = msgBox.addButton(tr("Cancel"), QMessageBox::RejectRole);
        msgBox.setDefaultButton(but);

        if(msgBox.exec() == QMessageBox::RejectRole) {
            return;
        }
    } else if(ui->actionCopyToDefaultLocation->isChecked()) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Information);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("You have chosen to have the files copied.\nUnfortunately,"
                          " this is not possible as streamCapture2 does not know the file names."));
//        msgBox.addButton(QMessageBox::Ok);
//        msgBox.addButton(QMessageBox::Cancel);
//        msgBox.setButtonText(QMessageBox::Ok, tr("Download anyway"));
//        msgBox.setButtonText(QMessageBox::Cancel, tr("Cancel"));
        msgBox.addButton(tr("Download anyway"), QMessageBox::AcceptRole);
        QPushButton *but = msgBox.addButton(tr("Cancel"), QMessageBox::RejectRole);
        msgBox.setDefaultButton(but);

        if(msgBox.exec() == QMessageBox::RejectRole) {
            return;
        }
    } else if(ui->actionCreateFolder->isChecked()) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Information);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("You have chosen to create folders for each file.\nUnfortunately,"
                          " this is not possible as streamCapture2 does not know the file names."));
//        msgBox.addButton(QMessageBox::Ok);
//        msgBox.addButton(QMessageBox::Cancel);
//        msgBox.setButtonText(QMessageBox::Ok, tr("Download anyway"));
//        msgBox.setButtonText(QMessageBox::Cancel, tr("Cancel"));
//        msgBox.setDefaultButton(QMessageBox::Cancel);
        msgBox.addButton(tr("Download anyway"), QMessageBox::AcceptRole);
        QPushButton *but = msgBox.addButton(tr("Cancel"), QMessageBox::RejectRole);
        msgBox.setDefaultButton(but);

        if(msgBox.exec() == QMessageBox::RejectRole) {
            return;
        }
    }

    QStringList ARG;

    switch(ui->comboResolution->currentIndex()) {
        case 1:
            ARG << "--resolution" << "720";
            break;

        case 2:
            ARG << "--resolution" << "1080";
    }

    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Stcookies");
    QString stcookie = settings.value("stcookie", "").toString();
    settings.endGroup();
    ARG << "--verbose";
    ARG << "--force";
    ARG << "--all-episodes";

    if(ui->chbSubtitle->isChecked()) {
        ARG << "--subtitle";
    }

    if(!stcookie.isEmpty()) {
        ARG << "--cookies"  << "st=" + stcookie;
    }

#ifdef Q_OS_LINUX
    QMessageBox msgBox;
    msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setText(tr("Once svtplay-dl has started downloading all episodes, streamCapture2 "
                      "no longer has control.\n"
                      "If you want to cancel, you may need to log out or restart your "
                      "computer.\nYou can try to cancel using the command\n\"sudo killall python3\"\n\nDo you want to start the download?"));
    QPushButton *but = msgBox.addButton(tr("Yes"), QMessageBox::AcceptRole);
    msgBox.addButton(tr("No"), QMessageBox::RejectRole);
    msgBox.setDefaultButton(but);

    if(msgBox.exec() != QMessageBox::AcceptRole) {
        return;
    }

#endif
    ui->teOut->setTextColor(QColor("black"));
    settings.beginGroup("Path");
    QString defaultdownloadlocation =
        settings.value("savepath").toString();
    settings.endGroup();
    ui->teOut->setReadOnly(true);
    ui->teOut->setText(
        tr("The request is processed...\nPreparing to download..."));
    QString save_path;

    if(ui->actionDownloadToDefaultLocation->isChecked()) {
        /* */
        const QFileInfo downloadlocation(
            QDir::toNativeSeparators(defaultdownloadlocation));

        if(!downloadlocation.exists()) {
            // cppcheck
            QMessageBox msgBox1;
            msgBox1.setIcon(QMessageBox::Critical);
            msgBox1.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox1.setText(tr("The default folder for downloading video streams cannot be "
                               "found.\nDownload is interrupted."));
//            msgBox1.addButton(QMessageBox::Ok);
//            msgBox1.setButtonText(QMessageBox::Ok, tr("Ok"));
            msgBox1.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox1.exec();
            ui->teOut->clear();
            return;
        }

        if(!downloadlocation.isWritable()) {
            QMessageBox msgBox1;
            msgBox1.setIcon(QMessageBox::Critical);
            msgBox1.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox1.setText(tr("You do not have the right to save to the folder.\nDownload is interrupted."));
//            msgBox1.addButton(QMessageBox::Ok);
//            msgBox1.setButtonText(QMessageBox::Ok, tr("Ok"));
            msgBox1.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox1.exec();
            ui->teOut->clear();
            return;
        }

        save_path = defaultdownloadlocation;
    } else {
        save_path = save(tr("Download all episodes to folder"));
    }

    if(save_path == "nothing") {
        ui->teOut->clear();
        return;
    }

#ifdef Q_OS_WIN
    // NTFS
    qt_ntfs_permission_lookup++; // turn checking on
#endif
    QFileInfo fi(save_path);

    if(!fi.isWritable()) {
        QMessageBox msgBox1;
        msgBox1.setIcon(QMessageBox::Critical);
        msgBox1.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox1.setText(tr("You do not have the right to save to the "
                           "folder.\nDownload is interrupted."));
//        msgBox1.addButton(QMessageBox::Ok);
//        msgBox1.setButtonText(QMessageBox::Ok, tr("Ok"));
        msgBox1.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox1.exec();
        ui->teOut->clear();
#ifdef Q_OS_WIN
        qt_ntfs_permission_lookup--; // turn checking of
#endif
        return;
    }

#ifdef Q_OS_WIN
    qt_ntfs_permission_lookup--; // turn checking of
#endif

    if(save_path != QStringLiteral(u"nothing")) {
        /* END CREATE FOLDER */
        CommProcess2 = new QProcess(nullptr);
        // Linux
        // SPARA VÄRDET PÅ "PATH" och konkatenera med det nya värdet
        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        const QString path =
            QDir::toNativeSeparators(QCoreApplication::applicationDirPath());
        const QString oldpath = env.value("PATH");
#ifdef Q_OS_LINUX
        env.insert("PATH", path + ":" + oldpath); // Add an environment variable
#endif
#ifdef Q_OS_WIN                               // QT5
        env.insert("PATH", path + ";" + oldpath); // Add an environment variable
#endif
        CommProcess2->setProcessEnvironment(env);
        //
        CommProcess2->setProcessChannelMode(QProcess::MergedChannels);
        //
        CommProcess2->setWorkingDirectory(save_path);

        if(ui->comboPayTV->currentIndex() != 0)  { // Password
            /* */
// cppcheck
//            QSettings settings(QSettings::IniFormat, QSettings::UserScope,
//                               DISPLAY_NAME, EXECUTABLE_NAME);
//            // settings.setIniCodec("UTF-8");
            QString provider = ui->comboPayTV->currentText();
            settings.beginGroup("Provider");
            QString username = settings.value(provider + "/username").toString();
            QString password = settings.value(provider + "/password").toString();
            settings.endGroup();

            if(password == "") {
                password = secretpassword;

                if(password == "") {
                    bool ok;
                    QString newpassword = QInputDialog::getText(
                                              nullptr, provider + " " + tr("Enter your password"),
                                              tr("Spaces are not allowed. Use only the characters your "
                                                 "streaming provider approves.\nThe Password will not be "
                                                 "saved."),
                                              QLineEdit::Password, "", &ok);

                    if(newpassword.indexOf(' ') >= 0) {
                        return;
                    }

                    if(ok) {
                        secretpassword = newpassword;
                    }
                }
            }

            ARG << "--username" << username << "--password" << password;
        }

        /* */
        ui->teOut->append(tr("Starts downloading all episodes: ") + address);
        connect(CommProcess2, SIGNAL(started()), this, SLOT(onCommProcessStart()));
        connect(CommProcess2, &QProcess::readyReadStandardOutput, [this]() {
            QString result(CommProcess2->readAllStandardOutput());

            if(ui->actionShowMore->isChecked()) {
                ui->teOut->setTextColor(QColor("purple"));
                ui->teOut->append(result);
                ui->teOut->setTextColor(QColor("black"));

                // progressbar
                if(ui->progressBar->value() >= 100) {
                    ui->progressBar->setValue(0);
                }

                ui->progressBar->setValue(ui->progressBar->value() + 10);
            } else {
                // progressbar
                if(ui->progressBar->value() >= 100) {
                    ui->progressBar->setValue(0);
                }

                ui->progressBar->setValue(ui->progressBar->value() + 10);

                if(result.trimmed().contains("Merge audio and video")) {
                    ui->teOut->append(tr("Merge audio and video..."));
                } else {
                    avbrutet = true;
                }

                if(result.trimmed().contains("removing old files")) {
                    ui->teOut->append(tr("Removing old files, if there are any..."));
                }

                if(result.trimmed().contains("No videos found. You need to login")) {
                    ui->teOut->setTextColor(QColor("red"));
                    ui->teOut->append(tr("The download failed. Did you forget to enter username and password?"));
                    ui->teOut->setTextColor(QColor("black"));
                    return;
                }

                if(result.trimmed().contains("Episode")) {
                    QString s = result.trimmed();
                    int episode = s.indexOf("Episode");
//                    int newline = s.indexOf("INFO");
                    s = s.mid(episode + 8, 8);
                    QStringList avsnitt = s.split("of");
                    QRegularExpression rx("(\\d+)");
                    QRegularExpressionMatchIterator i1 = rx.globalMatch(avsnitt.at(0));
                    QRegularExpressionMatchIterator i2 = rx.globalMatch(avsnitt.at(1));
                    ui->teOut->append(tr("Episode") + " " + i1.next().captured() + " " + tr("of") + " " + i2.next().captured() + "...");
                }
            }
        });
        connect(CommProcess2,
                QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
        [ = ](int exitCode, QProcess::ExitStatus exitStatus) {
            statusExit(exitStatus, exitCode);

            if(!avbrutet) {
                ui->teOut->setTextColor(QColor("darkBlue"));
                ui->teOut->append(
                    tr("The media files (and if you have selected the "
                       "subtitles) have been downloaded."));
                ui->teOut->setTextColor(QColor("black"));
                ui->progressBar->setValue(0);

                if(!ui->actionNotifications->isChecked()) {
                    // showNotification
                    showNotification(tr("Download completed"), 1);
                }
            } else {
                ui->teOut->append(tr("Download completed"));
                ui->progressBar->setValue(0);

                if(!ui->actionNotifications->isChecked()) {
                    // showNotification
                    showNotification(tr("Download completed"), 3);
                }
            }

            /* ... */
        });
        ARG << address;
        CommProcess2->start(svtplaydl, ARG);
        ARG.clear();
        processpid = CommProcess2->processId();
    } else {
        ui->teOut->setText(tr("No folder is selected"));
    }
}
