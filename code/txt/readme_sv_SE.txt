23:30 31 maj 2022
Version 2.5.1
"Sök efter svtplay-dl" kunde få programmet att krascha om det saknades internetanslutning.
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows).
Använder Qt 5.15.4, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.3.0, Kompilerad med GCC 9.4.0 (Linux, GLIBC >= 2.31)

20:00 28 maj 2022
Version 2.5.0
Gränssnittet har förenklats. Mer logiskt och lätt att förstå.
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows).
Använder Qt 5.15.4, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.3.0, Kompilerad med GCC 9.4.0 (Linux, GLIBC >= 2.31)

16:00 28 maj 2022
Version 2.4.4 BETA
Mindre ändringar i gränssnittet.
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows).
Använder Qt 5.15.4, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.3.0, Kompilerad med GCC 9.4.0 (Linux, GLIBC >= 2.31)

11:00 27 maj 2022
Version 2.4.3 BETA
Liten uppdatering av den italienska översättningen.
Mindre ändringar i gränssnittet.
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows).
Använder Qt 5.15.4, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.3.0, Kompilerad med GCC 9.4.0 (Linux, GLIBC >= 2.31).

11:00 27 maj 2022
Version 2.4.2 BETA
Uppdaterad italiensk och svensk översättning.
Mindre ändringar i gränssnittet.
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows).
Använder Qt 5.15.4, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.3.0, Kompilerad med GCC 9.4.0 (Linux, GLIBC >= 2.31).

01:00 27 maj 2022
Version 2.4.1 BETA
Gränssnittet har förenklats. Gjorts mer logiskt och lättförståeligt.
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows).
Använder Qt 5.15.4, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.3.0, Kompilerad med GCC 9.4.0 (Linux, GLIBC >= 2.31).

19:00 23 maj 2022
Version 2.4.0
Förenklat och tydligare gränssnitt på "Ladda ner svtplay-dl".
Information om senaste svtplay-dl ögonblicksbild visas bara om den är nyare än senaste stabila versionen.
Mindre förbättringar och förtydliganden.
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows).
Använder Qt 5.15.4, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.3.0, Kompilerad med GCC 9.4.0 (Linux, GLIBC >= 2.31).

16:00 21 maj 2022
Version 2.3.3 BETA
Förenklat och tydligare gränssnitt på "Ladda ner svtplay-dl".
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows).
Använder Qt 5.15.4, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.3.0, Kompilerad med GCC 9.4.0 (Linux, GLIBC >= 2.31).

02:00 20 maj 2022
Version 2.3.2 BETA
Information om senaste svtplay-dl ögonblicksbild visas bara om den är nyare än senaste stabila versionen.
Test av nytt offlineinstallationsprogram för Windows.
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows).
Använder Qt 5.15.4, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.3.0, Kompilerad med GCC 9.4.0 (Linux, GLIBC >= 2.31).

10:00 19 maj 2022
Version 2.3.1
Bugfix: "Ladda ner till standardplatsen" hade slutat fungera.
Mindre förbättringar och förtydliganden.
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows).
Använder Qt 5.15.4, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.3.0, Kompilerad med GCC 9.4.0 (Linux, GLIBC >= 2.31).

12:00 18 maj 2022
Version 2.3.0
Ny uppdatering från svtplay-dl, version 4.12 har implementerats. Ange önskad upplösning.
Möjlighet att välja upplösning när du klickar på "Lägg till alla episoder till nedladdningslistan".
"Download svtplay-dl" har ett mindre programfönster och mindre ikoner.
Bugfix: "Ladda ner svtplay-dl". Om sökvägen till bleedingedge och stable inte längre finns kommer detta att hanteras bättre.
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows).
Använder Qt 5.15.4, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.3.0, Kompilerad med GCC 9.4.0 (Linux, GLIBC >= 2.31).

00:00 18 maj 2022
Version 2.2.5 BETA
Buggrättning: "Ladda ner svtplay-dl". Om sökvägen till bleedingedge och stable inte längre finns hanteras detta bättre.
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows 64-bit).
Använder Qt 5.15.4, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.3.0, Kompilerad med GCC 9.4.0 (Linux, GLIBC >= 2.31).

23:00 17 maj 2022
Version 2.2.4 BETA
Möjlighet att välja upplösning när man klickar "Lägg till alla avsnitt till nedladdningslistan".
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows 64-bit).
Använder Qt 5.15.4, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.3.0, Kompilerad med GCC 9.4.0 (Linux, GLIBC >= 2.31).

23:00 16 maj 2022
Version 2.2.3 BETA
"Download svtplay-dl" har fått ett mindre programfönster.
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows 64-bit).
Använder Qt 5.15.4, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.3.0, Kompilerad med GCC 9.4.0 (Linux, GLIBC >= 2.31).

21:00 15 maj 2022
Version 2.2.2 BETA
Mindre ikoner på "ladda ner svtplay-dl".
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows 64-bit).
Använder Qt 5.15.4, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.3.0, Kompilerad med GCC 9.4.0 (Linux, GLIBC >= 2.31).

14:00 14 maj 2022
Version 2.2.1 BETA
Ny uppdatering från svtplay-dl, version 4.12 är implementerad. Ange önskad upplösning.
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows 64-bit).
Använder Qt 5.15.4, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.3.0, Kompilerad med GCC 9.4.0 (Linux, GLIBC >= 2.31).

23:30 13 maj 2022
Version 2.2.0
Högerklicka på aktivitetsfältet för att ta skärmdump. streamCapture och "ladda ner svtplay-dl"
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows 64-bit).
Använder Qt 5.15.4, Kompilerad med MinGW GCC 8.1.0 (Windows 32-bit).
Använder Qt 5.15.4, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.3.0, Kompilerad med GCC 9.4.0 (Linux, GLIBC >= 2.31).

23:00 10 maj 2022
Version 2.1.4
Laddar ner NFO information. Bland annat från svtplay.se.
Förbättrad visning av licensinformation och av "Om" svtplay-dl och FFmpeg.
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows 64-bit).
Använder Qt 5.15.3, Kompilerad med MinGW GCC 8.1.0 (Windows 32-bit).
Använder Qt 5.15.3, Kompilerad med GCC 7.5.0 (Linux, GLIBC >= 2.27).
Använder Qt 6.3.0, Kompilerad med GCC 9.4.0 (Linux, GLIBC >= 2.31).

20:30 May 9, 2022
Version 2.1.3 BETA
Ännu bättre visning av NFO information. Förbättrad visning av licensinformation och av "Om" svtplay-dl och FFmpeg
Uses Qt 6.3.0, Compiled with MinGW GCC 11.2.0 (Windows).
Uses Qt 5.15.3, Compiled with GCC 7.5.0 (Linux, GLIBC >= 2.27).
Uses Qt 6.3.0, Compiled with GCC 9.4.0 (Linux, GLIBC >= 2.31).

00:30 9 maj 2022
Version 2.1.2 BETA
Förbättrad visning av NFO informationen.
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows).
Använder Qt 5.15.3, Kompilerad med GCC 7.5.0 (Linux)

23:00 6 maj 2022
Version 2.1.1 BETA
Buggfix: Laddar ner NFO information. Bland annat från svtplay.se
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows).
Använder Qt 5.15.3, Kompilerad med GCC 7.5.0 (Linux).

18:00 6 maj 2022
Version 2.1.0 BETA
Laddar ner NFO information. Bland annat från svtplay.se
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows).
Använder Qt 5.15.3, Kompilerad med GCC 7.5.0 (Linux).

22:30 30 april 2022
Version 2.0.12
Uppdaterad italiensk översättning.
Uppdaterad svensk översättning.
Använder Qt 6.3.0 (Windows).
Använder Qt 6.3.0, Kompilerad med MinGW GCC 11.2.0 (Windows).
Använder Qt 5.15.3, Kompilerad med GCC 7.5.0 (Linux).

22:00 8 april 2022
Version 2.0.11
Buggrättning: Fungerar bättre när användaren väljer metod och kvalitet.
Använder Qt 5.15.3, Kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.3, Kompilerad med GCC 7.5.0 (Linux).

18:00 8 april 2022
Version 2.0.10
Visar ett meddelande om inte streamCapture2 får lov att öppna foldern där streamCapture2 finns.
Buggrättning: Felmeddelande när man klickar "Sök efter senaste svtplay-dl från bin.ceicer.com.." Allra första gången.
Qt uppdaterad till version 5.15.3 öppen källkod. Kompilerad på Windows 10. (Windows).
Använder Qt 5.15.3, Kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.3, Kompilerad med GCC 7.5.0 (Linux).

12:00 7 april 2022
Version 2.0.9 BETA
Svensk översättning.
Använder Qt 5.15.3, Kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.3, Kompilerad med GCC 7.5.0 (Linux).

16:00 3 april 2022
Version 2.0.8 BETA
Buggrättning: Felmeddelande när man klickar "Sök efter senaste svtplay-dl från bin.ceicer.com.." Allra första gången.
Använder Qt 5.15.3, Kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.3, Kompilerad med GCC 7.5.0 (Linux).

23:30 27 mars 2022
Version 2.0.7 BETA
Qt uppdaterad till version 5.15.3 öppen källkod. Kompilerad på Windows 10. (Windows).
Använder Qt 5.15.3, Kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.3, Kompilerad med GCC 7.5.0 (Linux).

04:00 26 mars 2022
Version 2.0.6 BETA
Visar ett meddelande om inte streamCapture får lov att öppna foldern där streamCapture2 finns.
Använder Qt 5.15.2, Kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.3, Kompilerad med GCC 7.5.0 (Linux).

23:00 22 mars 2022
Version 2.0.5
Buggrättning: Kvalitet (bithastighet) visades inte korrekt.
Buggrättning: Felstavning i "Ladda ner svtplay-dl..".
Buggrättning: Man måste välja mapp två gånger för att spara den
nedladdade filen om foldern är nyskapad.
Använder Qt 5.15.2, Kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.3, Kompilerad med GCC 7.5.0 (Linux).

21:00 20 mars 2022
Version 2.0.4
Buggrättning: streamCapture2 hittade inte alltid svtplay-dl.
Använder Qt 5.15.2, Kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.3, Kompilerad med GCC 7.5.0 (Linux).

17:00 11 mars 2022 
Version 2.0.2
Felaktig information om vilket operativsystem som har använts när programmet kompilerats.
Använder Qt 5.15.2, Kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.3, Kompilerad med GCC 7.5.0 (Linux).

17:00 6 mars 2022
Version 2.0.1
Felaktigt meddelande när streamCapture2 behöver uppdateras. Endast linux-versionen.
Använder Qt 5.15.2, Kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.3, Kompilerad med GCC 7.5.0 (Linux).

22:00 4 mars 2022
Version 2.0.0
Ny funktion: ladda ner direkt till "stable" och "bleedingedge".
Ny funktion: "Kolla efter nya versioner av svtplay-dl vid start."
Ny funktion: Leta efter senaste stabila versionen.
Ny funktion: Redigerbar "Ladda ner svtplay-dl" inställningar.
"Ladda ner svtplay-dl.." 
-Följer fonvalet från "Välg teckensnitt..".
-Möjlighet att göra texten större eller mindre.
Ny "Om" dialog.
Uppdaterad FFmpeg till version 5.0
Buggrättning: Skrivbordsgenväg och menygenväg.
Buggrättning: Linux version, "Sök efter uppdateringar när programmet startar".
Buggrättning: "Redigera inställningarna" allvarlig bugg.
Buggrättning: "readyupdate"
Använder Qt 5.15.2, Kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.3, Kompilerad med GCC 7.5.0 (Linux).

16:00 25 february 2022
Version 1.3.23 BETA
Uppdaterad italiensk översättning.
Använder Qt 5.15.2, Kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.3, Kompilerad med GCC 7.5.0 (Linux).

21:00 21 february 2022
Version 1.3.22 BETA
Buggrättning, "Sök efter senaste svtplay-dl från bin.ceicer.com.."
Större skrivbordsikon (Windows).
Använder Qt 5.15.2, Kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.3, Kompilerad med GCC 7.5.0 (Linux).

21:00 19 february 2022
Version 1.3.21 BETA
Uppdaterad svensk översättning.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.3, kompilerad med GCC 7.5.0 (Linux).

21:00 19 february 2022
Version 1.3.20 BETA
Buggrättning, "readyupdate"
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.3, kompilerad med GCC 7.5.0 (Linux).

16:00 17 february 2022
Version 1.3.19 BETA
Ny "Om" dialog.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.3, kompilerad med GCC 7.5.0 (Linux).

09:00 16 february 2022
Version 1.3.18 BETA
Bytt plats på "Sök senaste" och "Sök senaste stabila"
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

20:00 15 february 2022
Version 1.3.17 BETA
Förbättrad: ladda ner direkt till "stable" och "bleedingedge".
Snyggare ikoner.
Möjlig bugg rättad: ladda ner direkt till "stable" och "bleedingedge".
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

19:00 14 february 2022
Version 1.3.16 BETA
"Kolla efter ny versioner av svtplay-dl vid start."
-Förbättrad
Redigerbara "Ladda ner svtplay-dl" inställningar.
Mindre buggrättningar och förbättringar.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.3, kompilerad med GCC 7.5.0 (Linux).

19:00 13 february 2022
Version 1.3.15 BETA
Ny funktion "Kolla efter ny versioner av svtplay-dl vid start."
Mindre buggrättningar och förbättringar.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

10:00 12 february 2022
Version 1.3.14 BETA
Buggrättning, "Redigera inställningarn" allvarlig bugg.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

08:00 12 february 2022
Version 1.3.13 BETA
Uppdaterad FFmpeg till version 5.0
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

21:30 9 februari 2022
Version 1.3.12 BETA
Uppdaterad "Sök efter senaste svtplay-dl från bin.ceicer.com.."
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

21:30 8 februari 2022
Version 1.3.11 BETA
Bug fix, Linux version, "Check for updates at program start".
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

22:30 7 februari 2022
Version 1.3.10 BETA
Uppdaterad italiensk översättning.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

00:00 7 februari 2022
Version 1.3.9 BETA
"Ladda ner svtplay-dl.."
-uppdaterad
-svensk översättning.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

20:00 5 februari 2022
Version 1.3.8 BETA
"Ladda ner svtplay-dl.."
-uppdaterad
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

23:30 4 februari 2022
Version 1.3.7 BETA
Sök senaste stable.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

14:00 4 februari 2022
Version 1.3.6 BETA
Buggrättning: Skrivbords- och menygenväg.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

12:00 4 februari 2022
Version 1.3.5 BETA
Mindre buggrättningar och förbättringar.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

11:00 3 February 2022
Version 1.3.4 BETA
Översättning.
Linux versionen.
Mindre buggrättningar och förbättringar.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

02:00 3 februari 2022
Version 1.3.3 BETA
"Ladda ner svtplay-dl.."
-Förbättrad möjlighet att ladda ner direkt till "stable" och "bleedingedge".
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

02:00 1 februari 2022 
Version 1.3.2 BETA
"Ladda ner svtplay-dl.." 
-Följer teckensnittsvalet från "Välj teckensnitt".
-Möjligt att göra texten större eller mindre.
-Möjlighet att ladda ner direkt till "stable" och "bleedingedge".
Mindre buggrättningar och förbättringar.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

02:00 23 januari 2022 
Version 1.3.0
Uppdaterad "Välj teckensnitt dialog".
Uppdaterad "Sök efter senaste svtplay-dl från bin.ceicer.com.."
Högerklick på statusraden.
Mindre buggrättningar och förbättringar.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

02:00 31 december 2021 
Version 1.2.0
Uppdaterad "Ladda ner svtplay-dl".
Uppdaterad "Välj teckensnitt dialog".
Uppgraderad metod för att uppdatera Linux AppImage.
Uppdaterad svtplay-dl stabila version 4.10
Uppdaterad svtplay-dl senaste ögonblicksbild 4.10
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

18:00 18 december 2021 
Version 1.1.7
Standard  att dekomprimera och ta bort komprimerad fil när man laddar ner ny version av svtplay-dl.
Uppdaterad svtplay-dl senaste ögonblicksbild 4.9-14-g6966876
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

22:00 25 november 2021 
Version 1.1.6
Buggrättning, hittade inte alltid senaste stabila svtplay-dl vid sökning hos bin.ceicer.com.
Uppdaterad svtplay-dl senaste ögonblicksbild 4.9-7-gd9209f8
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

17:00 14 november 2021 
Version 1.1.5
Buggrättning, en ikon som inte var synlig, är synlig igen.
Uppdaterad svtplay-dl senaste ögonblicksbild 4.9-5-g77b43ea
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

17:30 13 november 2021 
Version 1.1.4
Buggrättning, "Ta bort alla inställningsfiler och avsluta" fungerar igen.
Ladda ner runtimefilen "Microsoft Visual C++ Redistributable" från bin.ceicer.com. Endast Windowsversionen.
Öppna mappen du laddat ner svtplay-dl till direkt från programmet.
Uppdaterad svtplay-dl senaste ögonblicksbild 4.9-3-g4f2fa55
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

11:00 4 november 2021 
Version 1.1.3
Buggrättning, en meddelanderuta visades två gånger.
Bättre information när du söker efter svtplay-dl att ladda ner från bin.ceicer.com.
Mindre förbättringar, lite snabbare kod.
Uppdaterad svtplay-dl stabila version 4.9
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

22:00 26 oktober 2021 
Version 1.1.2
Buggrättning, nu fungerar det att uppdatera AppImage.
Uppdaterad svtplay-dl stabila version 4.7
Uppdaterad svtplay-dl senaste ögonblicksbild 4.7-4-gb3fd51d
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

15:00 10 oktober 2021 
Version 1.1.1
Tydligare meddelanden när du laddar ner svtplay-dl.
Uppdaterad svtplay-dl stabila version 4.5
Uppdaterad svtplay-dl senaste ögonblicksbild 4.5-1-gadcaa73
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

14:00 5 september 2021 
Version 1.1.0
När du söker efter senaste svtplay-dl, visas senaste versionen i comboboxen.
Söker automatiskt när du byter operativsystem.
Uppdaterad svtplay-dl stabila version 4.2
Uppdaterad svtplay-dl senaste ögonblicksbild 4.2-5-g404373e
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

13:38 26 juli 2021 
Version 1.0.1
Uppdaterad svtplay-dl stabila version 4.2
Uppdaterad svtplay-dl senaste ögonblicksbild 4.2
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

19:00 23 juli, 2021 
Version 1.0.0
Efter att testat och testat och testat igen hoppas jag att allt fungerar. 
Om det inte gör det, rapportera alla buggar, tack!
Hjälpen uppdaterad.
Uppdaterad svtplay-dl senaste ögonblicksbild 4.1-5-gc05a736
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

13:30 23 juli, 2021 
Version 0.99.9
Snart version 1.0.0!
Italiensk översättning.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

23:00 21 juli, 2021 
Version 0.99.8
Snart version 1.0.0!
Mindre Buggrättningar.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

13:30 15 juli, 2021 
Version 0.99.7
Snart version 1.0.0!
Buggrättning: Fel när man använde 'st' cookie.
Fler och tydligare utskrift från svtplay-dl när "Visa mer" är ikryssat.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

01:00 15 juli, 2021 
Version 0.99.6
Snart version 1.0.0!
Programmet stoppar försök att spara till mappar som man saknar rättighet att spara till.
Tack till "Rickard" som uppmärksammade mig på detta.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

02:00 13 juli, 2021 
Version 0.99.5
Snart version 1.0.0!
Bättre menyer, tydligare förklaringar och snyggare ikoner.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

04:30 12 juli, 2021 
Version 0.99.4
Snart version 1.0.0!
Bättre information från FFmpeg.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

17:30 11 juli, 2021 
Version 0.99.3
Snart version 1.0.0!
Förbättrad "Spara som.." dialog
Bättre utformade dialogrutor.
Mindre buggrättningar.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

19:30 10 juli, 2021 
Version 0.99.2
Snart version 1.0.0!
Möjlighet att "Redigera sparade sökningar".
Några mindre Buggrättningar.
Uppdaterad svtplay-dl stabila version 4.1
Uppdaterad svtplay-dl senaste ögonblicksbild 4.1
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

15:30 9 juli, 2021 
Version 0.99.1
Snart version 1.0.0!
Några mindre Buggrättningar.
Automatiserad nedladdning av senaste svtplay-dl.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

19:30 8 juli 2021 
Version 0.99.0 
Snart version 1.0.0!
Buggrättning: Välja bithastighet.
Buggrättning: Lägga till på och ladda ner från nedladdningslistan.
Buggrättning: Ladda ner alla avsnitt i en TV serie.
Egen dialogruta för visning av olika saker,
för att inte störa sökningar och nedladdningar.
Förbättrad "ladda ner från bin.ceicer.com"
Uppdaterad svtplay-dl senaste ögonblicksbild 4.0-5-ga162357
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

16:01 6 juli 2021 
Version 0.29.5 BETA 2
Buggrättning: Välja bithastighet.
Buggrättning: Lägga till på och ladda ner från nedladdningslistan.
Buggrättning: Ladda ner alla avsnitt i en TV serie.
Egen dialogruta för visning av olika saker,
för att inte störa sökningar och nedladdningar.
Förbättrad "ladda ner från bin.ceicer.com"
Uppdaterad svtplay-dl senaste ögonblicksbild 4.0-4-gc5ae975
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

00:01 6 juli 2021 
Version 0.29.5 BETA
Buggrättning: Välja bithastighet.
Buggrättning: Lägga till på och ladda ner från nedladdningslistan.
Buggrättning: Ladda ner alla avsnitt i en TV serie.
Egen dialogruta för visning av olika saker.
För att inte störa sökningar och nedladdningar.
Uppdaterad svtplay-dl senaste ögonblicksbild 4.0-4-gc5ae975
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

17:30 1 juli 2021 
Version 0.29.4
Komplett italiensk översättning.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

03:30 1 juli 2021 
Version 0.29.3
Möjlighet att söka efter senaste svtplay-dl från bin.ceicer.com
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

19:30 30 juni 2021 
Version 0.29.2
Nedladdad komprimerad svtplay-dl kan automatiskt tas bort efter dekomprimering.
Uppdaterad svtplay-dl senaste ögonblicksbild 4.0-3-gc66590c
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

16:30 28 juni 2021
Version 0.29.1
En större omarbetning av programmet, det fungerar bättre att välja
nedladdningskvalitet och alla meddelanden från programmet
är tydligare.
Manualen är omarbetad.
Möjlighet att zooma in- och ut med ett tangentbordstryck.
Uppdaterad svtplay-dl senaste ögonblicksbild 4.0-2-g09843d6
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

20:30 25 juni 2021
Version 0.29.0 BETA 3
En större omarbetning av programmet, det ska fungera bättre att välja
nedladdningskvalitet och alla meddelanden från programmet
ska vara tydliga.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

17:00 25 juni 2021
Version 0.29.0 BETA 2
En större omarbetning av programmet, det ska fungera bättre att välja
nedladdningskvalitet och alla meddelanden från programmet
ska vara tydliga.
Uppdaterad svtplay-dl senaste ögonblicksbild 4.0-2-g09843d6
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

03:00 21 juni 2021
Version 0.29.0 BETA 1
En större omarbetning av programmet, det ska fungera bättre att välja
nedladdningskvalitet och alla meddelanden från programmet
ska vara tydliga.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

20:00 20 juni 2021
Version 0.28.15
Viktig bugfix: Nedladdningslistan fungerar nu.
Fungerar inte tidigare om man använde knappen "Lägg till i nedladdningslistan".
Menyn fungerade.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

15:00 20 juni 2021
Version 0.28.14
Kraftigt förbättrad hastighet på dekomprimeringen av nedladdade
svtplay-dl.zip (Windows).
Linuxversionen kan dekomprimera *.tar.gz och *.zip
Windowsversionen kan dekomprimera *.tar.gz och *.zip
Förbättrade dialogrutor för att spara mediafiler och för
att välja nedladdningsplats för svtplay-dl.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

20:00 19 juni 2021
Version 0.28.13 BETA 3
Kraftigt förbättrad hastighet på dekomprimeringen av nedladdade
svtplay-dl.zip (Windows).
Linuxversionen kan dekomprimera *.tar.gz och *.zip
Windowsversionen kan dekomprimera *.tar.gz och *.zip
Förbättrade dialogrutor för att spara mediafiler och för
att välja nedladdningsplats för svtplay-dl.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

17:35 18 juni 2021
Version 0.28.12
Uppdaterad svtplay-dl stabila version 4.0
Uppdaterad svtplay-dl senaste ögonblicksbild 4.0
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

23:30 15 juni 2021
Version 0.28.11
"download svtplay-dl från bin.ceicer.com" delar språkinställningarna
med huvudprogrammet.
Mindre förbättringar.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

19:00 14 juni 2021
Version 0.28.10 BETA
"download svtplay-dl från bin.ceicer.com" delar språkinställningarna
med huvudprogrammet.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

23:59 13 juni 2021
Version 0.28.9
Uppdaterad svtplay-dl senaste ögonblicksbild 3.9.1-2-g00e5c84
Underhållsverktyget startar med "Update components" (Windows).
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

23:45 12 juni 2021
Version 0.28.8
Snyggare messagebox vid uppdatering.
Flyttat konfigurationsfilen för "ladda ner svtplay-dl".
Besök källkod och binära filer.
Fullständig italiensk översättning.
Uppdaterad italiensk hjälpfil.
Mindre förbättringar.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

10:30 11 juni 2021
Version 0.28.7 BETA
Snyggare messagebox vid uppdatering.
Flyttat konfigurationsfilen för "ladda ner svtplay-dl".
Besök källkod och binära filer.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

00:30 10 juni 2021
Version 0.28.6
Bug fix: Det gick inte att ladda ner. Har tagit bort "--remux"
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

21:30 7 juni 2021
Version 0.28.5
Uppdaterad svtplay-dl stabila version 3.9.1
Uppdaterad svtplay-dl senaste ögonblicksbild 3.9.1
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

18:30 4 juni 2021
Version 0.28.4
Uppdaterad svtplay-dl senaste ögonblicksbild 3.8-27-gd32bc02
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

22:14 3 juni 2021
Version 0.28.3
Uppdaterad svtplay-dl senaste ögonblicksbild 3.8-25-g88432ed
"--list-quality" (sökfunktionen) uppdaterad.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

19:30 30 maj 2021
Version 0.28.2
Uppdaterad svtplay-dl senaste ögonblicksbild 3.8-23-g89284c9
Lagt till "Välj automatiskt" alternativ för bithastighet och metod.
"--list-quality" (sökfunktionen) ytterligare uppdaterad.
Möjlighet att testa språkfiler i "svtplay-dl från bin.ceicer.com"
Fullständig italiensk översättning.
Uppdaterade hjälpfiler, engelska, italienska och svenska.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

01:48 28 maj 2021
Version 0.28.0
"--list-quality" (sökfunktionen) uppdaterad.
Fungerar med senaste svtplay-dl.
Nyhet: Ladda ner svtplay-dl från streamCapture2.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

21:48 24 maj 2021
Version 0.27.6
Uppdaterad svtplay-dl senaste ögonblicksbild 3.8-22-g45fceaa
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

22:48 23 maj 2021
Version 0.27.5
Uppdaterad svtplay-dl senaste ögonblicksbild 3.8-18-g1c3d8f1
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

17:30 20 maj 2021
Version 0.27.4
Uppdaterad svtplay-dl senaste ögonblicksbild 3.8-13-g3d46e85
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

19:30 17 maj 2021
Version 0.27.3
Komplett italiensk översättning.
Uppdaterad svtplay-dl senaste ögonblicksbild 3.8-10-gc9a606d
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

14:05 16 maj 2021
Version 0.27.2
'st' cookie fungerar med nedladdningslistan.
Uppdaterad svtplay-dl senaste ögonblicksbild 3.8-3-g107d3e0
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

05:05 16 maj 2021
Version 0.27.1
Bug Fix: Gick inte att söka efter videoströmmar när det krävdes inloggning.
Hantera många olika 'st' cookies.
Redigera konfigurationsfilen.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

17:55 14 maj 2021
Version 0.26.7
Uppdaterad svtplay-dl senaste ögonblicksbild 3.8-2-g2c5101a
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

20:55 13 maj 2021
Version 0.26.6
Uppdaterad svtplay-dl stabila version 3.8
Uppdaterad svtplay-dl senaste ögonblicksbild 3.8
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

15:55 13 maj 2021
Version 0.26.5
Uppdaterad svtplay-dl senaste ögonblicksbild 3.7-12-ga5e4166
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

23:55 11 maj 2021
Version 0.26.4
Uppdaterad svtplay-dl senaste ögonblicksbild 3.7-12-ga5e4166
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

23:55 10 maj 2021
Version 0.26.3
Uppdaterad svtplay-dl senaste ögonblicksbild 3.7-10-gd575112
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

11:05 9 maj 2021
Version 0.26.2
Uppdaterad svtplay-dl senaste ögonblicksbild 3.7-4-g3497e05
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

14:05 6 maj 2021
Version 0.26.1
Möjligt att använda 'st' cookies.
Optimerad, effektivare kod.
Uppdaterad svtplay-dl stabila version 3.7
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

12:45 28 april 2021
Version 0.25.8
Uppdaterad svtplay-dl senaste ögonblicksbild 3.7
Förbättrad visning av nedladdningsbara videoströmmar.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

21:00 22 april 2021
Version 0.25.6
Bugg rättad: Det gick inte att söka efter videoströmmar.
Uppdaterad svtplay-dl senaste ögonblicksbild 3.6-3-gaae55fc
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

19:00 20 april 2021
Version 0.25.5
Uppdaterad svtplay-dl stabila version 3.6
Uppdaterad svtplay-dl senaste ögonblicksbild 3.6-2-g0dcb01f
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

10:00 15 april 2021
Version 0.25.4
Uppdaterad svtplay-dl stabila version 3.5
Uppdaterad svtplay-dl senaste ögonblicksbild 3.5-1-g8805627
Uppdaterad FFmpeg 4.4
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

23:59 13 april 2021
Version 0.25.3
Uppdaterad svtplay-dl stabila version 3.4
Ändringar i koden gör programmet en aning snabbare och något mindre.
Möjligt att redigera namnet på strömningstjänster.
Möjligt att ha mellanslag i namnet på strömningstjänster.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

00:30 8 april 2021
Version 0.25.1
Bugg rättad: Fel när man öppnar en egen språkfil i programmet.
Snyggare presentation av "Metod" och "Bithastighet".
Hjälpen översatt till italienska.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

12:31 3 april 2021
Version 0.25.0
Uppdaterad svtplay-dl senaste ögonblicksbild 3.3-2-gcdf394e
Namnbyte på "Betal TV" till "Inloggning"
"Direktnedladdning av alla videoströmmar i nuvarande serie (inte från nedladdningslistan)" visar när varje fil blivit nedladdad och hur många filer som återstår.
Förbättrad förloppsindikator.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

08:31 17 mars 2021
Version 0.24.0
Uppdaterad svtplay-dl stabila version 3.3
Uppdaterad svtplay-dl senaste ögonblicksbild 3.3
Förloppsindikator som visar nedladdningarna.
Buggrättning: Nu syns antalet sökningar som du tidigare valt att spara.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

10:00 16 mars 2021
Version 0.23.2
Uppdaterad svtplay-dl senaste ögonblicksbild 3.2-10-g7440e3f
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

12:05 15 mars 2021
Version 0.23.1
Uppdaterad manual.
Uppdaterad svtplay-dl senaste ögonblicksbild 3.2-9-g028971b
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

03:30 13 mars 2021
Version 0.23.0
Buggrättning nedladdning från Viafree.
Dra- och släpp-funktionalitet har lagts till.
Fler meddelanden i statusbaren.
Bättre översättningar i Linux AppImage.
Uppdaterad svtplay-dl stabila version 3.2
Uppdaterad svtplay-dl senaste ögonblicksbild 3.2-2-g6ef5d61
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

21:30 6 mars 2021
Version 0.22.4
Uppdaterad svtplay-dl senaste ögonblicksbild 3.1-5-gcb3612b
Linux AppImage uppdaterad till GLIBC 2.27 (2018-02-01). Fungerar nu med de flesta distributioner från maj 2018 och nyare.
Tydligare meddelanden om man försöker kopiera till förvald folder och filen redan finns.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 7.5.0 (Linux).

00:30 1 mars 2021
Version 0.22.3
Uppdaterad svtplay-dl senaste ögonblicksbild 3.1-4-g998f51f (Windows).
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 5.4.0 (Linux).

12:00 27 February 2021
Version 0.22.2
"Uppdatera" är inaktiverad om det inte finns någon uppdatering (Linux).
Mindre korrigeringar.
Uppdaterat grafiskt gränssnitt.
Uppdaterad FFmpeg 4.3.2 (Linux).
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 5.4.0 (Linux).

00:00 26 February 2021
Version 0.22.1
Uppdaterad svtplay-dl stabila version 3.0-7-ge423c1c (Linux).
Uppdaterad svtplay-dl stabila version 3.1 (Windows).
Uppdaterad svtplay-dl senaste ögonblicksbild 3.1 (Linux, Windows).
Uppdaterad FFmpeg 4.3.2
Uppdaterat grafiskt gränssnitt.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 5.4.0 (Linux).

00:50 18 februari 2021
Version 0.22.0
Uppdaterad svtplay-dl senaste ögonblicksbild 3.0-5-g8cd5793
Italiensk översättning.
Tagit bort ofullständig tysk översättning.
Möjlighet att testa språkfiler.
"Om" visar en fungerande e-postadress.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 5.4.0 (Linux).

00:50 15 februari 2021
Version 0.21.2
Uppdaterad svtplay-dl stabila version 2.9-8-ga263a66
Uppdaterad svtplay-dl senaste ögonblicksbild 3.0
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 5.4.0 (Linux).

12:50 25 januari 2021
Version 0.21.1
AppImage: "Ta bort alla inställningsfiler och avsluta"
Tar bort genvägar på skrivbordet och i programmenyn.
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 5.4.0 (Linux).

22:50 24 januari 2021
Version 0.21.0
Uppdaterad svtplay-dl senaste ögonblicksbild 2.8-11-gd193679
AppImage: Desktop fil och ikon på skrivbordet
AppImage: Desktop fil och ikon på programmenyn
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 5.4.0 (Linux).

22:50 4 januari 2021
Version 0.20.18
Uppdaterad svtplay-dl senaste ögonblicksbild 2.8-9-gc3ff052
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 5.4.0 (Linux). 

21:02 9 december 2020
Version 0.20.17 
Uppdaterad svtplay-dl stabila version 2.8-3-g18a8758
Använder Qt 5.15.2, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.2, kompilerad med GCC 5.4.0 (Linux). 

19:02 21 november 2020
Version 0.20.16 
Uppdaterad svtplay-dl stabila version 2.8
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux). 

18:01 15 november 2020
Version 0.20.15
Uppdaterad svtplay-dl version 2.7-5-gbf1f9c5
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux). 

21:55 6 november 2020
Version 0.20.14
Uppdaterad svtplay-dl version 2.7-4-gef557cb
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux). 

03:45 1 november 2020
Version 0.20.13
Varning för för hög bitrate vid misslyckad nedladdning.
Tillförlitligare uppdatering av AppImage.
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux). 

18:34 27 October 2020
Version 0.20.12
Buggrättning: svtplay-dl i systemsökvägen hittades inte.
Uppdaterad svtplay-dl version  2.7
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux). 

18:09 13 oktober 2020
Version 0.20.11
Uppdaterad svtplay-dl version 2.7-3-ga550bd1
Tillförlitligare uppdatering av AppImage.
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux).

18:09 11 oktober 2020
Version 0.20.10
Uppdaterad svtplay-dl version 2.6-4-gd6dc139
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux).

20:09 2 oktober 2020
Version 0.20.9
Uppdaterad svtplay-dl version 2.6
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux).

22:05 27 september 2020
Version 0.20.8
Uppdaterad svtplay-dl version 2.5-7-g7db9f7e
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux).

19:39 23 september 2020
Version 0.20.7
Uppdaterad svtplay-dl svtplay-dl stable version 2.5
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux).

19:39 22 september 2020
Version 0.20.6
Uppdaterad svtplay-dl version 2.5-4-ge92ebbb
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux).

00:55 20 september 2020
Version 0.20.5
Uppdaterad svtplay-dl svtplay-dl version 2.5
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux).

00:43 19 september 2020
Version 0.20.4
Uppdaterad svtplay-dl svtplay-dl version 2.4-71-g5fc7750
Använder Qt 5.15.1, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.1, kompilerad med GCC 5.4.0 (Linux).

19:43 14 september 2020
Version 0.20.3
Buggrättning: Fungerar bättre med senaste versionerna av svtplay-dl
Uppdaterad svtplay-dl version 2.4-67-g478fd3b
Uppdaterad FFmpeg till version 4.3.1 (Linux).
Använder Qt 5.15.0, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.0, kompilerad med GCC 5.4.0 (Linux).

18:43 13 september 2020
Version 0.20.2
Möjlighet att skape genvägar.
Programmet uppdaterar sig självt. (Gäller Linux AppImage.)
Uppdaterad svtplay-dl version 2.4-62-g2920bb9
Använder Qt 5.15.0, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.0, kompilerad med GCC 5.4.0 (Linux).

15:20 12 juli 2020
Version 0.20.1
Möjlighet att skape en genväg på skrivbordet.
Möjlighet att få aviseringar när nedladdning är klar.
Buggrättning: Programmet kunde försvinna om man växklar mellan olika antal skärmar.
Buggrättning: Svensk översättning av "FFmpeg can be found in system path" har rättats.
Mindre förbättringar.
Använder Qt 5.15.0, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.0, kompilerad med GCC 5.4.0 (Linux).

13:26 28 juni 2020
Version 0.20.0
Möjlighet att välja svtplay-dl från systemsökvägen.
Möjlighet att välja svtplay-dl från filsystemet.
Buggrättning: Nu kommer streamCapture2 ihåg valt teckensnitt.
Mindre förbättringar.
Använder Qt 5.15.0, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.0, kompilerad med GCC 5.4.0 (Linux).

21:43 15 juni 2020
Version 0.19.2
Buggrättning: Otrevlig bugg som fick programmet att krascha.
Detta hände när programmet sökte efter uppdateringar vid programstart.
Använder Qt 5.15.0, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.0, kompilerad med GCC 5.4.0 (Linux).

17:07 14 juni 2020
Version 0.19.1
Använder den inkluderade FFmpeg eller, om den inte finns, systemets FFmpeg.
Använder Qt 5.15.0, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.0, kompilerad med GCC 5.4.0 (Linux).

23:28 3 May 2020
Version 0.19.0
Uppdaterad svtplay-dl version 2.4-42-g916e199
Möjligt att växla mellan svtplay-dl stable och senaste ögonblicksbild.
Uppdaterad FFmpeg version 4.2.3
Enkel uppdatering med minimal nedladdmimg.
Använder Qt 5.15.0, kompilerad med MinGW GCC 8.1.0 (Windows).
Använder Qt 5.15.0, kompilerad med GCC 5.4.0 (Linux).

20:27 27 april 2020
Version 0.18.5
Bugfix: Genvägen på startmenyn fungerade inte i version 0.18.4 (Windows).
Länk till svtplay-dl forum för problem.
Valbart teckensnitt.
Bättre kontrasterande färger mellan teckensnitt och bakgrund.
Mindre förbättringar.
Använder Qt 5.14.2, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.14.2, kompilerad med GCC 5.4.0 (Linux).

12:42 9 april 2020
Version 0.18.4
Uppdaterat teckensnitt.
Uppdaterad svensk översättning.
Ny bakgrundsfärg.
Använder Qt 5.14.2, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.14.2, kompilerad med GCC 5.4.0 (Linux).

01:09 3 april 2020
Version 0.18.3
Uppdaterad svtplay-dl version 2.4-35-g81cb18f
Använder Qt 5.14.2, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.14.2, kompilerad med GCC 5.4.0 (Linux).

13:49 2 februari 2020
Version 0.18.2
Uppdaterad svtplay-dl version 2.4-31-g25d3105
Uppdaterad FFmpeg version 4.2.2
Använder Qt 5.14.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.14.0, kompilerad med GCC 5.4.0 (Linux).

12:35 31 december 2019
Version 0.18.1
Kontrollera att svtplay-dl och ffmpeg finns med och hittas.
Ingen uppgift om svtplay-dl-versionen i dialogrutan "Om".
Använder Qt 5.14.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.14.0, kompilerad med GCC 5.4.0 (Linux).

16:23 20 december 2019
Version 0.18.0
Efter "Lista alla avsnitt", välj och ladda ner valda avsnitt.
Använder Qt 5.14.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.14.0, kompilerad med GCC 5.4.0 (Linux).

22:55 25 november 2019
Version 0.17.0
Förbättrad visning av sökresultat.
Möjlighet att använda en förvald folder för nedladdning.
Möjlighet att använda en förvald folder dit mediafilerna och undertexterna kopieras.
Möjlighet att söka efter och lista alla episoder i en serie.
Möjlighet att ladda ner alla episoder på en gång.
Möjligt att avbryta pågående nedladdning. (Windows).
Möjlighet att radera alla inställningsfiler.
Förbättrad hantering av undertexter.
Många Buggrättningar.
Rättat felstavningar.
"Avinstalleraren" borttagen från Windows, Portable version.
Använder Qt 5.13.2, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.2, kompilerad med GCC 5.4.0 (Linux).

11:01 20 October 2019
Version 0.16.0
Statisk FFmpeg version 4.2.1 (Linux and Windows).
Uppdaterad svtplay-dl version 2.4-29-gc59a305
Tydligare meddelande från svtplay-dl om sökresultatet.
Mindre Buggrättningar.
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).

21:49 11 oktober 2019
Version 0.15.0
Bugg: "/" sist i adressen tas nu bort.
Menyval för att avinstallera (Windows).
Uppdaterad svtplay-dl version 2.4-26-g5dcc899 (Linux and Windows).
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).

21:24 16 september 2019
Version 0.14.0
Frågetecken i sökadressen tas bort.
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).

19:53 16 september 2019
Version 0.13.9
Uppdaterad svtplay-dl version 2.4-20-g0826b8f
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).

23:24 13 september 2019
Version 0.13.8
Uppdaterad svtplay-dl version 2.4-6-gce9bfd3
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).

21:59 11 september 2019
Version 0.13.7
Uppdaterad svtplay-dl version 2.4-2-g5466853
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).

09:19 7 september 2019
Version 0.13.6
Uppdaterad svtplay-dl version 2.3-23-gbc15c69
Uppdaterad ffmpeg version 4.2 (Windows).
Använder Qt 5.13.1, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.1, kompilerad med GCC 5.4.0 (Linux).
22:17 2 september 2019

Version 0.13.5
Uppdaterad svtplay-dl version 2.2-5-gd1904b2 (Linux).
Uppdaterad svtplay-dl version 2.2-7-g62cbf8a (Windows).
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

00:06 1 september 2019
Version 0.13.4
Uppdaterad svtplay-dl version 2.2-2-g838b3ec (Linux).
Uppdaterad svtplay-dl version 2.2-14-gc77a4a5 (Windows).
Mindre bugg. Visar inte "Söker.." efter misslyckad sökning.
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

18:03 28 augusti 2019
Version 0.13.3
Uppdaterad svtplay-dl version 2.2-1-g9146d0d
Nedladdningslänk pekar till gitlab.
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

09:48 24 augusti 2019
Version 0.13.2
Uppdaterad svtplay-dl version 2.1-65-gb64dbf3
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

17:43 21 augusti 2019
Version 0.13.1
Bug fix, Nu fungerar: "Kontrollera automatiskt efter uppdateringar när programmet startar"
Uppdaterad svtplay-dl version 2.1-57-gf429cfc
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

17:59 17 augusti 2019
Version 0.13.0
Information om vad som uppdateras när programmet söker efter uppdateringar.
Förbättrad "Om".
Flyttade "Lösenord" från "Arkiv" -menyn till "Betal-TV".
Bättre statusmeddelanden.
Bekräftelse på att videoströmmen har laddats ner
Visa antal nedladdade strömmar.
Operativsystemberoende visning av sökväg.
Mindre bug fix.
Architecture instruction set 64-bit.
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

12:07 6 augusti 2019
Version 0.12.0
Uppdaterad svtplay-dl version 2.1-56-gbe6005e (Windows).
Nedgradderad ffmpeg (Windows, version 4.1.4, stabil).
Skapa automatiska mappar, om det behövs. (Samma videoström i olika kvaliteter).
Möjlighet att se mer information från svtplay-dl.
Lägg till status-tips text.
Arkitekturens instruktionsuppsättning 64-bit.
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

16:32 30 juli 2019
Version 0.11.0
Använder Qt 5.13.0, kompilerad med  MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med  GCC 5.4.0 (Linux).

22:41 30 juli 2019
Version 0.11.0 RC 1
Förbättrad hjälp och manual.
Redigerbar Download list.
Möjlighet att spara samma stream  i olika storlekar.
Möjlighet att ange användarnamn och lösenord. För betalda streamingtjänster.
Ersätter gammal kod med modernare.
Använder QSettings.
Många bugfixar.
Använder Qt 5.13.0, kompilerad med  MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med  GCC 5.4.0 (Linux).

14:39 5 juli 2019
Version 0.10.7
Flera Buggrättningar. Fungerar bättre att ladda ner en enskild fil.
Dublikat i valet mellan olika videokvaliteer borttagna.
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

14:59 29 juni 2019
Version 0.10.5
Uppdaterad svtplay-dl version 2.1-53-gd33186e
Uppdaterad ffmpeg (version N-94129-g098ab93257).
Arkitekturens instruktionsuppsättning: 64-bit.
Använder Qt 5.13.0, kompilerad med MinGW GCC 7.3.0 (Windows).
Använder Qt 5.13.0, kompilerad med GCC 5.4.0 (Linux).

21:29 13 februari 2018
Version 0.10.3
Uppdaterad svtplay-dl version 1.9.7
Arkitekturens instruktionsuppsättning: 64-bit.
Använder Qt 5.10.0, kompilerad med MSVC++ 15.5 (MSVS 2017).

15:41 9 augusti 2017
Version 0.10.1
Kryssruta för att söka efter, ladda och
infoga undertexter i videofilen.
Uppdaterad ffmpeg version 3.3.3
Dynamiskt användargränssnitt.
Arkitekturens instruktionsuppsättning: 64-bit.
Använder Qt 5.9.1, kompilerad med MSVC++ 15.0 (MSVS 2017).

01:19 26 juli 2017
Version 0.10.0
Combo Box för att välja kvalitet.
Arkitekturens instruktionsuppsättning: 64-bit.
Använder Qt 5.9.1, kompilerad med MSVC++ 15.0 (MSVS 2017).

15:48 16 juli 2017
Version 0.9.4
Uppdaterad ffmpeg version 3.3.2
Arkitekturens instruktionsuppsättning: 64-bit.
Använder Qt 5.9.1, kompilerad med MSVC++ 15.0 (MSVS 2017).

09:38 7 maj 2017
Version 0.9.3
Uppdaterad svtplay-dl version 1.9.4
Uppdaterad ffmpeg version 3.2.4
Visar versionshistoriken.

16:33 27 january 2017
Version 0.9.2
Uppdaterad svtplay-dl (version 1.9.1).

18:00 14 januari 2017
Version 0.9.1
Uppdaterade svtplay-dl och ffmpeg
Tysk översättning.
Mindre förbättringar.

01:55 30 oktober 2016
Release version 0.9.0
