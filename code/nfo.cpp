// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>



#include "newprg.h"

void Newprg::nfo()
{
    QString tmppath = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
//    qDebug() << tmppath;
    QStringList ARG;
    ARG << "--nfo" << "--force-nfo" << "-o" << tmppath;
    QString adress = ui->leSok->text();
    adress = adress.trimmed();
    ARG << adress;
    QProcess *CommProcess = new QProcess(nullptr);
    CommProcess->setProcessChannelMode(QProcess::MergedChannels);
    CommProcess->start(svtplaydl, ARG);
    processpid = CommProcess->processId();
    QString *result = new QString;
    QStringList *list = new QStringList;
    connect(CommProcess, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
    [ = ](int exitCode, QProcess::ExitStatus exitStatus) {
        *result = CommProcess->readAllStandardOutput();
        QString failure;

        if(ui->leSok->text().isEmpty()) {
            failure = tr("The search field is empty!");
        } else {
            failure = tr("NFO files contain release information about the media. No NFO file was found.");
        }

        if(result->left(6) == QString("ERROR:")) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(failure);
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            return;
        }

        if(exitCode != 0 || exitStatus != QProcess::NormalExit) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(failure);
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            return;
        }

        if(result->isEmpty()) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(failure);
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            return;
        }

        *list = result->split("\n");

        if(list->at(1) == QString("")) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(failure);
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            return;
        }

        list->removeDuplicates();
        int i = 0;

        foreach(QString s, *list) {
            if(!s.contains("INFO: Outfile:")) {
                list->removeAt(i);
            } else {
                list->removeAt(i);
                s.remove(0, 15);
                list->push_front(s.trimmed());
                i++;
            }
        }

        QString path = tmppath + "/" + list->at(0);
        QFile file(path);
        file.remove();
        file.close();
        path = tmppath + "/" + list->at(1);
        file.setFileName(path);
        file.open(QIODevice::ReadOnly);
        QXmlStreamReader reader(&file);
        QStringList s;

        while(reader.readNextStartElement()) {
            // svtplay.se
            if(reader.name() == QString("episodedetails")) {
                while(reader.readNextStartElement()) {
                    if(reader.name() == QString("showtitle")) {
                        s << "<b>" + tr("Title:") + "</b> " + reader.readElementText();
                    }

                    if(reader.name() == QString("title")) {
                        s << "<b>" + tr("Episode title:") + "</b> " + reader.readElementText();
                    }

                    if(reader.name() == QString("season")) {
                        s << "<b>" + tr("Season:") + "</b> " + reader.readElementText();
                    }

                    if(reader.name() == QString("episode")) {
                        s << "<b>" + tr("Episode:") + "</b> " + reader.readElementText();
                    }

                    if(reader.name() == QString("plot")) {
                        s << "<b>" + tr("Plot:") + "</b> " + reader.readElementText();
                    }

                    if(reader.name() == QString("aired")) {
                        s << "<b>" + tr("Published:") + "</b> " + reader.readElementText();
                    }
                }
            }
        }

        file.close();
        file.remove();
        path = tmppath + "/nfofile.txt";
        file.setFileName(path);

        if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("An unexpected error occurred while downloading the file."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            return;
        }

        QTextStream out(&file);
#if QT_VERSION >= QT_VERSION_CHECK(6, 3, 0)
        out.setEncoding(QStringConverter::Utf8); // Qt6
#else
        out.setCodec("UTF-8");  // Qt5
#endif

        foreach(QString tmp, s) {
            out << tmp << "<br>";
        }

        file.close();
        QStringList *infoList = new QStringList;
        *infoList << (QString(tr("NFO Info")));
        *infoList << (QString("wordwrap"));
        DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
        mDownloadListDialog->openFile(infoList, &path, true);
        file.remove();
    });
}
