// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef INFO_H
#define INFO_H

#include <QWidget>
#include <QtGlobal>
#include <QApplication>
#include <sstream>
#include <string>
//#include <stdio.h>


// EDIT
// All
#define VERSION "2.5.1" // All
//#define FFMPEG // ALL

// Windows
#ifdef Q_OS_WIN
//#define PORTABLE // Windows
//#define OFFLINE_INSTALLER // Windows
#endif

// Linux
#ifdef Q_OS_LINUX
//#define EXPERIMENTAL // Linux
//#define ZSYNC2 // Linux
#endif

// END EDIT
#ifdef OFFLINE_INSTALLER
#define OFFLINE_INSTALLER_DOWNLOAD "https://bin.ceicer.com/streamcapture2/bin/"
#endif
#ifdef Q_OS_LINUX
// Only AppImage, no svtplay-dl or FFmpeg
#if QT_VERSION >= QT_VERSION_CHECK(6, 3, 0)
#define HIGH_GLIBC
#endif
#endif

#ifdef Q_OS_WIN

#define FONT 11
#define VERSION_PATH "http://bin.ceicer.com/streamcapture2/version_windows.txt"

#ifdef Q_PROCESSOR_X86_64 // Windows 64 bit
// TEST
//#define CHECK_LATEST_STABLE "http://bin.ceicer.com/svtplay-dl-test/Windows/64bit/version_stable.txt"
//#define CHECK_LATEST "http://bin.ceicer.com/svtplay-dl-test/Windows/64bit/version.txt"
#define CHECK_LATEST_STABLE "http://bin.ceicer.com/svtplay-dl/Windows/64bit/version_stable.txt"
#define CHECK_LATEST "http://bin.ceicer.com/svtplay-dl/Windows/64bit/version.txt"
#endif

#ifdef Q_PROCESSOR_X86_32 // Windows 32 bit
#define CHECK_LATEST_STABLE "http://bin.ceicer.com/svtplay-dl/Windows/32bit/version_stable.txt"
#define CHECK_LATEST "http://bin.ceicer.com/svtplay-dl/Windows/32bit/version.txt"
#endif
#endif // Windows


#ifdef Q_OS_LINUX
#define FONT 12
#define VERSION_PATH "http://bin.ceicer.com/streamcapture2/version_linux.txt"
// TEST
//#define CHECK_LATEST_STABLE "http://bin.ceicer.com/svtplay-dl-test/Linux version_stable.txt"
//#define CHECK_LATEST "http://bin.ceicer.com/svtplay-dl-test/Linux/version.txt"
#define CHECK_LATEST_STABLE "http://bin.ceicer.com/svtplay-dl/Linux/version_stable.txt"
#define CHECK_LATEST "http://bin.ceicer.com/svtplay-dl/Linux/version.txt"
#endif // Linux

#define SVTPLAYDL_ISSUES "https://github.com/spaam/svtplay-dl/issues"
#define DOWNLOAD_PATH                                                          \
    "https://gitlab.com/posktomten/streamcapture2/-/wikis/DOWNLOADS"
#define MANUAL_PATH "https://bin.ceicer.com/streamcapture2/help-current/index_"
//#define MANUAL_PATH "https://bin.ceicer.com/streamcapture2/help-current2/index_"
#define DISPLAY_NAME "streamCapture2"
#define EXECUTABLE_NAME "streamcapture2"
#define BUILD_DATE_TIME __DATE__ " " __TIME__
#define START_YEAR "2016"
#define CURRENT_YEAR __DATE__
#define LICENCE_VERSION "3"
#define PROGRAMMER_EMAIL "ic_streamcapture2@ceicer.com"
#define PROGRAMMER_NAME "Ingemar Ceicer"
#define PROGRAMMER_PHONE "+46706361747"
#define APPLICATION_HOMEPAGE                                                   \
    "https://gitlab.com/posktomten/streamcapture2/wikis/Home"
#define APPLICATION_HOMEPAGE_ENG                                               \
    "https://gitlab.com/posktomten/streamcapture2/wikis/Home"
#define SOURCEKODE "https://gitlab.com/posktomten/streamcapture2"
#define WIKI "https://gitlab.com/posktomten/streamcapture2/wikis/Home"
#define FFMPEG_HOMEPAGE "https://www.ffmpeg.org/";
#define SVTPLAYDL_HOMEPAGE "https://svtplay-dl.se/";
#define ITALIAN_TRANSLATER "bovirus"
#ifdef EXPERIMENTAL
#define EXPERIMENTAL_VERSION_LINK "\"https://bin.ceicer.com/streamcapture2/bin\""
#endif // Linux
#define SIZE_OF_RECENT 50
#define RED 218
#define GREEN 255
#define BLUE 221

#ifdef Q_OS_LINUX
//#define COMPILEDON QSysInfo::kernelType() + ' ' + QSysInfo::prettyProductName() + ' ' + QSysInfo::currentCpuArchitecture()
#ifdef Q_PROCESSOR_X86_64 // 64 bit
#ifndef HIGH_GLIBC
#define COMPILEDON "Lubuntu 18.04.6 LTS 64-bit, GLIBC 2.27"
#endif
#ifdef HIGH_GLIBC
#define COMPILEDON "Ubuntu 20.04.4 LTS 64-bit, GLIBC 2.31"
#endif
#endif
#ifdef Q_PROCESSOR_X86_32 // 32 bit
#define COMPILEDON "Lubuntu 18.04.6 LTS 32-bit, GLIBC 2.27"
#endif
#endif

#ifdef Q_OS_WINDOWS
//#define COMPILEDON QSysInfo::kernelType() + ' ' + QSysInfo::prettyProductName() + ' ' + QSysInfo::currentCpuArchitecture()
#ifdef Q_PROCESSOR_X86_64 // 64 bit
#define COMPILEDON "Microsoft Windows 10 Pro<br>Version: 21H2<br>OS build: 19044.1682<br>System type: 64-bit operating system, x64-based processor"
#endif
#ifdef Q_PROCESSOR_X86_32 // 32 bit
//#define COMPILEDON "Windows 10 Version 2009 i386"
#define COMPILEDON "Microsoft Windows 10 Pro<br>Version: 21H2<br>OS build: 19044.1682<br>System type: 32-bit operating system, x86-based processor"

#endif
#endif


#ifdef Q_OS_LINUX

#ifndef HIGH_GLIBC // not HIGH_GLIBC

#ifdef FFMPEG
#ifdef Q_PROCESSOR_X86_32 // 32 bit
#define ARG1 "streamcapture2-ffmpeg-i386.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/streamcapture2-ffmpeg-i386.AppImage.zsync"
#endif
#ifdef Q_PROCESSOR_X86_64 // 64 bit

#define ARG1 "streamcapture2-ffmpeg-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/streamcapture2-ffmpeg-x86_64.AppImage.zsync"
#endif
#endif

#ifndef FFMPEG
#ifdef Q_PROCESSOR_X86_32 // 32 bit
#define ARG1 "streamcapture2-i386.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/streamcapture2-i386.AppImage.zsync"
#endif
#ifdef Q_PROCESSOR_X86_64 // 64 bit
#define ARG1 "streamcapture2-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/streamcapture2-x86_64.AppImage.zsync"
#endif
#endif

#endif // not HIGH_GLIBC

#ifdef HIGH_GLIBC // HIGH_GLIBC
#ifndef FFMPEG
#ifdef ZSYNC2// zsync2
#define ARG1 "streamcapture2-x86_64.AppImage"
#define ARG2 "https://bin.ceicer.com/zsync2/HIGH_GLIBC/streamcapture2-x86_64.AppImage.zsync"

#endif
#ifndef ZSYNC2// NOT zsync2
#define ARG1 "streamcapture2-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/HIGH_GLIBC/streamcapture2-x86_64.AppImage.zsync"
#endif
#endif
#ifdef FFMPEG
#ifdef ZSYNC2// zsync2
#define ARG1 "streamcapture2-ffmpeg-x86_64.AppImage"
#define ARG2 "https://bin.ceicer.com/zsync2/HIGH_GLIBC/streamcapture2-ffmpeg-x86_64.AppImage.zsync"
#endif
#ifndef ZSYNC2// NOT zsync2
#define ARG1 "streamcapture2-ffmpeg-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/HIGH_GLIBC/streamcapture2-ffmpeg-x86_64.AppImage.zsync"
#endif
#endif
#endif // HIGH_GLIBC

#endif // Linux


class Info : public QWidget
{
    Q_OBJECT
public:
    void getSystem(int x, int y);
};

#endif // INFO_H
