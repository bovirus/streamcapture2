// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>


#include "newprg.h"
// DOWNLOAD ALL FROM DOWNLOAD LIST
void Newprg::downloadAll()
{
    if(!fileExists(svtplaydl)) {
        ui->teOut->setTextColor(QColor("red"));
#ifdef Q_OS_WIN
        ui->teOut->setText("svtplay-dl.exe " + tr("cannot be found or is not an executable program."));
        ui->teOut->append(tr("Please click on \"Tools\" and select svtplay-dl.exe."));
#endif
#ifdef Q_OS_LINUX
        ui->teOut->setText("svtplay-dl " + tr("cannot be found or is not an executable program."));
        ui->teOut->append(tr("Please click on \"Tools\" and select svtplay-dl."));
#endif
        ui->teOut->setTextColor(QColor("black"));
        return;
    }

    QStringList ARG;
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    avbrutet = false;
    ui->teOut->setTextColor(QColor("black"));
    QString save_all_path;
    settings.beginGroup("Path");
    QString savepath =
        settings.value("savepath").toString();
    QString copypath = settings.value("copypath").toString();
    settings.endGroup();

    if(ui->actionDownloadToDefaultLocation->isChecked()) {
        /* */
        const QFileInfo downloadlocation(QDir::toNativeSeparators(savepath));

        if(!downloadlocation.exists()) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("The default folder for downloading video streams cannot be "
                              "found.\nDownload is interrupted."));
//            msgBox.addButton(QMessageBox::Ok);
//            msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            ui->teOut->clear();
            return;
        }

        if(!downloadlocation.isWritable()) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("You do not have the right to save to the "
                              "folder.\nDownload is interrupted."));
//            msgBox.addButton(QMessageBox::Ok);
//            msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            ui->teOut->clear();
            return;
        }

        /* */
        save_all_path = savepath;
    } else {
        save_all_path = save(tr("Download streaming media to directory"));
    }

    if(save_all_path == "nothing") {
        ui->teOut->clear();
        return;
    }

#ifdef Q_OS_WIN
    // NTFS
    qt_ntfs_permission_lookup++; // turn checking on
#endif
    QFileInfo fi(save_all_path);

    if(!fi.isWritable()) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("You do not have the right to save to the "
                          "folder.\nDownload is interrupted."));
//        msgBox.addButton(QMessageBox::Ok);
//        msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
        ui->teOut->clear();
        return;
#ifdef Q_OS_WIN
        qt_ntfs_permission_lookup--; // turn checking of
#endif
        return;
    }

#ifdef Q_OS_WIN
    qt_ntfs_permission_lookup--; // turn checking of
#endif
    // Om lika filnamn
    QStringList temp;
    bool ischecked = ui->actionCreateFolder->isChecked();

    if(!ischecked) {
        for(int i = 0; i < downloadList.size(); i++) {
            QStringList list = downloadList.at(i).split(",");
            temp << list.at(0);
        }

        int lika = temp.removeDuplicates();

        if(lika > 0) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Information);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("You have chosen to download more than one file with the same name. "
                              "In order not to overwrite files, folders will be created for each file."));
//            msgBox.addButton(QMessageBox::Ok);
//            msgBox.addButton(QMessageBox::Cancel);
//            msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
//            msgBox.setButtonText(QMessageBox::Cancel, tr("Cancel"));
//            msgBox.setDefaultButton(QMessageBox::Cancel);
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            QPushButton *but = msgBox.addButton(tr("Cancel"), QMessageBox::RejectRole);
            msgBox.setDefaultButton(but);

            if(msgBox.exec() == QMessageBox::RejectRole) {
                return;
            }

            ui->actionCreateFolder->setChecked(true);
            ui->actionCreateFolder->setDisabled(true);;
        }
    }

    QString kopierastill;

    if(ui->actionCopyToDefaultLocation->isChecked()) {
        const QFileInfo copypathlocation(copypath);

        if(!copypathlocation.exists()) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("The default folder for copying video streams "
                              "cannot be found.\nDownload is interrupted."));
//            msgBox.addButton(QMessageBox::Ok);
//            msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            ui->teOut->clear();
            return;
        } else if(!copypathlocation.isWritable()) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("You do not have the right to save to the folder.\nDownload is interrupted."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            ui->teOut->clear();
            return;
        } else {
            kopierastill = tr("Selected folder to copy to is ") + QDir::toNativeSeparators("\"" + copypath) + "\"";
        }
    }

    if(save_all_path != "nothing") {
        ui->teOut->setReadOnly(true);
//        QStringList allDownloads;
        // ipac
        QVector<QString> allDownloads;

        foreach(QString listitems, downloadList) {
            int hittat = listitems.indexOf(',', 0);
            allDownloads.append(listitems.left(hittat));
        }

        ui->teOut->clear();
        ui->teOut->setText(tr("The video streams are saved in") + " \"" +
                           QDir::toNativeSeparators(save_all_path) + "\"");
        downloadList.removeDuplicates();
        QString preferred, quality, provider, sub, folderaddress, stcookie, amount, resolution;
        antalnedladdade = downloadList.size();
        QString *save_all_path2 = new QString;

// loop
        for(int antal = 0; antal < downloadList.size(); antal++) {
            ARG << "--force";
            ARG << "--verbose";
            QStringList list = downloadList.at(antal).split(",");
            address = list.at(0);
            preferred = list.at(1);
            preferred = preferred.trimmed();
            preferred = preferred.toLower();
            quality = list.at(2);
            quality = quality.trimmed();
            provider = list.at(3);
            sub = list.at(4);
            stcookie = list.at(5);
            amount = list.at(6);
            resolution = list.at(7);
            resolution = resolution.trimmed();
            folderaddress = address;
            QString username, password;

            if(provider != "npassword") {
// cppcheck
//                QSettings settings(QSettings::IniFormat, QSettings::UserScope,
//                                   DISPLAY_NAME, EXECUTABLE_NAME);
//                // settings.setIniCodec("UTF-8");
                settings.beginGroup("Provider");
                username = settings.value(provider + "/username").toString();
                password = settings.value(provider + "/password").toString();
                settings.endGroup();
                {
                    if(password == "") {
                        bool ok;
                        QInputDialog inputdialog;
                        inputdialog.setOkButtonText(tr("Ok"));
                        inputdialog.setCancelButtonText(tr("Cancel"));
                        inputdialog.setWindowTitle(tr("Enter your password"));
                        inputdialog.setInputMode(QInputDialog::TextInput);
                        inputdialog.setTextEchoMode(QLineEdit::Password);
                        inputdialog.setLabelText(tr("Spaces are not allowed. Use only the characters\nyour streaming provider approves."));
                        ok = inputdialog.exec();
                        QString newpassword = inputdialog.textValue();

                        if(newpassword.indexOf(' ') >= 0) {
                            delete save_all_path2;
                            return;
                        }

                        if(ok) {
                            secretpassword = newpassword;
                            password = newpassword;
                        } else {
                            delete save_all_path2;
                            return;
                        }
                    }
                }
            }

            CommProcess2 = new QProcess(this);
            QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
            const QString path =
                QDir::toNativeSeparators(QCoreApplication::applicationDirPath());
            const QString oldpath = env.value("PATH");
#ifdef Q_OS_LINUX
            env.insert("PATH", path + ":" + oldpath); // Add an environment variable
#endif
#ifdef Q_OS_WIN                                 // QT5
            env.insert("PATH", path + ";" + oldpath); // Add an environment variable
#endif
            CommProcess2->setProcessEnvironment(env);
            CommProcess2->setProcessChannelMode(QProcess::MergedChannels);
            /* MAKE FOLDER */
            int hittat = folderaddress.lastIndexOf('/');
            folderaddress = folderaddress.mid(hittat);
            QDir dir;
            QString ratt;

            if(ui->actionCreateFolder->isChecked()) {
                dir.setPath(save_all_path + folderaddress + "/" + preferred + "_" + quality + "_" + amount + '_' + resolution);

                if(!dir.exists()) {
                    dir.mkpath(".");
                }

                *save_all_path2 = dir.path();
                ratt = *save_all_path2;
            } else {
                dir.setPath(save_all_path);
                *save_all_path2 = save_all_path;
                ratt = *save_all_path2;
            }

            // ALLTID FOLDER
            /*     */

            if(!dir.exists()) {
                dir.mkpath(".");
            }

            /*    */

            if(ui->actionCopyToDefaultLocation->isChecked()) {
                ui->teOut->append(kopierastill);
            }

            CommProcess2->setWorkingDirectory(
                QDir::toNativeSeparators(*save_all_path2));

            /* END MAKE FOLDER */

            if(stcookie != "nst") {
                ARG << "--cookies"  << "st=" + stcookie;
            }

            if(provider != "npassword") {
                ARG << "--username" << username << "--password" << password;
            }

            if(sub == "ysub") {  // Subtitles
                ARG << "--subtitle";
            }

            if(preferred != "unknown" && quality != "unknown") {
                if(amount != "unknown") {
                    ARG << "--quality" << quality << "--preferred" << preferred << "--flexible-quality" << amount;
                } else {
                    ARG << "--quality" << quality << "--preferred" << preferred;
                }
            }

            if(resolution != "unknown") {
                ARG << "--resolution" << resolution;
            }

            ARG << address;
            ui->teOut->append(tr("Preparing to download") + " \"" + address + "\"");
            connect(CommProcess2,
                    QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            [ = ](int exitCode, QProcess::ExitStatus exitStatus) {
                statusExit(exitStatus, exitCode);
                QString filnamn = folderaddress.mid(1);
                QString fil;

                if(ui->actionCreateFolder->isChecked()) {
                    fil = ratt + folderaddress + ".mp4";
                } else {
                    fil = ratt + "/" + filnamn + ".mp4";
                }

                if(!avbrutet) {
                    if(QFile::exists(fil)) {
                        ui->teOut->setTextColor(QColor("darkBlue"));
                        ui->teOut->append(tr("Download succeeded") + " \"" +  filnamn + ".mp4" + " \"" + preferred + ", " + quality + ", " + amount + ", " + resolution + "\"");
                        ui->progressBar->setValue(10);
                        int langd = fil.size();
                        QString path_undertextfilen =
                            fil.replace(langd - 3, 3, "srt");

                        if(QFile::exists(path_undertextfilen)) {
                            ui->teOut->append(tr("Download succeeded") + " \"" + filnamn + ".srt");
                            ui->progressBar->setValue(10);
                        }

                        ui->teOut->setTextColor(QColor("black"));
// COPY
                        QString *copyto = new QString;

                        if(ui->actionCopyToDefaultLocation->isChecked()) {
                            if(ui->actionCreateFolder->isChecked()) {
                                *copyto = folderaddress + "/" + preferred + '_' + quality + '_' + amount + '_' + resolution;
                                QString *hela = new QString;
                                *hela = savepath + *copyto;
                                copyToDefaultLocation(filnamn + ".mp4", hela, copyto);
                            } else {
                                *copyto = "";
                                copyToDefaultLocation(filnamn + ".mp4", &savepath, copyto);
                            }
                        }

                        delete copyto;
                    }  else {
                        ui->teOut->setTextColor(QColor("red"));
                        ui->teOut->append(tr("The download failed. If a username and password or 'st' cookie is required, you must enter these.") + "\n" +  filnamn + ".mp4" + " (" + preferred + ", " + quality + ", " + amount + ")");
                        ui->teOut->setTextColor(QColor("black"));

                        if(!ui->actionNotifications->isChecked()) {
                            // showNotification
                            showNotification(tr("The download failed. If a username and password or 'st' cookie is required, you must enter these.") + "\n" +  filnamn + ".mp4" + " (" + preferred + ", " + quality + ", " + amount + ")", 3);
                        }
                    }

                    avbrutet = false;
                }

                antalnedladdade--;

                if(antalnedladdade == 0) {
                    ui->teOut->append(tr("Download completed"));
                    ui->actionCreateFolder->setChecked(ischecked);
                    ui->actionCreateFolder->setEnabled(true);
                    ui->progressBar->setValue(0);
                    ui->teOut->setTextColor(QColor("black"));

                    if(!ui->actionNotifications->isChecked()) {
                        // showNotification
                        showNotification(tr("Download completed"), 1);
                    }

                    processpid = 0;
                }

                /* ... */
            });
            connect(CommProcess2, SIGNAL(started()), this,
                    SLOT(onCommProcessStart()));
            // HIT
            ARG << "--output" << ratt + folderaddress;
            CommProcess2->start(svtplaydl, ARG);
            ARG.clear();
            processpid = CommProcess2->processId();
            connect(CommProcess2, &QProcess::readyReadStandardOutput, [this]() {
                QString result(CommProcess2->readAllStandardOutput());

                if(ui->actionShowMore->isChecked()) {
                    ui->teOut->setTextColor(QColor("purple"));
                    ui->teOut->append(result);
                    ui->teOut->setTextColor(QColor("black"));

                    // progressbar
                    if(ui->progressBar->value() >= 100) {
                        ui->progressBar->setValue(0);
                    }

                    ui->progressBar->setValue(ui->progressBar->value() + 1);
                } else {
                    // progressbar
                    if(ui->progressBar->value() >= 100) {
                        ui->progressBar->setValue(0);
                    }

                    if(result.trimmed().contains("Merge audio and video")) {
                        ui->teOut->append(tr("Merge audio and video..."));
                    }

                    if(result.trimmed().contains("removing old files")) {
                        ui->teOut->append(tr("Removing old files, if there are any..."));
                    }

                    ui->progressBar->setValue(ui->progressBar->value() + 1);
                }
            });
        }

        delete save_all_path2;
        // END LOOP
    }
}
