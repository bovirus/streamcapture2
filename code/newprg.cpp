// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"

Newprg::Newprg(QWidget *parent) : QMainWindow(parent), ui(new Ui::newprg)
{
//    this->setAttribute(Qt::WA_DeleteOnClose);
//    qDebug() << "1 " << QSysInfo::buildAbi();
//    qDebug() << "2 " << QSysInfo::buildCpuArchitecture();  // This
//    qDebug() << "3 " << QSysInfo::currentCpuArchitecture();
//    qDebug() << "4 " << QSysInfo::kernelType();
//    qDebug() << "5 " << QSysInfo::kernelVersion();
//    qDebug() << "6 " << QSysInfo::machineHostName();
//    qDebug() << "7 " << QSysInfo::machineUniqueId();
//    qDebug() << "8 " << QSysInfo::prettyProductName(); // And this
//    qDebug() << "9 " << QSysInfo::productType();
//    qDebug() << "10 " << "__TIMESTAMP__ " << __TIMESTAMP__;
//    qDebug() << "11 " << QSysInfo::kernelType() + ' ' + QSysInfo::prettyProductName() + ' ' + QSysInfo::currentCpuArchitecture();
//    qDebug()<< "12 "  << QSysInfo::windowsVersion();
//    qDebug() << "13 " << QT_VERSION_STR;
    ui->setupUi(this);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    QSettings settings2(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                        LIBRARY_NAME);
//    settings.beginGroup("FirstRun");
//    bool firstrun = settings.value("firstrun", true).toBool();
//    settings.endGroup();
    setStartConfig();

    if(ui->actionCheckSvtplayDlForUpdatesAtStart->isChecked()) {
        checkSvtplaydlAtStart();
    }

#ifdef Q_OS_LINUX
    QCoreApplication::setAttribute(Qt::AA_DontUseNativeDialogs);
#endif
    zoom();
    connect(ui->actionNfoInfo, &QAction::triggered, [this]() {
        nfo();
    });
    connect(ui->actionCheckLatestsvtplaydl, &QAction::triggered, [this]() {
        checkSvtplaydlForUpdatesHelp();
    });
    connect(ui->actionStreamCapture2Settings, &QAction::triggered, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
        QString inifile = settings.fileName();
        QFile fil(inifile);
        QString *content = new QString;
        content->clear();

        if(fil.open(QIODevice::ReadOnly)) {
            QTextStream in(&fil);

            while(!in.atEnd()) {
                *content += in.readLine() + "<br>";
            }

            fil.close();
        }

        // ipac1
        QStringList *infoList = new QStringList;
        *infoList << tr("Edit streamCapture2 settings");
        DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
        connect(mDownloadListDialog, &DownloadListDialog::sendContent, this, &Newprg::getSettings);
        mDownloadListDialog->initiate(infoList, content, false);
    });
    connect(ui->actionDownloadSvtplayDlSettings, &QAction::triggered, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, LIBRARY_NAME);
        QString inifile = settings.fileName();
        QFile fil(inifile);
        QString *content = new QString;
        content->clear();

        if(fil.open(QIODevice::ReadOnly)) {
            QTextStream in(&fil);

            while(!in.atEnd()) {
                *content += in.readLine() + "<br>";
            }

            fil.close();
        }

        // ipac1
        QStringList *infoList = new QStringList;
        *infoList << tr("Edit download svtplay-dl settings");
        DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
        connect(mDownloadListDialog, &DownloadListDialog::sendContent, this, &Newprg::getSettingsDownloadunpack);
        mDownloadListDialog->initiate(infoList, content, false);
    });
    /*  */
    /* KOLLAR VILKEN svtplay-dl SOM SKA ANVÄNDAS */
#ifdef Q_OS_WIN // Windows 32- and 64-bit
#ifndef PORTABLE
    ui->actionApplicationsMenuShortcut->setVisible(false);
#endif

    if(ui->actionSvtplayDlStable->isChecked()) {
        svtplaydl = QDir::toNativeSeparators(
                        QCoreApplication::applicationDirPath() + "/stable/svtplay-dl.exe");
    } else if(ui->actionSvtplayDlSystem->isChecked()) {
        QString path = findSvtplayDl();

        if(path == QStringLiteral(u"NOTFIND")) {
//            ui->teOut->setText(tr("svtplay-dl.exe is not found in the system path."));
//            ui->teOut->append(
//                tr("You can use svtplay-dl.exe that comes with streamCapture2."));
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("svtplay-dl is not found in the system path.\nYou can download any svtplay-dl of your choice."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
        } else {
            svtplaydl = path;
        }

        svtplaydl = QDir::toNativeSeparators(QStringLiteral(u"svtplay-dl.exe"));
    } else if(ui->actionSvtPlayDlManuallySelected->isChecked()) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Path");
        QString svtplaydlpath =
            settings.value("svtplaydlpath", "NOTFIND").toString();
        settings.endGroup();
        svtplaydl = QDir::toNativeSeparators(svtplaydlpath);
    }

#endif // Q_OS_WINDOWS

    if(ui->actionSvtplayDlStable->isChecked()) {
        settings2.beginGroup("Path");
        QString stablepath = settings2.value("stablepath").toString();
        settings2.endGroup();
#ifdef Q_OS_WINDOWS
        svtplaydl = stablepath + "/stable/svtplay-dl.exe";
#endif
#ifdef Q_OS_LINUX
        svtplaydl = stablepath + "/stable/svtplay-dl";
#endif
    }  else if(ui->actionSvtPlayDlManuallySelected->isChecked()) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Path");
        QString svtplaydlpath =
            settings.value(QStringLiteral(u"svtplaydlpath"), "NOTFIND").toString();
        settings.endGroup();
        svtplaydl = QDir::toNativeSeparators(svtplaydlpath);
    } else if(ui->actionSvtplayDlBleedingEdge->isChecked()) {
        settings2.beginGroup("Path");
        QString betapath = settings2.value("betapath").toString();
        settings2.endGroup();
#ifdef Q_OS_WINDOWS
        svtplaydl = betapath + "/beta/svtplay-dl.exe";
#endif
#ifdef Q_OS_LINUX
        svtplaydl = betapath + "/beta/svtplay-dl";
#endif
    } else {
        settings2.beginGroup("Path");
        QString stablepath = settings2.value("stablepath", "svtplay-dl").toString();
        settings2.endGroup();
#ifdef Q_OS_WINDOWS
        svtplaydl = stablepath + "/stable/svtplay-dl.exe";
#endif
#ifdef Q_OS_LINUX
        svtplaydl = stablepath + "/stable/svtplay-dl";
#endif

        if(!QFile::exists(svtplaydl)) {
            ui->teOut->setTextColor(QColor("red"));
            ui->teOut->setText("svtplay-dl" +
                               tr(" cannot be found or is not an executable program."));
            ui->teOut->append(tr("Please click on \"Tools\" and select svtplay-dl..."));
            ui->teOut->setTextColor(QColor("black"));
        }
    }

    // Download from ceicer
    connect(ui->actionDownloadSvtplayDlFromBinCeicerCom, &QAction::triggered, this, &Newprg::downloadFromCeicer);
// File
    connect(ui->actionSelectFont, &QAction::hovered, [this]() {
        QFont f = ui->teOut->currentFont();
        QString sf = f.family();
        int size = f.pointSize();
        bool bold = f.bold();
        bool italic = f.italic();
        QString stil = " , " + tr("normal");

        if(bold && italic) {
            stil = " , " + tr("bold and italic");
        } else {
            if(bold) {
                stil = ", " + tr("bold");
            } else if(italic) {
                stil = " , " + tr("italic");
            }
        }

        ui->actionSelectFont->setStatusTip(tr("Current font:") + " \"" + sf +
                                           "\", " + tr("size:") + " " +
                                           QString::number(size) + stil);
    });
    connect(ui->pbPast, &QPushButton::clicked, [this]() {
        ui->teOut->setReadOnly(true);
        QClipboard *clipboard = QApplication::clipboard();
        QString originalText = clipboard->text();
        ui->leSok->setText(originalText);
    });
    connect(ui->actionPast, &QAction::triggered, [this]() {
        ui->teOut->setReadOnly(true);
        QClipboard *clipboard = QApplication::clipboard();
        QString originalText = clipboard->text();
        ui->leSok->setText(originalText);
    });
    connect(ui->actionExit, &QAction::triggered, [this]() {
        close();
    });
// Recent
    connect(ui->menuRecent, SIGNAL(aboutToShow()), this,
            SLOT(slotRecent())); // newprg.cpp
// Download List
    connect(ui->actionViewDownloadList, &QAction::triggered, [this]() {
        QStringList *infoList = new QStringList;
        QString *content = new QString;
        content->clear();
        downloadList.removeDuplicates();

        for(const QString &s : qAsConst(downloadList)) {
            *content += s + "<br>";
        }

        *infoList << tr("View download list");
        DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
        connect(mDownloadListDialog, &DownloadListDialog::sendContent, this, &Newprg::getDownloadList);
        mDownloadListDialog->initiate(infoList, content, true);
    });
    // EDIT DOWNLOAD LIST
//    connect(ui->actionEditDownloadList, &QAction::hovered, [this]() {
//        ui->teOut->setReadOnly(false);
//    });
    connect(ui->actionEditDownloadList, &QAction::triggered, [this]() {
        QStringList *infoList = new QStringList;
        QString *content = new QString;
        content->clear();
        downloadList.removeDuplicates();

        for(const QString &s : qAsConst(downloadList)) {
            *content += s + "<br>";
        }

        *infoList << tr("Edit download list");
        DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
        connect(mDownloadListDialog, &DownloadListDialog::sendContent, this, &Newprg::getDownloadList);
        mDownloadListDialog->initiate(infoList, content, false);
    });
    connect(ui->actionDeleteDownloadList, &QAction::triggered, [this]() {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("The list of video streams to download will be deleted and can not be "
                          "restored.\nDo you want to continue?"));
//        msgBox.addButton(QMessageBox::Yes);
//        msgBox.addButton(QMessageBox::No);
//        msgBox.setDefaultButton(QMessageBox::No);
//        msgBox.setButtonText(QMessageBox::Yes, tr("Yes"));
//        msgBox.setButtonText(QMessageBox::No, tr("No"));
//        msgBox.setDefaultButton(QMessageBox::No);
        msgBox.addButton(tr("Yes"), QMessageBox::AcceptRole);
        QPushButton *but = msgBox.addButton(tr("No"), QMessageBox::RejectRole);
        msgBox.setDefaultButton(but);

        if(msgBox.exec() == QMessageBox::AcceptRole) {
            downloadList.clear();
            ui->pbDownloadAll->setDisabled(true);
            ui->actionDownloadAll->setDisabled(true);
            ui->teOut->setReadOnly(true);
        }
    });
#ifdef Q_OS_WIN
    connect(ui->actionDownloadMicrosoftRuntime, &QAction::triggered, this, &Newprg::downloadRuntime);
#endif
    connect(ui->actionCheckOnStart, &QAction::triggered, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Update");

        if(ui->actionCheckOnStart->isChecked()) {
            settings.setValue("checkonstart", "true");
        } else {
            settings.setValue("checkonstart", "false");
        }

        settings.endGroup();
    });
    connect(ui->actionCheckSvtplayDlForUpdatesAtStart, &QAction::triggered, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Update");

        if(ui->actionCheckSvtplayDlForUpdatesAtStart->isChecked()) {
            settings.setValue("checksvtplaydlonstart", "true");
        } else {
            settings.setValue("checksvtplaydlonstart", "false");
        }

        settings.endGroup();
    });
    connect(ui->actionShowMore, &QAction::triggered, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Settings");

        if(ui->actionShowMore->isChecked()) {
            settings.setValue("showmore", "true");
        } else {
            settings.setValue("showmore", "false");
        }

        settings.endGroup();
    });
    connect(ui->actionCreateFolder, &QAction::triggered, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Settings");

        if(ui->actionCreateFolder->isChecked()) {
            settings.setValue("createfolder", "true");
        } else {
            settings.setValue("createfolder", "false");
        }

        settings.endGroup();
    });
// Help
    connect(ui->actionHelp, &QAction::triggered, []() -> void {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Language");
        QString sp = settings.value("language", "").toString();
        settings.endGroup();

        if(sp == "")
        {
            sp = QLocale::system().name();
        } else if(sp == "de_DE")
        {
            sp = "en_US";
        }

        QDesktopServices::openUrl(QUrl(MANUAL_PATH + sp + ".html", QUrl::TolerantMode));


    });
#ifdef Q_OS_WIN
    connect(ui->actionCheckForUpdates, &QAction::triggered, [ = ]() {
#endif
#ifdef Q_OS_LINUX
        connect(ui->actionCheckForUpdates, &QAction::triggered, [this]() {
            QString *updateinstructions =
//                new QString(tr("Please click on \"Tools\" and \"Download svtplay-dl...\""));
                new QString(tr("Please click on \"Tools\" and \"Update\""));
#endif
#ifdef Q_OS_WIN
#ifndef PORTABLE
            QString *updateinstructions = new QString(
                tr("Select \"Tools\", \"Maintenance Tool\" and \"Update components\"."));
#else
            QString *updateinstructions = new QString(
                tr("Download a new") + " <a href=\"" DOWNLOAD_PATH "\"> portable</a>");
#endif
#endif
            auto *cfu = new CheckUpdate;
            cfu->check(DISPLAY_NAME, VERSION, VERSION_PATH, *updateinstructions);
            connect(cfu, &CheckUpdate::foundUpdate, [ = ](bool isupdated) {
                QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                                   EXECUTABLE_NAME);
                // settings.setIniCodec("UTF-8");
                settings.beginGroup("Update");

                if(isupdated) {
                    settings.setValue("readyupdate", true);
#ifdef Q_OS_LINUX
                    ui->actionMaintenanceTool->setEnabled(true);
#endif
                } else {
                    settings.setValue("readyupdate", false);
#ifdef Q_OS_LINUX
                    ui->actionMaintenanceTool->setDisabled(true);
#endif
                }

                settings.endGroup();
            });
        });
        // about
        connect(ui->actionVersionHistory, SIGNAL(triggered()), this,
                SLOT(versionHistory())); // about.cpp
        connect(ui->actionAbout, SIGNAL(triggered()), this,
                SLOT(about())); // about.cpp
        connect(ui->actionAboutSvtplayDl, SIGNAL(triggered()), this,
                SLOT(aboutSvtplayDl())); // about.cpp
        /* About svtplay-dl HOOVER */
        connect(ui->actionAboutSvtplayDl, &QAction::hovered, [this]() -> void {
            if(ui->actionSvtplayDlSystem->isChecked())
            {
                QString path = findSvtplayDl();

                if(path != QStringLiteral(u"NOTFIND")) {
#ifdef Q_OS_LINUX
                    ui->actionAboutSvtplayDl->setStatusTip(
                        tr("svtplay-dl is in the system path."));
                    svtplaydl = path;
#endif
#ifdef Q_OS_WIN
                    ui->actionAboutSvtplayDl->setStatusTip(
                        tr("svtplay-dl.exe is in the system path."));
                    svtplaydl = path;
#endif
                } else {
#ifdef Q_OS_LINUX
                    ui->actionAboutSvtplayDl->setStatusTip(
                        tr("ERROR! svtplay-dl is not in the system path."));
#endif
#ifdef Q_OS_WIN
                    ui->actionAboutSvtplayDl->setStatusTip(
                        tr("ERROR! svtplay-dl.exe is not in the system path."));
#endif
                }

                return;
            }

            if(fileExists(svtplaydl))
            {
#ifdef Q_OS_LINUX
                ui->actionAboutSvtplayDl->setStatusTip(tr("Path to svtplay-dl: ") + "\"" +
                                                       svtplaydl + "\"");
#endif
#ifdef Q_OS_WIN
                ui->actionAboutSvtplayDl->setStatusTip(tr("Path to svtplay-dl.exe: ") +
                                                       "\"" + svtplaydl + "\"");
#endif
            } else
            {
#ifdef Q_OS_LINUX
                ui->actionAboutSvtplayDl->setStatusTip(
                    tr("ERROR! svtplay-dl could not be found."));
#endif
#ifdef Q_OS_WIN
                ui->actionAboutSvtplayDl->setStatusTip(
                    tr("ERROR! svtplay-dl.exe could not be found."));
#endif
            }
        });
        connect(ui->actionSvtplayDlForum, &QAction::triggered, []() -> void {
            QDesktopServices::openUrl(
                QUrl(SVTPLAYDL_ISSUES, QUrl::TolerantMode));
        });
        connect(ui->actionAboutFfmpeg, SIGNAL(triggered()), this,
                SLOT(aboutFfmpeg())); // about.cpp
        /* HOOVERED ffmpeg */
        connect(ui->actionAboutFfmpeg, &QAction::hovered, [this]() -> void {
            QString pathtoffmpeg = selectFfmpeg();


            if(pathtoffmpeg == "NOTFIND")
            {
                ui->actionAboutFfmpeg->setStatusTip(tr("ERROR! FFmpeg cannot be found."));
            } else
            {
#ifdef Q_OS_WIN
                ui->actionAboutFfmpeg->setStatusTip(tr("Path to ffmpeg.exe: ") + "\"" + QDir::toNativeSeparators(pathtoffmpeg) + "\"");
#endif
#ifdef Q_OS_LINUX
                ui->actionAboutFfmpeg->setStatusTip(tr("Path to ffmpeg: ") + "\"" + QDir::toNativeSeparators(pathtoffmpeg) + "\"");
#endif
            }
        });
        connect(ui->actionLicense, SIGNAL(triggered()), this,
                SLOT(license())); // about.cpp
#ifdef FFMPEG
        connect(ui->actionLicenseFfmpeg, SIGNAL(triggered()), this,
                SLOT(licenseFfmpeg())); // about.cpp
#endif
        connect(ui->actionLicenseSvtplayDl, SIGNAL(triggered()), this,
                SLOT(licenseSvtplayDl())); // about.cpp
#ifdef Q_OS_WIN
        connect(ui->actionLicense7zip, SIGNAL(triggered()), this,
                SLOT(license7zip())); // about.cpp
#endif
        // Language
        connect(ui->actionSwedish, SIGNAL(triggered()), this,
                SLOT(swedish())); // language.cpp
        connect(ui->actionEnglish, SIGNAL(triggered()), this,
                SLOT(english())); // language.cpp
        connect(ui->actionItalian, SIGNAL(triggered()), this,
                SLOT(italian())); // language.cpp
        // Search and download
        connect(ui->pbSok, SIGNAL(pressed()), this, SLOT(initSok())); // download.cpp
        connect(ui->actionSearch, SIGNAL(triggered()), this,
                SLOT(initSok())); // download.cpp
        connect(ui->pbSok, SIGNAL(clicked()), this, SLOT(sok()));
        connect(ui->actionSearch, SIGNAL(triggered()), this, SLOT(sok()));
        // list all
        connect(ui->actionListAllEpisodes, SIGNAL(triggered()), this,
                SLOT(listAllEpisodes()));
        //
        connect(ui->pbDownload, SIGNAL(clicked()), this, SLOT(download()));
        connect(ui->actionDownload, SIGNAL(triggered()), this, SLOT(download()));
        connect(ui->pbAdd, &QPushButton::clicked, ui->actionAdd, &QAction::triggered);
        // ADD ALL EPISODES
        connect(ui->actionAddAllEpisodesToDownloadList, &QAction::triggered, [ = ]() {
            QString in, provider, sub;

            if((ui->comboPayTV->currentText().indexOf("-", 0)) == 0) {
                provider = QStringLiteral(u"npassword");
            } else {
                provider = ui->comboPayTV->currentText();
            }

            if(ui->chbSubtitle->isChecked()) {
                sub = QStringLiteral(u"ysub");
            } else {
                sub = QStringLiteral(u"nsub");
            }

            QString soktext = ui->leSok->text();

            //  Bugg V0.15.0
            if(soktext.right(1) == '/') {
                int storlek = soktext.size();
                soktext.remove(storlek - 1, 1);
            }

            /* ADD 'st' COOKIE TO DOWNLOAD LIST */
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Stcookies");
            QString stcookie = settings.value("stcookie", "").toString();
            //settings.endGroup();

            if(stcookie.isEmpty()) {
                stcookie = "nst";
            }

            settings.value("stcookie", "").toString();
            settings.endGroup();
            int hittat = soktext.indexOf('?');
            soktext = soktext.left(hittat);
            QString unknown = QStringLiteral(u"unknown");

            if(ui->actionAddAllEpisodesToDownloadList->isEnabled()) {
                QString data = ui->teOut->toPlainText();
                QStringList addresses = data.split(QRegularExpression("[\n]"), Qt::SkipEmptyParts);
                QString resolution;

                switch(ui->comboResolution->currentIndex()) {
                    case 0:
                        resolution = "unknown";
                        break;

                    case 1:
                        resolution = "720";
                        break;

                    case 2:
                        resolution = "1080";
                }

                foreach(QString tmp, addresses) {
                    if((tmp.left(8) == QStringLiteral(u"https://")) || (tmp.left(7) == QStringLiteral(u"http://"))) {
                        in = tmp + "," + unknown + "," + unknown + "," + provider + "," + sub + "," + unknown + "," + stcookie + "," + resolution;
                        downloadList.append(in);
                    }
                }

                ui->pbDownloadAll->setEnabled(true);
                ui->actionDownloadAll->setEnabled(true);
            }
        });
        // ADD EPISODE
        connect(ui->actionAdd, &QAction::triggered, [ = ]() {
            QString resolution;

            switch(ui->comboResolution->currentIndex()) {
                case 0:
                    resolution = "unknown";
                    break;

                case 1:
                    resolution = "720";
                    break;

                case 2:
                    resolution = "1080";
            }

            QString in, provider, sub;

            if((ui->comboPayTV->currentText().indexOf(QStringLiteral(u"-"), 0)) == 0) {
                provider = QStringLiteral(u"npassword");
            } else {
                provider = ui->comboPayTV->currentText();
            }

            if(ui->chbSubtitle->isChecked()) {
                sub = QStringLiteral(u"ysub");
            } else {
                sub = QStringLiteral(u"nsub");
            }

            QString soktext = ui->leSok->text();

            //  Bugg V0.15.0
            if(soktext.right(1) == '/') {
                int storlek = soktext.size();
                soktext.remove(storlek - 1, 1);
            }

            int hittat = soktext.indexOf('?');
            soktext = soktext.left(hittat);
            /* ADD 'st' COOKIE TO DOWNLOAD LIST */
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Stcookies");
            QString stcookie = settings.value("stcookie", "").toString();
            settings.endGroup();

            if(stcookie.isEmpty()) {
                stcookie = "nst";
            }

            QString amount;

            if(ui->comboAmount->currentText().at(0) == QString("-")) {
                amount = "unknown";
            } else {
                amount = ui->comboAmount->currentText();
            }

            QString method, quality;

            if((ui->comboBox->currentText().at(0)) == QString("-")) {
                method = "unknown";
                quality = "unknown";
            } else {
                method = ui->leMethod->text();
                method = method.trimmed();
                method = method.toLower();
                quality = ui->leQuality->text();
                quality = quality.trimmed();
                quality = quality.toLower();
            }

            in = soktext + "," + method + "," + quality + "," + provider + "," + sub + "," + stcookie + "," + amount + "," + resolution;

            if(ui->pbAdd->isEnabled()) {
                downloadList.append(in);
            }

            ui->pbDownloadAll->setEnabled(true);
            ui->actionDownloadAll->setEnabled(true);
        });
        connect(ui->pbDownloadAll, SIGNAL(clicked()), this, SLOT(downloadAll()));
        connect(ui->actionDownloadAll, SIGNAL(triggered()), this,
                SLOT(downloadAll()));
        // comboBox
        connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this,
                SLOT(comboBoxChanged()));
        // Pay TV
        connect(ui->actionCreateNew, &QAction::triggered,
        [this]() {
            newSupplier();
        });
        connect(ui->comboPayTV, QOverload<int>::of(&QComboBox::currentIndexChanged),
        [ = ](int index) {
            if(index == 0) {
                ui->pbPassword->setEnabled(false);
                ui->actionPassword->setEnabled(false);
            } else {
                ui->pbPassword->setEnabled(true);
                ui->actionPassword->setEnabled(true);
            }
        });
        connect(ui->pbPassword, &QPushButton::clicked, [this]() {
            QString provider = ui->comboPayTV->currentText();
            bool ok;
            QString newpassword = QInputDialog::getText(
                                      nullptr,
                                      provider + " " +
                                      QCoreApplication::translate("MainWindow", "Enter your password"),
                                      QCoreApplication::translate(
                                          "MainWindow",
                                          "Spaces are not allowed. Use only the characters your streaming "
                                          "provider approves.\nThe Password will not be saved."),
                                      QLineEdit::Password, QLatin1String(""), &ok);

            if(newpassword.indexOf(' ') >= 0) {
                return false;
            }

            if(ok) {
                secretpassword = newpassword;
                return true;
            }

            return true;
        });
        connect(ui->actionPassword, &QAction::triggered, [this]() {
            QString provider = ui->comboPayTV->currentText();
            bool ok;
            QString newpassword = QInputDialog::getText(
                                      nullptr,
                                      provider + " " +
                                      QCoreApplication::translate("MainWindow", "Enter your password"),
                                      QCoreApplication::translate(
                                          "MainWindow",
                                          "Spaces are not allowed. Use only the characters your streaming "
                                          "provider approves.\nThe Password will not be saved."),
                                      QLineEdit::Password, "", &ok);

            if(newpassword.indexOf(' ') >= 0) {
                return false;
            }

            if(ok) {
                secretpassword = newpassword;
                return true;
            }

            return true;
        });
        // Subtitle
        connect(ui->actionSubtitle, &QAction::toggled, [this](bool checked) {
            if(checked) {
                ui->chbSubtitle->setChecked(true);
            } else {
                ui->chbSubtitle->setChecked(false);
            }
        });
        connect(ui->chbSubtitle, &QCheckBox::stateChanged, [this](int state) {
            if(state == Qt::Checked) {
                ui->actionSubtitle->setChecked(true);
            } else {
                ui->actionSubtitle->setChecked(false);
            }
        });
        connect(ui->actionDownloadAllEpisodes, &QAction::triggered,
        [this]() {
            downloadAllEpisodes();
        });
        /* Delete all settings */
        connect(ui->actionDeleteAllSettings, &QAction::triggered,
        [this]() {
            deleteAllSettings();
        });
#ifdef Q_OS_WIN // QT5
        connect(ui->actionStopAllDownloads, &QAction::triggered, [this]() {
            if(processpid > 0) {
                //#ifdef Q_OS_LINUX
                // ipac
                // int rv = QProcess::execute("kill -9 " + QString::number(processpid));
                // int rv = QProcess::execute("pkill svtplay-dl");
                // int rv = QProcess::startDetached("pkill", {"svtplay-dl"});
                // int rv = QProcess::startDetached("pkill",
                // {QString::number(processpid)}); int rv = system("pkill svtplay-dl");
                //#endif
                //#ifdef Q_OS_WIN // QT5
                QStringList argument;
                argument << "/IM" << svtplaydl << "/F";
                int rv = QProcess::execute("taskkill", argument);
                //            int rv = QProcess::execute("taskkill /IM " + svtplaydl + "
                //            /F");

                //#endif

                switch(rv) {
                    case -1:
                        ui->teOut->setTextColor(QColor("red"));
                        ui->teOut->append(
                            QCoreApplication::translate("MainWindow", "svtplay-dl crashed."));
                        ui->teOut->setTextColor(QColor("black"));
                        break;

                    case -2:
                        ui->teOut->setTextColor(QColor("red"));
                        ui->teOut->append(QCoreApplication::translate(
                                              "MainWindow", "Could not stop svtplay-dl."));
                        ui->teOut->setTextColor(QColor("black"));
                        break;

                    default:
                        ui->teOut->setTextColor(QColor("red"));
                        ui->teOut->append(
                            QCoreApplication::translate("MainWindow",
                                                        "svtplay-dl stopped. Exit code ") +
                            QString::number(rv) + "\n" +
                            QCoreApplication::translate(
                                "MainWindow",
                                "Delete any files that may have already been downloaded."));
                        avbrutet = true;
                        ui->teOut->setTextColor(QColor("black"));
                        processpid = 0;
                }
            }
        });
#endif
        // SET DEFAULT LOCATION
        //
        connect(ui->actionCopyToDefaultLocation, &QAction::triggered, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Path");
            QString copypath = settings.value("copypath").toString();

            if(ui->actionCopyToDefaultLocation->isChecked()) {
                settings.setValue("copytodefaultlocation", "true");
                ui->statusBar->showMessage(
                    QCoreApplication::translate("MainWindow", "Copy to: ") + copypath);
            } else {
                settings.setValue("copytodefaultlocation", "false");
            }

            settings.endGroup();
        });
        connect(ui->actionSetDefaultCopyLocation, &QAction::triggered, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Path");
            QString defaultdownloadlocation = settings.value("copypath", QStandardPaths::writableLocation(QStandardPaths::MoviesLocation)).toString();
            settings.endGroup();
            QFileDialog filedialog(this);
            int x = this->x();
            int y = this->y();
            //        filedialog.setOption(QFileDialog::DontUseNativeDialog, true);
            filedialog.setDirectory(defaultdownloadlocation);
            filedialog.setLabelText(QFileDialog::Accept, tr("Choose"));
            filedialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
            filedialog.setWindowTitle(tr("Copy streaming media to directory"));
            filedialog.setGeometry(x + 50, y + 50, 900, 550);
            filedialog.setFileMode(QFileDialog::Directory);//Folder name

            if(filedialog.exec() == 1) {
                QDir dir = filedialog.directory();
                QString sdir = dir.path();
#ifdef Q_OS_WIN
                // NTFS
                qt_ntfs_permission_lookup++; // turn checking on
#endif
                QFileInfo fi(sdir);

                if(!fi.isWritable()) {
                    QMessageBox msgBox;
                    msgBox.setIcon(QMessageBox::Critical);
                    msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                    msgBox.setText(tr("You do not have the right to save to") + "\n" + QDir::toNativeSeparators(sdir) + "\n" + tr("and it cannot be used as the default folder to copy to."));
                    msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                    msgBox.exec();
                    ui->teOut->clear();
#ifdef Q_OS_WIN
                    qt_ntfs_permission_lookup--; // turn checking of
#endif
                    return;
                }

#ifdef Q_OS_WIN
                qt_ntfs_permission_lookup--; // turn checking of
#endif
                settings.beginGroup("Path");
                settings.setValue("copypath", sdir);
                settings.endGroup();
            }
        });
        /* DOWNLOAD LOCATION */
        connect(ui->actionSetDefaultDownloadLocation, &QAction::triggered, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Path");
            QString defaultdownloadlocation = settings.value("savepath", QStandardPaths::writableLocation(QStandardPaths::MoviesLocation)).toString();
            settings.endGroup();
            QFileDialog filedialog(this);
            int x = this->x();
            int y = this->y();
            //        filedialog.setOption(QFileDialog::DontUseNativeDialog, true);
            filedialog.setDirectory(defaultdownloadlocation);
            filedialog.setLabelText(QFileDialog::Accept, tr("Choose"));
            filedialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
            filedialog.setWindowTitle(tr("Download streaming media to directory"));
            filedialog.setGeometry(x + 50, y + 50, 900, 550);
            filedialog.setFileMode(QFileDialog::Directory);//Folder name

            if(filedialog.exec() == 1) {
                QDir dir = filedialog.directory();
                QString sdir = dir.path();
#ifdef Q_OS_WIN
                // NTFS
                qt_ntfs_permission_lookup++; // turn checking on
#endif
                QFileInfo fi(sdir);

                if(!fi.isWritable()) {
                    QMessageBox msgBox;
                    msgBox.setIcon(QMessageBox::Critical);
                    msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                    msgBox.setText(tr("You do not have the right to save to") + "\n" + QDir::toNativeSeparators(sdir) + "\n" + tr("and it cannot be used as the default folder for downloads."));
//                        msgBox.addButton(QMessageBox::Ok);
//                        msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                    msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                    msgBox.exec();
                    ui->teOut->clear();
#ifdef Q_OS_WIN
                    qt_ntfs_permission_lookup--; // turn checking of
#endif
                    return;
                }

#ifdef Q_OS_WIN
                qt_ntfs_permission_lookup--; // turn checking of
#endif
                settings.beginGroup("Path");
                settings.setValue("savepath", sdir);
                settings.endGroup();
            }
        });
        /* DOWNLOAD LOCATION  HOOVER */
        connect(ui->actionSetDefaultDownloadLocation, &QAction::hovered, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Path");
            QString defaultdownloadlocation =
                settings.value("defaultdownloadlocation", "NOTFIND").toString();
            settings.endGroup();

            if(defaultdownloadlocation != "NOTFIND")
                ui->actionSetDefaultDownloadLocation->setStatusTip(
                    defaultdownloadlocation);
        });
        connect(ui->actionDownloadToDefaultLocation, &QAction::triggered, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Path");
            QString defaultdownloadlocation =
                settings.value("defaultdownloadlocation").toString();

            if(ui->actionDownloadToDefaultLocation->isChecked()) {
                settings.setValue("downloadtodefaultlocation", "true");
                ui->statusBar->showMessage(
                    QCoreApplication::translate("MainWindow", "Download to: ") +
                    defaultdownloadlocation);
            } else {
                settings.setValue("downloadtodefaultlocation", "false");
            }

            settings.endGroup();
        });
        /* Font */
        connect(ui->actionSelectFont, &QAction::triggered, [this] {
            SelectFont *mSelectFont;
            mSelectFont = new SelectFont(this);
            mSelectFont->setAttribute(Qt::WA_DeleteOnClose);
            // QIcon icon(":/images/font.png");
            // mSelectFont->setFont(DISPLAY_NAME, EXECUTABLE_NAME, icon);
            mSelectFont->setFont(DISPLAY_NAME, EXECUTABLE_NAME);
            mSelectFont->show();
            connect(mSelectFont, &SelectFont::valueChanged, this, &Newprg::setValue);
        });
        /* Väljer svtplay-dl version */
        /* STABLE */
        connect(ui->actionSvtplayDlStable, &QAction::triggered, [this]() -> void {

            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, LIBRARY_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Path");
            QString path = settings.value("stablepath").toString();
            settings.endGroup();
            // ipac
#ifdef Q_OS_LINUX // Linux
            svtplaydl = path + "/stable/svtplay-dl";
#endif
#ifdef Q_OS_WINDOWS
            svtplaydl = QDir::toNativeSeparators(path + "/stable/svtplay-dl.exe");

#endif
//#ifdef Q_OS_LINUX // Linux
//                svtplaydl = QDir::toNativeSeparators(
//                    QCoreApplication::applicationDirPath() + QStringLiteral(u"/stable/svtplay-dl"));

//#endif
//#ifdef Q_OS_WIN // Windows 32- and 64-bit
//                svtplaydl = QDir::toNativeSeparators(
//                    QCoreApplication::applicationDirPath() + QStringLiteral(u"/stable/svtplay-dl.exe"));
//#endif

            if(!fileExists(svtplaydl))
            {
#ifdef Q_OS_LINUX
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("svtplay-dl cannot be found or is not an executable program.\nGo "
                                  "to \"Tools\" and \"Download svtplay-dl...\""));
//                    msgBox.addButton(QMessageBox::Ok);
//                    msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
#endif
#ifdef Q_OS_WIN
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("svtplay-dl.exe cannot be found or is not an executable program.\nPlease click on \"Tools\" and \"Download svtplay-dl...\""));
//                    msgBox.addButton(QMessageBox::Ok);
//                    msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
#endif
            }

            ui->actionSvtplayDlStable->setChecked(true);
            ui->actionSvtplayDlBleedingEdge->setChecked(false);
            ui->actionSvtplayDlSystem->setChecked(false);
            ui->actionSvtPlayDlManuallySelected->setChecked(false);
        });
        /* BLEEDING EDGE */
        connect(
        ui->actionSvtplayDlBleedingEdge, &QAction::triggered, [this]() -> void {


            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, LIBRARY_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Path");
            QString path = settings.value("betapath").toString();
            settings.endGroup();
            // ipac
#ifdef Q_OS_LINUX // Linux
            svtplaydl = path + "/beta/svtplay-dl";
#endif
#ifdef Q_OS_WINDOWS
            svtplaydl = path + "/beta/svtplay-dl.exe";

#endif


            if(!fileExists(svtplaydl))
            {
#ifdef Q_OS_LINUX
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("svtplay-dl cannot be found or is not an executable program.\n "
                                  "Please click on \"Tools\" and \"Download svtplay-dl...\""));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
#endif
#ifdef Q_OS_WIN
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("svtplay-dl cannot be found or is not an executable program.\nPlease click on \"Tools\" and \"Download svtplay-dl...\""));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
#endif
            }

            ui->actionSvtplayDlStable->setChecked(false);
            ui->actionSvtplayDlSystem->setChecked(false);
            ui->actionSvtPlayDlManuallySelected->setChecked(false);
            ui->actionSvtplayDlBleedingEdge->setChecked(true);

            /* SYSTEM */
        });
        connect(ui->actionSvtplayDlSystem, &QAction::triggered, [this]() -> void {
            QString EXECUTE = findSvtplayDl();

            if(EXECUTE == QStringLiteral(u"NOTFIND"))
            {
                svtplaydl = "NOTFIND";
#ifdef Q_OS_LINUX // Linux
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("Cannot find ") + "svtplay-dl" + tr(" in system path."));
//                    msgBox.addButton(QMessageBox::Ok);
//                    msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
#endif
#ifdef Q_OS_WIN // Windows
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("Cannot find ") + "svtplay-dl.exe" + tr(" in system path."));
//                    msgBox.addButton(QMessageBox::Ok);
//                    msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
#endif
            }

            ui->actionSvtplayDlStable->setChecked(false);
            ui->actionSvtPlayDlManuallySelected->setChecked(false);
            ui->actionSvtplayDlSystem->setChecked(true);
            ui->actionSvtplayDlBleedingEdge->setChecked(false);

            if(EXECUTE != "NOTFIND")
            {
#ifdef Q_OS_LINUX // Linux
                svtplaydl = QDir::toNativeSeparators(EXECUTE);
#endif
#ifdef Q_OS_WIN // Windows
                svtplaydl = QDir::toNativeSeparators(EXECUTE);
#endif
//                    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
//                                       DISPLAY_NAME, EXECUTABLE_NAME);
//                    settings.beginGroup(QStringLiteral(u"Path"));
//                    settings.setValue(QStringLiteral(u"svtplaydlpath"), svtplaydl);
//                    settings.endGroup();
            }
        });
        /* VÄLJA svtplay-dl */
        connect(
        ui->actionSvtPlayDlManuallySelect, &QAction::triggered, [this]() -> void {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                               DISPLAY_NAME, EXECUTABLE_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Path");
            QString svtplaydlpath = settings.value("svtplaydlpath", QStandardPaths::writableLocation(QStandardPaths::HomeLocation)).toString();
            settings.endGroup();
            QFileInfo info(svtplaydlpath);
            QString path = info.absolutePath();
#ifdef Q_OS_LINUX // Linux
//            QString fileName = QFileDialog::getOpenFileName(
//                this, tr("Select svtplay-dl"), path, tr("svtplay-dl (svtplay-dl)"));


            QFileDialog filedialog(this);
            int x = this->x();
            int y = this->y();
            //        filedialog.setOption(QFileDialog::DontUseNativeDialog, true);
            filedialog.setDirectory(path);
            filedialog.setLabelText(QFileDialog::Accept, tr("Choose"));
            filedialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
            filedialog.setWindowTitle(tr("Select svtplay-dl"));
            filedialog.setGeometry(x + 50, y + 50, 900, 550);
            filedialog.setFileMode(QFileDialog::ExistingFile);//Folder name
            QString fileName;

            if(filedialog.exec() == 1)
            {
                fileName = filedialog.selectedFiles()
                .at(0);
                settings.beginGroup(QStringLiteral(u"Path"));
                settings.setValue(QStringLiteral(u"svtplaydlpath"), fileName);
                settings.endGroup();
            }



#endif
#ifdef Q_OS_WIN // Windows 32- and 64-bit
            QString fileName = QFileDialog::getOpenFileName(
                this, tr("Select svtplay-dl"), path, tr("*.exe (svtplay-dl.exe)"));
#endif

            if(fileName.isNull())
            {
                return;
            }
            if(fileExists(fileName))
            {
                settings.beginGroup("Path");
                settings.setValue("svtplaydlpath", fileName);
                settings.endGroup();
                svtplaydl = fileName;
            } else
            {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
#ifdef Q_OS_LINUX
                msgBox.setText(tr("svtplay-dl is not an executable program."));
#endif
#ifdef Q_OS_WIN
                msgBox.setText(tr("svtplay-dl.exe is not an executable program."));
#endif
//                    msgBox.addButton(QMessageBox::Ok);
//                    msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
            }

            ui->actionSvtplayDlStable->setChecked(false);
            ui->actionSvtPlayDlManuallySelected->setChecked(true);
            ui->actionSvtplayDlSystem->setChecked(false);
            ui->actionSvtplayDlBleedingEdge->setChecked(false);
        });
        /* SLUT VÄLJA svtplay-dl */
        /* ANVÄNDA VALD svtplay-dl */
        connect(ui->actionSvtPlayDlManuallySelected, &QAction::triggered,
        [this]() -> void {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                               DISPLAY_NAME, EXECUTABLE_NAME);
            settings.beginGroup("Path");
            QString svtplaydlpath = settings.value("svtplaydlpath").toString();

            settings.endGroup();

            if(!fileExists(svtplaydlpath))
            {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);

                if(svtplaydlpath == "NOTFIND") {
                    svtplaydlpath = "svtplay-dl";
                }

                msgBox.setText(svtplaydlpath + tr("svtplay-dl cannot be found or is not an executable "
                                                  "program.\nClick \"Tools\", \"Select "
                                                  "svtplay-dl...\"\nto select svtplay-dl."));
//                    msgBox.addButton(QMessageBox::Ok);
//                    msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                svtplaydl = "";
            } else
            {
                svtplaydl = QDir::toNativeSeparators(svtplaydlpath);
            }
            ui->actionSvtplayDlStable->setChecked(false);
            ui->actionSvtPlayDlManuallySelected->setChecked(true);
            ui->actionSvtplayDlSystem->setChecked(false);
            ui->actionSvtplayDlBleedingEdge->setChecked(false);
        });
        connect(ui->actionSvtPlayDlManuallySelect, &QAction::hovered, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Path");
            QString svtplaydlpath = settings.value("svtplaydlpath", "NOTFIND").toString();
            settings.endGroup();

            if(svtplaydlpath != QStringLiteral(u"NOTFIND")) {
                ui->actionSvtPlayDlManuallySelect->setStatusTip(svtplaydlpath);
            }
        });
        /* End Väljer svtplay-dl */
        // ceicer2
        connect(ui->actionMaintenanceTool, &QAction::triggered, [ = ]() -> void {
#ifdef Q_OS_LINUX // Linux
// zsync
#ifdef EXPERIMENTAL
            QMessageBox msgBox;
            msgBox.setTextFormat(Qt::RichText);
            msgBox.setIcon(QMessageBox::Information);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("This is an experimental version. This AppImage cannot be updated.") + "<br>" + tr("You can find more experimental versions") + "<a href=" + EXPERIMENTAL_VERSION_LINK + " > " + tr("here") + ". < / a > ");
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
#endif
#ifndef EXPERIMENTAL
            // QObject class
            Update *up = new Update;
            connect(up, &Update::isUpdated, this, &Newprg::isUpdated);

            // QDialog class
            ud = new UpdateDialog;
            ud->show();

            up->doUpdate(ARG1, ARG2, DISPLAY_NAME, ud);



#endif

#endif // Linux
#ifdef Q_OS_WIN // Windows 32- and 64-bit
            //FROM
#ifndef OFFLINE_INSTALLER
            QString path =
            QDir::toNativeSeparators(QCoreApplication::applicationDirPath() +
                                     QStringLiteral(u"/streamCapture2MaintenanceTool.exe"));

            bool b = QProcess::startDetached(path, QStringList() << QLatin1String("--updater"));

            if(!b)


            {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("Maintenance Tool cannot be found.\nOnly if you install") + "\n" +
                               DISPLAY_NAME + "\n" +
                               tr("is it possible to update and uninstall the program."));
//                    msgBox.addButton(QMessageBox::Ok);
//                    msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
            }
#endif
#ifdef OFFLINE_INSTALLER

            offlineInstaller();


#endif // END OFFLINE_INSTALLER
#endif
        });
#ifdef Q_OS_WIN
        //#ifdef PORTABLE
        connect(ui->actionDesktopShortcut, &QAction::triggered, []() -> void {
            QString source =
            QDir::toNativeSeparators(QCoreApplication::applicationFilePath());
#ifdef PORTABLE
            QString target = QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation);
            QString target2 = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
#else
            QString target2 = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);

#endif
            QString appname = DISPLAY_NAME;

            appname.append(QStringLiteral(u".lnk"));

            if(QFile::exists(appname))
            {
                QFile::remove(appname);
            }

            if(!QFile::link(QDir::toNativeSeparators(source), QDir::toNativeSeparators(target2 + "/" + appname)))
            {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("Failed to create desktop shortcut.\nCheck your file permissions."));
//                    msgBox.addButton(QMessageBox::Ok);
//                    msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
            } else
            {
                QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                                   DISPLAY_NAME, EXECUTABLE_NAME);
                // settings.setIniCodec("UTF-8");
                settings.beginGroup(QStringLiteral(u"Path"));
                settings.setValue(
                    QStringLiteral(u"targetDesktopLocation"),
                    QDir::toNativeSeparators(target2 + QStringLiteral(u"/streamCapture2.lnk")));
                settings.endGroup();
            }

#ifdef PORTABLE

            if(!QFile::link(QDir::toNativeSeparators(source), QDir::toNativeSeparators(target + "/" + appname)))
            {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("Failed to create desktop shortcut.\nCheck your file permissions."));
//                    msgBox.addButton(QMessageBox::Ok);
//                    msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
            } else
            {
                QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                                   DISPLAY_NAME, EXECUTABLE_NAME);
                // settings.setIniCodec("UTF-8");
                settings.beginGroup(QStringLiteral(u"Path"));
                settings.setValue("targetApplicationLocation",
                                  QDir::toNativeSeparators(target + QStringLiteral(u"/streamCapture2.lnk")));
                settings.endGroup();
            }

#endif
        });
//#endif
#endif
        connect(ui->actionDesktopShortcut, &QAction::triggered, [this](bool checked) -> void {


            if(checked)
            {
                chortcutdesktop(2);
            } else
            {
                chortcutdesktop(1);
            }


        });
        connect(ui->actionApplicationsMenuShortcut, &QAction::triggered, [this](bool checked) -> void {


            if(checked)
            {
                chortcutapplications(2);
            } else
            {
                chortcutapplications(1);
            }


        });
        /* END */
        // ipac
    }
#ifdef Q_OS_LINUX // Linux
    void Newprg::isUpdated(bool isupdated) {
//    If the update is successful, the old program will close.
        if(isupdated) {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Update");
            settings.setValue("readyupdate", false);
            settings.endGroup();
            close();
        }
    }
#endif
    void Newprg::setValue(const QFont & font) {
        ui->teOut->setFont(font);
    }
// private slots:
    void Newprg::comboBoxChanged() {
        if(ui->comboBox->currentIndex() != 0) {
            QString val = ui->comboBox->currentText();
//            QRegularExpression number(QStringLiteral(u"\\d+"));
            // ipac
            QString method = val;
            QString quality = val;
            quality.remove(QRegularExpression("[a-zA-Z]"));
            method.remove(QRegularExpression("[0-9]"));
            method = method.trimmed();
            method = method.toLower();
            quality = quality.trimmed();
            ui->leQuality->setText(quality);
            ui->leMethod->setText(method);
            ui->comboAmount->setEnabled(true);
        } else {
            ui->leQuality->setText(tr("Auto select"));
            ui->leMethod->setText(tr("Auto select"));
            ui->comboAmount->setEnabled(false);
        }
    }
    void Newprg::onCommProcessStart() {
        ui->teOut->append(tr("Downloading..."));
    }
    void Newprg::onCommProcessExit_sok(int exitCode,
                                       QProcess::ExitStatus exitStatus) {
        CommProcess2->waitForFinished(-1);

        if(exitCode == 0) {
            QTextStream stream(CommProcess2->readAllStandardOutput());
            QString s = "", sf = "", line = "";
            QStringList varannan;

            while(!stream.atEnd()) {
                line = stream.readLine();
                line = line.simplified();
                varannan << line;
            }

            varannan.removeDuplicates();

            foreach(QString listitm, varannan) {
                ui->comboBox->addItem(listitm);
                ui->teOut->append(listitm);
            }

            ui->pbDownload->setEnabled(true);
            ui->actionDownload->setEnabled(true);
            ui->actionDownloadAllEpisodes->setEnabled(true);
            ui->actionAddAllEpisodesToDownloadList->setEnabled(true);
            QString val = ui->comboBox->currentText();
            QRegularExpression number("\\d+");
            int first_number = val.indexOf(QRegularExpression(number));
            int last_number = val.lastIndexOf(QRegularExpression(number));
            QString quality = val.mid(first_number, last_number - first_number + 1);
            quality = quality.trimmed();
//        first_number = val.indexOf(" ", -1);
//        QString method = val.mid(first_number, val.size() - first_number);
            QString method = val.remove(QRegularExpression("[0-9]"));
            method = method.trimmed();
            method = method.toLower();
            ui->leQuality->setText(quality);
            method = method.trimmed();
            ui->leMethod->setText(method);
            bool lyckatsok = true;

            if(sf != "") {
                ui->teOut->setText(
                    tr("Can not find any video streams, please check the address.\n\n"));
                ui->teOut->append(sf);
                lyckatsok = false;
            }

            if((s.left(6) == QStringLiteral(u"ERROR:")) || (!lyckatsok)) {
                ui->leQuality->clear();
                ui->leMethod->clear();
                ui->pbDownload->setDisabled(true);
                ui->actionDownload->setDisabled(true);
            } else {
                recentFiles.append(address);

                if(ui->comboBox->currentText().isEmpty()) {
                    ui->pbAdd->setDisabled(true);
                    ui->actionAdd->setDisabled(true);
                } else {
                    ui->pbAdd->setEnabled(true);
                    ui->actionAdd->setEnabled(true);
                }
            }

            ui->teOut->append("\n" + tr("The search is complete") + "\n");
        } else {
            ui->teOut->append("\n" + tr("The search failed") + "\n");
            ui->leMethod->setText("");
            ui->leQuality->setText("");
        }

        statusExit(exitStatus, exitCode);
        delete CommProcess2;
        ui->comboBox->insertItem(0, "- " + tr("Auto select"));
        ui->comboBox->setCurrentIndex(0);
    }
    void Newprg::slotRecent() {
        recentFiles.removeDuplicates();

        while(recentFiles.size() > MAX_RECENT) {
            recentFiles.removeFirst();
        }

        ui->menuRecent->clear();
        QMenu *cmenu = new QMenu;
        cmenu->setContextMenuPolicy(Qt::CustomContextMenu);

        for(int i = recentFiles.size() - 1; i >= 0; i--) {
            QString path = recentFiles.at(i);
            auto actionRF = new QAction(path, this);
            actionRF->installEventFilter(this);
            actionRF->setShortcutVisibleInContextMenu(true);
            ui->menuRecent->addAction(actionRF);
            actionRF->setStatusTip(tr("Click to copy to the search box."));
            QAction *tabort = new QAction("Ta bort");
            cmenu->addAction(tabort);
            connect(tabort, &QAction::triggered,
            [cmenu]() {
                cmenu->exec();
            });
            /********************************
             * deprected
            QSignalMapper* signalMapperRF  = new QSignalMapper(this);
            connect(actionRF, SIGNAL(triggered()), signalMapperRF, SLOT(map()));
            signalMapperRF->setMapping(actionRF, path);
            connect(signalMapperRF, SIGNAL(mapped(const QString)), this,
            SLOT(recentRequested(const QString)));
            ***************************************************************/
            connect(actionRF, &QAction::triggered,
            [this, path]() {
                ui->leSok->setText(path);
            });
        }

        ui->menuRecent->addSeparator();
        QAction *actionAntalRecentFiles =
            new QAction(tr("The number of previous searches to be saved..."), this);
        ui->menuRecent->addAction(actionAntalRecentFiles);
        actionAntalRecentFiles->setStatusTip(tr(
                "Specify how many previous searches you want to save. If the number of "
                "searches exceeds the specified number, the oldest search is "
                "deleted."));
        connect(actionAntalRecentFiles, &QAction::triggered, [this]() {
            bool ok;
            QInputDialog inputdialog;
            inputdialog.setOkButtonText(tr("Ok"));
            inputdialog.setCancelButtonText(tr("Cancel"));
            inputdialog.setWindowTitle(DISPLAY_NAME " " VERSION);
            inputdialog.setInputMode(QInputDialog::IntInput);
            inputdialog.setIntValue(MAX_RECENT);
            inputdialog.setIntMinimum(0);
            inputdialog.setIntStep(1);
            inputdialog.setIntMaximum(SIZE_OF_RECENT);
            inputdialog.setLabelText(tr("The number of searches to be saved: ") +
                                     "(0-" + QString::number(SIZE_OF_RECENT) + + ")");
            ok = inputdialog.exec();
            int antal = inputdialog.intValue();

            if(ok) {
                MAX_RECENT = antal;
            }
        });
        QAction *actionEditAntalRecentFiles = new QAction(tr("Edit saved searches..."), this);
        ui->menuRecent->addAction(actionEditAntalRecentFiles);
        actionEditAntalRecentFiles->setStatusTip(
            tr("Click to edit all saved searches."));
        // ipac1
        connect(actionEditAntalRecentFiles, &QAction::triggered,
        [this]() {
            QString *content = new QString(recentFiles.join("\n"));
            QStringList *infoList = new QStringList;
            *infoList << tr("Edit saved searches");
            DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
            connect(mDownloadListDialog, &DownloadListDialog::sendContent, this, &Newprg::getRecentSearches);
            mDownloadListDialog->initiate(infoList, content, false);
        });
        QAction *actionClearAntalRecentFiles =
            new QAction(tr("Remove all saved searches"), this);
        ui->menuRecent->addAction(actionClearAntalRecentFiles);
        actionClearAntalRecentFiles->setStatusTip(
            tr("Click to delete all saved searches."));
        connect(actionClearAntalRecentFiles, &QAction::triggered,
        [this]() {
            recentFiles.clear();
        });
    }
// private:
    void Newprg::deleteAllSettings() {
        // Delete chortcuts, if anny
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Path");
#ifdef Q_OS_LINUX
#ifdef PORTABLE
        QString targetApplicationsLocation = settings.value("targetApplicationsLocation", "NOTHING").toString();
#endif
//    QString targetDesktopLocation = settings.value("targetDesktopLocation", "NOTHING").toString();
#endif
#ifdef Q_OS_WIN
        QString targetDesktopLocation = settings.value("targetDesktopLocation").toString();
#endif
#ifdef PORTABLE
        QString targetApplicationLocation = settings.value("targetApplicationLocation").toString();
#endif
        settings.endGroup();
        const QString settingsFile = settings.fileName();
        int hitta = settingsFile.indexOf(QStringLiteral(u"/streamcapture2.ini"));
        int lagom = settingsFile.size() - hitta;
        QString settingsPath = settingsFile.mid(0, settingsFile.size() - lagom);
        QDir dir(settingsPath);
        // ipac
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("All your saved settings will be deleted.\nAll lists of files to "
                          "download will disappear.\nDo you want to continue?"));
        msgBox.addButton(tr("Yes"), QMessageBox::AcceptRole);
        QPushButton *but = msgBox.addButton(tr("No"), QMessageBox::RejectRole);
        msgBox.setDefaultButton(but);

        if(msgBox.exec() == QMessageBox::AcceptRole) {
#ifdef Q_OS_LINUX
            QString applicationslocations = QDir::toNativeSeparators(
                                                QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation) + "/streamCapture2.desktop");

            if(QFile::exists(applicationslocations)) {
                QFile::remove(applicationslocations);
            }

            QString desktopLocation = QDir::toNativeSeparators(
                                          QStandardPaths::writableLocation(QStandardPaths::DesktopLocation) + "/streamCapture2.desktop");

            if(QFile::exists(desktopLocation)) {
                QFile::remove(desktopLocation);
            }

#endif
#ifdef Q_OS_WIN

            if(QFile::exists(targetDesktopLocation)) {
                QFile::remove(targetDesktopLocation);
            }

#endif
#ifdef PORTABLE

            if(QFile::exists(targetApplicationLocation)) {
                QFile::remove(targetApplicationLocation);
            }

#endif

            if(dir.removeRecursively()) {
                deleteSettings = true;
                exit(0);
            } else {
                // cppcheck
                QMessageBox msgBox1;
                msgBox1.setIcon(QMessageBox::Critical);
                msgBox1.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox1.setText(tr("Failed to delete your configuration files.\n"
                                   "Check your file permissions."));
                msgBox1.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox1.exec();
            }
        }
    }
    void Newprg::statusExit(QProcess::ExitStatus exitStatus, int exitCode) {
        if(ui->actionShowMore->isChecked()) {
            QString info;

            switch(exitStatus) {
                case QProcess::NormalExit:
                    info = QStringLiteral(u"svtplay-dl") + " " + tr("was normally terminated. Exit code = ") + QString::number(exitCode);
                    break;

                case QProcess::CrashExit:
                    info = QStringLiteral(u"svtplay-dl") + " " + tr("crashed. Exit code = ") + QString::number(exitCode);
                    break;
            }

            ui->statusBar->showMessage(info);
        }
    }
