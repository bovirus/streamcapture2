// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"

void Newprg::createSt()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    QInputDialog inputdialogprovider;
    inputdialogprovider.setOkButtonText(tr("Ok"));
    inputdialogprovider.setCancelButtonText(tr("Cancel"));
    inputdialogprovider.setWindowTitle(tr("Streaming service"));
    inputdialogprovider.setInputMode(QInputDialog::TextInput);
    inputdialogprovider.setLabelText(tr("Enter the name of your streaming service."));

    if(inputdialogprovider.exec() == QInputDialog::Accepted) {
        QInputDialog inputdialogst;
        inputdialogst.setOkButtonText(tr("Ok"));
        inputdialogst.setCancelButtonText(tr("Cancel"));
        inputdialogst.setWindowTitle(tr("Enter 'st' cookie"));
        inputdialogst.setInputMode(QInputDialog::TextInput);
        inputdialogst.setLabelText(tr("Enter the 'st' cookie that your video stream provider has saved in your browser."));
        QString stnewprovider = inputdialogprovider.textValue();

        if(inputdialogst.exec() == QInputDialog::Accepted) {
            QString stcookie = inputdialogst.textValue().trimmed();
            settings.beginGroup("Stcookies");
            settings.setValue(stnewprovider + "/st", stcookie);
            settings.endGroup();
            QList<QAction*> list = ui->menuStCookies->actions();

            for(QAction *a : qAsConst(list)) {
                a->deleteLater();
            }

            actionSt();
        }
    }
}

