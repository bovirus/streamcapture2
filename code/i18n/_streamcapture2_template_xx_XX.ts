<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>DownloadListDialog</name>
    <message>
        <location filename="../downloadlistdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="75"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="92"/>
        <location filename="../downloadlistdialog.cpp" line="92"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="109"/>
        <source>Save as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="93"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="94"/>
        <source>Save as text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="111"/>
        <source>Could not save the file.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="193"/>
        <source>Unable to find file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="194"/>
        <source>Unable to find</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="112"/>
        <location filename="../downloadlistdialog.cpp" line="197"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <location filename="../info.cpp" line="31"/>
        <source> is free software, license </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="34"/>
        <source>A graphical shell for </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="36"/>
        <source> and </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="39"/>
        <source> streamCapture2 handles downloads of video streams.&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="40"/>
        <source>Many thanks to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="41"/>
        <source> for the Italian translation. And for many good ideas that have made the program better.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="62"/>
        <source>Created:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="64"/>
        <source>Compiled on:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="67"/>
        <source>Qt version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="113"/>
        <source>Full version number:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="238"/>
        <source>Revision:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="103"/>
        <source>compatible with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="77"/>
        <source>for</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="79"/>
        <location filename="../info.cpp" line="95"/>
        <location filename="../info.cpp" line="212"/>
        <source>Compiler:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="207"/>
        <source>Unknown version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="223"/>
        <source>Unknown compiler.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="249"/>
        <source>Home page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="251"/>
        <source>Source code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="253"/>
        <source>Wiki</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="256"/>
        <source>Phone: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="268"/>
        <source>is located in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../info.cpp" line="269"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../newprg.cpp" line="718"/>
        <location filename="../newprg.cpp" line="742"/>
        <source>Enter your password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="719"/>
        <location filename="../newprg.cpp" line="743"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="808"/>
        <source>svtplay-dl crashed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="814"/>
        <source>Could not stop svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="822"/>
        <source>svtplay-dl stopped. Exit code </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="825"/>
        <source>Delete any files that may have already been downloaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="847"/>
        <source>Copy to: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="980"/>
        <source>Download to: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Newprg</name>
    <message>
        <location filename="../about.cpp" line="43"/>
        <source>Version history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="55"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="75"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="91"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="162"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="280"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="300"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="316"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="416"/>
        <location filename="../coppytodefaultlocation.cpp" line="40"/>
        <location filename="../download.cpp" line="165"/>
        <location filename="../download.cpp" line="190"/>
        <location filename="../download.cpp" line="208"/>
        <location filename="../download.cpp" line="230"/>
        <location filename="../download.cpp" line="244"/>
        <location filename="../download.cpp" line="299"/>
        <location filename="../downloadall.cpp" line="63"/>
        <location filename="../downloadall.cpp" line="77"/>
        <location filename="../downloadall.cpp" line="108"/>
        <location filename="../downloadall.cpp" line="144"/>
        <location filename="../downloadall.cpp" line="170"/>
        <location filename="../downloadall.cpp" line="179"/>
        <location filename="../downloadall.cpp" line="240"/>
        <location filename="../downloadallepisodes.cpp" line="157"/>
        <location filename="../downloadallepisodes.cpp" line="170"/>
        <location filename="../downloadallepisodes.cpp" line="200"/>
        <location filename="../newprg.cpp" line="131"/>
        <location filename="../newprg.cpp" line="886"/>
        <location filename="../newprg.cpp" line="938"/>
        <location filename="../newprg.cpp" line="1036"/>
        <location filename="../newprg.cpp" line="1046"/>
        <location filename="../newprg.cpp" line="1084"/>
        <location filename="../newprg.cpp" line="1092"/>
        <location filename="../newprg.cpp" line="1117"/>
        <location filename="../newprg.cpp" line="1127"/>
        <location filename="../newprg.cpp" line="1220"/>
        <location filename="../newprg.cpp" line="1255"/>
        <location filename="../newprg.cpp" line="1290"/>
        <location filename="../newprg.cpp" line="1330"/>
        <location filename="../newprg.cpp" line="1371"/>
        <location filename="../newprg.cpp" line="1395"/>
        <location filename="../newprg.cpp" line="1610"/>
        <location filename="../newprg.cpp" line="1728"/>
        <location filename="../nfo.cpp" line="55"/>
        <location filename="../nfo.cpp" line="65"/>
        <location filename="../nfo.cpp" line="75"/>
        <location filename="../nfo.cpp" line="87"/>
        <location filename="../nfo.cpp" line="157"/>
        <location filename="../offline_installer.cpp" line="58"/>
        <location filename="../offline_installer.cpp" line="91"/>
        <location filename="../paytv_create.cpp" line="58"/>
        <location filename="../paytv_create.cpp" line="87"/>
        <location filename="../paytv_create.cpp" line="119"/>
        <location filename="../paytv_edit.cpp" line="115"/>
        <location filename="../paytv_edit.cpp" line="146"/>
        <location filename="../paytv_edit.cpp" line="206"/>
        <location filename="../setgetconfig.cpp" line="75"/>
        <location filename="../setgetconfig.cpp" line="444"/>
        <location filename="../setgetconfig.cpp" line="457"/>
        <location filename="../setgetconfig.cpp" line="603"/>
        <location filename="../setgetconfig.cpp" line="627"/>
        <location filename="../shortcuts.cpp" line="69"/>
        <location filename="../shortcuts.cpp" line="140"/>
        <location filename="../st_create.cpp" line="27"/>
        <location filename="../st_create.cpp" line="35"/>
        <location filename="../st_edit.cpp" line="109"/>
        <location filename="../st_edit.cpp" line="124"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="53"/>
        <source>License streamCapture2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="63"/>
        <location filename="../about.cpp" line="73"/>
        <source>License svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="84"/>
        <source>License 7zip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="100"/>
        <source> could not be found. Please download a portable streamCapture2 where FFmpeg is included.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="103"/>
        <source> could not be found. Please go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install FFmpeg.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="105"/>
        <location filename="../about.cpp" line="109"/>
        <source>Or install </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="105"/>
        <location filename="../about.cpp" line="109"/>
        <source> in your system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="108"/>
        <source> could not be found. Please download an AppImage where FFmpeg is included.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="137"/>
        <source>About FFmpeg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="151"/>
        <source>svtplay-dl is not found in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="153"/>
        <location filename="../about.cpp" line="157"/>
        <source> You can download svtplay-dl from bin.ceicer.com. Select &quot;Tools&quot;, &quot;Download svtplay-dl...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="156"/>
        <source>svtplay-dl.exe is not found in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="199"/>
        <location filename="../about.cpp" line="204"/>
        <source> cannot be found. Go to &quot;Tools&quot;, &quot;Download svtplay-dl...&quot; to download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="207"/>
        <source>Please click &quot;Tools&quot; and select svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="221"/>
        <source>version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="160"/>
        <location filename="../about.cpp" line="211"/>
        <location filename="../about.cpp" line="246"/>
        <source>About svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="37"/>
        <source>You have not selected any place to copy the media files.
Please select a location before proceeding.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="63"/>
        <source>A file with the same name already exists. The file will not be copied.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="70"/>
        <location filename="../coppytodefaultlocation.cpp" line="94"/>
        <source>Copy succeeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="74"/>
        <location filename="../coppytodefaultlocation.cpp" line="98"/>
        <source>Copy failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="30"/>
        <location filename="../downloadallepisodes.cpp" line="27"/>
        <location filename="../listallepisodes.cpp" line="28"/>
        <location filename="../newprg.cpp" line="194"/>
        <location filename="../setgetconfig.cpp" line="509"/>
        <location filename="../sok.cpp" line="30"/>
        <source> cannot be found or is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="33"/>
        <location filename="../downloadallepisodes.cpp" line="28"/>
        <location filename="../listallepisodes.cpp" line="29"/>
        <location filename="../setgetconfig.cpp" line="510"/>
        <location filename="../sok.cpp" line="31"/>
        <source>Please click on &quot;Tools&quot; and select svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="64"/>
        <source>The request is processed...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="64"/>
        <source>Preparing to download...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="140"/>
        <source>The request is processed...
Preparing to download...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="78"/>
        <source>Download streaming media to folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="177"/>
        <source>The video stream is saved in </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="186"/>
        <location filename="../downloadall.cpp" line="59"/>
        <location filename="../downloadallepisodes.cpp" line="153"/>
        <source>The default folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="204"/>
        <source>You do not have the right to save to the default folder.
Download is interrupted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="226"/>
        <source>The folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="161"/>
        <location filename="../download.cpp" line="240"/>
        <location filename="../downloadall.cpp" line="73"/>
        <location filename="../downloadall.cpp" line="104"/>
        <location filename="../downloadall.cpp" line="178"/>
        <location filename="../downloadallepisodes.cpp" line="167"/>
        <location filename="../downloadallepisodes.cpp" line="196"/>
        <source>You do not have the right to save to the folder.
Download is interrupted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="251"/>
        <location filename="../downloadall.cpp" line="184"/>
        <source>Selected folder to copy to is </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="300"/>
        <location filename="../download_runtime.cpp" line="29"/>
        <location filename="../downloadall.cpp" line="145"/>
        <location filename="../downloadall.cpp" line="241"/>
        <location filename="../downloadallepisodes.cpp" line="45"/>
        <location filename="../downloadallepisodes.cpp" line="62"/>
        <location filename="../downloadallepisodes.cpp" line="80"/>
        <location filename="../language.cpp" line="33"/>
        <location filename="../language.cpp" line="72"/>
        <location filename="../language.cpp" line="111"/>
        <location filename="../newprg.cpp" line="867"/>
        <location filename="../newprg.cpp" line="917"/>
        <location filename="../newprg.cpp" line="1174"/>
        <location filename="../newprg.cpp" line="1611"/>
        <location filename="../offline_installer.cpp" line="35"/>
        <location filename="../paytv_create.cpp" line="59"/>
        <location filename="../paytv_create.cpp" line="88"/>
        <location filename="../paytv_create.cpp" line="120"/>
        <location filename="../paytv_edit.cpp" line="31"/>
        <location filename="../paytv_edit.cpp" line="116"/>
        <location filename="../paytv_edit.cpp" line="147"/>
        <location filename="../paytv_edit.cpp" line="207"/>
        <location filename="../save.cpp" line="36"/>
        <location filename="../st_create.cpp" line="28"/>
        <location filename="../st_create.cpp" line="36"/>
        <location filename="../st_edit.cpp" line="32"/>
        <location filename="../st_edit.cpp" line="110"/>
        <location filename="../st_edit.cpp" line="125"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="301"/>
        <location filename="../downloadall.cpp" line="242"/>
        <location filename="../downloadallepisodes.cpp" line="252"/>
        <location filename="../listallepisodes.cpp" line="108"/>
        <location filename="../paytv_create.cpp" line="121"/>
        <location filename="../paytv_edit.cpp" line="150"/>
        <location filename="../sok.cpp" line="110"/>
        <source>Enter your password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="304"/>
        <location filename="../downloadall.cpp" line="245"/>
        <location filename="../paytv_create.cpp" line="91"/>
        <location filename="../paytv_create.cpp" line="124"/>
        <location filename="../paytv_edit.cpp" line="120"/>
        <location filename="../paytv_edit.cpp" line="152"/>
        <source>Spaces are not allowed. Use only the characters
your streaming provider approves.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="326"/>
        <source>Starts downloading: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="359"/>
        <location filename="../downloadall.cpp" line="448"/>
        <location filename="../downloadallepisodes.cpp" line="297"/>
        <source>Merge audio and video...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="363"/>
        <location filename="../downloadall.cpp" line="452"/>
        <location filename="../downloadallepisodes.cpp" line="303"/>
        <source>Removing old files, if there are any...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="391"/>
        <location filename="../download.cpp" line="400"/>
        <location filename="../downloadall.cpp" line="359"/>
        <location filename="../downloadall.cpp" line="366"/>
        <source>Download succeeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="451"/>
        <location filename="../download.cpp" line="456"/>
        <source>The download failed </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="433"/>
        <location filename="../download.cpp" line="438"/>
        <location filename="../downloadall.cpp" line="404"/>
        <location filename="../downloadall.cpp" line="412"/>
        <location filename="../downloadallepisodes.cpp" line="341"/>
        <location filename="../downloadallepisodes.cpp" line="344"/>
        <location filename="../downloadallepisodes.cpp" line="349"/>
        <source>Download completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="31"/>
        <source>Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="379"/>
        <location filename="../download.cpp" line="382"/>
        <source>The download failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="473"/>
        <location filename="../downloadallepisodes.cpp" line="360"/>
        <source>No folder is selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="493"/>
        <location filename="../download.cpp" line="494"/>
        <location filename="../listallepisodes.cpp" line="51"/>
        <location filename="../listallepisodes.cpp" line="52"/>
        <source>Searching...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="495"/>
        <location filename="../listallepisodes.cpp" line="53"/>
        <source>The request is processed...
Starting search...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="166"/>
        <source>The default folder for copying video streams cannot be found.
Download is interrupted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="137"/>
        <source>You have chosen to download more than one file with the same name. In order not to overwrite files, folders will be created for each file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="28"/>
        <location filename="../downloadall.cpp" line="32"/>
        <source>cannot be found or is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="29"/>
        <source>Please click on &quot;Tools&quot; and select svtplay-dl.exe.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="200"/>
        <source>The video streams are saved in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="342"/>
        <source>Preparing to download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="389"/>
        <location filename="../downloadall.cpp" line="394"/>
        <source>The download failed. If a username and password or &apos;st&apos; cookie is required, you must enter these.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="124"/>
        <location filename="../newprg.cpp" line="290"/>
        <location filename="../newprg.cpp" line="1682"/>
        <location filename="../paytv_create.cpp" line="142"/>
        <location filename="../paytv_edit.cpp" line="170"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="125"/>
        <location filename="../newprg.cpp" line="291"/>
        <location filename="../newprg.cpp" line="1683"/>
        <location filename="../paytv_create.cpp" line="143"/>
        <location filename="../paytv_edit.cpp" line="171"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="44"/>
        <location filename="../downloadallepisodes.cpp" line="61"/>
        <location filename="../downloadallepisodes.cpp" line="79"/>
        <source>Download anyway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="37"/>
        <source>You have chosen to create folders for each file and have the files copied.
Unfortunately, this is not possible as streamCapture2 does not know the file names.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="55"/>
        <source>You have chosen to have the files copied.
Unfortunately, this is not possible as streamCapture2 does not know the file names.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="72"/>
        <source>You have chosen to create folders for each file.
Unfortunately, this is not possible as streamCapture2 does not know the file names.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="120"/>
        <source>Once svtplay-dl has started downloading all episodes, streamCapture2 no longer has control.
If you want to cancel, you may need to log out or restart your computer.
You can try to cancel using the command
&quot;sudo killall python3&quot;

Do you want to start the download?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="178"/>
        <source>Download all episodes to folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="253"/>
        <location filename="../listallepisodes.cpp" line="109"/>
        <location filename="../sok.cpp" line="111"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="272"/>
        <source>Starts downloading all episodes: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="308"/>
        <source>The download failed. Did you forget to enter username and password?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="322"/>
        <source>Episode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="322"/>
        <source>of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadallepisodes.cpp" line="334"/>
        <source>The media files (and if you have selected the subtitles) have been downloaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../language.cpp" line="34"/>
        <location filename="../language.cpp" line="73"/>
        <location filename="../language.cpp" line="112"/>
        <source>Restart Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../language.cpp" line="37"/>
        <location filename="../language.cpp" line="76"/>
        <location filename="../language.cpp" line="115"/>
        <source>The program must be restarted for the new language settings to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="67"/>
        <location filename="../nfo.cpp" line="45"/>
        <location filename="../sok.cpp" line="65"/>
        <source>The search field is empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="71"/>
        <location filename="../sok.cpp" line="69"/>
        <source>Incorrect URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="147"/>
        <location filename="../sok.cpp" line="153"/>
        <source> crashed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="149"/>
        <location filename="../sok.cpp" line="155"/>
        <source>Can not find any video streams, please check the address.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="209"/>
        <source>normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="212"/>
        <source>bold and italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="215"/>
        <source>bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="217"/>
        <source>italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="221"/>
        <source>Current font:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="222"/>
        <source>size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="254"/>
        <source>View download list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="273"/>
        <source>Edit download list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="271"/>
        <source>Select &quot;Tools&quot;, &quot;Update&quot; to update.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="393"/>
        <source>Select &quot;Tools&quot;, &quot;Maintenance Tool&quot; and &quot;Update components&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="396"/>
        <location filename="../setgetconfig.cpp" line="279"/>
        <source>Download a new</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="438"/>
        <source>svtplay-dl is in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="443"/>
        <source>svtplay-dl.exe is in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="449"/>
        <source>ERROR! svtplay-dl is not in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="453"/>
        <source>ERROR! svtplay-dl.exe is not in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="463"/>
        <source>Path to svtplay-dl: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="467"/>
        <source>Path to svtplay-dl.exe: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="474"/>
        <source>ERROR! svtplay-dl could not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="478"/>
        <source>ERROR! svtplay-dl.exe could not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="495"/>
        <source>ERROR! FFmpeg cannot be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="866"/>
        <location filename="../newprg.cpp" line="916"/>
        <location filename="../newprg.cpp" line="1173"/>
        <location filename="../save.cpp" line="35"/>
        <source>Choose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="868"/>
        <source>Copy streaming media to directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1739"/>
        <source>was normally terminated. Exit code = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1743"/>
        <source>crashed. Exit code = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="86"/>
        <location filename="../newprg.cpp" line="918"/>
        <source>Download streaming media to directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1114"/>
        <location filename="../newprg.cpp" line="1124"/>
        <source>Cannot find </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1175"/>
        <location filename="../newprg.cpp" line="1194"/>
        <source>Select svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1194"/>
        <source>*.exe (svtplay-dl.exe)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1213"/>
        <source>svtplay-dl is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1250"/>
        <source>svtplay-dl cannot be found or is not an executable program.
Click &quot;Tools&quot;, &quot;Select svtplay-dl...&quot;
to select svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1289"/>
        <source>This is an experimental version. This AppImage cannot be updated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1289"/>
        <source>You can find more experimental versions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1368"/>
        <location filename="../newprg.cpp" line="1392"/>
        <source>Failed to create desktop shortcut.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1475"/>
        <location filename="../newprg.cpp" line="1476"/>
        <location filename="../newprg.cpp" line="1558"/>
        <location filename="../sok.cpp" line="204"/>
        <source>Auto select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1481"/>
        <source>Downloading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1527"/>
        <source>Can not find any video streams, please check the address.

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1549"/>
        <source>The search is complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1551"/>
        <source>The search failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1578"/>
        <source>Click to copy to the search box.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1601"/>
        <source>The number of previous searches to be saved...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1603"/>
        <source>Specify how many previous searches you want to save. If the number of searches exceeds the specified number, the oldest search is deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1618"/>
        <source>The number of searches to be saved: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1627"/>
        <source>Edit saved searches...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1636"/>
        <source>Edit saved searches</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1630"/>
        <source>Click to edit all saved searches.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="81"/>
        <source>Edit streamCapture2 settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="105"/>
        <source>Edit download svtplay-dl settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="130"/>
        <source>svtplay-dl is not found in the system path.
You can download any svtplay-dl of your choice.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="195"/>
        <source>Please click on &quot;Tools&quot; and select svtplay-dl...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="282"/>
        <source>The list of video streams to download will be deleted and can not be restored.
Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="388"/>
        <source>Please click on &quot;Tools&quot; and &quot;Update&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="499"/>
        <source>Path to ffmpeg.exe: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="502"/>
        <source>Path to ffmpeg: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="885"/>
        <location filename="../newprg.cpp" line="935"/>
        <source>You do not have the right to save to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="885"/>
        <source>and it cannot be used as the default folder to copy to.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="935"/>
        <source>and it cannot be used as the default folder for downloads.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1032"/>
        <source>svtplay-dl cannot be found or is not an executable program.
Go to &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1043"/>
        <source>svtplay-dl.exe cannot be found or is not an executable program.
Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1082"/>
        <source>svtplay-dl cannot be found or is not an executable program.
 Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1091"/>
        <source>svtplay-dl cannot be found or is not an executable program.
Please click on &quot;Tools&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1114"/>
        <location filename="../newprg.cpp" line="1124"/>
        <source> in system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1216"/>
        <source>svtplay-dl.exe is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1289"/>
        <source>here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1325"/>
        <source>Maintenance Tool cannot be found.
Only if you install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1327"/>
        <source>is it possible to update and uninstall the program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1642"/>
        <source>Remove all saved searches</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1645"/>
        <source>Click to delete all saved searches.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1680"/>
        <source>All your saved settings will be deleted.
All lists of files to download will disappear.
Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1726"/>
        <source>Failed to delete your configuration files.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="60"/>
        <location filename="../paytv_edit.cpp" line="208"/>
        <location filename="../st_create.cpp" line="29"/>
        <location filename="../st_edit.cpp" line="111"/>
        <source>Streaming service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="62"/>
        <location filename="../paytv_edit.cpp" line="210"/>
        <location filename="../st_create.cpp" line="31"/>
        <location filename="../st_edit.cpp" line="114"/>
        <source>Enter the name of your streaming service.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="89"/>
        <location filename="../paytv_edit.cpp" line="118"/>
        <source>Enter your username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="134"/>
        <location filename="../paytv_edit.cpp" line="162"/>
        <source>Save password?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="136"/>
        <location filename="../paytv_edit.cpp" line="164"/>
        <source>Do you want to save the password? (unsafe)?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="26"/>
        <source>Manage Login details for </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="27"/>
        <source>Edit, rename or delete
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="28"/>
        <location filename="../st_edit.cpp" line="30"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="29"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="30"/>
        <location filename="../st_edit.cpp" line="31"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="58"/>
        <source>Create New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="67"/>
        <location filename="../setgetconfig.cpp" line="386"/>
        <source>No Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsystemtrayicon.cpp" line="27"/>
        <source>The mission failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsystemtrayicon.cpp" line="30"/>
        <source>Mission accomplished!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="52"/>
        <source>Open the folder where streamCapture2 is located</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="56"/>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="61"/>
        <source>Force update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="74"/>
        <source>streamCapture2 is not allowed to open </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="74"/>
        <source> Use the file manager instead.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="40"/>
        <source>Save a screenshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="42"/>
        <source>Images (*.png)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="111"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="112"/>
        <source>Update this AppImage to the latest version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="276"/>
        <source>Please click on &quot;Tools&quot; and &quot;Maintenance Tool...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="317"/>
        <source>Up and running</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="381"/>
        <source>Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="441"/>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Download an AppImage that contains FFmpeg.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="452"/>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Download a portable streamCapture2 that contains FFmpeg.&lt;br&gt;&lt;br&gt;Or download and place ffmpeg.exe in the same folder as streamapture2.exe.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="455"/>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="599"/>
        <source>Could not save a file to store Recent Search list.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="624"/>
        <source>Could not save a file to store the list of downloads.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="68"/>
        <source>Failure!
The shortcut could not be created in
&quot;~/.local/share/applications&quot;
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="115"/>
        <location filename="../shortcuts.cpp" line="117"/>
        <source>Download video streams.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="139"/>
        <source>Failure!
The shortcut could not be created.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="37"/>
        <source>The information from svtplay-dl may or may not contain:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="38"/>
        <source>Quality, Method, Codec, Resolution, Language and Role</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="162"/>
        <source>Unable to log in. Forgot your username and password?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="214"/>
        <source>ERROR: No videos found. Cant find video id for the video.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../st_create.cpp" line="37"/>
        <location filename="../st_edit.cpp" line="126"/>
        <source>Enter &apos;st&apos; cookie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../st_create.cpp" line="39"/>
        <location filename="../st_edit.cpp" line="129"/>
        <source>Enter the &apos;st&apos; cookie that your video stream provider has saved in your browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="25"/>
        <source>Manage &apos;st&apos; cookie for </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="27"/>
        <source>Use, Do not use, Edit or Delete
&apos;st&apos; cookie for
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="28"/>
        <source>Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="29"/>
        <source>Do not use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="176"/>
        <source>Set new &apos;st&apos; cookie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="177"/>
        <source>Paste and save the &apos;st&apos; cookie that your streaming provider has downloaded to your browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="33"/>
        <source>Open your language file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="33"/>
        <source>Compiled language file (*.qm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../zoom.cpp" line="31"/>
        <source>The font size changes to the selected font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="126"/>
        <source>You are using version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="137"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="141"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="146"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="148"/>
        <source>Stable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="137"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="141"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="146"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="148"/>
        <source>You have selected the option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="127"/>
        <source>You are NOT using the latest version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="28"/>
        <source>You have downloaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="31"/>
        <source>Nothing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="32"/>
        <source>To folder &quot;stable&quot;: Nothing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="34"/>
        <source>To folder &quot;stable&quot;: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="39"/>
        <source>To folder &quot;beta&quot;: Nothing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="41"/>
        <source>To folder &quot;beta&quot;: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="57"/>
        <source>An unexpected error occurred.&lt;br&gt;Information about svtplay-dl beta can not be found.&lt;br&gt;Check your internet connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="77"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="93"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="282"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="302"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="318"/>
        <source>An unexpected error occurred.&lt;br&gt;Information about svtplay-dl stable can not be found.&lt;br&gt;Check your internet connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="126"/>
        <source>of svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="130"/>
        <source>You are not using svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="137"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="141"/>
        <source>Beta:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="137"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="141"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="146"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="148"/>
        <source>svtplay-dl available for download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="137"/>
        <source>You are using the beta version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="139"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="146"/>
        <source>You are using the stable version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="224"/>
        <source>&quot;Use stable svtplay-dl.&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="226"/>
        <source>&quot;Use stable svtplay-dl&quot;, but it can not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="245"/>
        <source>&quot;Use svtplay-dl beta.&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="247"/>
        <source>&quot;Use svtplay-dl beta&quot;, but it can not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="373"/>
        <source>&lt;b&gt;Welcome to streamCapture2!&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="378"/>
        <source>&lt;b&gt;svtplay-dl stable&lt;/b&gt; can not be found. You have entered an incorrect path or no path has been specified at all.&lt;br&gt;Or the files have been moved or deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="379"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="397"/>
        <source>&lt;i&gt;Click &quot;Download&quot; and &quot;Download to directory stable&quot;&lt;/i&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="385"/>
        <source>&lt;b&gt;svtplay-dl beta&lt;/b&gt; can not be found. You have entered an incorrect path or no path has been specified at all.&lt;br&gt;Or the files have been moved or deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="386"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="408"/>
        <source>&lt;i&gt;Click &quot;Download&quot; and &quot;Download to directory beta&quot;&lt;/i&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="394"/>
        <source>&lt;b&gt;svtplay-dl stable is available for download.&lt;/b&gt;&lt;br&gt;Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="405"/>
        <source>&lt;b&gt;svtplay-dl beta is available for download.&lt;/b&gt;&lt;br&gt;Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="260"/>
        <source>&quot;Use the selected svtplay-dl.&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="266"/>
        <source>svtplay-dl is not found in the specified path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="262"/>
        <source>&quot;Use the selected svtplay-dl&quot;, but it can not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download_runtime.cpp" line="27"/>
        <source>Only if svtplay-dl does not work do you need to install
&quot;Microsoft Visual C++ Redistributable&quot;.
Download and double-click to install.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="161"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="415"/>
        <location filename="../download_runtime.cpp" line="28"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="47"/>
        <source>NFO files contain release information about the media. No NFO file was found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="121"/>
        <source>Title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="125"/>
        <source>Episode title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="129"/>
        <source>Season:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="133"/>
        <source>Episode:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="137"/>
        <source>Plot:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="141"/>
        <source>Published:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="156"/>
        <source>An unexpected error occurred while downloading the file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="175"/>
        <source>NFO Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="31"/>
        <source>To the website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="34"/>
        <source>You have installed with an offline installer.&lt;br&gt;&lt;b&gt;You must uninstall before you can install a new version.&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="37"/>
        <source>Uninstall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="39"/>
        <source>Uninstall and install new version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="60"/>
        <location filename="../offline_installer.cpp" line="93"/>
        <source>An unexpected error occurred.&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../offline_installer.cpp" line="60"/>
        <location filename="../offline_installer.cpp" line="93"/>
        <source>&lt;br&gt;can not be found or is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>newprg</name>
    <message>
        <location filename="../newprg.ui" line="35"/>
        <source>TEST</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="68"/>
        <source>Paste the link to the page where the video is displayed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="71"/>
        <location filename="../newprg.ui" line="939"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="117"/>
        <location filename="../newprg.ui" line="930"/>
        <source>Search for video streams.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="120"/>
        <location filename="../newprg.ui" line="927"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="149"/>
        <source>Add current video to the list of files that will be downloaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="246"/>
        <source>The number of bits that are conveyed or processed per unit of time. Higher numbers give better quality and larger file..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="255"/>
        <source>Quality (Bitrate)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="309"/>
        <source>Media streaming communications protocol.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="324"/>
        <source>Method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="377"/>
        <source>Searches for the subtitle and downloads it at the same time as the video stream.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="380"/>
        <location filename="../newprg.ui" line="1087"/>
        <source>Include Subtitle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="418"/>
        <source>Download the file you just searched for.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="453"/>
        <source>Download all files you added to the list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="456"/>
        <location filename="../newprg.ui" line="1125"/>
        <source>Download all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="421"/>
        <location filename="../newprg.ui" line="954"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="472"/>
        <source>Select quality on the video you download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="481"/>
        <source>Quality (bitrate) and method. Higher bitrate gives better quality and larger file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="517"/>
        <source>Select quality on the video you download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="561"/>
        <source>Select provider. If yoy need a password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="574"/>
        <location filename="../newprg.ui" line="1075"/>
        <location filename="../newprg.ui" line="1183"/>
        <source>If no saved password is found, click here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="577"/>
        <location filename="../newprg.ui" line="1072"/>
        <location filename="../newprg.ui" line="1180"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="672"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="685"/>
        <source>&amp;Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="729"/>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="778"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="796"/>
        <source>&amp;Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="829"/>
        <source>&apos;&amp;st&apos; cookies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="858"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="867"/>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="733"/>
        <source>Check for updates at program start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="876"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="885"/>
        <source>Check for updates...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="894"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="897"/>
        <source>Exits the program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="900"/>
        <source>F4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="909"/>
        <source>About svtplay-dl...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="918"/>
        <source>About FFmpeg...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="942"/>
        <source>Paste the link to the page where the video is displayed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="957"/>
        <source>Download the stream you just searched for.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="966"/>
        <source>License streamCapture2...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="975"/>
        <source>License svtplay-dl...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="984"/>
        <source>License FFmpeg...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="989"/>
        <source>Recent files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1001"/>
        <source>Help...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1016"/>
        <source>Look at the list of all the streams to download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1033"/>
        <source>Delete download list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1028"/>
        <source>All saved streams in the download list are deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="801"/>
        <source>&amp;Download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="813"/>
        <source>L&amp;ogin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="821"/>
        <source>&amp;All Episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="834"/>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1013"/>
        <source>View Download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1025"/>
        <source>Delete Download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1042"/>
        <source>Italian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1051"/>
        <source>Version history...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1060"/>
        <source>Create new user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1063"/>
        <source>Save the name of a video stream provider, your username and, if you want, your password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1090"/>
        <source>Searching for and downloading subtitles.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1098"/>
        <source>Explain what is going on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="152"/>
        <location filename="../newprg.ui" line="1110"/>
        <source>Add to Download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1113"/>
        <source>Add current video to the list of streams that will be downloaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1128"/>
        <source>Download all the streams in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1139"/>
        <source>Create folder &quot;method_quality_amount&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1227"/>
        <source>Looking for video streams for all episodes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1246"/>
        <source>Direct copy to the default copy location.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1261"/>
        <source>Save the location where the finished video file is copied. If you use &quot;Direct download of all...&quot; no files are ever copied.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1295"/>
        <source>Adds all episodes to the download list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1347"/>
        <source>Use svtplay-dl beta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1361"/>
        <source>Use svtplay-dl stable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1387"/>
        <source>Select svtplay-dl that you have in your computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1398"/>
        <source>Use svtplay-dl that you selected in your computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1454"/>
        <source>Useful when testing your own translation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="740"/>
        <source>Edit settings (Advanced)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1142"/>
        <source>Automatically creates a folder for each downloaded video stream. If you use &quot;Direct download of all...&quot; no folders are ever created.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1151"/>
        <source>Edit Download List (Advanced)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1154"/>
        <source>Change method or quality. Remove a download from the list. NOTE! If you change incorrectly, it will not work.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1162"/>
        <source>Show more</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1188"/>
        <source>Uninstall streamCapture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1191"/>
        <source>Uninstall and remove all components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1207"/>
        <source>Download after Date...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1216"/>
        <source>Stop all downloads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1219"/>
        <source>Trying to stop svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1232"/>
        <location filename="../newprg.ui" line="1336"/>
        <source>Delete all settings and Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1235"/>
        <source>All saved searches and the list of streams to be downloaded will be deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1243"/>
        <source>Copy to Selected Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1258"/>
        <source>Select Copy Location...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1270"/>
        <source>Select Default Download Location...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1273"/>
        <source>Save the location for direct download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1281"/>
        <source>Download to Default Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1284"/>
        <source>Direct download to the default location.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1199"/>
        <source>Direct Download of all Episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="533"/>
        <source>720p=1280x720, 1080p=1920x1080</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="603"/>
        <source>Allows given quality to differ by an amount. 300 usually works well. (Bit rate +/- 300).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1136"/>
        <source>Create folder &quot;method_quality_amount_resolution&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1165"/>
        <source>View more information from svtplay-dl. Appears in purple text color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1202"/>
        <source>Trying to immediately download all episodes. Unable to create folders or select quality.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1224"/>
        <source>List all Episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1292"/>
        <source>Add all Episodes to Download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1304"/>
        <source>Select font...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1313"/>
        <source>Visit svtplay-dl forum for issues...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1325"/>
        <source>Maintenance Tool...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1328"/>
        <source>Starts the Maintenance Tool. To update or uninstall.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1339"/>
        <source>All saved searches, download list and settings are deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1372"/>
        <source>Use svtplay-dl from the system path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1375"/>
        <source>Uses (if available) svtplay-dl in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1384"/>
        <source>Select svtplay-dl...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1395"/>
        <source>Use the selected svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1406"/>
        <source>Do not show notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1409"/>
        <source>Do not show notifications when the download is complete.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1421"/>
        <source>Create a shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1432"/>
        <source>Desktop Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1435"/>
        <source>Create a shortcut to streamCapture2 on the desktop.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1443"/>
        <source>Applications menu Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1446"/>
        <source>Create a shortcut to streamCapture2 in the operating system menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1451"/>
        <source>Load external language file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1462"/>
        <source>Set new &apos;st&apos; cookie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1471"/>
        <source>Download svtplay-dl...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1474"/>
        <source>Download and decompress svtplay-dl from bin.ceicer.com</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1486"/>
        <source>Zoom In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1489"/>
        <source>Increase the font size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1492"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1497"/>
        <source>Zoom Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1500"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1509"/>
        <source>Zoom Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1512"/>
        <source>Decrease the font size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1515"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1524"/>
        <source>Check for the latest svtplay-dl from bin.ceicer.com...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1533"/>
        <location filename="../newprg.ui" line="1536"/>
        <location filename="../newprg.ui" line="1539"/>
        <source>Download Microsoft runtime (required for svtplay-dl)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1542"/>
        <source>Download the runtime file from bin.ceicer.com.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1554"/>
        <source>License 7zip...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1565"/>
        <source>Check streamCapture2 for updates at start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1573"/>
        <source>Check for new versions of svtplay-dl at start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1578"/>
        <source>streamCapture2 settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1583"/>
        <source>download svtplay-dl settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1592"/>
        <source>NFO info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1595"/>
        <source>NFO files contain media release information. Available at svtplay.se, among other places.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
