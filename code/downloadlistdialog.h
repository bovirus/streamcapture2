// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Program name
//          Copyright (C) 2020 Ingemar Ceicer
//          http://ceicer.org/ingemar/
//          programmering1 (at) ceicer (dot) org
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef DOWNLOADLISTDIALOG_H
#define DOWNLOADLISTDIALOG_H

#include <QDialog>
#include <QStandardPaths>
#include "info.h"
#include "newprg.h"
namespace Ui
{
class DownloadListDialog;
}

class DownloadListDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DownloadListDialog(QWidget *parent = nullptr);
    ~DownloadListDialog();
    void initiate(const QStringList *instruktions, const QString *content, bool readonly);
    void openFile(const QStringList *instruktions, const QString *filename, bool readonly);
private:
    Ui::DownloadListDialog *ui;
    void closeEvent(QCloseEvent * e);

signals:
    void sendContent(QString *modefiedContent);


};

#endif // DOWNLOADLISTDIALOG_H
