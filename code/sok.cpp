// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"

/* SOK */
void Newprg::sok()
{
    ui->comboBox->clear();

    if(!fileExists(svtplaydl)) {
        ui->teOut->setTextColor(QColor("red"));
        ui->teOut->setText(svtplaydl +
                           tr(" cannot be found or is not an executable program."));
        ui->teOut->append(tr("Please click on \"Tools\" and select svtplay-dl."));
        ui->teOut->setTextColor(QColor("black"));
        return;
    }

    ui->teOut->setTabStopDistance(170);
    ui->teOut->append(tr("The information from svtplay-dl may or may not contain:"));
    ui->teOut->append(tr("Quality, Method, Codec, Resolution, Language and Role") + "\n");
//        ui->teOut->append(tr("Quality") + "\t" + tr("Method") + "\t" + tr("Codec") + "\t" + tr("Resolution") + "\t" + tr("Language") + "\t" + tr("Role"));
    QStringList ARG;
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Stcookies");
    QString stcookie = settings.value("stcookie", "").toString();
    settings.endGroup();

    if(!stcookie.isEmpty()) {
        ARG << "--cookies"  << "st=" + stcookie;
    }

    ui->teOut->setTextColor("black");
    ui->pbSok->setEnabled(false);
    ui->actionSearch->setEnabled(false);
    ui->actionListAllEpisodes->setEnabled(false);
    QTimer::singleShot(2000, this, [this] {
        ui->pbSok->setEnabled(true);
        ui->actionSearch->setEnabled(true);
        ui->actionListAllEpisodes->setEnabled(true);
    });
    ui->teOut->setReadOnly(true);
    QString sok = ui->leSok->text();
    sok = sok.simplified();

    if(sok.isEmpty()) {
        ui->teOut->setText(tr("The search field is empty!"));
        ui->leQuality->setText(QLatin1String(""));
        ui->leMethod->setText(QLatin1String(""));
    } else if((sok.left(7) != QStringLiteral(u"http://")) && (sok.left(8) != QStringLiteral(u"https://"))) {
        ui->teOut->setText(tr("Incorrect URL"));
        ui->leQuality->setText(QLatin1String(""));
        ui->leMethod->setText(QLatin1String(""));
    } else {
        address = ui->leSok->text().trimmed();
        int questionmark = address.indexOf('?');
        address = address.left(questionmark);
        // Hit

        if(ui->comboPayTV->currentIndex() == 0) {  // No Password
//            tesstsearch
//            QString test = doTestSearch(address);
//            if(test.trimmed() != "") {
//                ui->teOut->setText(test);
//                return;
//            }
//            if(test != "") {
//                ui->teOut->setText(test);
//                //   return;
//            }
//            QUrl url;
//            url.setUrl(address);
//            address = url.toString();
            ARG << "--list-quality" << address;
        } else { // Password
// cppcheck
//            QSettings settings(QSettings::IniFormat, QSettings::UserScope,
//                               DISPLAY_NAME, EXECUTABLE_NAME);
//            // settings.setIniCodec("UTF-8");
            QString provider = ui->comboPayTV->currentText();
            settings.beginGroup("Provider");
            QString username = settings.value(provider + "/username").toString();
            QString password = settings.value(provider + "/password").toString();
            settings.endGroup();

            if(password.isEmpty()) {
                password = secretpassword;

                if(password == "") {
                    bool ok;
                    QString newpassword = QInputDialog::getText(
                                              nullptr, provider + " " + tr("Enter your password"),
                                              tr("Spaces are not allowed. Use only the characters your "
                                                 "streaming provider approves.\nThe Password will not be "
                                                 "saved."),
                                              QLineEdit::Password, QLatin1String(""), &ok);

                    if(newpassword.indexOf(' ') >= 0) {
                        return;
                    }

                    if(ok) {
                        secretpassword = newpassword;
                        password = newpassword;
                    }
                }
            }

// tesstsearch
//            QString test = doTestSearch(address, username, password);
//            if(test.trimmed() != "") {
//                ui->teOut->setText(test);
//                return;
//            }
            ARG << "--list-quality" << "--username" << username << "--password" << password << address;
        }

        CommProcess2 = new QProcess(nullptr);
        CommProcess2->setProcessChannelMode(QProcess::MergedChannels);
        CommProcess2->start(svtplaydl, ARG);
        processpid = CommProcess2->processId();
        connect(CommProcess2, SIGNAL(finished(int, QProcess::ExitStatus)), this,
                SLOT(onCommProcessExit_sok(int, QProcess::ExitStatus)));
        connect(CommProcess2, &QProcess::readyReadStandardOutput, [this]() {
            QString result(CommProcess2->readAllStandardOutput());

            if(ui->actionShowMore->isChecked()) {
                ui->teOut->setTextColor(QColor("purple"));
                ui->teOut->append(result);
                ui->teOut->setTextColor(QColor("black"));
            }

            if(result.mid(0, 25) == QStringLiteral(u"ERROR: svtplay-dl crashed")) {
                ui->teOut->setTextColor(QColor("red"));
                ui->teOut->append(svtplaydl + tr(" crashed."));
                ui->teOut->append(
                    tr("Can not find any video streams, please check the address."));
                ui->teOut->setTextColor(QColor("black"));
                return;
            }

            if(result.contains(QStringLiteral(u"ERROR: No videos found. You need to login"))) {
                ui->teOut->setTextColor(QColor("red"));
                ui->teOut->append(tr("Unable to log in. Forgot your username and password?"));
                ui->teOut->setTextColor(QColor("black"));
                return;
            }

//            QStringList l;

            if(result.mid(0, 6) != QStringLiteral(u"ERROR:")) {
                ui->pbAdd->setEnabled(true);
                ui->actionAdd->setEnabled(true);
                ui->pbDownload->setEnabled(true);
                result = result.simplified();
                // 2021-07-07
                // Adaptation for 4.0-5-ga162357

                if(result.contains("{'url':")) {
                    result.clear();
                }

                QStringList list = result.split(QStringLiteral(u"INFO:"));
                list.removeDuplicates();

                for(auto& str : list)
                    str = str.trimmed();

                list.removeAll("");

                for(auto &s : list) {
                    if(s.contains("Quality:")) {
                        continue;
                    }

                    s.replace(' ', '\t');
                    ui->teOut->append(s);
                    int hittat = s.indexOf('\t');
                    hittat = s.indexOf('\t', hittat + 1);
                    s.replace('\t', ' ');
                    ui->comboBox->addItem(s.mid(0, hittat));
//                    for(QString s : qAsConst(list)) {
//                        ui->teOut->append(s);
//                    }
                    ui->comboAmount->clear();
                    ui->comboAmount->addItem("- " + tr("Auto select"));

                    for(int i = 0; i <= 10; i++) {
                        ui->comboAmount->addItem(QString::number(i * 100));
                    }

                    ui->comboAmount->setCurrentIndex(0);
                }
            } else {
                if(result.contains("ERROR: No videos found. Cant find video id for the video")) {
                    QString notfind = tr("ERROR: No videos found. Cant find video id for the video.");
                    ui->teOut->append(notfind);
                } else {
                    ui->teOut->append(result);
                }
            }
        });
    }
}

/* END SOK */
