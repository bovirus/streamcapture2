// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"

void Newprg::about()
{
    auto *information = new Info;
    information->getSystem(this->x(), this->y());
    delete information;
}
void Newprg::versionHistory()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Language");
    QString sp = settings.value("language", "en_US").toString();
    settings.endGroup();
    QString readme_sp(":/txt/readme_" + sp + ".txt");
    QFileInfo fi(readme_sp);

    if(!fi.exists()) {
        readme_sp = ":/txt/readme_en_US.txt";
    }

    QStringList *infoList = new QStringList;
    *infoList << tr("Version history");
    *infoList << QString("wordwrap");
    DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
    mDownloadListDialog->openFile(infoList, &readme_sp, true);
}


void Newprg::license()
{
    QStringList *infoList = new QStringList;
    *infoList << tr("License streamCapture2");
    *infoList << QString("wordwrap");
    QString *filename = new QString(QStringLiteral(u":/txt/license.txt"));
    DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
    mDownloadListDialog->openFile(infoList, filename, true);
}

void Newprg::licenseSvtplayDl()
{
    QStringList *infoList = new QStringList;
    *infoList << tr("License svtplay-dl");
    *infoList << QString("wordwrap");
    QString *filename = new QString(QStringLiteral(u":/txt/license_svtplay-dl.txt"));
    DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
    mDownloadListDialog->openFile(infoList, filename, true);
}
#ifdef FFMPEG
void Newprg::licenseFfmpeg()
{
    QStringList *infoList = new QStringList;
    *infoList << tr("License svtplay-dl");
    *infoList << QString("wordwrap");
    QString *filename = new QString(QStringLiteral(u":/txt/license_FFmpeg.txt"));
    DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
    mDownloadListDialog->openFile(infoList, filename, true);
}
#endif
#ifdef  Q_OS_WINDOWS
void Newprg::license7zip()
{
    QStringList *infoList = new QStringList;
    *infoList << tr("License 7zip");
    *infoList << QString("wordwrap");
    QString *filename = new QString(QStringLiteral(u":/txt/license_7zip.txt"));
    DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
    mDownloadListDialog->openFile(infoList, filename, true);
}
#endif

void Newprg::aboutFfmpeg()
{
    QString *instring = new QString;
    QString EXECUTE = selectFfmpeg();

    if(EXECUTE == QStringLiteral(u"NOTFIND")) {
#ifdef Q_OS_WINDOWS
#ifdef PORTABLE
        *instring = "FFmpeg" + tr(" could not be found. Please download a portable streamCapture2 where FFmpeg is included.");
#endif
#ifndef PORTABLE
        *instring = "FFmpeg" + tr(" could not be found. Please go to \"Tools\", \"Maintenance Tool\" to install FFmpeg.");
#endif
        * instring += " " + tr("Or install ") + "FFmpeg" + tr(" in your system.");
#endif
#ifdef Q_OS_LINUX
        *instring = "FFmpeg" + tr(" could not be found. Please download an AppImage where FFmpeg is included.");
        *instring += " " + tr("Or install ") + "FFmpeg" + tr(" in your system.");
#endif
    } else {
        auto *CommProcess = new QProcess(this);
        CommProcess->setProcessChannelMode(QProcess::MergedChannels);
        CommProcess->start(EXECUTE, QStringList() << "--help");
        CommProcess->waitForFinished(-1);
        QTextStream stream(CommProcess->readAllStandardOutput());
        QString line;
        int i = 0;

//        line = stream.readAll();
//        *instring += line;
        while(!stream.atEnd()) {
            line = stream.readLine();

//            line = line.simplified();
            if(i != 0) {
                *instring += "<br>" + line;
            } else {
                *instring += "<b>" + line + "</b><br>";
            }

            i++;
        }
    }

    QStringList *infoList = new QStringList;
    *infoList << tr("About FFmpeg");
    *infoList << QString("wordwrap");
    DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
    mDownloadListDialog->initiate(infoList, instring, true);
    delete instring;
}

void Newprg::aboutSvtplayDl()
{
    QString *s = new QString;

    if(ui->actionSvtplayDlSystem->isChecked()) {
        if(findSvtplayDl() == QStringLiteral(u"NOTFIND")) {
#ifdef Q_OS_LINUX
            *s = tr("svtplay-dl is not found in the system path.");
//            *s += " ";
            *s += tr(" You can download svtplay-dl from bin.ceicer.com. Select \"Tools\", \"Download svtplay-dl...\"");
#endif
#ifdef Q_OS_WIN
            *s += tr("svtplay-dl.exe is not found in the system path.");
            *s += tr(" You can download svtplay-dl from bin.ceicer.com. Select \"Tools\", \"Download svtplay-dl...\"");
#endif
            QStringList *infoList = new QStringList;
            *infoList << tr("About svtplay-dl");
            DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
            mDownloadListDialog->initiate(infoList, s, true);
            return;
        }
    }

    if(ui->actionSvtplayDlStable->isChecked()) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME,
                           LIBRARY_NAME);
        //    // settings.setIniCodec("UTF-8");
        settings.beginGroup("Path");
        QString stablepath = settings.value("stablepath").toString();
        settings.endGroup();
#ifdef Q_OS_WINDOWS
        svtplaydl = stablepath + "/stable/svtplay-dl.exe";
#endif
#ifdef Q_OS_LINUX
        svtplaydl = stablepath + "/stable/svtplay-dl";
#endif
    } else if(ui->actionSvtplayDlBleedingEdge->isChecked()) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME,
                           LIBRARY_NAME);
        //    // settings.setIniCodec("UTF-8");
        settings.beginGroup("Path");
        QString betapath = settings.value("betapath").toString();
        settings.endGroup();
#ifdef Q_OS_WINDOWS
        svtplaydl = betapath + "/beta/svtplay-dl.exe";
#endif
#ifdef Q_OS_LINUX
        svtplaydl = betapath + "/beta/svtplay-dl";
#endif
    }

    if(!fileExists(svtplaydl)) {
#ifdef Q_OS_WIN

        if(!ui->actionSvtPlayDlManuallySelected->isChecked()) {
            *s += svtplaydl + tr(" cannot be found. Go to \"Tools\", ""\"Download svtplay-dl...\" to download.");
#endif
#ifdef Q_OS_LINUX

            if(!ui->actionSvtPlayDlManuallySelected->isChecked()) {
                *s += svtplaydl + tr(" cannot be found. Go to \"Tools\", ""\"Download svtplay-dl...\" to download.");
#endif
            } else {
                *s += tr("Please click \"Tools\" and select svtplay-dl.");
            }

            QStringList *infoList = new QStringList;
            *infoList << tr("About svtplay-dl");
            *infoList << QString("wordwrap");
            DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
            mDownloadListDialog->initiate(infoList, s, true);
            return;
        }

        QString *currentVersion =  new QString(getSvtplaydlVersion(&svtplaydl));

        if(!currentVersion->isEmpty()) {
            *s += "<b>svtplay-dl " + tr("version ") + *currentVersion + "</b><br><br>";
            auto *CommProcess = new QProcess(this);
            CommProcess->setProcessChannelMode(QProcess::MergedChannels);
            CommProcess->start(svtplaydl, QStringList() << QStringLiteral(u"--help"));
            CommProcess->waitForFinished(-1);
            QTextStream stream(CommProcess->readAllStandardOutput());
            QString line;
            line.clear();
            int i = 0;

            while(!stream.atEnd()) {
                line = stream.readLine();
                line = line.simplified();

                if(i != 0) {
                    *s += "<br>" + line;
                } else {
                    *s += line;
                }

                i++;
            }
        }

        QStringList *infoList = new QStringList;
        *infoList << tr("About svtplay-dl");
        *infoList << QString("wordwrap");
        DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
        mDownloadListDialog->initiate(infoList, s, true);
        delete currentVersion;
    }

// END void Newprg::aboutSvtplayDl()
//
    bool Newprg::fileExists(QString & path) {
        QFileInfo check_file(path);

        // check if file exists and if yes: Is it really a file and no directory?
        // and is it executable?
        if(check_file.exists() && check_file.isFile() && check_file.isExecutable()) {
            return true;
        } else {
            return false;
        }
    }
    QString Newprg::getSvtplaydlVersion(QString * svtplaydl) {
        auto *CommProcess = new QProcess(nullptr);
        CommProcess->setProcessChannelMode(QProcess::MergedChannels);
        CommProcess->start(*svtplaydl, QStringList() << QStringLiteral(u"--version"));
        CommProcess->waitForFinished(-1);
        QString line;
        QTextStream stream(CommProcess->readAllStandardOutput());

        while(!stream.atEnd()) {
            line = stream.readLine();
        }

        line = line.mid(11);
        return line;
    }
    QString Newprg::selectFfmpeg() {
#ifdef Q_OS_WIN // QT5
        QString EXECUTE =
            QCoreApplication::applicationDirPath() + QStringLiteral(u"/ffmpeg.exe");
#endif
#ifdef Q_OS_LINUX
        QString EXECUTE =
            QCoreApplication::applicationDirPath() + QStringLiteral(u"/ffmpeg");
#endif

        if(fileExists(EXECUTE)) {
            return EXECUTE;
        } else {
            QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
            QString senv = env.value(QStringLiteral(u"PATH"));
#ifdef Q_OS_LINUX
            QStringList slenv = senv.split(QStringLiteral(u":"));
#endif
#ifdef Q_OS_WIN
            QStringList slenv = senv.split(QStringLiteral(u";"));
#endif

            foreach(QString s, slenv) {
                s = QDir::fromNativeSeparators(s);
#ifdef Q_OS_LINUX
                s.append(QStringLiteral(u"/ffmpeg"));
#endif
#ifdef Q_OS_WIN
                s.append(QStringLiteral(u"/ffmpeg.exe"));
#endif

                if(fileExists(s)) {
//                if(s.contains("ffmpeg.exe")) {
#ifdef Q_OS_LINUX
                    return QStringLiteral(u"ffmpeg");
#endif
#ifdef Q_OS_WIN
//                    return QStringLiteral(u"ffmpeg.exe");
                    return s;
#endif
                }
            }
        }

        return QStringLiteral(u"NOTFIND");
    }
