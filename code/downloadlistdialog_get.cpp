// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"

void Newprg::getDownloadList(QString *content)
{
    downloadList = content->split(QRegularExpression("[\n]"), Qt::SkipEmptyParts);
}
void Newprg::getSettings(QString *content)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    QString inifile = settings.fileName();
#ifdef Q_OS_WINDOWS
//    QStringList list = content->split("\r\n", Qt::SkipEmptyParts);
    QStringList list = content->split("\r\n", Qt::SkipEmptyParts);
#endif
#ifdef Q_OS_LINUX
    QStringList list = content->split("\n", Qt::SkipEmptyParts);
#endif
    QFile fil(inifile);

    if(fil.open(QIODevice::WriteOnly)) {
        QTextStream in(&fil);

        foreach(const QString s, list) {
#ifdef Q_OS_LINUX
            in << s + "\n";
#endif
#ifdef Q_OS_WINDOWS
            in << s + "\r\n";
#endif
        }
    }

    fil.close();
}
void Newprg::getSettingsDownloadunpack(QString *content)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, LIBRARY_NAME);
    QString inifile = settings.fileName();
#ifdef Q_OS_WINDOWS
//    QStringList list = content->split("\r\n", Qt::SkipEmptyParts);
    QStringList list = content->split("\r\n", Qt::SkipEmptyParts);
#endif
#ifdef Q_OS_LINUX
    QStringList list = content->split("\n", Qt::SkipEmptyParts);
#endif
    QFile fil(inifile);

    if(fil.open(QIODevice::WriteOnly)) {
        QTextStream in(&fil);

        foreach(const QString s, list) {
#ifdef Q_OS_LINUX
            in << s + "\n";
#endif
#ifdef Q_OS_WINDOWS
            in << s + "\r\n";
#endif
        }
    }

    fil.close();
}
void Newprg::getRecentSearches(QString *content)
{
//    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
//    QString inifile = settings.fileName();
//    QFileInfo fi(inifile);
//    QString recentsearches = fi.absolutePath() + "/recentfiles.list";
//    QStringList list = content->split(QRegularExpression("[\n]"), Qt::SkipEmptyParts);
#ifdef Q_OS_WINDOWS
    QStringList list = content->split("\r\n", Qt::SkipEmptyParts);
#endif
#ifdef Q_OS_LINUX
    QStringList list = content->split("\n", Qt::SkipEmptyParts);
#endif
//    QFile fil(recentsearches);
    recentFiles.clear();

//    if(fil.open(QIODevice::WriteOnly)) {
//        QTextStream in(&fil);

    for(const QString &s : qAsConst(list)) {
//            in << s + "\n";
        recentFiles << s;
    }

//        fil.close();
//    }
}
