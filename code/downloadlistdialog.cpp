
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "downloadlistdialog.h"
#include "ui_downloadlistdialog.h"

DownloadListDialog::DownloadListDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DownloadListDialog)
{
    ui->setupUi(this);
#ifdef Q_OS_LINUX
    QCoreApplication::setAttribute(Qt::AA_DontUseNativeDialogs);
#endif
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Downloadlistdialog");
    restoreGeometry(settings.value("geometry", saveGeometry()).toByteArray());
    settings.endGroup();
    QColor c(RED, GREEN, BLUE);
    QPalette p = ui->textEdit->palette(); // define pallete for textEdit..
    p.setColor(QPalette::Base, c);     // set color "c" for textedit base
    ui->textEdit->setPalette(p);          // change textedit palette
    /* Font */
    int id = QFontDatabase::addApplicationFont(":/fonts/Ubuntu-R.ttf");
    QString familj = QFontDatabase::applicationFontFamilies(id).at(0);
    QFont f(familj);
    this->setFont(f);
    settings.beginGroup("Font");
    QString family = settings.value("family", f.family()).toString();
    int size = settings.value("size", FONT).toInt();
    bool bold = settings.value("bold", false).toBool();
    bool italic = settings.value("italic", false).toBool();
    bool boldItalic = settings.value("boldItalic", false).toBool();
    settings.endGroup();
    QFont f2(family, size);

    if(boldItalic) {
        f2.setBold(true);
        f2.setItalic(true);
    } else {
        if(bold) {
            f2.setBold(true);
        } else if(italic) {
            f2.setItalic(true);
        } else {
            f2.setBold(false);
            f2.setItalic(false);
        }
    }

    /* End Font */
    QTextDocument *doc = new QTextDocument;
    qreal real = 8;
    doc->setDocumentMargin(real);
    ui->textEdit->setDocument(doc);
    QTextCursor cursor = ui->textEdit->textCursor();
    ui->textEdit->selectAll();
    ui->textEdit->setFont(f2);
    ui->textEdit->setTextCursor(cursor);
    connect(ui->pbSave, &QPushButton::clicked, [this]() {
        QString *data =  new QString(ui->textEdit->toPlainText());
        emit sendContent(data);
    });
    connect(ui->pbSaveAs, &QPushButton::clicked, [ this ]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Downloadlistdialog");
        QString copyfileto = settings.value("copyfileto", QStandardPaths::writableLocation(QStandardPaths::HomeLocation)).toString();
        settings.endGroup();
        int x = this->x();
        int y = this->y();
        QFileDialog *dialog = new QFileDialog(this);
        dialog->setLabelText(QFileDialog::Accept, tr("Save"));
        dialog->setLabelText(QFileDialog::Reject, tr("Cancel"));
        dialog->setWindowTitle(tr("Save as text file"));
        dialog->setGeometry(x + 50, y + 50, 900, 550);
        dialog->setViewMode(QFileDialog::Detail);
        dialog->setFileMode(QFileDialog::AnyFile);
        dialog->setDirectory(copyfileto);
        dialog->setAcceptMode(QFileDialog::AcceptSave);

        if(dialog->exec() == 1) {
            QString newfile = dialog->selectedFiles().at(0);
            QFileInfo fi(newfile);
            QString path = fi.absolutePath();
            QFileInfo fi1(path);

            if(!fi1.isWritable()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("Could not save the file.\nCheck your file permissions."));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                return;
            }

            QFile file_newfile(newfile);
            QString text = ui->textEdit->toPlainText();
            QStringList list = text.split(QRegularExpression("[\r\n]"), Qt::KeepEmptyParts);
            file_newfile.open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream outStream(&file_newfile);

            for(const QString &s : qAsConst(list)) {
                outStream << s + "\n";
            }

            file_newfile.close();
            fi.setFile(file_newfile);
            copyfileto = fi.absolutePath();
            settings.beginGroup("Downloadlistdialog");
            settings.setValue("copyfileto", copyfileto);
            settings.endGroup();
        }
    });
//    connect(ui->pbClose, &QPushButton::clicked, this, &DownloadListDialog::close);
    connect(ui->pbClose, &QPushButton::clicked, [this]() {
        close();
    });
}



void DownloadListDialog::initiate(const QStringList *instructions, const QString *content, bool readonly)
{
    this->setWindowTitle(instructions->at(0));
    ui->textEdit->clear();

    if(readonly) {
        ui->textEdit->setReadOnly(true);
        ui->pbSave->setDisabled(true);
        ui->pbSave->hide();
    }

    QStringList strList = content->split(QRegularExpression("[\n]"), Qt::SplitBehavior(Qt::KeepEmptyParts));

    for(const QString &s : qAsConst(strList)) {
        ui->textEdit->insertHtml(s + "<br>");
    }

    ui->textEdit->moveCursor(QTextCursor::Start);
    this->show();
}



void DownloadListDialog::openFile(const QStringList *instructions, const QString *filename, bool readonly)
{
    this->setWindowTitle(instructions->at(0));
    ui->textEdit->clear();

    if(readonly) {
        ui->textEdit->setReadOnly(true);
        ui->pbSave->setDisabled(true);
        ui->pbSave->hide();
    }

    if(instructions->length() == 2) {
        if(instructions->at(1) == QString("wordwrap")) {
            ui->textEdit->setWordWrapMode(QTextOption::WordWrap);
        }
    } else {
        ui->textEdit->setWordWrapMode(QTextOption::WrapAnywhere);
    }

    QFile file(*filename);
    QString content;

    if(!file.exists()) {
        int hittat = filename->lastIndexOf('/');
        QString filnamnet = filename->mid(hittat + 1);
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setWindowTitle(tr("Unable to find file"));
        msgBox.setText(tr("Unable to find") + " \"" + filnamnet + "\"");
//        msgBox.addButton(QMessageBox::Ok);
//        msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
    } else {
        file.open(QIODevice::ReadOnly);

        while(!file.atEnd()) {
            content += file.readLine() + "<br>";
        }

        file.close();
    }

    ui->textEdit->insertHtml(content);
    ui->textEdit->moveCursor(QTextCursor::Start);
    this->show();
}
void DownloadListDialog::closeEvent(QCloseEvent * e)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Downloadlistdialog");
    settings.setValue("geometry", saveGeometry());
    settings.endGroup();
    e->accept();
}

DownloadListDialog::~DownloadListDialog()
{
    delete ui;
}
