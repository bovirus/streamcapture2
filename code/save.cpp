// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"

QString Newprg::save(QString windowTitle)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup(QStringLiteral(u"Path"));
    QString savepath = settings.value("savepath", QStandardPaths::writableLocation(QStandardPaths::MoviesLocation)).toString();
    settings.endGroup();
    QFileDialog filedialog(this);
    int x = this->x();
    int y = this->y();
    //        filedialog.setOption(QFileDialog::DontUseNativeDialog, true);
    filedialog.setDirectory(savepath);
    filedialog.setLabelText(QFileDialog::Accept, tr("Choose"));
    filedialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
    filedialog.setWindowTitle(windowTitle);
    filedialog.setGeometry(x + 50, y + 50, 900, 550);
    filedialog.setFileMode(QFileDialog::Directory);//Folder name

    if(filedialog.exec() == 1) {
        QDir dir = filedialog.directory();
        QString sdir = dir.path();
        settings.beginGroup("Path");
        settings.setValue("savepath", sdir);
        settings.endGroup();
        settings.sync();
        return sdir;
    } else {
        return "nothing";
    }

//    QString dir = QFileDialog::getExistingDirectory(
//                      this, tr("Save streaming media to directory"), savepath,
//                      QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
//    if(dir != "") {
//        settings.beginGroup("Path");
//        settings.setValue("savepath", dir);
//        settings.endGroup();
//    } else {
//        return "nothing";
//    }
//    return dir;
}
