// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"

void Newprg::editOrDelete(const QString &provider)
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Question);
    msgBox.setWindowTitle(tr("Manage Login details for ") + provider);
    msgBox.setText(tr("Edit, rename or delete\n") + provider + "?");
    QPushButton *btnEdit = msgBox.addButton(tr("Edit"), QMessageBox::ActionRole);
    QPushButton *btnRename = msgBox.addButton(tr("Rename"), QMessageBox::ActionRole);
    QPushButton *btnDelete = msgBox.addButton(tr("Delete"), QMessageBox::ActionRole);
    QPushButton *btnCancel = msgBox.addButton(tr("Cancel"), QMessageBox::ActionRole);
    msgBox.setDefaultButton(btnEdit);
    msgBox.exec();

    if(msgBox.clickedButton() == btnEdit) {
        editprovider(provider);
    } else if(msgBox.clickedButton() == btnRename) {
        renameProvider(provider);
    } else if(msgBox.clickedButton() == btnDelete) {
        deleteProvider(provider);
    } else if(msgBox.clickedButton() == btnCancel) {
        return;
    }
}

void Newprg::deleteProvider(const QString &provider)
{
    ui->menuPayTV->clear();
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Provider");
    QStringList keys = settings.childGroups();
    settings.endGroup();
    int find = keys.indexOf(provider);
    keys.removeAt(find);
    QIcon icon(":/images/new.png");
    QAction *actionCreateNew = new QAction(icon, tr("Create New"), nullptr);
    ui->menuPayTV->addAction(actionCreateNew);
    connect(actionCreateNew, &QAction::triggered, [this]() {
        newSupplier();
    });
    // cppcheck
    // QAction *actionPayTV;
    keys.sort(Qt::CaseInsensitive);
    ui->comboPayTV->clear();
    ui->comboPayTV->addItem("- " + tr("No Password"));

    foreach(QString p, keys) {
        // cppcheck
        // actionPayTV = new QAction(p, nullptr);
        QAction *actionPayTV = new QAction(p, nullptr);
        ui->menuPayTV->addAction(actionPayTV);
        ui->comboPayTV->addItem(p);
        connect(actionPayTV, &QAction::triggered,
        [this, actionPayTV]() {
            editOrDelete(actionPayTV->text());
        });
    }

    settings.beginGroup("Provider");
    settings.remove(provider);
    settings.endGroup();
}

void Newprg::editprovider(QString provider)
{
    secretpassword = "";
    provider = provider.trimmed();
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Provider");
    const QString username =
        settings.value(provider + "/username", "").toString();
    const QString password =
        settings.value(provider + "/password", "").toString();
    settings.endGroup();
    bool b;

    do {
        b = testinputusername_edit(username, provider);
    } while(!b);

    do {
        b = testinputpassword_edit(password, provider);
    } while(!b);
}
// private:
bool Newprg::testinputusername_edit(const QString &username,
                                    const QString &provider)
{
    bool ok;
    QInputDialog inputdialog;
    inputdialog.setOkButtonText(tr("Ok"));
    inputdialog.setCancelButtonText(tr("Cancel"));
    inputdialog.setTextValue(username);
    inputdialog.setWindowTitle(tr("Enter your username"));
    inputdialog.setInputMode(QInputDialog::TextInput);
    inputdialog.setLabelText(tr("Spaces are not allowed. Use only the characters\nyour streaming provider approves."));
    ok = inputdialog.exec();
    QString newusername = inputdialog.textValue();

    if(newusername.indexOf(' ') >= 0) {
        return false;
    }

    if(ok) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Provider");
        settings.setValue(provider + "/username", newusername);
        settings.endGroup();
        return true;
    }

    return true;
}
// private:
bool Newprg::testinputpassword_edit(const QString &password,
                                    const QString &provider)
{
    bool ok;
    QInputDialog inputdialog;
    inputdialog.setOkButtonText(tr("Ok"));
    inputdialog.setCancelButtonText(tr("Cancel"));
    inputdialog.setTextValue(password);
    inputdialog.setTextEchoMode(QLineEdit::Password);
    inputdialog.setWindowTitle(tr("Enter your password"));
    inputdialog.setInputMode(QInputDialog::TextInput);
    inputdialog.setLabelText(tr("Spaces are not allowed. Use only the characters\nyour streaming provider approves."));
    ok = inputdialog.exec();
    QString newpassword = inputdialog.textValue();

    if(newpassword.indexOf(' ') >= 0) {
        return false;
    }

    if(ok) {
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Save password?"));
        msgBox.setIcon(QMessageBox::Question);
        msgBox.setText(tr("Do you want to save the password? (unsafe)?"));
//        msgBox.setStandardButtons(QMessageBox::Yes);
//        msgBox.addButton(QMessageBox::No);
//        msgBox.setDefaultButton(QMessageBox::No);
//        msgBox.setButtonText(QMessageBox::Yes, tr("Yes"));
//        msgBox.setButtonText(QMessageBox::No, tr("No"));
        msgBox.addButton(tr("Yes"), QMessageBox::AcceptRole);
        QPushButton *but = msgBox.addButton(tr("No"), QMessageBox::RejectRole);
        msgBox.setDefaultButton(but);
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Provider");

        if(msgBox.exec() == QMessageBox::AcceptRole) {
            settings.setValue(provider + "/password", newpassword);
            secretpassword = newpassword;
        } else {
            settings.setValue(provider + "/password", "");
            secretpassword = newpassword;
        }

        settings.endGroup();
        return true;
    }

    return true;
}

bool Newprg::renameProvider(const QString &provider)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Provider");
    const QString username =
        settings.value(provider + "/username", "").toString();
    const QString password =
        settings.value(provider + "/password", "").toString();
    settings.endGroup();
    bool ok;
    QInputDialog inputdialog;
    inputdialog.setOkButtonText(tr("Ok"));
    inputdialog.setCancelButtonText(tr("Cancel"));
    inputdialog.setWindowTitle(tr("Streaming service"));
    inputdialog.setInputMode(QInputDialog::TextInput);
    inputdialog.setLabelText(tr("Enter the name of your streaming service."));
    inputdialog.setTextValue(provider);
    ok = inputdialog.exec();
    QString newprovider = inputdialog.textValue();
    deleteProvider(provider);
    QAction *actionPayTV;
    actionPayTV = new QAction(newprovider);
    ui->menuPayTV->addAction(actionPayTV);
    ui->comboPayTV->addItem(newprovider);
    connect(actionPayTV, &QAction::triggered,
    [actionPayTV, this]() {
        editOrDelete(actionPayTV->text());
    });
    settings.beginGroup("Provider");
    settings.setValue(newprovider + "/username", username);
    settings.setValue(newprovider + "/password", password);
    settings.endGroup();
    return ok;
}
