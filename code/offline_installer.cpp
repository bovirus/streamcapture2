
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>


#include "newprg.h"
#include "info.h"
#ifdef OFFLINE_INSTALLER


void Newprg::offlineInstaller()
{
    QMessageBox *msgBox = new QMessageBox(this);
//    msgBox->setSizeGripEnabled(true);
    msgBox->setInformativeText("<a href = \""  APPLICATION_HOMEPAGE_ENG   "\"><b>" + tr("To the website") + "</b></a>");
    msgBox->setIcon(QMessageBox::Information);
    msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
    msgBox->setText(tr("You have installed with an offline installer.<br><b>You must uninstall before you can install a new version.</b>"));
    QAbstractButton *pbCancel = msgBox->addButton(tr("Cancel"), QMessageBox::RejectRole);
    pbCancel->setFixedSize(QSize(150, 40));
    QAbstractButton *pbUninstall = msgBox->addButton(tr("Uninstall"), QMessageBox::AcceptRole);
    pbUninstall->setFixedSize(QSize(150, 40));
    QAbstractButton *pbUninstallInstall = msgBox->addButton(tr("Uninstall and install new version"), QMessageBox::AcceptRole);
    pbUninstallInstall->setFixedSize(QSize(300, 40));
    pbUninstallInstall->setFocus();
    msgBox->exec();

    if(msgBox->clickedButton() == pbCancel) {
        return;
    }

    if(msgBox->clickedButton() == pbUninstall) {
        const QString EXECUTE =
            QCoreApplication::applicationDirPath() + "/" +
            "uninstall.exe";
        //
        QFileInfo fi(EXECUTE);

        if(!fi.isExecutable()) {
            QMessageBox *msgBox = new QMessageBox(this);
            msgBox->setIcon(QMessageBox::Critical);
            msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox->setText(tr("An unexpected error occurred.<br>") + "\"" + QDir::toNativeSeparators(EXECUTE) + "\"" + tr("<br>can not be found or is not an executable program."));
            msgBox->exec();
            return;
        }

        //
        QProcess p;
        p.setProgram(EXECUTE);
        p.startDetached();
        close();
        return;
    } else if(msgBox->clickedButton() == pbUninstallInstall) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Language");
        QString sp = settings.value("language", "nothing").toString();
        settings.endGroup();

        if(sp == "nothing") {
            sp = "en_US";
        }

        const QString EXECUTE =
            QCoreApplication::applicationDirPath() + "/" +
            "uninstall.exe";
        QFileInfo fi(EXECUTE);

        if(!fi.isExecutable()) {
            QMessageBox *msgBox = new QMessageBox(this);
            msgBox->setIcon(QMessageBox::Critical);
            msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox->setText(tr("An unexpected error occurred.<br>") + "\"" + QDir::toNativeSeparators(EXECUTE) + "\"" + tr("<br>can not be found or is not an executable program."));
            msgBox->exec();
            return;
        }

        QProcess p;
        p.setProgram(EXECUTE);
        p.startDetached();
        close();
        QDesktopServices::openUrl(QUrl(OFFLINE_INSTALLER_DOWNLOAD "." + sp + "_install.php"));
        return;
    }
}



#endif
