// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"


void Newprg::newSupplier()
{
    bool proceed = true;
    QString newprovider;
    bool b;

    do {
        b = testinputprovider(newprovider, proceed);
    } while(!b);

    if(proceed) {
        do {
            b = testinputusername(newprovider);
        } while(!b);

        do {
            b = testinputpassword(newprovider);
        } while(!b);

        QAction *actionPayTV;
        actionPayTV = new QAction(newprovider);
        ui->menuPayTV->addAction(actionPayTV);
        ui->comboPayTV->addItem(newprovider);
        connect(actionPayTV, &QAction::triggered,
        [actionPayTV, this]() {
            editOrDelete(actionPayTV->text());
        });
    }
}

// private:
bool Newprg::testinputprovider(QString &newprovider, bool &proceed)
{
    bool ok;
    QInputDialog inputdialog;
    inputdialog.setOkButtonText(tr("Ok"));
    inputdialog.setCancelButtonText(tr("Cancel"));
    inputdialog.setWindowTitle(tr("Streaming service"));
    inputdialog.setInputMode(QInputDialog::TextInput);
    inputdialog.setLabelText(tr("Enter the name of your streaming service."));
    ok = inputdialog.exec();
    newprovider = inputdialog.textValue();

//    if(newprovider.indexOf(' ') >= 0) {
//        return false;
//    }

    if(ok) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Provider");
        settings.endGroup();
        return true;
    }

    proceed = false;
    return true;
}
// private:
bool Newprg::testinputusername(const QString &provider)
{
    bool ok;
    QInputDialog inputdialog;
    inputdialog.setOkButtonText(tr("Ok"));
    inputdialog.setCancelButtonText(tr("Cancel"));
    inputdialog.setWindowTitle(tr("Enter your username"));
    inputdialog.setInputMode(QInputDialog::TextInput);
    inputdialog.setLabelText(tr("Spaces are not allowed. Use only the characters\nyour streaming provider approves."));
    ok = inputdialog.exec();
    QString newusername  = inputdialog.textValue();

    if(newusername.indexOf(' ') >= 0) {
        return false;
    }

    if(ok) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Provider");
        settings.setValue(provider + "/username", newusername);
        settings.endGroup();
        return true;
    }

    return true;
}



// private:
bool Newprg::testinputpassword(const QString &provider)
{
    bool ok;
    QInputDialog inputdialog;
    inputdialog.setOkButtonText(tr("Ok"));
    inputdialog.setCancelButtonText(tr("Cancel"));
    inputdialog.setWindowTitle(tr("Enter your password"));
    inputdialog.setTextEchoMode(QLineEdit::Password);
    inputdialog.setInputMode(QInputDialog::TextInput);
    inputdialog.setLabelText(tr("Spaces are not allowed. Use only the characters\nyour streaming provider approves."));
    ok = inputdialog.exec();
    QString newpassword  = inputdialog.textValue();

    if(newpassword.indexOf(' ') >= 0) {
        return false;
    }

    if(ok) {
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Save password?"));
        msgBox.setIcon(QMessageBox::Question);
        msgBox.setText(tr("Do you want to save the password? (unsafe)?"));
//        msgBox.setStandardButtons(QMessageBox::Yes);
//        msgBox.addButton(QMessageBox::No);
//        msgBox.setDefaultButton(QMessageBox::No);
//        msgBox.setButtonText(QMessageBox::Yes, tr("Yes"));
//        msgBox.setButtonText(QMessageBox::No, tr("No"));
        msgBox.addButton(tr("Yes"), QMessageBox::AcceptRole);
        QPushButton *but = msgBox.addButton(tr("No"), QMessageBox::RejectRole);
        msgBox.setDefaultButton(but);
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Provider");

        if(msgBox.exec() == QMessageBox::AcceptRole) {
            settings.setValue(provider + "/password", newpassword);
            secretpassword = newpassword;
        } else {
            settings.setValue(provider + "/password", "");
            secretpassword = newpassword;
        }

        settings.endGroup();
        return true;
    }

    return true;
}


