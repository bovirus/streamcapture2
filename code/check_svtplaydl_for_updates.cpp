
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"

void Newprg::checkSvtplaydlForUpdatesHelp()
{
    QString *selectedsvtplaydl = new QString(hittaSvtplaydl());
    QString *currentVersion =  new QString(getSvtplaydlVersion(&svtplaydl));
    QStringList currenStableBleedingedge = checkSvtplaydlForUpdates();
    QString inuse = QString("<b>" + tr("You have downloaded") + "</b>");

    if(currenStableBleedingedge.at(0).isEmpty()) {
        currenStableBleedingedge.replace(0, tr("Nothing"));
        inuse += QString("<br>" + tr("To folder \"stable\": Nothing."));
    } else {
        inuse += QString("<br>" + tr("To folder \"stable\": ") + currenStableBleedingedge.at(0));
    }

    if(currenStableBleedingedge.at(1).isEmpty()) {
//        currenStableBleedingedge.replace(1, tr("Nothing"));
        inuse += QString("<br>" + tr("To folder \"beta\": Nothing."));
    } else {
        inuse += QString("<br>" + tr("To folder \"beta\": ") + currenStableBleedingedge.at(1));
    }

    QString *pinuse = new QString(inuse);
    /*     */
    QByteArray *bytes = new QByteArray;
    QUrl url(CHECK_LATEST);
    auto *nam = new QNetworkAccessManager(nullptr);
    auto *reply = nam->get(QNetworkRequest(url));
    reply->waitForBytesWritten(1000);

    if(reply->error() != QNetworkReply::NoError) {
        QMessageBox *msgBox = new QMessageBox(this);
        msgBox->setIcon(QMessageBox::Critical);
        msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox->setText(tr("An unexpected error occurred.<br>Information about svtplay-dl beta can not be found.<br>Check your internet connection."));
        msgBox->exec();
        return;
    }

    QObject::connect(
        reply, &QNetworkReply::finished,
    [this, reply,  bytes, currentVersion, pinuse, selectedsvtplaydl]() {
        /*   */
        QUrl url_stable(CHECK_LATEST_STABLE);
        auto *nam_stable = new QNetworkAccessManager(nullptr);
        auto *reply_stable = nam_stable->get(QNetworkRequest(url_stable));
        reply_stable->waitForBytesWritten(1000);
        *bytes = reply->readAll(); // bytes

        if(reply->error() != QNetworkReply::NoError) {
            QMessageBox *msgBox = new QMessageBox(this);
            msgBox->setIcon(QMessageBox::Critical);
            msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox->setText(tr("An unexpected error occurred.<br>Information about svtplay-dl stable can not be found.<br>Check your internet connection."));
            msgBox->exec();
            return;
        }

        QObject::connect(
            reply_stable, &QNetworkReply::finished,
        [this, reply_stable, bytes, currentVersion, pinuse, selectedsvtplaydl] {
            *bytes += reply_stable->readAll(); // bytes

            if(reply_stable->error() == QNetworkReply::ContentNotFoundError)
            {
                QMessageBox *msgBox = new QMessageBox(this);
                msgBox->setIcon(QMessageBox::Critical);
                msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox->setText(tr("An unexpected error occurred.<br>Information about svtplay-dl stable can not be found.<br>Check your internet connection."));
                msgBox->exec();
                return;
            }
            QString latestVersion = (QString) * bytes;


#ifdef Q_OS_LINUX
            QString splittrar(QStringLiteral("\n"));
//            QString splittrar = "\n";
#endif

#ifdef Q_OS_WINDOWS
            QString splittrar(QStringLiteral("\r\n"));
//            QString splittrar = "\r\n";
#endif

            latestVersion = latestVersion.trimmed();
            QStringList versions = latestVersion.split(splittrar);
            // version 1.1.6
            versions.removeAll("");
            //

            QMessageBox msgBox;


            msgBox.setIcon(QMessageBox::Information);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            QString ut;
            QString youAreUsing, yourVersion;

            if(!currentVersion->isEmpty())
            {
                youAreUsing = tr("You are using version") + " " + *currentVersion + " " + tr("of svtplay-dl.");
                yourVersion = tr("You are NOT using the latest version.");
            } else
            {
                youAreUsing = tr("You are not using svtplay-dl.");
                yourVersion = "";
            }

            if(versions.at(0) > versions.at(1))
            {
                if(*currentVersion == versions.at(0)) {
                    ut = "<b>" + tr("svtplay-dl available for download") + "</b><br>" + tr("Stable:") + " " + versions.at(1) + "<br>" + tr("Beta:") + " " + versions.at(0) + "<br><br>" + *pinuse + "<br><br>" + tr("You have selected the option") + "<br>" + *selectedsvtplaydl + "<br>" + youAreUsing + "<br><b>" + tr("You are using the beta version.") + " </b > ";
                } else if(*currentVersion == versions.at(1)) {
                    ut = "<b>" + tr("svtplay-dl available for download") + "</b><br>" + tr("Stable:") + " " + versions.at(1) + "<br>" + tr("Beta:") + " " + versions.at(0) + "<br><br>" + *pinuse + "<br><br>" + tr("You have selected the option") + "<br>" + *selectedsvtplaydl + "<br>" + youAreUsing +  "<br><b>" + tr("You are using the stable version.") + "</b>";
                } else if((*currentVersion != versions.at(1)) && (*currentVersion != versions.at(0))) {
                    ut = "<b>" + tr("svtplay-dl available for download") + "</b><br>" + tr("Stable:") + " " + versions.at(1) + "<br>" + tr("Beta:") + " " + versions.at(0) + "<br><br>" + *pinuse + "<br><br>" + tr("You have selected the option") + "<br>" +  *selectedsvtplaydl + "<br>" + youAreUsing + "</b>";
                }
            } else
            {
                if(*currentVersion == versions.at(0)) {
                    ut = "<b>" + tr("svtplay-dl available for download") + "</b><br>" + tr("Stable:") + " " + versions.at(1) + "<br><br>" + *pinuse + "<br><br>" + tr("You have selected the option") + "<br>" + *selectedsvtplaydl + "<br>" + youAreUsing +  "<br><b>" + tr("You are using the stable version.") + "</b>";
                } else if((*currentVersion != versions.at(1)) && (*currentVersion != versions.at(0))) {
                    ut = "<b>" + tr("svtplay-dl available for download") + "</b><br>" + tr("Stable:") + " " + versions.at(1) + "<br><br>" + *pinuse + "<br><br>" + tr("You have selected the option") + "<br>" +  *selectedsvtplaydl + "<br>" + youAreUsing + "</b>";
                }

//                if(*currentVersion == versions.at(0)) {
//                    ut = "<b>" + tr("The latest svtplay-dl<br>available for download at bin.ceicer.com is") + "</b><br>" + tr("Stable:") + " " + versions.at(1) + "<br><br>" + *pinuse + "<br><br>" + tr("You have selected the option") + "<br>" + *selectedsvtplaydl + "<br>" + youAreUsing + "<br><b>" + tr("You are using the beta version.") + " </b > ";
//                } else if(*currentVersion == versions.at(1)) {
//                    ut = "<b>" + tr("The latest svtplay-dl<br>available for download at bin.ceicer.com is") + "</b><br>" + tr("Stable:") + " " + versions.at(1) + "<br><br>" + *pinuse + "<br><br>" + tr("You have selected the option") + "<br>" + *selectedsvtplaydl + "<br>" + youAreUsing +  "<br><b>" + tr("You are using the stable version.") + "</b>";
//                } else if((*currentVersion != versions.at(1)) && (*currentVersion != versions.at(0))) {
//                    ut = "<b>" + tr("The latest svtplay-dl<br>available for download at bin.ceicer.com is") + "</b><br>" + tr("Stable:") + " " + versions.at(1) + "<br><br>" + *pinuse + "<br><br>" + tr("You have selected the option") + "<br>" +  *selectedsvtplaydl + "<br>" + youAreUsing + "</b>";
//                }
            }
            ut.replace(" ", "&nbsp;");
            msgBox.setText(ut);
            QPushButton *btnDownload = msgBox.addButton(tr("Download"), QMessageBox::RejectRole);
            QPushButton *btnOk = msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.setDefaultButton(btnOk);
            msgBox.exec();

            if(msgBox.clickedButton() == btnDownload)
            {
                downloadFromCeicer();
            }
        });
        /*   */
    });
    /*    */
}




QStringList Newprg::checkSvtplaydlForUpdates()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       LIBRARY_NAME);
    settings.beginGroup("Path");
    QString stablepath = settings.value("stablepath").toString();
    QString betapath = settings.value("betapath").toString();
//    QString svtplaydlpath = settings.value("svtplaydlpath").toString();
    settings.endGroup();
#ifdef Q_OS_WINDOWS
    QString *ls = new QString(QDir::toNativeSeparators(stablepath + "/stable/svtplay-dl.exe"));
    QString *l = new QString(QDir::toNativeSeparators(betapath + "/beta/svtplay-dl.exe"));
#endif
#ifdef Q_OS_LINUX
    QString * ls = new QString(QDir::toNativeSeparators(stablepath + "/stable/svtplay-dl"));
    QString *l = new QString(QDir::toNativeSeparators(betapath + "/beta/svtplay-dl"));
#endif
    QStringList versions;
    versions << getSvtplaydlVersion(ls);
    versions << getSvtplaydlVersion(l);
    /*    */
    return versions;
}



QString Newprg::hittaSvtplaydl()
{
    QString tempSvtplaydl;

    if(ui->actionSvtplayDlStable->isChecked()) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME,
                           LIBRARY_NAME);
        settings.beginGroup("Path");
        QString stablepath = settings.value("stablepath").toString();
        settings.endGroup();
#ifdef Q_OS_WINDOWS
        tempSvtplaydl = stablepath + "/stable/svtplay-dl.exe";
#endif
#ifdef Q_OS_LINUX
        tempSvtplaydl = stablepath + "/stable/svtplay-dl";
#endif

        if(fileExists(tempSvtplaydl)) {
            svtplaydl = tempSvtplaydl;
            return tr("\"Use stable svtplay-dl.\"");
        } else {
            return tr("\"Use stable svtplay-dl\", but it can not be found.");
        }
    }

    if(ui->actionSvtplayDlBleedingEdge->isChecked()) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME,
                           LIBRARY_NAME);
        settings.beginGroup("Path");
        QString betapath = settings.value("betapath").toString();
        settings.endGroup();
#ifdef Q_OS_WINDOWS
        tempSvtplaydl = betapath + QStringLiteral("/beta/svtplay-dl.exe");
#endif
#ifdef Q_OS_LINUX
        tempSvtplaydl = betapath + QStringLiteral("/beta/svtplay-dl");
#endif

        if(fileExists(tempSvtplaydl)) {
            svtplaydl = tempSvtplaydl;
            return tr("\"Use svtplay-dl beta.\"");
        } else {
            return tr("\"Use svtplay-dl beta\", but it can not be found.");
        }
    }

    if(ui->actionSvtPlayDlManuallySelected->isChecked()) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Path");
        QString svtplaypath = settings.value("svtplaydlpath").toString();
        settings.endGroup();

        if(fileExists(svtplaypath)) {
            svtplaydl = svtplaypath;
            return tr("\"Use the selected svtplay-dl.\"");
        } else {
            return tr("\"Use the selected svtplay-dl\", but it can not be found.");
        }
    }

    return tr("svtplay-dl is not found in the specified path.");
}

void Newprg::checkSvtplaydlAtStart()
{
    QByteArray *bytes = new QByteArray;
    QUrl url(CHECK_LATEST);
    auto *nam = new QNetworkAccessManager(nullptr);
    auto *reply = nam->get(QNetworkRequest(url));
    reply->waitForBytesWritten(1000);

    if(reply->error() != QNetworkReply::NoError)  {
        QMessageBox *msgBox = new QMessageBox(this);
        msgBox->setIcon(QMessageBox::Critical);
        msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox->setText(tr("An unexpected error occurred.<br>Information about svtplay-dl stable can not be found.<br>Check your internet connection."));
        msgBox->exec();
        return;
    }

    QObject::connect(
        reply, &QNetworkReply::finished,
    [ reply, bytes, this]() {
        /*   */
        QUrl url_stable(CHECK_LATEST_STABLE);
        auto *nam_stable = new QNetworkAccessManager(nullptr);
        auto *reply_stable = nam_stable->get(QNetworkRequest(url_stable));
        reply_stable->waitForBytesWritten(1000);
        *bytes = reply->readAll(); // bytes

        if(reply->error() != QNetworkReply::NoError)  {
            QMessageBox *msgBox = new QMessageBox(this);
            msgBox->setIcon(QMessageBox::Critical);
            msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox->setText(tr("An unexpected error occurred.<br>Information about svtplay-dl stable can not be found.<br>Check your internet connection."));
            msgBox->exec();
            return;
        }

        QObject::connect(
            reply_stable, &QNetworkReply::finished,
        [ reply_stable, bytes, this] {
            *bytes += reply_stable->readAll(); // bytes

            if(reply_stable->error() == QNetworkReply::ContentNotFoundError)
            {
                QMessageBox *msgBox = new QMessageBox(this);
                msgBox->setIcon(QMessageBox::Critical);
                msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox->setText(tr("An unexpected error occurred.<br>Information about svtplay-dl stable can not be found.<br>Check your internet connection."));
                msgBox->exec();
                return;
            }
            QString latestVersion = (QString) * bytes;
#ifdef Q_OS_WINDOWS
            QStringList latestVersions = latestVersion.split("\r\n");
#endif
#ifdef Q_OS_LINUX
            QStringList latestVersions = latestVersion.split("\n");
#endif

            latestVersions.removeAll(QString(""));


            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               LIBRARY_NAME);
            settings.beginGroup("Path");
            QString stablepath = settings.value("stablepath", "nothing").toString();
            QString betapath = settings.value("betapath", "nothing").toString();
            settings.endGroup();



#ifdef Q_OS_LINUX
            QFile fileStable(stablepath + "/stable/svtplay-dl");
            QFile filebeta(betapath + "/beta/svtplay-dl");
#endif
#ifdef Q_OS_WINDOWS
            QFile fileStable(stablepath + "/stable/svtplay-dl.exe");
            QFile filebeta(betapath + "/beta/svtplay-dl.exe");
#endif


#ifdef Q_OS_LINUX
            QString latest_stable = getSvtplaydlVersion(stablepath + "/stable/svtplay-dl");
            QString latest = getSvtplaydlVersion(betapath + "/beta/svtplay-dl");
#endif
#ifdef Q_OS_WINDOWS
            QString latest_stable = getSvtplaydlVersion(stablepath + "/stable/svtplay-dl.exe");
            QString latest = getSvtplaydlVersion(betapath + "/beta/svtplay-dl.exe");
#endif


            bool betapathExists = filebeta.exists();
            bool stablepathExists = fileStable.exists();
            QString ut = "";
            QSettings settings2(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                                EXECUTABLE_NAME);
            settings2.beginGroup("FirstRun");
            bool firstrun = settings2.value("firstrun", true).toBool();
            settings2.endGroup();

            if(firstrun)
            {
                ut += tr("<b>Welcome to streamCapture2!</b>");
                ut += "<br><br>";
            } else
            {
                if(!stablepathExists) {
                    ut += tr("<b>svtplay-dl stable</b> can not be found. You have entered an incorrect path or no path has been specified at all.<br>Or the files have been moved or deleted.");
                    ut += "<br><br>" + tr("<i>Click \"Download\" and \"Download to directory stable\"</i>");
                    ut += "<br><br>";
                }

                if(!betapathExists) {
                    if(latestVersions.at(0) > latestVersions.at(1)) {
                        ut += tr("<b>svtplay-dl beta</b> can not be found. You have entered an incorrect path or no path has been specified at all.<br>Or the files have been moved or deleted.");
                        ut += "<br><br>" + tr("<i>Click \"Download\" and \"Download to directory beta\"</i>");
                        ut += "<br><br>";
                    }
                }
            }

            if(latest_stable != latestVersions.at(1))
            {
                ut += tr("<b>svtplay-dl stable is available for download.</b><br>Version: ") + latestVersions.at(1);

                if(firstrun) {
                    ut += "<br>" + tr("<i>Click \"Download\" and \"Download to directory stable\"</i>");
                }

                ut += "<br><br>";
            }
            if(latest != latestVersions.at(0))
            {
                if(latestVersions.at(0) > latestVersions.at(1)) {
                    ut += tr("<b>svtplay-dl beta is available for download.</b><br>Version: ") + latestVersions.at(0);

                    if(firstrun) {
                        ut += "<br>" + tr("<i>Click \"Download\" and \"Download to directory beta\"</i>");
                    }
                }
            }
            if(!ut.isEmpty())
            {
                QMessageBox *msgBox = new QMessageBox(this);
                QPushButton *btnDownload = msgBox->addButton(tr("Download"), QMessageBox::RejectRole);
                QPushButton *btnOk = msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox->setText(ut);
                msgBox->setDefaultButton(btnOk);
                msgBox->exec();

                if(msgBox->clickedButton() == btnDownload) {
                    downloadFromCeicer();
                }
            }
        });
    });
}

QString Newprg::getSvtplaydlVersion(QString path)
{
    auto *CommProcess = new QProcess(this);
    CommProcess->setProcessChannelMode(QProcess::MergedChannels);
    CommProcess->start(path, QStringList() << QStringLiteral(u"--version"));
    CommProcess->waitForFinished(-1);
    QTextStream stream(CommProcess->readAllStandardOutput());
    QString line;

    while(!stream.atEnd()) {
        line += stream.readLine();
    }

    line.remove(0, 11);
    CommProcess->deleteLater();
    return line;
}
