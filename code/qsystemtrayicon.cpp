// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//    		Program name
//    		Copyright (C) 2020 Ingemar Ceicer
//    		https://gitlab.com/posktomten/streamcapture2/
//    		ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "newprg.h"

void Newprg::showNotification(QString notifikation, int ikonType) {

  QSystemTrayIcon::MessageIcon typ;
  QString rubrik;
  if (ikonType == 3) {
    typ = QSystemTrayIcon::Critical;
    rubrik = tr("The mission failed!");
  } else {
    typ = QSystemTrayIcon::Information;
    rubrik = tr("Mission accomplished!");
  }

  QIcon icon(":/images/icon.ico");
  QSystemTrayIcon *trayikon = new QSystemTrayIcon(this);
  trayikon->setIcon(icon);
  trayikon->setVisible(true);
  //  trayikon->show();
  trayikon->showMessage(rubrik, notifikation, typ);
}
