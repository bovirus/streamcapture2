// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"


void Newprg::firstRun()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("FirstRun");
    settings.setValue("firstrun", false);
    settings.endGroup();
    settings.sync();
#ifdef Q_OS_WINDOWS
#ifdef PORTABLE
    settings.beginGroup("Settings");
    settings.setValue("portable", true);
    settings.endGroup();
#endif
#ifndef PORTABLE
    settings.beginGroup("Settings");
    settings.setValue("portable", false);
    settings.endGroup();
#endif
#endif
#ifdef Q_OS_LINUX
    settings.sync();
    QString inifile = settings.fileName();
    QFileInfo fi(inifile);
    QString iconpath = QDir::toNativeSeparators(fi.absolutePath());
    QDir pathDir(iconpath);

    if(pathDir.exists()) {
        iconpath.append(QDir::toNativeSeparators("/streamcapture2.png"));
        const QString pngpath = QDir::toNativeSeparators(QCoreApplication::applicationDirPath() + "/streamcapture2.png");

        if(QFile::exists(iconpath)) {
            QFile::remove(iconpath);
        }

        if(QFile::copy(pngpath, iconpath)) {
            QFile file(iconpath);
            file.setPermissions(QFileDevice::ReadUser | QFileDevice::WriteUser | QFileDevice::ReadOther);
        }
    }

#endif
}

