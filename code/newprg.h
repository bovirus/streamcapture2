// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef NEWPRG_H
#define NEWPRG_H

#include <QClipboard>
#include <QFileDialog>
#include <QInputDialog>
#include <QMainWindow>
#include <QProcess>
#include <QContextMenuEvent>
#include <QDateEdit>
#include <QDesktopServices>
#include <QEvent>
#include <QFontDatabase>
#include <QMessageBox>
#include <QMouseEvent>
#include <QPoint>
#include <QRegularExpression>
#include <QScreen>
#include <QSettings>
#include <QSystemTrayIcon>
#include <QTextStream>
#include <QTimer>
#include <QtWidgets>
#include <QDropEvent>
#include <QActionEvent>
//#include <QDebug>
#include <QObject>
#include <QtXml>

#include "selectfont.h"
#include "selectfont_global.h"
#ifdef Q_OS_LINUX // Linux
#include "update.h"
#include "updatedialog.h"
#endif
#include "checkupdate.h"
#include "downloadunpack.h"
#include "downloadunpack_global.h"
#include "downloadunpack.h"
#include "info.h"
#include "ui_newprg.h"
#include "downloadlistdialog.h"
namespace Ui
{
class newprg;
}

class Newprg : public QMainWindow
{
    Q_OBJECT

public:
    explicit Newprg(QWidget *parent = nullptr);
    QString getSvtplaydlVersion(QString *);
    void setEndConfig();


protected:
    void dropEvent(QDropEvent *ev)  override;
    void dragEnterEvent(QDragEnterEvent *ev)  override;

private slots:


    void about();
    void aboutSvtplayDl();

    void aboutFfmpeg();

    void english();
    void swedish();
    void italian();
    void initSok();
    void sok();
    void download();
    void license();
#ifdef FFMPEG
    void licenseFfmpeg();
#endif
#ifdef  Q_OS_WINDOWS
    void license7zip();
#endif
    void licenseSvtplayDl();
    void versionHistory();
    void slotRecent();
    void downloadAll();
    void onCommProcessStart();
    void onCommProcessExit_sok(int, QProcess::ExitStatus);
    void comboBoxChanged();
    // Pay TV
    void newSupplier();
    void editOrDelete(const QString &);
    void listAllEpisodes();
    void downloadFromCeicer();
    void takeAScreenshot();

private:



    Ui::newprg *ui;
    void closeEvent(QCloseEvent *event) override;

    void setStartConfig();

    QStringList recentFiles;
    QStringList downloadList;
    int MAX_RECENT{};
    QString address{};
    QString save(QString windowTitle);
    void statusExit(QProcess::ExitStatus, int);
    // Pay TV
    bool testinputprovider(QString &, bool &);
    bool testinputusername(const QString &);
    bool testinputpassword(const QString &);
    bool testinputusername_edit(const QString &, const QString &);
    bool testinputpassword_edit(const QString &, const QString &);
    void editprovider(QString);
    void deleteProvider(const QString &);
    bool renameProvider(const QString &provider);
    QString secretpassword;
//    tesstsearch
//    QString doTestSearch(const QString &);
//    QString doTestSearch(const QString &, const QString &, const QString &);
    void downloadAllEpisodes();
    void deleteAllSettings();

    QProcess *CommProcess2{};
    int antalnedladdade{};
    qint64 processpid{};
    bool avbrutet{};
    bool deleteSettings{};

//    void copyToDefaultLocation(QString, const QString *, QString copypath);
    void copyToDefaultLocation(QString, const QString *, const QString *copyto);

    bool fileExists(QString &path);
    QString selectFfmpeg();

    QString svtplaydl;

    QString findSvtplayDl();
    // Zoom
    void zoom();
#ifdef Q_OS_WIN
    void downloadRuntime();
#endif


    void firstRun();

    void chortcutdesktop(int state);
    void chortcutapplications(int state);
    bool makeDesktopFile(QString path);
    bool chortcutExists(QString path);
    bool chortcutIsExecutable(QString path);

    void showNotification(QString notifikation, int ikonType);
#ifdef Q_OS_LINUX // Linux
    UpdateDialog *ud;
#endif

    void testTranslation();
    // st cookie
    void createSt();
    void editSt(QAction *provider, bool iscurrentst);
    void editOrDeleteSt(QAction *provider);
    void actionSt();


    QStringList checkSvtplaydlForUpdates();
    void checkSvtplaydlForUpdatesHelp();
    QString hittaSvtplaydl();
    void checkSvtplaydlAtStart();
    QString getSvtplaydlVersion(QString path);

    void nfo();
#ifdef OFFLINE_INSTALLER
    void offlineInstaller();
#endif


public slots:
    void setValue(const QFont &font);

    // Zoom
    void textBrowserTeOutTextChanged();
    void zoomPlus();
    void zoomMinus();
    void zoomDefault();
    // DownloadListDialog
    void getDownloadList(QString *content);

    void getSettings(QString *content);
    void getSettingsDownloadunpack(QString *content);
    void getRecentSearches(QString *content);


#ifdef Q_OS_LINUX // Linux
    //    Receives a Boolean value from the "Update" class.
    //    True if the update was successful and false if the update failed
    void isUpdated(bool uppdated);
#endif

signals:


};
#ifdef Q_OS_WIN
extern Q_CORE_EXPORT int qt_ntfs_permission_lookup;
#endif
#endif // NEWPRG_H
