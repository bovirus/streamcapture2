// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"

void Newprg::zoom()
{
    connect(ui->actionDefault, &QAction::triggered, this, &Newprg::zoomDefault);
    connect(ui->actionOut, &QAction::triggered,  this, &Newprg::zoomMinus);
    connect(ui->actionIn, &QAction::triggered, this, &Newprg::zoomPlus);
    connect(ui->teOut, &QTextEdit::textChanged, this, &Newprg::textBrowserTeOutTextChanged);
    connect(ui->actionDefault, &QAction::hovered, [this]() {
        QFont f = ui->teOut->currentFont();
        int size = f.pointSize();
        ui->actionDefault->setStatusTip(tr("The font size changes to the selected font size") + " (" + QString::number(size) + ").");
    });
}

void Newprg::textBrowserTeOutTextChanged()
{
    QTextCursor cursor = ui->teOut->textCursor();
    cursor.movePosition(QTextCursor::End);
    ui->teOut->setTextCursor(cursor);
}

void Newprg::zoomDefault()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Font");
    int pointsize = settings.value("size", FONT).toInt();
    settings.endGroup();
    QFont f = ui->teOut->font();
    f.setPointSize(pointsize);
    ui->teOut->setFont(f);
    QTextCursor cursor = ui->teOut->textCursor();
    ui->teOut->selectAll();
    ui->teOut->setFontPointSize(pointsize);
    ui->teOut->setTextCursor(cursor);
    ui->teOut->zoomOut();
}

void Newprg::zoomMinus()
{
    QFont f = ui->teOut->font();
    int pointsize = f.pointSize();

    if(pointsize > 1) {
        QTextCursor cursor = ui->teOut->textCursor();
        ui->teOut->selectAll();
        ui->teOut->setFontPointSize(pointsize - 1);
        ui->teOut->setTextCursor(cursor);
        ui->teOut->zoomOut(1);
    }
}

void Newprg::zoomPlus()
{
    QFont f = ui->teOut->font();
    int pointsize = f.pointSize();
    QTextCursor cursor = ui->teOut->textCursor();
    ui->teOut->selectAll();
    ui->teOut->setFontPointSize(pointsize  + 1);
    ui->teOut->setTextCursor(cursor);
    ui->teOut->zoomIn(1);
}

//void Newprg::wheelEvent(QWheelEvent *ev)
//{
//    if(QApplication::queryKeyboardModifiers() == Qt::ControlModifier) {
//        QPoint numDegrees = ev->angleDelta();
//        int direction = numDegrees.y();


//        if(direction < 0) {
//            zoomMinus();
//        } else if(direction > 0) {
//            zoomPlus();
//        }

//        ev->accept();
//    }
//}

