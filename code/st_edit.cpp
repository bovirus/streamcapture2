// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"

void Newprg::editOrDeleteSt(QAction *provider)
{
    QMessageBox msgBox;
    msgBox.setWindowTitle(tr("Manage 'st' cookie for ") + provider->text());
    msgBox.setIcon(QMessageBox::Question);
    msgBox.setText(tr("Use, Do not use, Edit or Delete\n'st' cookie for\n") + provider->text() + "?");
    QPushButton *btnUse = msgBox.addButton(tr("Use"), QMessageBox::ActionRole);
    QPushButton *btnDoNotUse = msgBox.addButton(tr("Do not use"), QMessageBox::ActionRole);
    QPushButton *btnEdit = msgBox.addButton(tr("Edit"), QMessageBox::ActionRole);
    QPushButton *btnDelete = msgBox.addButton(tr("Delete"), QMessageBox::ActionRole);
    QPushButton *btnCancel = msgBox.addButton(tr("Cancel"), QMessageBox::ActionRole);
    msgBox.setDefaultButton(btnUse);
    msgBox.exec();
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");

    /* USE */
    if(msgBox.clickedButton() == btnUse) {
        QList<QAction*> list = ui->menuStCookies->actions();

        for(auto *a : qAsConst(list)) {
            a->setChecked(false);
        }

        provider->setChecked(true);
        settings.beginGroup("Stcookies");
        QString stcookie = settings.value(provider->text() + QStringLiteral(u"/st"), "").toString();
        settings.setValue("stcookie", stcookie);
        settings.endGroup();
        /* DO NOT USE  */
    } else if(msgBox.clickedButton() == btnDoNotUse) {
        if(!provider->isChecked()) {
            settings.beginGroup("Stcookies");
            settings.setValue("stcookie", "");
            settings.endGroup();
        }

        provider->setChecked(false);
        /* DELETE  */
    } else if(msgBox.clickedButton() == btnDelete) {
        settings.beginGroup("Stcookies");
        QString stcookie = settings.value("stcookie", "").toString();
        QString newstcookie = settings.value(provider->text() + QStringLiteral(u"/st")).toString();

        if((!stcookie.isEmpty()) && (stcookie == newstcookie)) {
            settings.setValue("stcookie", "");
        }

        settings.remove(provider->text());
        settings.endGroup();
        QList<QAction*> list = ui->menuStCookies->actions();

        for(QAction *a : qAsConst(list)) {
            a->deleteLater();
        }

        // st
        actionSt();
        /* EDIT  */
    } else if(msgBox.clickedButton() == btnEdit) {
        bool iscurrentst;

        if(provider->isChecked()) {
            provider->setChecked(false);
            iscurrentst = false;
        } else {
            iscurrentst = true;
            provider->setChecked(true);
        }

        editSt(provider, iscurrentst);
    } else if(msgBox.clickedButton() == btnCancel) {
        if(provider->isChecked()) {
            provider->setChecked(false);
        } else {
            provider->setChecked(true);
        }

        return;
    }
}

void Newprg::editSt(QAction *provider, bool iscurrentst)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    QInputDialog inputdialogprovider;
    inputdialogprovider.setOkButtonText(tr("Ok"));
    inputdialogprovider.setCancelButtonText(tr("Cancel"));
    inputdialogprovider.setWindowTitle(tr("Streaming service"));
    inputdialogprovider.setTextValue(provider->text());
    inputdialogprovider.setInputMode(QInputDialog::TextInput);
    inputdialogprovider.setLabelText(tr("Enter the name of your streaming service."));

    if(inputdialogprovider.exec() == QInputDialog::Accepted) {
// cppcheck
//        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
//        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Stcookies");
        QString stcookie = settings.value(provider->text() + QStringLiteral(u"/st")).toString();
        settings.endGroup();
        QInputDialog inputdialogst;
        inputdialogst.setOkButtonText(tr("Ok"));
        inputdialogst.setCancelButtonText(tr("Cancel"));
        inputdialogst.setWindowTitle(tr("Enter 'st' cookie"));
        inputdialogst.setTextValue(stcookie);
        inputdialogst.setInputMode(QInputDialog::TextInput);
        inputdialogst.setLabelText(tr("Enter the 'st' cookie that your video stream provider has saved in your browser."));
        QString newstprovider = inputdialogprovider.textValue();

        if(newstprovider != provider->text()) {
            settings.beginGroup("Stcookies");
            settings.remove(provider->text());
            settings.endGroup();
        }

        if(inputdialogst.exec() == QInputDialog::Accepted) {
            QString newstcookie = inputdialogst.textValue();
            settings.beginGroup("Stcookies");
            settings.setValue(newstprovider + QStringLiteral(u"/st"), newstcookie);

            if(iscurrentst) {
                settings.setValue("currentst", newstcookie);
            }

            settings.endGroup();
        } else {
            settings.beginGroup("Stcookies");
            settings.setValue(newstprovider + QStringLiteral(u"/st"), stcookie);
            settings.endGroup();
        }

        QList<QAction*> list = ui->menuStCookies->actions();

        for(QAction *a : qAsConst(list)) {
            a->deleteLater();
        }

        // st
        actionSt();
    }
}

void Newprg::actionSt()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Stcookies");
    QString stcookie = settings.value("stcookie", "").toString();
    QStringList keys = settings.childGroups();
    keys.sort(Qt::CaseInsensitive);
    settings.endGroup();
    const QIcon ikon(":/images/cookie.png");
    QAction *actionStCookie;
    actionStCookie = new QAction(ikon, tr("Set new 'st' cookie", nullptr));
    actionStCookie->setStatusTip(tr("Paste and save the 'st' cookie that your streaming provider has downloaded to your browser."));
    connect(actionStCookie, &QAction::triggered, [this]() {
        createSt();
    });
    ui->menuStCookies->addAction(actionStCookie);
    ui->menuStCookies->addSeparator();

    for(const QString &p : qAsConst(keys)) {
        QAction *actionSt;
        actionSt = new QAction(p, nullptr);
        ui->menuStCookies->addAction(actionSt);
        actionSt->setCheckable(true);
        settings.beginGroup("Stcookies");

        if((!stcookie.isEmpty()) && (stcookie == settings.value(p + "/st").toString())) {
            actionSt->setChecked(true);
        }

        settings.endGroup();
        connect(actionSt, &QAction::triggered,
        [this, actionSt]() {
            editOrDeleteSt(actionSt);
        });
    }
}
