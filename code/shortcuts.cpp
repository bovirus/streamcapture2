// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"

void Newprg::chortcutdesktop(int state)
{
    const QString shortcutlocation = QDir::toNativeSeparators(QStandardPaths::writableLocation(QStandardPaths::DesktopLocation));
#ifdef Q_OS_WIN
    QString link(QDir::toNativeSeparators(shortcutlocation + "/streamCapture2.lnk"));
#endif
#ifdef Q_OS_LINUX
    QString link(QDir::toNativeSeparators(shortcutlocation + "/streamCapture2.desktop"));
#endif

    if(state == 2) {
        if(chortcutExists(link)) {
            QFile::remove(link);
        }

        if(!makeDesktopFile(link)) {
            ui->actionDesktopShortcut->setChecked(false);
        } else {
            ui->actionDesktopShortcut->setChecked(true);
        }
    } else {
        if(chortcutExists(link)) {
            QFile::remove(link);
        }
    }
}

void Newprg::chortcutapplications(int state)
{
    const QString shortcutlocation = QDir::toNativeSeparators(QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation));
#ifdef Q_OS_WIN
    QString link(QDir::toNativeSeparators(shortcutlocation + "/streamCapture2.lnk"));
#endif
#ifdef Q_OS_LINUX
    QString link(QDir::toNativeSeparators(shortcutlocation + "/streamCapture2.desktop"));
    QDir dir(shortcutlocation);

//    ipac
    if(!dir.exists(shortcutlocation)) {
        dir.mkpath(shortcutlocation);
    }

    if(!dir.mkpath(shortcutlocation)) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("Failure!\nThe shortcut could not be created in\n\"~/.local/share/applications\"\nCheck your file permissions."));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
        return;
    }

#endif

    if(state == 2) {
        if(chortcutExists(link)) {
            QFile::remove(link);
        }

        if(!makeDesktopFile(link)) {
            ui->actionApplicationsMenuShortcut->setChecked(false);
        } else {
            ui->actionApplicationsMenuShortcut->setChecked(true);
        }
    } else {
        if(chortcutExists(link)) {
            QFile::remove(link);
        }
    }
}


bool Newprg::makeDesktopFile(QString path)
{
    QFile file(path);
#ifdef Q_OS_LINUX
    const QString source = QDir::toNativeSeparators(qgetenv("APPIMAGE"));
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    QString inifile = settings.fileName();
    QFileInfo fi(inifile);
    const QString iconpath = QDir::toNativeSeparators(fi.absolutePath() + "/streamcapture2.png");
#endif
#ifdef Q_OS_WIN
    const QString source = QDir::toNativeSeparators(QCoreApplication::applicationDirPath() + "/streamcapture2.exe");
#endif
#ifdef Q_OS_LINUX

    if(file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QTextStream out(&file);

        if(QFile::exists(iconpath)) {
            out << "[Desktop Entry]\nVersion=1.0\nType=Application\nName=streamCapture2\nIcon=" + iconpath + "\nTerminal=false\nComment=" + tr("Download video streams.") + "\nExec=\"" + source + "\"\nCategories=AudioVideo;\n";
        } else {
            out << "[Desktop Entry]\nVersion=1.0\nType=Application\nName=streamCapture2\nTerminal=false\nComment=" + tr("Download video streams.") + "\nExec=\"" + source + "\"\nCategories=AudioVideo;\n";
        }

        file.close();
        file.setPermissions(QFileDevice::ReadUser | QFileDevice::ExeUser |
                            QFileDevice::WriteUser | QFileDevice::ReadOther |
                            QFileDevice::ExeOther);
        return true;
    }

#endif
#ifdef Q_OS_WIN

    if(QFile::link(QDir::toNativeSeparators(source), QDir::toNativeSeparators(path))) {
        return true;
    }

#endif
    else {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("Failure!\nThe shortcut could not be created.\nCheck your file permissions."));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
        return false;
    }

    return true;
}


bool Newprg::chortcutExists(QString path)
{
    QFileInfo file(path);

    if(file.exists()) {
        return true;
    } else {
        return false;
    }
}

bool Newprg::chortcutIsExecutable(QString path)
{
    QFileInfo file(path);

    if(file.isExecutable()) {
        return true;
    } else {
        return false;
    }
}
