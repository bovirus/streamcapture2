#!/bin/bash
HERE=`pwd`
echo -------------------------------------------------------------
echo "Making Linux lib \"downloadunpack\", \"libcheckupdate\","
echo "\"libselectfont\" and \"libzsyncupdateappimage\"."
echo "Copy to \"AppDir\""
echo "and upload to \"bin.ceicer.com/public_html/streamcapture2/lib\""
PS3='Please enter your choice: '
options=("Qt 5.15.2 64-bit" "Qt 5.15.4 64-bit" "Qt 6.3.0 64-bit" "Quit")
echo -------------------------------------------------------------
select opt in "${options[@]}"
do
    case $opt in
        "Qt 5.15.2 64-bit")
            echo "You chose choice $REPLY which is $opt"
            qmakePath="/opt/Qt/5.15.2/gcc_64/bin/qmake"
            
			# Path Release and Debug
			lib_Qt="/home/ingemar/PROGRAMMERING/streamcapture2/lib_Linux64/lib_Qt5"
			lib_all_Qt="/home/ingemar/PROGRAMMERING/streamcapture2/lib_Linux64"
			lib_from="Qt5.15.2-64-bit"
			lib="/home/ingemar/PROGRAMMERING/streamcapture2/lib5"

			# Path Release
			lib_Qt_release="/home/ingemar/PROGRAMMERING/streamcapture2/lib_Qt5_release"
			AppDir="/home/ingemar/PROGRAMMERING/streamcapture2/AppDir/usr/lib"
			AppDir_ffmpeg="/home/ingemar/PROGRAMMERING/streamcapture2/AppDir_ffmpeg/usr/lib"
            AppDir2="/home/ingemar/PROGRAMMERING/streamcapture2/AppDir2/usr/lib"
			AppDir_ffmpeg2="/home/ingemar/PROGRAMMERING/streamcapture2/AppDir_ffmpeg2/usr/lib"
            break;
            ;;
            
        "Qt 5.15.4 64-bit")
            echo "You chose choice $REPLY which is $opt"
            qmakePath="/opt/Qt/Qt5.15.4/bin/qmake"
                     
            
			# Path Release and Debug
			lib_Qt="/home/ingemar/PROGRAMMERING/streamcapture2/lib_Linux64/lib_Qt5"
			lib_all_Qt="/home/ingemar/PROGRAMMERING/streamcapture2/lib_Linux64"
			lib_from="Qt5.15.4-64-bit"
			lib="/home/ingemar/PROGRAMMERING/streamcapture2/lib5"

			# Path Release
			lib_Qt_release="/home/ingemar/PROGRAMMERING/streamcapture2/lib_Qt5_release"
			AppDir="/home/ingemar/PROGRAMMERING/streamcapture2/AppDir/usr/lib"
			AppDir_ffmpeg="/home/ingemar/PROGRAMMERING/streamcapture2/AppDir_ffmpeg/usr/lib"
			AppDir2="/home/ingemar/PROGRAMMERING/streamcapture2/AppDir2/usr/lib"
			AppDir_ffmpeg2="/home/ingemar/PROGRAMMERING/streamcapture2/AppDir_ffmpeg2/usr/lib"
            break;
            ;;
        "Qt 6.3.0 64-bit")
            echo "You chose choice $REPLY which is $opt"
            qmakePath="/opt/Qt/6.3.0/gcc_64/bin/qmake"
            # Path Release and Debug
			lib_Qt="/home/ingemar/PROGRAMMERING/streamcapture2/lib_Linux64/lib_Qt6.3.0_GLIBC-2.31"
			lib_all_Qt="lib_Linux64"
			lib_from="Qt6.3.0-64-bit"
			lib="/home/ingemar/PROGRAMMERING/streamcapture2/lib6"

			# Path Release
			lib_Qt_release="/home/ingemar/PROGRAMMERING/streamcapture2/lib_Qt6_release/"
			AppDir="/home/ingemar/PROGRAMMERING/streamcapture2/AppDir/usr/lib/"
			AppDir_ffmpeg="/home/ingemar/PROGRAMMERING/streamcapture2/AppDir_ffmpeg/usr/lib/"
			AppDir2="/home/ingemar/PROGRAMMERING/streamcapture2/AppDir2/usr/lib/"
			AppDir_ffmpeg2="/home/ingemar/PROGRAMMERING/streamcapture2/AppDir_ffmpeg2/usr/lib/"
            break;
            ;;
        "Quit")
            exit 0
            break;
            ;;
        *) echo "invalid option $REPLY";;
    esac
done


# Path
downloadunpack=/home/ingemar/PROGRAMMERING/downloadunpack/bibliotek/code/downloadunpack.pro
libcheckupdate=/home/ingemar/PROGRAMMERING/libcheckupdate/bibliotek/code/checkupdate.pro
libselectfont=/home/ingemar/PROGRAMMERING/libselectfont/code/selectfont.pro
libzsyncupdateappimage=/home/ingemar/PROGRAMMERING/libzsyncupdateappimage/bibliotek/code/libzsyncupdateappimage.pro






# downloadunpack Debug
mkdir build
cd build
$qmakePath -project $downloadunpack
$qmakePath $downloadunpack -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug && /usr/bin/make qmake_all
make -j4
make clean -j4
cd ..
rm -r build

# downloadunpack Release
mkdir build
cd build
$qmakePath -project $downloadunpack
$qmakePath $downloadunpack -spec linux-g++ CONFIG+=qtquickcompiler && /usr/bin/make qmake_all
make -j4
make clean -j4
cd ..
rm -r build

# libcheckupdate Debug
mkdir build
cd build
$qmakePath -project $libcheckupdate
$qmakePath $libcheckupdate -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug && /usr/bin/make qmake_all
make -j4
make clean -j4
cd ..
rm -r build

# libcheckupdate Release
mkdir build
cd build
$qmakePath -project $libcheckupdate
$qmakePath $libcheckupdate -spec linux-g++ CONFIG+=qtquickcompiler && /usr/bin/make qmake_all
make -j4
make clean -j4
cd ..
rm -r build

# libselectfont Debug
mkdir build
cd build
$qmakePath -project $libselectfont
$qmakePath $libselectfont -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug && /usr/bin/make qmake_all
make -j4
make clean -j4
cd ..
rm -r build

# libselectfont Release
mkdir build
cd build
$qmakePath -project $libselectfont
$qmakePath $libselectfont -spec linux-g++ CONFIG+=qtquickcompiler && /usr/bin/make qmake_all
make -j4
make clean -j4
cd ..
rm -r build


# libzsyncupdateappimage Debug
mkdir build
cd build
$qmakePath -project $libzsyncupdateappimage
$qmakePath $libzsyncupdateappimage -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug && /usr/bin/make qmake_all
make -j4
make clean -j4
cd ..
rm -r build

# libzsyncupdateappimage Release
mkdir build
cd build
$qmakePath -project $libzsyncupdateappimage
$qmakePath $libzsyncupdateappimage -spec linux-g++ CONFIG+=qtquickcompiler && /usr/bin/make qmake_all
make -j4
make clean -j4
cd ..
rm -r build

# copy



# Debug and Release
cp -f build-lib/*.* "$lib_Qt"
cp -f build-lib/*.* "$lib"


cd build-lib

cp -f `ls | egrep -v '^*.d.so'` "$lib_Qt_release"
cp -f `ls | egrep -v '^*.d.so'` "$AppDir"
cp -f `ls | egrep -v '^*.d.so'` "$AppDir_ffmpeg"

cp -f `ls | egrep -v '^*.d.so'` "$AppDir2"
cp -f `ls | egrep -v '^*.d.so'` "$AppDir_ffmpeg2"

rm -r code

cd ..
PS3='Please enter your choice: '
options=("Yes" "No")
echo -------------------------------------------------------------
echo "Do you want to upload the library files?"
        
		
		
select opt in "${options[@]}"

do
    case $opt in
        "Yes")
        
        cd "$HERE"
        echo "$HERE"
        date_now=$(date "+%FT%H-%M-%S")
        
fil="lib-"${lib_from}-${date_now}.tar.gz
tar -czf "$fil" "build-lib"
./md5_lib.sh "$fil"


#fil=${lib_all_Qt}-${lib_from}-${date_now}.tar.gz

./md5_lib.sh "$fil"

HOST="`sed -n 1p ../../secret`"
USER="`sed -n 2p ../../secret`"
PASSWORD="`sed -n 3p ../../secret`"
echo "$HOST"
echo "$USER"
echo "$PASSWORD"

ftp -v -inv $HOST <<EOF
user $USER $PASSWORD
cd "/bin.ceicer.com/public_html/streamcapture2/lib"
passive
put $fil
put "$fil"-MD5.txt
bye
EOF
echo `pwd`
#rm "$fil"
#rm ${fil}-MD5.txt   
rm -r build-lib     

            break;
            ;;

        "No")
            break;
            ;;
        *) echo "invalid option $REPLY"
        ;;
    esac
done



echo "Finished!"
