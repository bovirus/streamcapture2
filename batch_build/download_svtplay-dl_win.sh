#!/bin/bash

HOST="`sed -n 1p ../../secret`"
USER="`sed -n 2p ../../secret`"
PASSWORD="`sed -n 3p ../../secret`"


#function stable32 {
  
#}

function stable64 {
echo ------------------------------
echo "Windows 64-bit, stable"
 wget --ftp-user=${USER} --ftp-password=${PASSWORD} -np   "ftp://${HOST}//bin.ceicer.com/public_html/svtplay-dl/Linux/version_stable.txt"
ver=`cat version_stable.txt`
svtplaydlver=svtplay-dl-${ver}.zip
wget --ftp-user=${USER} --ftp-password=${PASSWORD} -np   "ftp://${HOST}//bin.ceicer.com/public_html/svtplay-dl/Windows/64bit/${svtplaydlver}"
echo Decompressing...
unzip -q ${svtplaydlver}
rm -r -f ../build-executable5/stable/
if [ "$executable5" -eq 1 ] ;
then
    echo "Copying stable to build-executable5/stable..."
	cp -f -r svtplay-dl-${ver}/ ../build-executable5/stable/
fi
if [ "$executable6" -eq 1 ] ;
then
    echo "Copying stable to build-executable6/stable..."
	cp -f -r svtplay-dl-${ver}/ ../build-executable6/stable/
fi
rm -r -f ../build-executable6/stable/
cp -f -r svtplay-dl-${ver}/ ../build-executable6/stable/
rm version_stable.txt
rm -r svtplay-dl-${ver}
rm ${svtplaydlver}
}

#function bleedingedge32 {
  
#}

function bleedingedge64 {
  echo ------------------------------
echo "Windows 64-bit, bleedingedge"
wget --ftp-user=${USER} --ftp-password=${PASSWORD} -np   "ftp://${HOST}//bin.ceicer.com/public_html/svtplay-dl/Linux/version.txt"
ver=`cat version.txt`
svtplaydlver=svtplay-dl-${ver}.zip
wget --ftp-user=${USER} --ftp-password=${PASSWORD} -np   "ftp://${HOST}//bin.ceicer.com/public_html/svtplay-dl/Windows/64bit/${svtplaydlver}"
echo Decompressing...
unzip -q ${svtplaydlver}
if [ "$executable5" -eq 1 ] ;
then
    echo "Copying bleedingedge to build-executable5/stable..."
	cp -f -r svtplay-dl-${ver}/ ../build-executable5/stable/
fi
if [ "$executable6" -eq 1 ] ;
then
    echo "Copying bleedingedge to build-executable6/stable..."
	cp -f -r svtplay-dl-${ver}/ ../build-executable6/stable/
fi
rm version.txt
rm -r svtplay-dl-${ver}
rm ${svtplaydlver}
}

PS3='Please enter your choice: '
options=("Windows 32-bit" "Windows 64-bit" "Quit")
echo -----------------------------------------------------------------
echo "Download Windows version of svtplay-dl to \"build-executavle5\""
echo "and \"build-executavle6\" from \"bin.ceicer.com\""
echo -----------------------------------------------------------------
select opt in "${options[@]}"
do
    case $opt in
        "Windows 32-bit")
            echo "You chose choice $REPLY which is $opt"
			bitar=32
            break;
            ;;
        "Windows 64-bit")
            echo "You chose choice $REPLY which is $opt"
			bitar=64
            break;
            ;;

        "Quit")
            exit 0
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done

PS3='Please enter your choice: '
options=("Copy to build-executavle5" "Copy to build-executavle6" "Copy to build-executavle5 and build-executavle6" "Quit")
echo -----------------------------------------------------------
select opt in "${options[@]}"
do
    case $opt in
        "Copy to build-executavle5")
            echo "You chose choice $REPLY which is $opt"
			executable5=1
            break;
            ;;
        "Copy to build-executavle6")
            echo "You chose choice $REPLY which is $opt"
			executable6=1
            break;
            ;;
        "Copy to build-executavle5 and build-executavle6")
            echo "You chose choice $REPLY which is $opt"
			executable5=1
			executable6=1
            break;
            ;;

        "Quit")
            exit 0
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done

PS3='Please enter your choice: '
options=("stable" "bleedingedge" "stable and bleedingedge")
echo -----------------------------------------------------------
select opt in "${options[@]}"
do
    case $opt in
        "stable")
            echo "You chose choice $REPLY which is $opt"
            if [ "$bitar" -eq 64 ] ;
			then 
			  stable64
			else
			  stable32
			fi

            break;
            ;;
        "bleedingedge")
            echo "You chose choice $REPLY which is $opt"
			if [ "$bitar" -eq 64 ] ;
			then 
			  bleedingedge64
			else
			  bleedingedge32
			fi
  
            break;
            ;;
			
		  "stable and bleedingedge")
            echo "You chose choice $REPLY which is $opt"
			
			if [ "$bitar" -eq 64 ] ;
			then 
			  stable64
			  bleedingedge64
			else
			  stable32
			  bleedingedge32
			fi
			
           break;
            ;;

        "Quit")
            exit 0
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done





