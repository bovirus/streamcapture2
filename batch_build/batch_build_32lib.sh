#!/bin/bash
echo -----------------------------------------------------------------
echo "Making 32-bit Linux lib \"downloadunpack\", \"libcheckupdate\"," 
echo "\"libselectfont\" and \"libzsyncupdateappimage\"."
echo "Copy to \"AppDir\""
echo "and upload to \"bin.ceicer.com/public_html/streamcapture2/lib\""
echo -----------------------------------------------------------------
PS3='Please enter your choice: '
options=("Qt 5.15.2 32-bit" "Qt 5.15.4 32-bit" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Qt 5.15.2 32-bit")
            echo "You chose choice $REPLY which is $opt"
            qmakePath="/opt/Qt5.15.2_shared/bin/qmake"
			# Path Release
			lib_Qt="/home/ingemar/PROGRAMMERING/streamcapture2/lib_Linux32/lib_Qt5/"
			lib_all_Qt="lib_Linux32"
			lib_from="Qt5.15.2-32-bit"
			lib="/home/ingemar/PROGRAMMERING/streamcapture2/lib5/"

			AppDir="/home/ingemar/PROGRAMMERING/streamcapture2/AppDir/usr/lib/"
			AppDir_ffmpeg="/home/ingemar/PROGRAMMERING/streamcapture2/AppDir_ffmpeg/usr/lib/"
            break;
            ;;

            
       "Qt 5.15.4 32-bit")
            echo "You chose choice $REPLY which is $opt"
            qmakePath="/opt/Qt/Qt5.15.4/bin/qmake"
			# Path Release
			lib_Qt="/home/ingemar/PROGRAMMERING/streamcapture2/lib_Linux32/lib_Qt5/"
			lib_all_Qt="lib_Linux32"
			lib_from="Qt5.15.4-32-bit"
			lib="/home/ingemar/PROGRAMMERING/streamcapture2/lib5/"

			AppDir="/home/ingemar/PROGRAMMERING/streamcapture2/AppDir/usr/lib/"
			AppDir_ffmpeg="/home/ingemar/PROGRAMMERING/streamcapture2/AppDir_ffmpeg/usr/lib/"
            break;
            ;;
     
        "Quit")
            echo "Goodbye!"
            exit 0
            break;
            ;;
        *) echo "invalid option $REPLY";;
    esac
done


# Path
downloadunpack="/home/ingemar/PROGRAMMERING/downloadunpack/bibliotek/code/downloadunpack.pro"
libcheckupdate="/home/ingemar/PROGRAMMERING/libcheckforupdates/bibliotek/code/checkupdate.pro"
libselectfont="/home/ingemar/PROGRAMMERING/libselectfont/code/selectfont.pro"
libzsyncupdateappimage="/home/ingemar/PROGRAMMERING/libzsyncupdateappimage/bibliotek/code/libzsyncupdateappimage.pro"



# downloadunpack Release


mkdir build
cd build
$qmakePath -project $downloadunpack
$qmakePath $downloadunpack -spec linux-g++ CONFIG+=qtquickcompiler && /usr/bin/make qmake_all
make -j4
make clean -j4
cd ..
rm -r build





# libcheckupdate Release

mkdir build
cd build
$qmakePath -project $libcheckupdate
$qmakePath $libcheckupdate -spec linux-g++ CONFIG+=qtquickcompiler && /usr/bin/make qmake_all
make -j4
cd ..
rm -r build



# libselectfont Release

mkdir build
cd build
$qmakePath -project $libselectfont
$qmakePath $libselectfont -spec linux-g++ CONFIG+=qtquickcompiler && /usr/bin/make qmake_all
make -j4
make clean -j4
cd ..
rm -r build



# libzsyncupdateappimage Release

mkdir build
cd build
$qmakePath -project $libzsyncupdateappimage
$qmakePath $libzsyncupdateappimage -spec linux-g++ CONFIG+=qtquickcompiler && /usr/bin/make qmake_all
make -j4
make clean -j4
cd ..
rm -r build



# copy
rm -r code/home/ingemar/PROGRAMMERING/streamcapture2/AppDir_ffmpeg/lib/

# Release
#cp -f build-lib/*.* "$lib_Qt"
#cp -f build-lib/*.* "$lib"


cd build-lib

cp -f `ls | egrep -v '^*.d.so'` "$lib_Qt"
cp -f `ls | egrep -v '^*.d.so'` "$AppDir"
cp -f `ls | egrep -v '^*.d.so'` "$AppDir_ffmpeg"
                           
cp -f `ls | egrep -v '^*.d.so'` "$lib_Qt_release"
cp -f `ls | egrep -v '^*.d.so'` "$AppDir"
cp -f `ls | egrep -v '^*.d.so'` "$AppDir_ffmpeg"



cd ..

echo -----------------------------------------------------------
        
		PS3='Do you want to upload the libraries? '
		options=("Yes" "No")
		select opt in "${options[@]}"

do
    case $opt in
        "Yes")
        
        cd "$HERE"
        echo "$HERE"
        date_now=$(date "+%FT%H-%M-%S")
        
fil="lib-"${lib_from}-${date_now}.tar.gz
tar -czf "$fil" "build-lib"


#fil=${lib_all_Qt}-${lib_from}-${date_now}.tar.gz

./md5_lib.sh "$fil"

HOST="`sed -n 1p ../../secret`"
USER="`sed -n 2p ../../secret`"
PASSWORD="`sed -n 3p ../../secret`"


ftp -v -inv $HOST <<EOF
user $USER $PASSWORD
passive
cd "/bin.ceicer.com/public_html/streamcapture2/lib"
put $fil
put "$fil"-MD5.txt
bye
EOF

#rm "$fil"
#rm ${fil}-MD5.txt   
rm -r build-lib     

            break;
            ;;

        "No")
            break;
            ;;
        *) echo "invalid option $REPLY"
        ;;
    esac
done
