#!/bin/bash
date_now=$(date "+%FT%H-%M-%S")

	echo "--------------------------------------------------" > "$1-MD5.txt"
	echo "MD5 HASH SUM gggg" >> "$1-MD5.txt"
	md5sum "$1" >> "$1-MD5.txt"
	echo "Created $date_now" >> "$1-MD5.txt"
	echo "--------------------------------------------------" >> "$1-MD5.txt"
	echo "HOW TO CHECK THE HASH SUM" >> "$1-MD5.txt"
	echo "--------------------------------------------------" >> "$1-MD5.txt"
	echo "LINUX" >> "$1-MD5.txt"
	echo "\"md5sum\" is included in most Linus distributions." >> "$1-MD5.txt"
	echo "md5sum $1" >> "$1-MD5.txt"
	echo "--------------------------------------------------" >> "$1-MD5.txt"
	echo "WINDOWS" >> "$1-MD5.txt"
	echo "\"certutil\" is included in Windows." >> "$1-MD5.txt"
	echo "certutil -hashfile $1 md5" >> "$1-MD5.txt"
	echo "--------------------------------------------------" >> "$1-MD5.txt"
	echo "READ MORE" >> "$1-MD5.txt"
	echo "https://gitlab.com/posktomten" >> "$1-MD5.txt"
	echo "--------------------------------------------------" >> "$1-MD5.txt"

